<?php

class App_Utils
{
    /**
     * Encrypt string
     * 
     * @param string $string
     * @return string
     */
    public static function encryptString($string)
    {
        return @mcrypt_encrypt(MCRYPT_RIJNDAEL_128, ENCRYPTION_KEY, $string, MCRYPT_MODE_CFB);
    }

    /**
     * Decrypt string
     * 
     * @param string $string
     * @return string
     */
    public static function decryptString($string)
    {
        return @mcrypt_decrypt(MCRYPT_RIJNDAEL_128, ENCRYPTION_KEY, $string, MCRYPT_MODE_CFB);
    }

    /**
     * Normalize phone number
     * 
     * @param string $phone
     * @return string
     */
    public static function normalizePhone($phone)
    {
        if(0 === strpos($phone, '+1')) {
            $phone = substr($phone, 2);
        }
        return preg_replace('/([^0-9]+)/', '', $phone);
    }

    /**
     * Returns file extension
     * 
     * @param string $path
     * @return string
     */
    public static function getFileExtension($path)
    {
      return $ext = strrchr($path, '.');
    }

    /**
     * Convert abbreviation to date
     * 
     * @param string $dateRange
     * @param string|null $default
     * @return string
     */
    public static function abbrevToDate($dateRange=null, $default = 'today', $offset = 0)
    {
        $today = new DateTime();
        $dateRange = !empty($dateRange) ? $dateRange : $default;

        $value = 1; $period = 'D';
        switch ($dateRange) {
            case 'today':
                $dateFrom = $today->format('Y-m-d');
                break;
            case '7d':
            case '30d':
            case '12m':
                $value = (int)$dateRange; 
                $period = strtoupper(preg_replace("/[0-9]/", "", $dateRange));
                break;
            case 'wtd':
                $value = 7; $period = 'D';
                $dateFrom = date('Y-m-d', strtotime('Last Monday'));
                break;
            case 'mtd':
                $period = 'M';
                $dateFrom = date('Y-m-01');
                break;
             case 'ytd':
                 $period = 'Y';
                $dateFrom = date('Y-01-01');
                break;
        }
        if(empty($dateFrom)) {
            $dateFrom = $today->sub(new DateInterval("P{$value}{$period}"))->format('Y-m-d');
        }

        if($offset) {
            $from = new DateTime($dateFrom); $today = new DateTime();
            $offsetDateFrom = $from->sub(new DateInterval("P{$value}{$period}"))->format('Y-m-d');
            $offsetdateTo = ($dateRange == 'today') ? $offsetDateFrom : $today->sub(new DateInterval("P{$value}{$period}"))->format('Y-m-d');
            return array($offsetDateFrom, $offsetdateTo);
        }

        return $dateFrom;
    }

    /**
     * Find given keywords in string. Return keyword's id.
     * 
     * @param string $string
     * @param array $keywords
     * @return int|boolean
     */
    public static function findKeyword($string, $keywords = array())
    {
        foreach($keywords as $key => $keyword) {
            if(false !== strpos($string, $keyword)) {
                return $key;
            }
        }

        return false;
    }

    /**
     * Find all server's IP addresses
     * 
     * @return array
     */
    public static function getServerIpList()
    {
        $ipAddresses = array();

        //find all Server IP addresses
        $serverIps = shell_exec("/sbin/ifconfig | grep 'inet ' | grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}'");
        $serverIps = explode(chr(10), $serverIps);
        if(is_array($serverIps)) {
            if($ipAddresses = array_filter($serverIps)) {
                $ipAddresses = array_combine($ipAddresses, $ipAddresses);
            }
        }

        return $ipAddresses;
    }

    /**
     * Find all main server's IP address
     * 
     * @return string
     */
    public static function getServerMainIp()
    {
        $serverIp = shell_exec("/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'");
        return $serverIp ?: $_SERVER['SERVER_ADDR'];
    }

    /**
     * Get List of timezones
     * 
     * @return array
     */
    public static function getTimezones()
    {
        return array(
            'Pacific/Midway'       => "(GMT-11:00) Midway Island",
            'US/Samoa'             => "(GMT-11:00) Samoa",
            'US/Hawaii'            => "(GMT-10:00) Hawaii",
            'US/Alaska'            => "(GMT-09:00) Alaska",
            'US/Pacific'           => "(GMT-08:00) Pacific Time (US &amp; Canada)",
            'America/Tijuana'      => "(GMT-08:00) Tijuana",
            'US/Arizona'           => "(GMT-07:00) Arizona",
            'US/Mountain'          => "(GMT-07:00) Mountain Time (US &amp; Canada)",
            'America/Chihuahua'    => "(GMT-07:00) Chihuahua",
            'America/Mazatlan'     => "(GMT-07:00) Mazatlan",
            'America/Mexico_City'  => "(GMT-06:00) Mexico City",
            'America/Monterrey'    => "(GMT-06:00) Monterrey",
            'Canada/Saskatchewan'  => "(GMT-06:00) Saskatchewan",
            'US/Central'           => "(GMT-06:00) Central Time (US &amp; Canada)",
            'US/Eastern'           => "(GMT-05:00) Eastern Time (US &amp; Canada)",
            'US/East-Indiana'      => "(GMT-05:00) Indiana (East)",
            'America/Bogota'       => "(GMT-05:00) Bogota",
            'America/Lima'         => "(GMT-05:00) Lima",
            'America/Caracas'      => "(GMT-04:30) Caracas",
            'Canada/Atlantic'      => "(GMT-04:00) Atlantic Time (Canada)",
            'America/La_Paz'       => "(GMT-04:00) La Paz",
            'America/Santiago'     => "(GMT-04:00) Santiago",
            'Canada/Newfoundland'  => "(GMT-03:30) Newfoundland",
            'America/Buenos_Aires' => "(GMT-03:00) Buenos Aires",
            'Greenland'            => "(GMT-03:00) Greenland",
            'Atlantic/Stanley'     => "(GMT-02:00) Stanley",
            'Atlantic/Azores'      => "(GMT-01:00) Azores",
            'Atlantic/Cape_Verde'  => "(GMT-01:00) Cape Verde Is.",
            'Africa/Casablanca'    => "(GMT) Casablanca",
            'Europe/Dublin'        => "(GMT) Dublin",
            'Europe/Lisbon'        => "(GMT) Lisbon",
            'Europe/London'        => "(GMT) London",
            'Africa/Monrovia'      => "(GMT) Monrovia",
            'Europe/Amsterdam'     => "(GMT+01:00) Amsterdam",
            'Europe/Belgrade'      => "(GMT+01:00) Belgrade",
            'Europe/Berlin'        => "(GMT+01:00) Berlin",
            'Europe/Bratislava'    => "(GMT+01:00) Bratislava",
            'Europe/Brussels'      => "(GMT+01:00) Brussels",
            'Europe/Budapest'      => "(GMT+01:00) Budapest",
            'Europe/Copenhagen'    => "(GMT+01:00) Copenhagen",
            'Europe/Ljubljana'     => "(GMT+01:00) Ljubljana",
            'Europe/Madrid'        => "(GMT+01:00) Madrid",
            'Europe/Paris'         => "(GMT+01:00) Paris",
            'Europe/Prague'        => "(GMT+01:00) Prague",
            'Europe/Rome'          => "(GMT+01:00) Rome",
            'Europe/Sarajevo'      => "(GMT+01:00) Sarajevo",
            'Europe/Skopje'        => "(GMT+01:00) Skopje",
            'Europe/Stockholm'     => "(GMT+01:00) Stockholm",
            'Europe/Vienna'        => "(GMT+01:00) Vienna",
            'Europe/Warsaw'        => "(GMT+01:00) Warsaw",
            'Europe/Zagreb'        => "(GMT+01:00) Zagreb",
            'Europe/Athens'        => "(GMT+02:00) Athens",
            'Europe/Bucharest'     => "(GMT+02:00) Bucharest",
            'Africa/Cairo'         => "(GMT+02:00) Cairo",
            'Africa/Harare'        => "(GMT+02:00) Harare",
            'Europe/Helsinki'      => "(GMT+02:00) Helsinki",
            'Europe/Istanbul'      => "(GMT+02:00) Istanbul",
            'Asia/Jerusalem'       => "(GMT+02:00) Jerusalem",
            'Europe/Kiev'          => "(GMT+02:00) Kyiv",
            'Europe/Minsk'         => "(GMT+02:00) Minsk",
            'Europe/Riga'          => "(GMT+02:00) Riga",
            'Europe/Sofia'         => "(GMT+02:00) Sofia",
            'Europe/Tallinn'       => "(GMT+02:00) Tallinn",
            'Europe/Vilnius'       => "(GMT+02:00) Vilnius",
            'Asia/Baghdad'         => "(GMT+03:00) Baghdad",
            'Asia/Kuwait'          => "(GMT+03:00) Kuwait",
            'Africa/Nairobi'       => "(GMT+03:00) Nairobi",
            'Asia/Riyadh'          => "(GMT+03:00) Riyadh",
            'Asia/Tehran'          => "(GMT+03:30) Tehran",
            'Europe/Moscow'        => "(GMT+04:00) Moscow",
            'Asia/Baku'            => "(GMT+04:00) Baku",
            'Europe/Volgograd'     => "(GMT+04:00) Volgograd",
            'Asia/Muscat'          => "(GMT+04:00) Muscat",
            'Asia/Tbilisi'         => "(GMT+04:00) Tbilisi",
            'Asia/Yerevan'         => "(GMT+04:00) Yerevan",
            'Asia/Kabul'           => "(GMT+04:30) Kabul",
            'Asia/Karachi'         => "(GMT+05:00) Karachi",
            'Asia/Tashkent'        => "(GMT+05:00) Tashkent",
            'Asia/Kolkata'         => "(GMT+05:30) Kolkata",
            'Asia/Kathmandu'       => "(GMT+05:45) Kathmandu",
            'Asia/Yekaterinburg'   => "(GMT+06:00) Ekaterinburg",
            'Asia/Almaty'          => "(GMT+06:00) Almaty",
            'Asia/Dhaka'           => "(GMT+06:00) Dhaka",
            'Asia/Novosibirsk'     => "(GMT+07:00) Novosibirsk",
            'Asia/Bangkok'         => "(GMT+07:00) Bangkok",
            'Asia/Jakarta'         => "(GMT+07:00) Jakarta",
            'Asia/Krasnoyarsk'     => "(GMT+08:00) Krasnoyarsk",
            'Asia/Chongqing'       => "(GMT+08:00) Chongqing",
            'Asia/Hong_Kong'       => "(GMT+08:00) Hong Kong",
            'Asia/Kuala_Lumpur'    => "(GMT+08:00) Kuala Lumpur",
            'Australia/Perth'      => "(GMT+08:00) Perth",
            'Asia/Singapore'       => "(GMT+08:00) Singapore",
            'Asia/Taipei'          => "(GMT+08:00) Taipei",
            'Asia/Ulaanbaatar'     => "(GMT+08:00) Ulaan Bataar",
            'Asia/Urumqi'          => "(GMT+08:00) Urumqi",
            'Asia/Irkutsk'         => "(GMT+09:00) Irkutsk",
            'Asia/Seoul'           => "(GMT+09:00) Seoul",
            'Asia/Tokyo'           => "(GMT+09:00) Tokyo",
            'Australia/Adelaide'   => "(GMT+09:30) Adelaide",
            'Australia/Darwin'     => "(GMT+09:30) Darwin",
            'Asia/Yakutsk'         => "(GMT+10:00) Yakutsk",
            'Australia/Brisbane'   => "(GMT+10:00) Brisbane",
            'Australia/Canberra'   => "(GMT+10:00) Canberra",
            'Pacific/Guam'         => "(GMT+10:00) Guam",
            'Australia/Hobart'     => "(GMT+10:00) Hobart",
            'Australia/Melbourne'  => "(GMT+10:00) Melbourne",
            'Pacific/Port_Moresby' => "(GMT+10:00) Port Moresby",
            'Australia/Sydney'     => "(GMT+10:00) Sydney",
            'Asia/Vladivostok'     => "(GMT+11:00) Vladivostok",
            'Asia/Magadan'         => "(GMT+12:00) Magadan",
            'Pacific/Auckland'     => "(GMT+12:00) Auckland",
            'Pacific/Fiji'         => "(GMT+12:00) Fiji",
        );
    }
}