<?php

/**
 * Acl Controller Plugin
 */
class App_Controller_Plugin_Acl extends Zend_Controller_Plugin_Abstract
{
    protected $_acl;
    protected $_action;
    protected $_controller;
    protected $_currentRole;

    public function __construct(Zend_Acl $acl)
    {
        $this->_acl = $acl;
    }

    protected function _init($request)
    {
        $this->_action = $request->getActionName();
        $this->_controller = $request->getControllerName();
        $this->_currentRole = $this->_getCurrentUserRole();
    }

    /**
     * Redirect user if he doesn't have permission
     * 
     * @param Zend_Controller_Request_Abstract $request
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $this->_init($request);

        if (!$this->_acl->isAllowed($this->_currentRole, $this->_controller, $this->_action)) {
            if ('guest' == $this->_currentRole) {
                $controller = 'auth';
                $action = 'login';
            } else {
                $controller = 'index';
                $action = 'index';
            }

            $this->getResponse()->setRedirect( "/{$controller}/{$action}" )->sendResponse( );
        }
    }

    /**
     * Find current user's role
     * 
     * @return string
     */
    protected function _getCurrentUserRole()
    {
        $role = 'guest';
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $currentUser = Zend_Registry::get('user');
            $role = $currentUser->role;

            $layout = Zend_Layout::getMvcInstance();
            $view = $layout->getView();
            $view->currentUser = $currentUser;

            if($currentUser->show_notifications) {
                $affiliateCampaignsModel = new Application_Model_DbTable_AffiliateCampaigns();
                $view->connectAffiliate = $affiliateCampaignsModel->getNotPairedCampaigns($currentUser->id, null, $withRevenueOnly = true);
            }

            $refreshData = new Zend_Session_Namespace('refresh_data');
            if($refreshData->notification) {
                $view->notification = $refreshData->notification;
                $view->notificationText = $refreshData->notificationText;
                unset($refreshData->notification, $refreshData->notificationText);
            }

            $userAgent = new Zend_Http_UserAgent();
            $device = $userAgent->getDevice(); 
            $view->is_desktop = $device->getFeature('is_desktop');
        }

        return $role;
    }
}