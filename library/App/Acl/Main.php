<?php

/**
 * App Acl
 * 
 */
class App_Acl_Main extends Zend_Acl {

    /**
     * construct
     * 
     * convert ACL configs to Zend_Acl Roles and Resources
     * 
     * @return void
     */
    public function __construct()
    {
        $aclConfig = Zend_Registry::get('acl');
        $roles = $aclConfig->acl->roles;
        $resources = $aclConfig->acl->resources;
        $this->_addRoles($roles);
        $this->_addResources($resources);
    }

    /**
     * add roles to Zend_Acl
     * 
     * @param Zend_Config $roles
     * @return void
     */
    protected function _addRoles($roles)
    {
        foreach ($roles as $name => $parents) {
            if (!$this->hasRole($name)) {
                if (empty($parents)) {
                    $parents = null;
                } else {
                    $parents = explode(',', $parents);
                }
                $this->addRole(new Zend_Acl_Role($name), $parents);
            }
        }
    }

    /**
     * add resources to Zend_Acl
     * 
     * @param Zend_Config $resources
     * @return void
     */
    protected function _addResources($resources)
    {
        foreach ($resources as $permissions => $controllers) {
            foreach ($controllers as $controller => $actions) {
                if ($controller == 'all') {
                    $controller = null;
                } else {
                    if (!$this->has($controller)) {
                        $this->add(new Zend_Acl_Resource($controller));
                    }
                }

                foreach ($actions as $action => $role) {
                    if ($action == 'all') {
                        $action = null;
                    }

                    if ($permissions == 'allow') {
                        $this->allow($role, $controller, $action);
                    } elseif ($permissions == 'deny') {
                        $this->deny($role, $controller, $action);
                    }
                }
            }
        }
    }
}