<?php

class App_Mail extends Zend_Mail
{
    /**
     * @var Zend_View;
     */
    private $_bodyAdapter;

    /**
     * @var string
     */
    private $_templateName;

    public function __construct($templateName, $subject = null)
    {
        if ($subject) {
            $this->setSubject($subject);
        }

        $this->_bodyAdapter = $this->_getBodyAdapter();
        $this->_templateName = $templateName;
    }

    /**
     * @return Zend_View
     */
    private function _getBodyAdapter()
    {
        $view = new Zend_View();
        $view->setScriptPath(EMAIL_TEMPLATE_PATH);
        return $view;
    }

    /**
     * @param  string|array The assignment strategy to use.
     * @param  mixed (Optional) If assigning a named variable, use this
     * as the value.
     * @return App_Mail
     */
    public function assign($spec, $value = null)
    {
        $this->_bodyAdapter->assign($spec, $value);
        return $this;
    }

    /**
     * Send email using html template
     * 
     * @param  Zend_Mail_Transport_Abstract $transport
     * @return boolean
     */
    public function send($transport = null)
    {
        $this->_bodyAdapter->assign('template', $this->_templateName);
        $this->setBodyHtml($this->_bodyAdapter->render('general.phtml'), 'UTF-8');
        try {
            parent::send($transport);
        } catch (Zend_Mail_Exception $e) {
            return false;
        }
        return true;
    }
}