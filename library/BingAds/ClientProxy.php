<?php
Class ClientProxy
{
    private $username;
    private $password;
    private $developerToken;
    private $wsdlUrl;
    private $accountId;
    private $customerId;
    private $service;
    private $namespace;

    public function __construct($username, $password, $token, $wsdl, $accountId = null, $customerId = null)
    {
        $this->username = $username;
        $this->password = $password;
        $this->developerToken = $token;
        $this->accountId = $accountId;
        $this->customerId = $customerId;
        $this->wsdlUrl = $wsdl;
        $this->service = $this->GetProxy($wsdl);
    }

    public function GetService() { return $this->service; }
    public function GetNamespace() { return $this->namespace; }
    public function GetWsdl() { return $this->wsdlUrl; }

    // This function gets the namespace from the WSDL, so you do
    // not have to hardcode it in the client.

    private function GetServiceNamespace($url)
    {
        $doc = new DOMDocument;
        $doc->Load($url);

        $xpath = new DOMXPath($doc);
        $query = "/*/@targetNamespace";

        $attrs = $xpath->query($query);

        // The query will return only one node in the node list.

        foreach($attrs as $attr)
        {
            $namespace = $attr->value;
        }

        return $namespace;
    }

    private function GetProxy($wsdl)
    {
        $this->namespace = $this->GetServiceNamespace($wsdl);

        // Define the SOAP headers. Customer ID is required
        // to get editorial reasons.

        $headers = array();

        $headers[] = new SoapHeader(
                $this->namespace,
                'DeveloperToken',
                $this->developerToken
        );

        $headers[] = new SoapHeader(
                $this->namespace,
                'UserName',
                $this->username
        );

        $headers[] = new SoapHeader(
                $this->namespace,
                'Password',
                $this->password
        );

        if($this->accountId) {
            $headers[] = new SoapHeader(
                 $this->namespace,
                 'CustomerAccountId',
                 $this->accountId
                 );
        }

        if($this->customerId) {
            $headers[] = new SoapHeader(
                 $this->namespace,
                 'CustomerId',
                 $this->customerId
                 );
        }

        // By default, PHP does not return single item arrays as an array type.
        // To force PHP to always return an array for an array type in the
        // response, specify the SOAP_SINGLE_ELEMENT_ARRAYS feature.

        $options = array(
                'trace' => TRUE,
                'exceptions' => TRUE,
                'features' => SOAP_SINGLE_ELEMENT_ARRAYS,

                // Map long type to string type. For details, see
                // from_long_xml and to_long_xml callbacks.

                'typemap' => array(
                        array(
                                'type_ns' => 'http://www.w3.org/2001/XMLSchema',
                                'type_name' => 'long',
                                'to_xml' => 'to_long_xml',
                                'from_xml' => 'from_long_xml'
                        ),
                )
        );

        $proxy = @new SOAPClient($this->wsdlUrl, $options);

        $proxy->__setSoapHeaders($headers);

        return $proxy;
    }
}

// Converts long types found in SOAP responses to string types in PHP.

function from_long_xml($xmlFragmentString)
{
    return (string)strip_tags($xmlFragmentString);
}

// Converts PHP string types to long types in SOAP requests.

function to_long_xml($longVal)
{
    return '<long>' . $longVal . '</long>';
}