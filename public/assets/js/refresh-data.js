$(function() {
    /**
     * Check if the stats was updated after page was loaded
     * Refresh page if necessary
     */
    refreshPage = function(lastUpdate){
        $.ajax({
            url: '/accounting/refresh-page',
            data: {last_update: lastUpdate},
            success: function(result) {
                if(result) {
                    window.location.reload();
                }
            },
            type: 'POST',
            dataType: 'json'
        })
    }
})