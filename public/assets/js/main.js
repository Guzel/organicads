var cancelUpdate = false;

var validationOptions = { 
    'errorClass': 'help-block',
    'errorElement': 'span',
    highlight: function(element, errorClass, validClass) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.form-group').removeClass('has-error');
    }
};

var tooltipValidationOptions = { 
    'errorClass': 'help-block',
    'errorElement': 'div',
    showErrors: function(errorMap, errorList) {
        $.each(this.validElements(), function (index, element) {
            var $element = $(element);
            $element.data("title", "").removeClass("error").tooltip("destroy");
            $element.closest('.form-group').removeClass('has-error');
        });

        $.each(errorList, function (index, error) {
            var $element = $(error.element);

            $element.tooltip("destroy")
                .data("title", error.message)
                .addClass("error")
                .tooltip('show');
            $element.closest('.form-group').addClass('has-error')
        });
    }
};

$(function() {
    $('[data-rel=tooltip]').tooltip();
    $('[data-rel=popover]').popover();

    $('#update-data-btn').click(updateData);

    $('#login-form, #edituser-form, #account-connect-form, #reset-password-form, \n\
        #user-profile-form, #campaign-stat-form, #create-field-form, #create-notification-form, \n\
        #upload-targets').validate(validationOptions);

    if($.isFunction($.fn.editable)) {
        //x-editable settings
        $.fn.editable.defaults.mode = 'inline';
        $.fn.editableform.loading = "<div class='editableform-loading'><i class='light-blue icon-2x icon-spinner icon-spin'></i></div>";
        $.fn.editableform.buttons = '<button type="submit" class="btn btn-success editable-submit"><i class="icon-ok icon-white"></i></button>'+
                                    '<button type="button" class="btn editable-cancel"><i class="icon-remove"></i></button>';
    }

    //Forgot Password validation:
    $('body').on('submit', '#forgot-password-form', function() {
        var form = $(this);
        form.ajaxSubmit({
            beforeSubmit: function() {
                return form.validate(validationOptions).form();
            },
            success: function(response) {
                if(response.success) {
                    $('#forgot-password-block').hide();
                    $('#forgot-password-success').removeClass('hide');
                } else {
                    var errorArray = {};
                    errorArray['email'] = 'Email was not found';
                    form.validate(validationOptions).showErrors(errorArray);
                }
            }
        });
        return false;
    })

    $('.back-to-login-link').click(function(e) {
        e.preventDefault();
        showBox('login-box');
    })

    $('.forgot-password-link').click(function(e) {
        e.preventDefault();
        showBox('forgot-box');
    })

    /*
     * Disable user
     */
    $('body').on('click', '.disable-user', function(e) {
        e.preventDefault();
        var link = $(this);
        var dialog = bootbox.confirm("<span class='bigger-110'>Are you sure you want to diactivate user?</span>",
            function(result) {
                if(result) {
                    changeUserStatus(link.attr('userId'), 'disable');
                }
                dialog.modal('hide'); return false;
            }
        );
    })

    /**
     * Activate user
     */
    $('body').on('click', '.activate-user', function(e) {
        e.preventDefault();
        changeUserStatus($(this).attr('userId'), 'active');
    })

    /**
     * Delete user
     */
    $('body').on('click', '.delete-user', function(e) {
        e.preventDefault();
        var link = $(this);
        var dialog = bootbox.confirm('Are you sure you want to delete user? The user will be deleted.', function(result) {
            if(result) {
                changeUserStatus(link.attr('userId'), 'delete');
            }
            dialog.modal('hide'); return false;
        })
    })

    /**
     * Edit campaigns; affiliate, ad campaigns. Wizard -> Settings, Accounting -> Campaign-list
     */
    $('.accounting-settings .editable').editable({
        url: '/wizard/settings',
        validate: function(value) {
            if(typeof value.password != 'undefined') {
                if($.trim(value.password) == '' || $.trim(value.confirm_password) == '') {
                    return 'All fields are required'; 
                }
                if($.trim(value.password) != $.trim(value.confirm_password)) {
                    return 'Invalid password confirmation'; 
                }
            } else {
                if($.trim(value) == '') return 'This field is required';
            }
        },
        params: function(params) {
            params.resource = $(this).data('resource');
            params.operation = 'edit';
            params.field = $(this).data('edit');
            params.id = params.pk;
            params.value = params.value.password ? params.value.password : params.value;
            return params;
        }
    });

    /**
     * Delete account/campaign. Wizard -> Settings
     */
    $('body').on('click', '.wizard-delete', function(e) {
        e.preventDefault();
        var link = $(this);
        var text = link.attr('data-resource') == 'campaign' ? 'Are you sure you want to delete this Campaign?' 
            : 'Are you sure you want to delete this account? All imported data will be deleted.';
        var dialog = bootbox.confirm(text, function(result) {
            if(result) {
                updateSettings(link.data('id'), link.data('resource'), 'delete');
            }
            dialog.modal('hide'); return false;
        })
    })

    /**
     * Disable account. Wizard -> Settings
     */
    $('body').on('click', '.disable-affiliate, .disable-ad', function(e) {
        e.preventDefault();
        var link = $(this);
        var dialog = bootbox.confirm("<span class='bigger-110'>Are you sure you want to disable sync?</span>",
            function(result) {
                if(result) {
                    updateSettings(link.data('id'), link.data('resource'), 'disable');
                }
                dialog.modal('hide'); return false;
            }
        );
    })

    /**
     * Activate account. Wizard -> Settings
     */
    $('body').on('click', '.activate-affiliate, .activate-ad', function(e) {
        e.preventDefault();
        updateSettings($(this).data('id'), $(this).data('resource'), 'active');
    })

    /**
     * Delete field. CRM -> Fields
     */
    $('body').on('click', '.delete-field', function(e) {
        e.preventDefault();
        var link = $(this);
        var dialog = bootbox.confirm('Are you sure you want to delete this field?', function(result) {
            if(result) {
                deleteFormField(link.attr('data-id'));
            }
            dialog.modal('hide'); return false;
        })
    })

    /**
     * Delete form. CRM -> Forms
     */
    $('body').on('click', '.delete-form', function(e) {
        e.preventDefault();
        var link = $(this);
        var dialog = bootbox.confirm('Are you sure you want to delete this form?', function(result) {
            if(result) {
                deleteForm(link.attr('data-id'));
            }
            dialog.modal('hide'); return false;
        })
    })

    /**
     * Delete notifications/contact. Notifications -> Contacts
     */
    $('body').on('click', '.delete-notification-contact', function(e) {
        e.preventDefault();
        var link = $(this);
        var text = 'Are you sure you want to delete this Contact?';
        var dialog = bootbox.confirm(text, function(result) {
            if(result) {
                deleteNotificationContacts(link.attr('data-id'), 'delete');
            }
            dialog.modal('hide'); return false;
        })
    })

    //A click anywhere in the browser will close the gritter notification:
    $(document).mouseup(function (e){
        var container = $("#gritter-notice-wrapper");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.fadeOut();
        }
    });
    
    /*
    * Disable notification
    */
    $('body').on('click', '.disable-notification', function(e) {
       e.preventDefault();
       var link = $(this);
       var dialog = bootbox.confirm("<span class='bigger-110'>Are you sure you want to diactivate notification?</span>",
           function(result) {
               if(result) {
                   changeNotificationStatus(link.attr('notificationId'), 'disable');
               }
               dialog.modal('hide'); return false;
           }
       );
    })

    /**
    * Activate notification
    */
    $('body').on('click', '.activate-notification', function(e) {
       e.preventDefault();
       changeNotificationStatus($(this).attr('notificationId'), 'active');
    })

    /**
    * Delete user
    */
    $('body').on('click', '.delete-notification', function(e) {
       e.preventDefault();
       var link = $(this);
       var dialog = bootbox.confirm('Are you sure you want to delete this notification?', function(result) {
           if(result) {
               changeNotificationStatus(link.attr('notificationId'), 'delete');
           }
           dialog.modal('hide'); return false;
       })
    })
})


/*
 * Change user status
 */
function changeUserStatus(userId, type)
{
    if(!userId) {
        return;
    }
    $.post(
        '/adminpanel/remove-user',
        {'id': userId, 'type': type},
        function(result) {
            var userRow = $('#user-info-' + userId);
            if(type == 'delete') {
                userRow.fadeOut(400, function() { userRow.remove(); });
            } else {
                var newClass = type == 'disable' ? 'disable' : 'active';
                $('#user-info-'+userId).attr("class", "user-row-"+newClass);
            }
        }
    )
}

/*
 * Change notification status
 */
function changeNotificationStatus(notificationId, type)
{
    if(!notificationId) {
        return;
    }
    $.post(
        '/notifications/edit-notification',
        {'id': notificationId, 'type': type},
        function(result) {
            var row = $('#notification-info-' + notificationId);
            if(type == 'delete') {
                row.fadeOut(400, function() { row.remove(); });
            } else {
                var newClass = type == 'disable' ? 'disable' : 'active';
                $('#notification-info-'+notificationId).attr("class", "notification-row-"+newClass);
            }
        }
    )
}

function updateSettings(id, resource, operation)
{
    if(!id) {
        return;
    }
    var data = {'id': id, 'resource' : resource, 'operation': operation};
    if(operation != 'delete') {
        data.field = 'disabled';
        data.value = (operation == 'disable') ? 1 : 0;
        data.operation = 'edit';
    }
    $.post(
        '/wizard/settings',
        data,
        function(result) {
            var row = $('#' + resource + '-' + id);
            if(operation == 'delete') {
                row.fadeOut(400, function() { row.remove(); });
            } else {
                var newClass = operation == 'disable' ? 'disable' : 'active';
                $('#'+resource+'-'+id).attr("class", resource+"-row-"+newClass);
            }
        }
    )
}

function deleteFormField(id)
{
    if(!id) {
        return;
    }
    $.post(
        '/crm/fields',
        {'id': id, 'delete' : 1},
        function(result) {
            var row = $('#form-field-' + id);
            row.fadeOut(400, function() { row.remove(); });
        }
    )
}

function deleteForm(id)
{
    if(!id) {
        return;
    }
    $.post(
        '/crm/edit-form',
        {'id': id, 'delete' : 1},
        function(result) {
            var row = $('#form-' + id);
            row.fadeOut(400, function() { row.remove(); });
        }
    )
}

function deleteNotificationContacts(id, operation)
{
    if(!id) {
        return;
    }
    $.post(
        '/notifications/contact',
        {'id': id, 'operation': operation},
        function(result) {
            var row = $('#contact-' + id);
            row.fadeOut(400, function() { row.remove(); });
        }
    )
}

function showBox(id) {
    $('.widget-box.visible').removeClass('visible');
    $('#'+id).addClass('visible');
}

function showNotification(text, title, sticky, center)
{
    var class_center = center ? 'gritter-center' : '';
    $.gritter.add({
        title: title ? title : 'New Notification!',
        text: text,
        class_name: 'gritter-error gritter-light ' + class_center,
        sticky: sticky,
        time: ''
    });
}

function showBodyShadow(){
    $('body').append('<div id="body-shadow"></div>');
    $("#body-shadow").css("height", $(document).height());
}

function showLargeSpinner()
{
    showBodyShadow();
    var spinnerOptions = {
        lines: 13, // The number of lines to draw
        length: 30, // The length of each line
        width: 10, // The line thickness
        radius: 30, // The radius of the inner circle
        color: '#FFF', // #rgb or #rrggbb
        className: 'spinner spinner-large' // The CSS class to assign to the spinner
    };

    var spinner = new Spinner(spinnerOptions).spin();
    $('#body-shadow').append(spinner.el);
}

function hideLargeSpinner()
{
    $("#body-shadow").remove();
}

function updateData()
{
    cancelUpdate = false;
    var spinner = new Spinner({lines: 11, length: 11,
        width: 8, radius: 20, color: '#000'}).spin();
    $('#spinner-modal-content').append(spinner.el);
    $('#update-data-modal').modal('show');

    sendUpdateRequest();
}

function sendUpdateRequest()
{
    $.ajax({
        url: '/accounting/update-data',
        data: {},
        success: function(result) {
            if(!result.completed && !cancelUpdate) {
                sendUpdateRequest();
            } else {
                $('#update-data-modal').modal('hide');
                if(!cancelUpdate) {
                    window.location.reload();
                }
            }
        },
        //async: false,
        type: 'POST',
        dataType: 'json'
    })
}

function updateCancel()
{
    cancelUpdate = true;
    $('#update-data-modal').modal('hide');
}

$.extend($.expr[":"], {
    "containsIN": function(elem, i, match, array) {
        return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
});

updateNotificationContacts = function() {
    $.ajax({
        type: "POST",
        url: "/notifications/contacts/format/html",
        data: {},
        success: function(data){
            $('#notifications-contacts').html(data);
        }
    });
}