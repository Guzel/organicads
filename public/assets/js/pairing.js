var DataSourceTree = function(options) {
    this.url = options.url;
}
var selectedCampaigns;
var campaigns = {};
var accounts = {};
var selectedAds = {};
var subidSettings = [];

DataSourceTree.prototype.data = function(options, callback) {
    var $data = null;

    if(!("name" in options) && !("type" in options)){
        $.ajax({
            url: this.url,
            data: {},
            type: 'POST',
            dataType: 'json',
            success : function(response) {
                if(response.status == "OK") {
                    setAdSources(response.data);
                    callback({ data: response.data })
                }
            }
        })
    } else if("type" in options && options.type == "folder") {
        if("additionalParameters" in options && "children" in options.additionalParameters)
            $data = options.additionalParameters.children;
        else $data = {}//no data
    }

    if($data != null)//this setTimeout is only for mimicking some random delay
        setTimeout(function(){callback({ data: $data });} , parseInt(Math.random() * 500) + 200);
};

function setAdSources(data) {
    var selectedTraffic = [];
    for(source in data) {
        if(data[source].source != 'ad') {
            return;
        }
        var adName = data[source].name;
        var children = data[source].additionalParameters.children;
        for(i in children) {
            var currentChildren = children[i];
            if(isSelected(currentChildren)) {
                currentChildren.ad = adName;
                selectedTraffic.push(currentChildren);
            }
        }
    }
}

function isSelected(children) {
    var selected = false;
    if(typeof children.additionalParameters != "undefined") {
        if(children['id'] && $.isNumeric(children['id'])) {
            accounts[children['id']] = children;
        }

        var subchild = children.additionalParameters.children;
        for(i in subchild) {
            if((selected = isSelected(subchild[i]))) {
                return true;
            }
        }
    } else {
        campaigns[children['id']] = children;
        keywordMatch = false;
        var keyword = $('#keyword').val() ? $.trim($('#keyword').val()) : '';

        if(keyword && children['name']) {
            keywordMatch = (children['name'].indexOf(keyword) != -1 ? true : false);
        }
        return (keywordMatch || children['campaign_id']) ? true : false;
    }
    return selected;
}

$(function() {
    enablePairingOptions();
    $('form').validate(validationOptions);

    $('#cancel-pairing').click(function() {
        location.href='/accounting/campaign-list'; return false;
    })

    $('#btn-save-changes').click(function(e) {
        if(!(isValid = $('form').valid())) {
            return;
        }
        isValid = false;
        $.ajax({
            url: '/wizard/pairing',
            data: {'keyword':$('#keyword').val(), 'name': $('#name').val(), 'id': $('#campaign_id').val()},
            type: 'POST',
            dataType: 'json',
            async: false,
            success : function(response) {
                isValid = response.valid;

                if(!isValid) {
                    hideLargeSpinner();
                    var errors = '';
                    for(i in response.errors) {
                        for(key in response.errors[i]) {
                            errors += response.errors[i][key] + "\r\n";
                        }
                    }
                    bootbox.dialog({
                        message: "<span class='bigger-110'>"+errors+"</span>",
                        buttons:{
                            "click":{
                                "label" : "OK",
                                "className" : "btn-sm btn-primary"
                            }
                        }
                    });
                }
            }
        })
        if(!isValid) {
            return;
        }

        findSelected('affiliate_campaigns');
        findSelected('ad_campaigns');

        if(!$('#affiliate_campaigns').val()) {
            bootbox.dialog({
                message: "<span class='bigger-110'>You must select at least 1 Affiliate Network!</span>",
                buttons:{
                    "click":{
                        "label" : "OK",
                        "className" : "btn-sm btn-primary"
                    }
                }
            });
        } else {
            $('form').submit();
        }
    });

    /**
     * Search Ad/Affiliate Campaigns
     */
    $('.campaign_search').keyup(function() {
        var query = $.trim($(this).val());
        var source = $(this).data('source');
        var selector = $("#"+source+"_campaigns_tree");

        if(query) {
            selector.find('.tree-item-name').parent().hide();
            selector.find('.tree-item-name:containsIN(' + query + ')').parent().show();
        } else {
            selector.find('.tree-item-name').filter(function () {
                return this.textContent;
            }).parent().show();
        }
    })

    /**
     * "Select All" functionality
     */
    $('.select-all').click(function(e) {
        e.preventDefault();
        var source = $(this).data('source');
        var query = $.trim($('#'+source+'-search').val());
        if(query) {
            campaign_found = $('#'+source+'_campaigns_tree').find('div.tree-item-name:containsIN(' + query + ')');
            $.each(campaign_found, function() {
                var parent = $(this).closest('.tree-item');
                if(!parent.hasClass('tree-selected')) {
                    parent.click();
                }
            })
        }
    })

    function newTreeElement(id, action, hideSpinner){
        var readyIds = { };
        var url = '/wizard/' + action + '?campaign='+$('#name').val();

        $('#' + id).ace_tree({
            dataSource: new DataSourceTree({url: url}) ,
            multiSelect:true,
            loadingHTML:'<div class="tree-loading"><i class="icon-refresh icon-spin blue"></i></div>',
            'open-icon' : 'icon-minus',
            'close-icon' : 'icon-plus',
            'selectable' : true,
            'selected-icon' : 'icon-ok',
            'unselected-icon' : 'icon-remove'
        }).on('loaded', function (evt, data) {
            //mark already selected campaigns
            if($(this).hasClass('tree')) {
                $folder = $(this).find('.tree-folder-header');
                $.each($folder, function (index, value) {
                    var children = $(this).parent().find('.tree-folder-content').children().length;
                    if ($(value).data('id') && children == 0 && !readyIds[$(value).data('id')]) {
                        readyIds[$(value).data('id')] = true;
                        $('#' + id).tree("selectFolder", $(value));
                        $('#' + id).tree("selectFolder", $(value));
                        var $sel = $(value).parent().find('.tree-item');
                    }
                });
            }

            var keyword = $('#keyword').val() ? $.trim($('#keyword').val()) : '';

            var $sel = $('#' + id).find('.tree-item');
            $.each($sel, function (index, value) {
                //mark as "selected" all campaign with selected keyword
                selected = false;
                if(keyword && (campName = $(value).data('name'))) {
                    selected = (campName.indexOf(keyword) != -1 ? true : false);
                }

                if(($(value).data('campaign_id') || selected) && !$(value).hasClass('tree-selected')) {
                    $('#' + id).tree("selectItem", value);
                }
            });

            if(hideSpinner) {
                hideLargeSpinner();
            }
        }).on('selected', function(evt, data) {
            //console.log(evt, data, $(this));
            var parentId = $(this).first('tree').attr('id');
            if(parentId == 'ad_campaigns_tree') {
                for(i in data['info']) {
                    var ad_id = data['info'][i]['ad_id'];
                    if(!selectedAds[ad_id]) {
                        selectedAds[ad_id] = [];
                    }
                    selectedAds[ad_id][data['info'][i]['id']] = data['info'][i];
                    selectedAds[ad_id]['selected'] = true;
                }

                for(ad_id in selectedAds) {
                    var source = accounts[ad_id]['source'];
                    var row_id = 'ad_subid_'+source;

                    if(!selectedAds[ad_id]['selected']) {
                        $('.' + row_id).remove();
                        delete selectedAds[ad_id]; continue;
                    }
                    selectedAds[ad_id]['selected'] = false;

                    if($("." + row_id).length != 0) {
                        continue;
                    }
                    $('.traffic_source_list').each(function() {
                        var affiliate = $(this).data('affiliate');
                        var row = '';
                        row += '<td>' + source + '</td>';
                        row += '<td><input type="text" name="subid_settings['+affiliate+']['+source+']"></td>';
                        $(this).find('tbody').append('<tr class="'+row_id+'">' + row + '</tr>');

                        //Set Current Subid Code values
                        for(i in subidSettings) {
                            var setting = subidSettings[i];
                            if(setting.ad == source && setting.affiliate == affiliate) {
                                $("input[name='subid_settings["+setting.affiliate+"]["+setting.ad+"]']").val(setting.code);
                            }
                        }
                    })
                }

                /*for(ad_id in selectedAds) {
                    var row_id = 'ad_subid_'+ad_id;

                    if(!selectedAds[ad_id]['selected']) {
                        $('.' + row_id).remove();
                        delete selectedAds[ad_id]; continue;
                    }
                    selectedAds[ad_id]['selected'] = false;

                    if($("." + row_id).length != 0) {
                        continue;
                    }
                    row = '';
                    row += '<td>' + accounts[ad_id]['source'] + '</td>';
                    //row += '<td><input type="text" name="subid_settings['+ad_id+'][number]"></td>';
                    row += '<td><input type="text" name="subid_settings['+ad_id+'][code]"></td>';
                    $('.traffic_source_list tbody').append('<tr class="'+row_id+'">' + row + '</tr>');
                }*/
            }
        })
    }

    //Find Selected tree elements
    function findSelected(id) {
        var $sel = $('#'+id+'_tree').find('.tree-selected');
        var data = [];

        $.each($sel, function (index, value) {
            data.push($(value).data('id'));
        });
        $('#'+id).val(data.join(','));
    }

    function enablePairingOptions() {
        showLargeSpinner();
        //Affiliate Networks Tree:
        $('#affiliate_campaigns_tree').remove();
        $('#affiliate-tree-cont').append('<div id="affiliate_campaigns_tree" class="tree"></div>');
        newTreeElement('affiliate_campaigns_tree', 'get-affiliates');

        //Ad Platforms Tree:
        $('#ad_campaigns_tree').remove();
        $('#ad-tree-cont').append('<div id="ad_campaigns_tree" class="tree"></div>');
        newTreeElement('ad_campaigns_tree', 'get-ads', true);
    }
})