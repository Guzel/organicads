var DataSourceTree = function(options) {
    this.url = options.url;
}

DataSourceTree.prototype.data = function(options, callback) {
    var $data = null;

    if(!("name" in options) && !("type" in options)){
        $.ajax({
            url: this.url,
            data: {},
            type: 'POST',
            dataType: 'json',
            success : function(response) {
                if(response.status == "OK") {
                    callback({ data: response.data })
                }
            }
        })
    } else if("type" in options && options.type == "folder") {
        if("additionalParameters" in options && "children" in options.additionalParameters)
            $data = options.additionalParameters.children;
        else $data = {}//no data
    }

    if($data != null)//this setTimeout is only for mimicking some random delay
        setTimeout(function(){callback({ data: $data });} , parseInt(Math.random() * 500) + 200);
};

$(function() {
    $('form').validate(validationOptions);
    $('.chosen-select').chosen();
    $('#campaign_chosen').after($('#campaign'));

    $('#cancel-pairing').click(function() {
        location.href='/wizard/pairing'; return false;
    })

    $('#fuelux-wizard').ace_wizard().on('change' , function(e, info){
        if(info.step == 1) {
            $('#campaign-name').html($('#campaign').val());

            if(isValid = $('#campaign').valid()) {
                 showLargeSpinner();

                //Affiliate Networks Tree:
                $('#affiliate_campaigns_tree').remove();
                $('#affiliate-tree-cont').append('<div id="affiliate_campaigns_tree" class="tree"></div>');
                newTreeElement('affiliate_campaigns_tree', 'get-affiliates');

                //Ad Platforms Tree:
                $('#ad_campaigns_tree').remove();
                $('#ad-tree-cont').append('<div id="ad_campaigns_tree" class="tree"></div>');
                newTreeElement('ad_campaigns_tree', 'get-ads', true);

                $('#cancel-pairing').show();
            }
            return isValid;
        }else if(info.step == 2 && info.direction == 'next') {
            findSelected('affiliate_campaigns');
            findSelected('ad_campaigns');

            if(!$('#affiliate_campaigns').val() || !$('#ad_campaigns').val()) {
                bootbox.dialog({
                    message: "<span class='bigger-110'>You must select at least 1 Affiliate Network and 1 Ad Platform!</span>",
                    buttons:{
                        "click":{
                            "label" : "OK",
                            "className" : "btn-sm btn-primary"
                        }
                    }
                });
                return false;
            } else {
                showAdPerformance();
            }
        }
    }).on('finished', function(e) {
        $('form').submit();
    });

    function newTreeElement(id, action, hideSpinner){
        var readyIds = { };
        var url = '/wizard/' + action + '?campaign='+$('#campaign').val()+'&ad_pairing=1';

        $('#' + id).ace_tree({
            dataSource: new DataSourceTree({url: url}) ,
            multiSelect:true,
            loadingHTML:'<div class="tree-loading"><i class="icon-refresh icon-spin blue"></i></div>',
            'open-icon' : 'icon-minus',
            'close-icon' : 'icon-plus',
            'selectable' : true,
            'selected-icon' : 'icon-ok',
            'unselected-icon' : 'icon-remove'
        }).on('loaded', function (evt, data) {
            //mark selected already selected campaigns
            if($(this).hasClass('tree')) {
                $folder = $(this).find('.tree-folder-header');
                $.each($folder, function (index, value) {
                    var children = $(this).parent().find('.tree-folder-content').children().length;
                    if ($(value).data('id') && children == 0 && !readyIds[$(value).data('id')]) {
                        readyIds[$(value).data('id')] = true;
                        $('#' + id).tree("selectFolder", $(value));
                        $('#' + id).tree("selectFolder", $(value));
                        var $sel = $(value).parent().find('.tree-item');
                    }
                });
            }

            var $sel = $('#' + id).find('.tree-item');
            $.each($sel, function (index, value) {

                if($(value).data('ad_pair') && !$(value).hasClass('tree-selected')) {
                    $('#' + id).tree("selectItem", value);
                }
            });

            if(hideSpinner) {
                hideLargeSpinner();
            }
        })
    }

    showAdPerformance = function() {
        showLargeSpinner();
        setTimeout(function(){getAdvertsList()}, 500);
        return false;
    }

    getAdvertsList = function() {
         $.ajax({
            url: '/wizard/ad-pairing-list/format/html',
            data: {'campaign-name': $('#campaign').val(), 
                'ad_campaigns': $('#ad_campaigns').val(),
                'affiliate_campaigns': $('#affiliate_campaigns').val()
            },
            type: 'POST',
            async: false,
            success : function(data) {
                $('#subid-pairing').html(data);
                $('.chosen-subids').chosen({ width: '300px' });
                hideLargeSpinner();
            }
         })
    }

    //Find Selected tree elements
    function findSelected(id) {
        var $sel = $('#'+id+'_tree').find('.tree-selected');
        var data = [];

        $.each($sel, function (index, value) {
            data.push($(value).data('id'));
        });
        $('#'+id).val(data.join(','));
    }
})