$(function() {
    $('#saved').fadeOut(3000);

    //Add/Edit Client Form:
    if($('#fuelux-wizard').length) {
        //Form Steps:
        $('#create-client-form').validate(validationOptions);

        $('.chosen-select').chosen({width: "220px"});
        $('#form_id_chosen').after($('#form_id'));

        //step 2
        $('#add-fiter').validate(validationOptions);
        $('#state_filter_search_value_chosen').after($('#state_filter-search_value'));

        $('#add-fiter').children('dt').hide();

        switchFilter('search_data_filter');
        switchFilterCriteria();
        $('#search_data_filter-criteria').change(switchFilterCriteria);

        $('.filter_switch').click(function(e) {
            e.preventDefault();
            switchFilter($(this).attr('data-filter'));
        })

        $('body').on('click', '.remove_filter', function(e) {
            e.preventDefault();
            $(this).closest('tr').remove();
        })

        var filterId = 1;
        $('#save-filter').click(function() {
            if(!$('#add-fiter').valid()) {
                return;
            }

            var activeFilter = $('.filter:visible').attr('id').replace('fieldset-', '');
            var fieldId = $('#'+activeFilter+'-field_id').val();
            var criteria = $('#'+activeFilter+'-criteria').val();
            var filterValue = searchValue = $('#'+activeFilter+'-search_value').val();

            if(activeFilter == 'state_filter') {
                filterValue = [];
                $('#cap_state_list tbody').html('');
                $.each($('#'+activeFilter+'-search_value option:selected'), function() {
                    filterValue.push($(this).html());
                    step3Row = '';
                    step3Row += '<td><input type="checkbox" class="cap_state_enable" name="cap[enable]['+$(this).val()+']"></td>';
                    step3Row += '<td>'+ $(this).html() + '</td>';
                    step3Row += '<td><input type="text" name="cap[states]['+$(this).val()+']" disabled="disabled"></td>';
                    $('#cap_state_list tbody').append('<tr>' + step3Row + '</tr>');
                })
                filterValue = filterValue.join(', ');
            }

            //For Betweet Criteria:
            if(!filterValue) {
                filterValue = [];
                $.each($('.' + criteria), function() {
                    filterValue.push($(this).val());
                })
                filterValue = searchValue = filterValue.join(', ');
            }

            var newRow = '';
            newRow += '<td>' + $('#'+activeFilter+'-search_data_type option:selected').html() + '</td>';
            newRow += '<td>' + $('#'+activeFilter+'-field_id option:selected').html() + '</td>';
            newRow += '<td>' + $('#'+activeFilter+'-criteria option:selected').html() + '</td>';
            newRow += '<td>' + filterValue + '</td>';
            newRow += '<td><a href="#" class="remove_filter">Remove</a></td>';

            var newFormFields = '';
            newFormFields += '<input type="hidden" name="filters[' + filterId + '][type]" value="' + $('#'+activeFilter+'-search_data_type').val() + '">';
            newFormFields += '<input type="hidden" name="filters[' + filterId + '][field_id]" value="' + fieldId + '" class="filter_field_id">';
            newFormFields += '<input type="hidden" name="filters[' + filterId + '][criteria]" value="' + criteria + '">';
            newFormFields += '<input type="hidden" name="filters[' + filterId + '][search_value]" value="' + searchValue + '">';

            $('#filter-list tbody').find('.'+fieldId+'_'+criteria).remove(); //remove dublicate rows
            $('#filter-list tbody').append('<tr id="filter-' + filterId + '" class="'+fieldId+'_'+criteria+'">' + newRow + newFormFields + '</tr>');
            filterId++;

            $('#modal-filter').modal('hide')
        })

        //step 3
        $('#fieldset-cap').children('dt').hide();
        $('#cap-capped_delivery_interval').ace_spinner({value:$('#cap-capped_delivery_interval').val() * 1, min:5, max:100, step:5, icon_up:'icon-plus', icon_down:'icon-minus', btn_up_class:'btn-success' , btn_down_class:'btn-danger'});
        $('#timed_delivery-postpone_delivery_interval').ace_spinner({value:$('#timed_delivery-postpone_delivery_interval').val() * 1, min:5, max:100, step:5, icon_up:'icon-plus', icon_down:'icon-minus', btn_up_class:'btn-success' , btn_down_class:'btn-danger'});

        switchCapDelivery($('#cap_enabled'));
        switchCapDelivery($('#timed_delivery_enabled'));

        $('#cap-criteria').change(switchCapCriteria);
        $('.cap_delivery_options').click(function() {
            switchCapDelivery($(this))
        });

        $('body').on('click', '.cap_state_enable', function() {
            var input = $(this).closest('tr').find("input[type='text']");
            input.attr('disabled', 'disabled');

            if($(this).is(':checked')) {
                input.removeAttr('disabled');
            }
        })

        $('#timed_delivery-begin').timepicker({showMeridian: false, disableFocus:true, defaultTime: '8:00'})
        $('#timed_delivery-stop').timepicker({showMeridian: false, disableFocus:true, defaultTime: '17:00'})

        //step 4
        switchDelivery();
        $('#delivery-type').change(switchDelivery);

        switchResponseFilter();
        $('#delivery-response_filter').click(switchResponseFilter);


        $('#add_delivery_email').click(function() {
            var id = $(this).next().attr('id').replace('delivery_email_', '');
            var newId = id*1 + 1;

            var newInput = $(this).next().clone().val('').attr('id', 'delivery_email_'+newId).attr('class', 'email');
            $(this).after($(this).next()).after('<div class="clearfix"' + '></div>');
            $(this).after(newInput);
        })

        $('body').on('click', '.addDeliveryField', function() {
            var first = $('#delivery_fields_list ol li').first();

            var newId = 1;
            $.each($('#delivery_fields_list').find('.dd2-content'), function() { 
                var id = $(this).attr('id').replace('delivery_field_row_', '');
                if(id > newId) {
                    newId = id;
                }
            });
            newId = newId*1+1;

            var newRow = first.clone();
            newRow.find('.dd2-content').attr('id', 'delivery_field_row_' + newId);
            newRow.find('.disable_field').removeAttr('checked');
            newRow.find('input[type=text], select').val('').attr('disabled', 'disabled');

            first.before(newRow);
        }).on('click', '.disable_field', function() {
            if($(this).is(':checked')) {
                $(this).closest('div').find("input[type='text'], select").removeAttr('disabled');
            } else {
                $(this).closest('div').find("input[type='text'], select").attr('disabled', 'disabled');
            }
        }).on('click', '.addStaticField', function() {
            var id = $(this).closest('tr').attr('id').replace('static_field_row_', '');
            var newId = id*1+1;
            var newRow = $(this).closest('tr').clone().attr('id', 'static_field_row_' + newId);
            newRow.find('.static_field_name').attr('name', 'delivery[static_fields]['+newId+'][name]').val('');
            newRow.find('.static_field_value').attr('name', 'delivery[static_fields]['+newId+'][value]').val('');
            $(this).closest('tr').before(newRow);
            $(this).remove();
        })


        //Form Wizard
        $('#fuelux-wizard').ace_wizard().on('change' , function(e, info){
            if(info.step == 1) {
                isValid = true;

                var fields = $('#fieldset-client_general').filter(':visible').find('input, select');
                $.each(fields, function() { 
                    if(!$(this).valid()) {
                        isValid = false;
                    }
                });

                if(isValid) {
                    var currentFormId = $('#client_general-form_id').val();

                    if(currentFormId != formId) {
                        $('#filter-list tbody').html(''); //remove old filters from Step2
                        $('#cap_state_list tbody').html(''); //remove old fields from Step3
                        $('.old_delivery_fields').remove();
                    }
                    //get fields by selected Form
                    $.post(
                        '/crm/edit-form',
                        {'id': $('#client_general-form_id').val(), 'get_fields': 1},
                        function(result) {
                            $("#search_data_filter-field_id option").remove();
                            for(i in result) {
                                $("#search_data_filter-field_id").append('<option value="' + i + '">'+result[i]+'</option>');

                                $('#delivery_field_1').append('<option value="' + i + '">'+result[i]+'</option>');
                            }
                        }
                    )
                    formId = currentFormId;
                }

                return isValid;
            }
            if(info.step == 2 && info.direction == 'next') {
                /*if(!$('.filter_field_id').length) {
                    bootbox.dialog({
                        message: "<span class='bigger-110'>You must add at least 1 Filter!</span>",
                        buttons:{
                            "click":{
                                "label" : "OK",
                                "className" : "btn-sm btn-primary"
                            }
                        }
                    });
                    e.preventDefault();
                }*/

                //Add/Remove 'State' option for Caps:
                if(!$('#cap_state_list tbody tr').length) {
                    $("#cap-criteria option[value='state']").remove();
                } else if(!$("#cap-criteria option[value='state']").length) {
                    $("#cap-criteria").append('<option value="state">State</option>');
                }

                if($('#cap_enabled').is(':checked')) {
                    $('#cap-element').show();
                }
                if($('#timed_delivery_enabled').is(':checked')) {
                    $('#timed_delivery-element').show();
                }
            }
            if(info.step == 3 && info.direction == 'next') {
                var isValid = true;

                var fields = $('#step3').find('input, select').filter(':visible');
                $.each(fields, function() { 
                    if(!$(this).valid()) {
                        isValid = false;
                    }
                });

                if($('#timed_delivery_enabled').is(':checked')) {
                    var checkedWeekDays = $('#step3').find("input[name='timed_delivery[weekdays][]']").filter(':checked').length;
                    if(!checkedWeekDays || !$('#timed_delivery-begin').val() || !$('#timed_delivery-stop').val()) {
                        bootbox.dialog({
                            message: "<span class='bigger-110'>For Timed Delivery select at least 1 week day and set up delivery time!</span>",
                            buttons:{
                                "click":{
                                    "label" : "OK",
                                    "className" : "btn-sm btn-primary"
                                }
                            }
                        });
                        isValid = false;
                    }
                }

                return isValid;
            }
            if(info.step == 4 && info.direction == 'next') {
                var isValid = true;

                var fields = $('#step4').find('input, select').filter(':visible');
                $.each(fields, function() { 
                    if(!$(this).valid()) {
                        isValid = false;
                    }
                });

                return isValid;
            }
        }).on('finished', function(e) {
            $('#create-client-form').submit();
        });
    }

    if($('#update-clients-priority').length) {
        //Update Clients Priority:
        $('.dd').nestable({maxDepth: 1}).on('change', function(e) {
            var sorting = $(this).nestable('serialize');
            var clientsSort = [];

            for(i in sorting) {
               clientsSort.push(sorting[i].id);
            }

            $('#clients_priority').val(clientsSort.join(','));
        });
    }

    //Update Delivery Fields Order
    if($('#delivery_fields_list').length) {
        $('.dd').nestable({maxDepth: 1});
    }
})

switchFilter = function(selected) {
    $('.filter').hide();
    $('#fieldset-'+selected).show();
}

switchFilterCriteria = function() {
    var criteria = $('#search_data_filter-criteria');
    criteria.closest('fieldset').find('input[type=text]').closest('.form-group').hide();

    var currentValue = criteria.val();
    var showInputClass = criteria.closest('fieldset').find('.' + currentValue).length ? currentValue : 'default';

    criteria.closest('fieldset').find('.' + showInputClass).closest('.form-group').show();
}

switchDelivery = function() {
    $('.delivery_options').hide();
    $('#fieldset-'+$('#delivery-type').val()+'_delivery').show();
}

switchCapCriteria = function() {
    $('.cap-options').hide();
    if($('#cap_enabled').is(':checked')) {
        $('#fieldset-' + $('#cap-criteria').val()).show();
    }
}

switchCapDelivery = function(checkbox) {
    var id = checkbox.attr('id').replace('_enabled', '');
    if(checkbox.is(':checked')) {
       $('#' + id + '-element').show();
    } else {
        $('#' + id + '-element').hide();
    }
    switchCapCriteria();
}

switchResponseFilter = function() {
    $('#response_filter_options').hide();
    if($('#delivery-response_filter').is(':checked')) {
        $('#response_filter_options').show();
    }
}