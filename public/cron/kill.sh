#!/bin/bash

DIRECTORY=$(cd `dirname $0` && pwd)

cd $DIRECTORY/..

max_age=120 # (seconds)
naughty=$(pgrep -f "php /home/organic/public_html/crm/public/cron/uploadData.php")
echo Found: $naughty
if [[ -n "$naughty" ]]; then # naughty is running
  age_in_seconds=$(echo "$(date +%s) - $(stat -c %X /proc/$naughty)" | bc)
  if [[ "$age_in_seconds" -ge "$max_age" ]]; then # naughty is too old!
    kill -s 9 "$naughty"
  fi
fi