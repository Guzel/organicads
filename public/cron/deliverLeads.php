<?php
/**
 * Deliver postpone leads
 * Run every minute
*/

if(empty($argv[1]) || $argv[1] !== '*runFromCommandLine*') {
    return;
}

define('DONT_RUN_APP', true);
//define('APPLICATION_ENV', 'development');

require(dirname(__FILE__) . '/../index.php');
$application->bootstrap();

//select Clients for Postpone delivery by time
$clientsModel = new Application_Model_DbTable_Clients();
$select = $clientsModel->getAdapter()->select()
    ->from('clients')
    ->join(new Zend_Db_Expr('(SELECT * FROM `leads` ORDER BY `leads`.`postpone_delivery` ASC)'), 't.client_id = clients.id', array('lead_id' => 't.id', 'lead_state' => 't.state'))
    ->joinLeft('client_cap', 'client_cap.client_id = clients.id', array('regularity', 'all_leads', 'states_options'))
    ->where("delivered IS NULL")
    ->group('clients.id');

$selectTimedLeads = clone $select;
$selectTimedLeads->where(new Zend_Db_Expr("(last_postpone_delivery + INTERVAL `postpone_delivery_interval` MINUTE <= NOW() OR `last_postpone_delivery` IS NULL) AND `postpone_delivery_interval` IS NOT NULL AND t.postpone_reason = 'time'"));
$timedClients = $clientsModel->getAdapter()->fetchAll($selectTimedLeads);

$selectCappedLeads = clone $select;
$selectCappedLeads->where(new Zend_Db_Expr("(last_capped_delivery + INTERVAL `capped_delivery_interval` MINUTE <= NOW() OR `last_capped_delivery` IS NULL) AND `capped_delivery_interval` IS NOT NULL AND t.postpone_reason = 'limit'"));
$cappedClients = $clientsModel->getAdapter()->fetchAll($selectCappedLeads);

$selectResponseErrorLeads = clone $select;
$selectResponseErrorLeads->where(new Zend_Db_Expr("t.postpone_reason = 'response_error'"));
$errorClients = $clientsModel->getAdapter()->fetchAll($selectResponseErrorLeads);

$clients = array_merge($timedClients, $cappedClients, $errorClients);

$leadsModel = new Application_Model_DbTable_Leads();
$leads = new Application_Model_Leads();
$leads->setClients($clients);

$deliveryUpdate = array();
foreach($clients as $client) {
    $client = (object)$client;
    $leads->state = $client->lead_state;
    $delivery = true;

    if(!$delivery = $leads->checkDeliveryTime($client)) {
        //postpone lead delivery
        $delivery = false;
        echo 'OFF DELIVERY!';
    } else {
        $cappedLeads = $leads->getCappedLeads($client);
        if($cappedLeads && $cappedLeads->capped >= $cappedLeads->limit) {
            //postpone lead delivery
            $delivery = false;
            echo 'CAPPED!';
        }
    }

    $lead = $leadsModel->find($client->lead_id)->current();
    var_dump($lead->postpone_reason);
    var_dump('CLIENT', $client->id, $delivery);
    if($delivery) {
        $leads->deliverLead($lead);
    } else {
        $lead->postpone_delivery = new Zend_Db_Expr('`postpone_delivery` + INTERVAL 1 HOUR');
        $lead->save();
    }

    $deliveryUpdate[$lead->postpone_reason][] = $client->id;
    $log = 'Date: ' . date('F j, Y H:i:s') . ' Client: ' . $client->id . ' Lead: ' . $lead->id . ' Delivedred: ' . (int)$delivery . "\r\n";
    file_put_contents('delivery_log', $log, FILE_APPEND);
}

if(!empty($deliveryUpdate['limit'])) {
    $clientsModel->update(array('last_capped_delivery' => new Zend_Db_Expr('NOW()')), array('id IN(?)' => $deliveryUpdate['limit']));
}
if(!empty($deliveryUpdate['time'])) {
    $clientsModel->update(array('last_postpone_delivery' => new Zend_Db_Expr('NOW()')), array('id IN(?)' => $deliveryUpdate['time']));
}