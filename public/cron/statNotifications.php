<?php
/**
 * Send Stat Notifications
 * Run at 00:00:00 every day
*/

if(empty($argv[1]) || $argv[1] !== '*runFromCommandLine*') {
    return;
}

define('DONT_RUN_APP', true);
define('APPLICATION_ENV', 'testing');

require(dirname(__FILE__) . '/../index.php');
$application->bootstrap();

$notificationsModel = new Application_Model_Notifications();
$notificationsModel->processStatNotifications();