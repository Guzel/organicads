<?php
/**
 * Upload SubId stats
 * Run every hour
*/

if(empty($argv[1]) || $argv[1] !== '*runFromCommandLine*') {
    return;
}
set_time_limit(150);

define('DONT_RUN_APP', true);

require(dirname(__FILE__) . '/../index.php');
$application->bootstrap();

if(date('i') == 0 || date('i') == 30) {
    sleep(60);
}

$affiliateModel = new Application_Model_DbTable_Affiliates();
$select  = $affiliateModel->select()
    ->where("last_subid_statistic_date IS NULL OR last_subid_statistic_date < NOW() - INTERVAL 30 MINUTE")
    ->where("source = 'linktrust'")
    ->limit(1);
$affiliates = $affiliateModel->fetchAll($select);

foreach($affiliates as $affiliate) {
    $decryptedCredentials = App_Utils::decryptString(base64_decode($affiliate->credentials));
    $credentials = unserialize($decryptedCredentials);

    $linkTrust = new Application_Model_LinkTrust($credentials);
    $linkTrust->importSubIdStatistics($affiliate);

    $affiliate->last_subid_statistic_date =  new Zend_Db_Expr('NOW()');
    $affiliate->save();
}