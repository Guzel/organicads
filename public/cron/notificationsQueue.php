<?php
/**
 * Notification Queue
 * Run every 15 minutes
*/

if(empty($argv[1]) || $argv[1] !== '*runFromCommandLine*') {
    return;
}

define('DONT_RUN_APP', true);

require(dirname(__FILE__) . '/../index.php');
$application->bootstrap();

$notificationQueueModel = new Application_Model_DbTable_NotificationsQueue();
$notifications = $notificationQueueModel->notificationsToSend();

foreach($notifications as $notification) {
    if($notification->email) {
        $subject = $notification->subject . ' Notification - Organicads';

        $mail = new App_Mail('new-notification', $subject);
        $mail->addTo($notification->email);
        $mail->assign('text', $notification->text);
        $mail->send();
    } else {
        $twilioModel = new Application_Model_Twilio();
        $smsId = $twilioModel->sendMessage($notification->phone, $notification->text, $notification->from);
        $notification->message_id = $smsId;
        sleep(1); //make a 1 second delay
    }
    $notification->sent = new Zend_Db_Expr('NOW()');
    $notification->save();
}