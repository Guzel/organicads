#!/bin/bash

DIRECTORY=$(cd `dirname $0` && pwd)

cd $DIRECTORY/..

i=$( pgrep -f "php /home/organic/public_html/crm/public/cron/syncRequests.php" | wc -l )
if [ $i -eq 0 ]; then
echo "Starting $0"
php /home/organic/public_html/crm/public/cron/syncRequests.php $1 $2
else
echo "$0 $i program already running\n";
fi