#!/bin/bash

DIRECTORY=$(cd `dirname $0` && pwd)

cd $DIRECTORY/..

max_age=300 # (seconds)
naughty=$(pgrep -f "php /home/organic/public_html/crm/public/cron/uploadData.php")
if [[ -n "$naughty" ]]; then # naughty is running
  age_in_seconds=$(echo "$(date +%s) - $(stat -c %X /proc/$naughty)" | bc)
  if [[ "$age_in_seconds" -ge "$max_age" ]]; then # naughty is too old!
    kill -s 9 "$naughty"
  fi
fi

i=$( pgrep -f "php /home/organic/public_html/crm/public/cron/uploadData.php" | wc -l )
if [ $i -eq 0 ]; then
echo "Starting $0"
php /home/organic/public_html/crm/public/cron/uploadData.php $1 $2
else
echo "$0 $i program already running\n";
fi