<?php
/**
 * Process Sync Requests by SMS
 * Run every minute
*/

if(empty($argv[1]) || $argv[1] !== '*runFromCommandLine*') {
    return;
}

define('DONT_RUN_APP', true);

require(dirname(__FILE__) . '/../index.php');
$application->bootstrap();

$notificationsModel = new Application_Model_Notifications();
$statistics = new Application_Model_Statistic();
$syncRequestsModel = new Application_Model_DbTable_StatisticSyncRequests();
$usersModel = new Application_Model_DbTable_Users();

$requests = $syncRequestsModel->getActiveRequests();

foreach($requests as $request) {
    $user = $usersModel->find($request->user_id)->current();
    $lastUpdate = new DateTime($user->last_update);
    if(!$user->last_update || ((time() - $lastUpdate->getTimestamp())/60 > 15)) {
        $statistics->update($request->user_id);
    }

    //process notifications:
    $queue = $notificationsModel->addSyncStatToQueue($request->user_id, $request->from, $request->to);
    $request->processed = new Zend_Db_Expr('NOW()');
    $request->queue_id = $queue->id;
    $request->save();

    $usersModel->save(array('last_update' => date('Y-m-d H:i:00')), $user->id);
}