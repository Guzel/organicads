<?php
/**
 * Upload new data
 * Run every hour
*/

if(empty($argv[1]) || $argv[1] !== '*runFromCommandLine*') {
    return;
}

set_time_limit(150);
define('DONT_RUN_APP', true);
//define('APPLICATION_ENV', 'testing');

require(dirname(__FILE__) . '/../index.php');
$application->bootstrap();

if(date('i') == 0 || date('i') == 30) {
    sleep(60);
}

$usersModel = new Application_Model_DbTable_Users();
$users = $usersModel->getUsersForStatUpdate();
if(!$users) {
    die();
}

$updateDate = date('Y-m-d H:i:00');
$statistics = new Application_Model_Statistic();
$notificationsModel = new Application_Model_Notifications();

$user = current($users);
$lastUpdate = $user['last_update'];

$usersModel->save(array('last_update' => $updateDate), $user['id']);
$statistics->update($user['id']);

//Dily Stats Notification
if($lastUpdate){
    $today = new DateTime();
    $lastUpdate = new DateTime($lastUpdate);
    if($lastUpdate->format('d') != $today->format('d') && (int)$today->format('H') == 0) {
        $notificationsModel->processNotifications('stat', $user['id']);
    }
}

//process notifications:
$notificationsModel->processNotifications('auto', $user['id']); //send auto stat after each update, if requested
$notificationsModel->processNotifications(array('profit', 'roi'), $user['id']);