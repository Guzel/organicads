<?php

class Application_Form_AccountConnect extends Application_Form_Main
{
    protected $_exportResources = array();

    public function init()
    {
        $this->setAttrib('id', 'account-connect-form');

        $selectApi = new Zend_Form_SubForm();

        $accountTypeOptions = array('affiliate' => 'Affiliate Network', 
            'ad' => 'Ad Platform');

        $accountType = $this->createElement('select', 'account_type')
            ->setLabel('Account type')
            ->setRequired(true) 
            ->addMultiOptions($accountTypeOptions);
        $selectApi->addElement($accountType);

        $resource = $this->createElement('select', 'resource')
            ->setLabel('Select resource')
            ->setRequired(true) 
            ->addMultiOptions($this->_exportResources);
        $selectApi->addElement($resource);

        $this->addSubForm($selectApi, 'selectapi');

        //Link Trust Credentials SubForm
        $linkTrust = new Zend_Form_SubForm();
        $linkTrust->setAttrib('class', 'connection_credentials');

        $apiUserId = $this->createElement('text', 'api_user_id')
            ->setLabel('API User ID')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('Digits')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $linkTrust->addElement($apiUserId);

        $apiKey = $this->createElement('text', 'api_key')
            ->setLabel('API Key')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 500));
        $linkTrust->addElement($apiKey);

        $this->addSubForm($linkTrust, 'linktrust');

        //Leadnomics Credentials SubForm
        $leadnomics = new Zend_Form_SubForm();
        $leadnomics->setAttrib('class', 'connection_credentials');

        $leadnomics->addElement($this->getEmailElement());
        $leadnomics->addElement($this->getPasswordElement());

        $this->addSubForm($leadnomics, 'leadnomics');

        //Cakemarketing Credentials SubForm
        $cakemarketing = new Zend_Form_SubForm();
        $cakemarketing->setAttrib('class', 'connection_credentials');

        $username = $this->createElement('text', 'username')
            ->setLabel('Username')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $cakemarketing->addElement($username);

        $affiliateId = $this->createElement('text', 'api_affiliate_id')
            ->setLabel('Affiliate ID')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('Digits')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $cakemarketing->addElement($affiliateId);

        $apiKey = $this->createElement('text', 'api_key')
            ->setLabel('API Key')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 500));
        $cakemarketing->addElement($apiKey);

        $domain = $this->createElement('text', 'domain')
            ->setLabel('API Domain')
            ->setRequired(true)
            ->setAttrib('class', 'required url')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $cakemarketing->addElement($domain);

        $this->addSubForm($cakemarketing, 'cakemarketing');

        //BingoRoyalty Credentials SubForm
        $bingoroyalty = new Zend_Form_SubForm();
        $bingoroyalty->setAttrib('class', 'connection_credentials');

        $bingoroyalty->addElement($this->getEmailElement());
        $bingoroyalty->addElement($this->getPasswordElement());

        $this->addSubForm($bingoroyalty, 'bingoroyalty');

        //HasOffers SubForm
        $hasoffers = new Zend_Form_SubForm();
        $hasoffers->setAttrib('class', 'connection_credentials');

        $hasoffersApiKey = $this->createElement('text', 'api_key')
            ->setLabel('API Key')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 500));
        $hasoffers->addElement($hasoffersApiKey);
        $this->addSubForm($hasoffers, 'hasoffers');

        $hasoffersNetworkId = $this->createElement('text', 'network_id')
            ->setLabel('Network ID')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 500));
        $hasoffers->addElement($hasoffersNetworkId);
        $this->addSubForm($hasoffers, 'hasoffers');

        /* Advertiser Account Connection */
        //Facebook SubForm
        $facebook = new Zend_Form_SubForm();
        $facebook->setAttrib('class', 'connection_credentials');

        $facebook->addElement($this->getEmailElement());
        $facebook->addElement($this->getPasswordElement());

        $ips = $this->createElement('select', 'ip')
            ->setLabel('IP Address')
            ->addMultiOptions(array('' => 'Select') + App_Utils::getServerIpList());
        $facebook->addElement($ips);

        $this->addSubForm($facebook, 'facebook');

        //Google AdWords SubForm
        $adwords = new Zend_Form_SubForm();
        $adwords->setAttrib('class', 'connection_credentials');

        $adwords->addElement($this->getEmailElement());

        /*$adwordsClientId = $this->createElement('text', 'client_id')
            ->setLabel('Client Id')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $adwords->addElement($adwordsClientId);

        $adwordsClientSecret = $this->createElement('text', 'client_secret')
            ->setLabel('Client Secret')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $adwords->addElement($adwordsClientSecret);

        $adwordsDevToken = $this->createElement('text', 'dev_token')
            ->setLabel('Developer Token')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 500));
        $adwords->addElement($adwordsDevToken);*/

        $adwordsGoogle = $this->createElement('hidden', 'google');
        $adwords->addElement($adwordsGoogle);

        $this->addSubForm($adwords, 'google');

        //Bing SubForm
        $bing = new Zend_Form_SubForm();
        $bing->setAttrib('class', 'connection_credentials');

        $bingUsername = $this->createElement('text', 'username')
            ->setLabel('Username')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $bing->addElement($bingUsername);

        $bing->addElement($this->getPasswordElement());

        $bingToken = $this->createElement('text', 'token')
            ->setLabel('Token')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 500));
        $bing->addElement($bingToken);

        $this->addSubForm($bing, 'bing');

        //50onRed SubForm
        $fiftyonred = new Zend_Form_SubForm();
        $fiftyonred->setAttrib('class', 'connection_credentials');

        $fiftyonredAccountName = $this->createElement('text', 'account_name')
            ->setLabel('Account Nickname')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $fiftyonred->addElement($fiftyonredAccountName);

        $fiftyonredApiKey = $this->createElement('text', 'api_key')
            ->setLabel('API Key')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $fiftyonred->addElement($fiftyonredApiKey);

        $fiftyonredSecretKey = $this->createElement('text', 'secret_key')
            ->setLabel('Secret Key')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 500));
        $fiftyonred->addElement($fiftyonredSecretKey);

        $this->addSubForm($fiftyonred, 'fiftyonred');

        //Yahoo Streamads SubForm
        $yahoo = new Zend_Form_SubForm();
        $yahoo->setAttrib('class', 'connection_credentials');

        $yahooLogin = $this->createElement('text', 'login')
            ->setLabel('Login')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $yahoo->addElement($yahooLogin);

        $yahoo->addElement($this->getPasswordElement());

        $this->addSubForm($yahoo, 'yahoo');

        //Traffic Vance SubForm
        $trafficvance = new Zend_Form_SubForm();
        $trafficvance->setAttrib('class', 'connection_credentials');

        $trafficvanceApiKey = $this->createElement('text', 'api_key')
            ->setLabel('API Key')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 500));
        $trafficvance->addElement($trafficvanceApiKey);
        $this->addSubForm($trafficvance, 'trafficvance');

        //Lead Impact SubForm
        $leadimpact = new Zend_Form_SubForm();
        $leadimpact->setAttrib('class', 'connection_credentials');

        $leadimpact->addElement($this->getEmailElement());
        $leadimpact->addElement($this->getPasswordElement());
        $this->addSubForm($leadimpact, 'leadimpact');

        //Hastraffic Credentials SubForm
        $hastraffic = new Zend_Form_SubForm();
        $hastraffic->setAttrib('class', 'connection_credentials');

        $hastraffic->addElement($this->getEmailElement());
        $hastraffic->addElement($this->getPasswordElement());

        $this->addSubForm($hastraffic, 'hastraffic');

        //Zeropark Credentials SubForm
        $zeropark = new Zend_Form_SubForm();
        $zeropark->setAttrib('class', 'connection_credentials');

        $zeropark->addElement($this->getEmailElement());
        $zeropark->addElement($this->getPasswordElement());

        $this->addSubForm($zeropark, 'zeropark');

        //Airpush SubForm
        $airpush = new Zend_Form_SubForm();
        $airpush->setAttrib('class', 'connection_credentials');

        $airpush->addElement($this->getEmailElement());

        $airpushApiKey = $this->createElement('text', 'api_key')
            ->setLabel('API Key')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 500));
        $airpush->addElement($airpushApiKey);
        $this->addSubForm($airpush, 'airpush');

        $this->addElement('submit', 'submit', array('label' => 'Connect'));
    }

    public function setResources($resources)
    {
        foreach($resources as $resource) {
            $this->_exportResources = array_merge($this->_exportResources, $resource);
        }
    }
}