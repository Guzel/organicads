<?php

class Application_Form_ResetPassword extends Application_Form_Main
{
    public function init()
    {
        $password = $this->getPasswordElement()
            ->setLabel('')
            ->setAttrib('class', 'required form-control')
            ->setAttrib('placeholder', 'Password');
        $this->addElement($password);
        $this->password->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $confirmPassword = $this->getPasswordElement()->setName('confirmPassword')
            ->setLabel('')
            ->addValidator('identical', false, array('token' => 'password'))
            ->setAttrib('class', 'required form-control')
            ->setAttrib('placeholder', 'Confirm Password')
            ->setAttrib('equalTo', '#password');
        $this->addElement($confirmPassword);
        $this->confirmPassword->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $this->addElement('submit', 'submit', array('label' => 'Save'));
    }
}