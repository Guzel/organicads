<?php

class Application_Form_CreateNotification extends Application_Form_Main
{
    protected $_campaigns;
    protected $_contacts;

    public function init()
    {
        $this->setAttrib('id', 'create-notification-form');
        $notificationTypes = array('stat' => 'Stats', 'profit' => 'Profit/Loss', 'roi' => 'ROI');

        $general = new Zend_Form_SubForm();
        $notificationName = $this->createElement('text', 'name')
            ->setLabel('Name')
            ->setRequired(true)
            ->setAttribs(array('class' => 'required'))
            ->addFilter('StringTrim')
            ->addValidator('Alnum', false, array('allowWhiteSpace' => true))
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $general->addElement($notificationName);

        $notificationContact = $this->createElement('Multiselect', 'contact_id')
            ->setLabel('Send To')
            ->setRequired(true) 
            ->addMultiOptions($this->_contacts)
            ->setAttrib('class', 'chosen-select required')
            ->setAttrib('data-placeholder', 'Select Contacts');
        $general->addElement($notificationContact);

        $notificationType = $this->createElement('select', 'type')
            ->setLabel('Type')
            ->setRequired(true) 
            ->addMultiOptions($notificationTypes)
            ->setAttrib('class', 'required');
        $general->addElement($notificationType);

        $this->addSubForm($general, 'general');

        $statNotification = new Zend_Form_SubForm();
        $statNotification->setAttrib('class', 'notification_settings');

        $frequencyOptions = array('day' => 'Daily', 'week' => 'Weekly', 'month' => 'Monthly');
        $notificationFrequency = $this->createElement('select', 'frequency')
            ->setLabel('Frequency')
            ->setRequired(true) 
            ->addMultiOptions($frequencyOptions)
            ->setAttrib('class', 'required');;
        $statNotification->addElement($notificationFrequency);

        $this->addSubForm($statNotification, 'stat');


        $campaignsOptions = array('All Campaigns') + $this->_campaigns;

        $profitNotification = new Zend_Form_SubForm();
        $profitNotification->setAttrib('class', 'notification_settings');

        $notificationCampaign = $this->createElement('select', 'campaign_id')
            ->setLabel('Select Campaign')
            ->setRequired(true) 
            ->addMultiOptions($campaignsOptions)
            ->setAttrib('class', 'chosen-select required');
        $profitNotification->addElement($notificationCampaign);

        $threshold = $this->createElement('text', 'threshold')
            ->setLabel('If profit goes below')
            ->setRequired(true)
            ->setAttribs(array('class' => 'required'))
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $profitNotification->addElement($threshold);

        $this->addSubForm($profitNotification, 'profit');

        $roiNotification = new Zend_Form_SubForm();
        $roiNotification->setAttrib('class', 'notification_settings');

        $notificationCampaign = $this->createElement('select', 'campaign_id')
            ->setLabel('Select Campaign')
            ->setRequired(true) 
            ->addMultiOptions($campaignsOptions)
            ->setAttrib('class', 'chosen-select required');
        $roiNotification->addElement($notificationCampaign);

        $threshold = $this->createElement('text', 'threshold')
            ->setLabel('If ROI falls below')
            ->setRequired(true)
            ->setAttribs(array('class' => 'required'))
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $roiNotification->addElement($threshold);

        $this->addSubForm($roiNotification, 'roi');

        $this->addElement('submit', 'submit', array('label' => 'Save'));
    }

    public function setCampaigns($campaigns)
    {
        $this->_campaigns = $campaigns;
    }

    public function setContacts($contacts)
    {
        $this->_contacts = $contacts;
    }

    public function populate(array $values)
    {
        $notificationContactsModel = new Application_Model_DbTable_NotificationContacts();
        $contacts = $notificationContactsModel->getContacts($values['user_id'], $values['id']);
        $values['contact_id'] = array_keys($contacts);

        parent::populate($values);
    }
}