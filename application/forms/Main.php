<?php

class Application_Form_Main extends Twitter_Form
{
    public function getNameElement()
    {
        return $this->createElement('text', 'first_name')
            ->setLabel('First name')
            ->setRequired(true)
            ->setAttribs(array('class' => 'required', 'data-rule-maxlength' => 250))
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
    }

    public function getPasswordElement()
    {
        return $this->createElement('password', 'password')
            ->setLabel('Password')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250))
            ->addFilter('StringTrim');
    }

    public function getEmailElement()
    {
        return $this->createElement('text', 'email')
            ->setLabel('Email')
            ->addFilter('StringTrim')
            ->setRequired(true)
            ->setAttrib('class', 'required email')
            ->addValidator('EmailAddress')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
    }

    public function getPhoneElement()
    {
        $regex = new Zend_Validate_Regex('/[0-9 -]+/');
        $regex->setMessages(array(
            Zend_Validate_Regex::NOT_MATCH => 'Format is incorrect',
            Zend_Validate_Regex::INVALID   => 'Format is incorrect',
            Zend_Validate_Regex::ERROROUS  => 'Format is incorrect'
        ));

        return $this->createElement('text', 'phone')
            ->setLabel('Phone')
            ->setRequired(true)
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 15))
            ->addValidator($regex)
            ->setAttrib('class', 'required phoneUS');
    }
}