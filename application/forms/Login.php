<?php

class Application_Form_Login extends Application_Form_Main
{
    public function init()
    {
        $this->setAttrib('id', 'login-form');

        $email = $this->createElement('text', 'email')
            ->setRequired(true)
            ->setAttrib('class', 'required email form-control')
            ->setAttrib('placeholder', 'Email')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $this->addElement($email);
        $this->email->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $password = $this->getPasswordElement();
        $password->setAttrib('class', 'required form-control')->setLabel('')
            ->setAttrib('placeholder', 'Password');
        $this->addElement($password);
        $this->password->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $this->addElement('submit', 'submit', array('label' => 'Login'));
    }
}