<?php

class Application_Form_ForgotPassword extends Application_Form_Main
{
    public function init()
    {
        $email = $this->getEmailElement()->setLabel('')
            ->setAttrib('class', 'required email form-control')
            ->setAttrib('placeholder', 'Email')
            ->addValidator(
                new Zend_Validate_Db_RecordExists(array(
                'table' => 'users',
                'field' => 'email'
            )));
        $this->addElement($email);
        $this->email->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $this->addElement('submit', 'submit', array('label' => 'Send Me!'));
    }
}