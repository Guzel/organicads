<?php

class Application_Form_EditUser extends Application_Form_Main
{
    public function init()
    {
        $this->setAttrib('id', 'edituser-form');

        $aclConfig = Zend_Registry::get('acl');

        $role = $this->createElement('select', 'role')
            ->setLabel('Role')
            ->setRequired(true) 
            ->addMultiOptions($aclConfig->acl->public_roles->toArray());
        $this->addElement($role);

        $username = $this->createElement('text', 'username')
            ->setLabel('Username')
            ->addFilter('StringTrim')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addValidator('Alnum')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250))
            ->addValidator(
                new Zend_Validate_Db_NoRecordExists(array(
                'table' => 'users',
                'field' => 'username'
            )));
        $this->addElement($username);

        $email = $this->getEmailElement()
            ->addValidator(
                new Zend_Validate_Db_NoRecordExists(array(
                'table' => 'users',
                'field' => 'email'
            )));
        $this->addElement($email);

        $this->addElement($this->getPasswordElement()->setName('user_password'));

        $name = $this->createElement('text', 'name')
            ->setLabel('Name')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addFilter('StringTrim')
            ->addValidator('Alnum', false, array('allowWhiteSpace' => true))
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $this->addElement($name);

        $this->addElement('submit', 'submit', array('Label' => 'Save'));
    }

    public function populate(array $values) {
        parent::populate($values);

        $this->user_password->setRequired(false)->setAttrib('class', '');
        $this->username->getValidator('NoRecordExists')
                ->setExclude("username != '{$values['username']}'");
        $this->email->getValidator('NoRecordExists')
                ->setExclude("email != '{$values['email']}'");
    }
}