<?php

class Application_Form_CreateForm extends Application_Form_Main
{
    public function init()
    {
        $formTitle = $this->createElement('text', 'form_title')
            ->setRequired(true)
            ->setAttribs(array('class' => 'required', 'placeholder' => 'Form Title'))
            ->addFilter('StringTrim')
            ->addValidator('Alnum', false, array('allowWhiteSpace' => true))
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $this->addElement($formTitle);

        $redirectPage = $this->createElement('text', 'redirect_page')
            ->setRequired(true)
            ->setAttribs(array('class' => 'required url', 'placeholder' => 'Thank you redirect page'))
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $this->addElement($redirectPage);

        $user = Zend_Registry::get('user');
        $fieldsModel = new Application_Model_DbTable_Fields();
        $fieldsOptions = $fieldsModel->getPairs($user->id);

        $formFields = $this->createElement('Multiselect', 'form_fields')
            ->addMultiOptions($fieldsOptions)
            ->setAttrib('class', 'chosen-select required');
        $this->addElement($formFields);

        $this->addElement('hidden', 'form_id');
    }

    public function populate(array $values) {
        $populateArray = $values;
        $populateArray['form_title'] = $values['title'];
        $populateArray['form_fields'] = unserialize($values['fields']);
        $populateArray['form_id'] = $values['id'];

        parent::populate($populateArray);
    }
}