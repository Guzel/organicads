<?php

class Application_Form_Profile extends Application_Form_Main
{
    public function init()
    {
        //Basic settings SubForm
        $basicSubForm = new Zend_Form_SubForm();

        $firstName = $this->createElement('text', 'name')
            ->setLabel('Name')
            ->setRequired(true)
            ->setAttribs(array('class' => 'required', 'placeholder' => 'Fist Name Last Name'))
            ->addFilter('StringTrim')
            ->addValidator('Alnum', false, array('allowWhiteSpace' => true))
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $basicSubForm->addElement($firstName);

        $email = $this->getEmailElement()
            ->addValidator(
                new Zend_Validate_Db_NoRecordExists(array(
                'table' => 'users',
                'field' => 'email'
            )));
        $basicSubForm->addElement($email);

        $regex = new Zend_Validate_Regex('/[0-9 ]+/');
        $regex->setMessages(array(
            Zend_Validate_Regex::NOT_MATCH => 'Format is incorrect',
            Zend_Validate_Regex::INVALID   => 'Format is incorrect',
            Zend_Validate_Regex::ERROROUS  => 'Format is incorrect'
        ));

        $phone = $this->createElement('text', 'phone')
            ->setLabel('Phone')
            ->setRequired(true)
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 15))
            ->addValidator($regex)
            ->setAttrib('class', 'required');
        $basicSubForm->addElement($phone);

        $zip = $this->createElement('text', 'zip')
            ->setLabel('ZIP')
            ->setRequired(true)
            ->addFilter('StringTrim')
            ->addValidator('Digits')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 5))
            ->setAttrib('class', 'required');
        $basicSubForm->addElement($zip);

        $address = $this->createElement('text', 'address')
            ->setLabel('Address')
            ->setRequired(true)
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250))
            ->setAttrib('class', 'required');
        $basicSubForm->addElement($address);

        $city = $this->createElement('text', 'city')
            ->setLabel('City')
            ->setRequired(true)
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250))
            ->setAttrib('class', 'required');
        $basicSubForm->addElement($city);

        $state = $this->createElement('text', 'state')
            ->setLabel('State')
            ->setRequired(true)
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250))
            ->setAttrib('class', 'required');
        $basicSubForm->addElement($state);

        $this->addSubForm($basicSubForm, 'edit-basic');

        //Settings SubForm
        $settingsSubForm = new Zend_Form_SubForm();

        $settingsSubForm->addElement('hidden', 'theme');

        $defaultPagesOptions = array(
            '/' => 'Home',
            '/accounting/summary' => 'Dashboard',
            '/accounting/campaigns' => 'Accounting > Campaigns',
            '/accounting/overview' => 'Accounting > Overview',
            '/accounting/recap' => 'Accounting > Recap',
            '/crm/overview' => 'CRM > Overview',
            '/user/profile' => 'User Profile'
        );

        $defaulPages = $this->createElement('select', 'default_page')
            ->setLabel('Default page after login')
            ->addMultiOptions($defaultPagesOptions);
        $settingsSubForm->addElement($defaulPages);

        $logoLink = $this->createElement('select', 'logo_link')
            ->setLabel('Logo link to')
            ->addMultiOptions($defaultPagesOptions);
        $settingsSubForm->addElement($logoLink);

        $notificationsSettings = $this->createElement('checkbox', 'show_notifications')
            ->setLabel('Show notifications');
        $settingsSubForm->addElement($notificationsSettings);

        $statAutoUpdateSettings = $this->createElement('checkbox', 'auto_stat_update')
            ->setLabel('Automatic stat loading');
        $settingsSubForm->addElement($statAutoUpdateSettings);

        $updateInterval = $this->createElement('text', 'stat_update_interval')
            ->setLabel('Update every (minutes)')
            ->setAttrib('readonly', 'true')
            ->setValue(60);
        $settingsSubForm->addElement($updateInterval);

        $loginStatUpdateSettings = $this->createElement('checkbox', 'login_stat_update')
            ->setLabel('Stat loading after login');
        $settingsSubForm->addElement($loginStatUpdateSettings);

        $this->addSubForm($settingsSubForm, 'edit-settings');

        //Password settings SubForm
        $passwordSubForm = new Zend_Form_SubForm();

        $currentPassword = $this->getPasswordElement()
            ->setName('current_password')->setLabel('Current Password');
        $passwordSubForm->addElement($currentPassword);

        $newPassword = $this->getPasswordElement()
            ->setName('new_password')->setLabel('New Password');
        $passwordSubForm->addElement($newPassword);

        $confirmPassword = $this->getPasswordElement()
            ->setName('confirm_password')->setLabel('Confirm Password')
            ->addValidator('identical', false, array('token' => 'new_password'))
            ->setAttrib('equalTo', '#new_password');
        $passwordSubForm->addElement($confirmPassword);


        $this->addSubForm($passwordSubForm, 'edit-password');

        $this->addElement('hidden', 'form_id');
        $this->addElement('submit', 'submit', array('Label' => 'Save'));
    }

    public function populate(array $values) {
        parent::populate($values);

        $this->getSubForm('edit-basic')->email->getValidator('NoRecordExists')
                ->setExclude("email != '{$values['email']}'");

        //Remove page from Default if user doesn't have access to it
        $defaultOptions = $this->getSubForm('edit-settings')->default_page->getMultiOptions();
        foreach($defaultOptions as $key => $option) {
            if($values['role'] == Application_Model_DbTable_Users::USER_CRM 
                    && (false !== strpos($key, 'accounting'))) {
                unset($defaultOptions[$key]);
            }
            if($values['role'] == Application_Model_DbTable_Users::USER_ACC 
                    && (false !== strpos($key, 'crm'))) {
                unset($defaultOptions[$key]);
            }
        }
        $defaultOptions = $this->getSubForm('edit-settings')->default_page->setMultiOptions($defaultOptions);
    }
}