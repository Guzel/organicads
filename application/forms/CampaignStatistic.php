<?php

class Application_Form_CampaignStatistic extends Application_Form_Main
{
    public function init()
    {
        $this->setAttrib('id', 'campaign-stat-form');

        $user = Zend_Registry::get('user');

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $options = array('' => 'Select Campaign') + $campaignsModel->getPairs($user->id);

        $campaign = $this->createElement('select', 'campaign')
            ->setRequired(true)
            ->setMultiOptions($options)
            ->setAttrib('class', 'chosen-select required')
            ->setAttrib('data-placeholder', 'Select Campaign');
        $this->addElement($campaign);
        $this->campaign->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $dateFrom = $this->createElement('hidden', 'date_from')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            //->setRequired(true)
            ->setAttrib('class', 'dateSelect');
        $this->addElement($dateFrom);
        $this->date_from->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $dateTo = $this->createElement('hidden', 'date_to')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            //->setRequired(true)
            ->setAttrib('class', 'dateSelect');
        $this->addElement($dateTo);
        $this->date_to->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $this->addElement('submit', 'Show', array('Label' => 'Show'));
    }
}