<?php

class Application_Form_CampaignPairing extends Application_Form_Main
{
    public function init()
    {
        $campaignId = $this->createElement('hidden', 'id')
                ->setAttrib('id', 'campaign_id');
        $this->addElement($campaignId);

        $campaign = $this->createElement('text', 'name')
            ->setLabel('Campaign Name')
            ->setRequired(true)
            ->setAttrib('class', 'required')
            ->addValidator(
                new Zend_Validate_Db_NoRecordExists(array(
                'table' => 'campaigns',
                'field' => 'name'
            )));
        $this->addElement($campaign);

        $nickname = $this->createElement('text', 'nickname')
            ->setLabel('Nickname')
            ->addFilter('StringTrim')
            ->setAttribs(array('data-rule-maxlength' => 250))
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $this->addElement($nickname);

        $affiliateCampaigns = $this->createElement('hidden', 'affiliate_campaigns');
        $this->addElement($affiliateCampaigns);

        $adCampaigns = $this->createElement('hidden', 'ad_campaigns');
        $this->addElement($adCampaigns);

        $keyword = $this->createElement('text', 'keyword')
            ->setLabel('Pairing Code')
            ->setAttribs(array('data-rule-maxlength' => 250))
            ->addValidator(
                new Zend_Validate_Db_NoRecordExists(array(
                'table' => 'campaigns',
                'field' => 'keyword'
            )));
        $this->addElement($keyword);

        $fbEnable = $this->createElement('checkbox', 'fb_enable')
            ->setLabel('Enable Facebook Ad Pairing');
        $this->addElement($fbEnable);
    }

    public function populate(array $values) {
        parent::populate($values);

        $this->name->setAttrib('readonly', 'true');
        $this->name->getValidator('NoRecordExists')->setExclude("id != {$values['id']}");
        $this->keyword->getValidator('NoRecordExists')->setExclude("id != {$values['id']}");
    }
}