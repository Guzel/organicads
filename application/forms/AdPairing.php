<?php

class Application_Form_AdPairing extends Application_Form_Main
{
    public function init()
    {
        $user = Zend_Registry::get('user');

        $options = array('' => 'Select Campaign');

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        if($campaigns = $campaignsModel->getPairs($user->id)) {
            $options += array_combine($campaigns, $campaigns);
        }

        $campaign = $this->createElement('select', 'campaign')
            ->setLabel('Campaign')
            ->setRequired(true)
            ->setMultiOptions($options)
            ->setAttrib('class', 'chosen-select required')
            ->setAttrib('data-placeholder', 'Select/Add Campaign');
        $this->addElement($campaign);

        $affiliateCampaigns = $this->createElement('hidden', 'affiliate_campaigns');
        $this->addElement($affiliateCampaigns);

        $adCampaigns = $this->createElement('hidden', 'ad_campaigns');
        $this->addElement($adCampaigns);

        $advertlist = $this->createElement('Multiselect', 'advertslist')
                ->setRegisterInArrayValidator(false);
        $this->addElement($advertlist);
    }
}