<?php

class Application_Form_CreateFilter extends Twitter_Form_SubForm
{
    public function init()
    {
        $this->setAttrib('class', 'filter');

        $searchDataType = $this->createElement('select', 'search_data_type')
            ->setLabel('Type')
            ->addMultiOptions(array('require' => 'Require', 'exclude' => 'Exclude'))
            ->setAttrib('class', 'required');
        $this->addElement($searchDataType);

        $fieldList = $this->createElement('select', 'field_id')
            ->setLabel('Field')
            ->addMultiOptions(array())
            ->setAttrib('class', 'required');
        $this->addElement($fieldList);

        $criteriaOptions = array('begins_with' => 'Text begins With',
            'contains' => 'Text Contains',
            'equal' => 'Equal To',
            'greater' => 'Greater Than',
            'less' => 'Less Than',
            'not_equal' => 'Not Equal',
            'between' => 'Number Between');

        $criteria = $this->createElement('select', 'criteria')
            ->setLabel('Where')
            ->addMultiOptions($criteriaOptions)
            ->setAttrib('class', 'required');
        $this->addElement($criteria);

        $searchValue = $this->createElement('text', 'search_value')
            ->setLabel('Value')
            ->setAttribs(array('class' => 'required default'))
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 250));
        $this->addElement($searchValue);
    }
}