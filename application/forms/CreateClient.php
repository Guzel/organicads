<?php

class Application_Form_CreateClient extends Application_Form_Main
{
    public function init()
    {
        $this->setAttrib('id', 'create-client-form');

        $clientGeneral = new Zend_Form_SubForm();

        $clientGeneral->addElement($this->getNameElement());

        $lastName = $this->getNameElement()->setName('last_name')->setLabel('Last name');
        $clientGeneral->addElement($lastName);

        $displayName = $this->getNameElement()->setName('display_name')->setLabel('Display Name');
        $clientGeneral->addElement($displayName);

        $clientGeneral->addElement($this->getEmailElement());

        $clientGeneral->addElement($this->getPhoneElement());
        $clientGeneral->addElement('hidden', 'id');

        $timezone = $this->createElement('select', 'timezone')
            ->setLabel('Timezone')
            ->setRequired(true)
            ->addMultiOptions(App_Utils::getTimezones())
            ->setAttrib('class', 'chosen-select required');
        $clientGeneral->addElement($timezone);

        $user = Zend_Registry::get('user');
        $formsModel = new Application_Model_DbTable_Forms();

        $formId = $this->createElement('select', 'form_id')
            ->setLabel('Form')
            ->setRequired(true)
            ->addMultiOptions($formsModel->getPairs($user->id))
            ->setAttrib('class', 'chosen-select required');
        $clientGeneral->addElement($formId);

        $this->addSubForm($clientGeneral, 'client_general');


        //Step 2: Create Filters
        $fieldsModel = new Application_Model_DbTable_Fields();

        $searchDataFilter = new Application_Form_CreateFilter();
        $searchValueMin = $this->createElement('text', 'search_value_min')
            ->setLabel('Minimum')
            ->setAttribs(array('class' => 'required between'))
            ->addFilter('StringTrim');
        $searchDataFilter->addElement($searchValueMin);

        $searchValueMax = $this->createElement('text', 'search_value_max')
            ->setLabel('Maximum')
            ->setAttribs(array('class' => 'required between'))
            ->addFilter('StringTrim');
        $searchDataFilter->addElement($searchValueMax);
        $this->addSubForm($searchDataFilter, 'search_data_filter');

        $zipFilter = new Application_Form_CreateFilter();

        $zipFilter->search_data_type->setAttrib('class', 'hide')->removeDecorator('Label')->setValue('require')
            ->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $zipId = $fieldsModel->idByName('zip');
        $zipFilter->field_id->addMultiOptions(array($zipId => 'Zip'))
            ->setAttrib('class', 'hide')->removeDecorator('Label')
            ->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $zipFilter->criteria->setValue('contains')
            ->setAttrib('class', 'hide')->removeDecorator('Label')
            ->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $searchValue = $this->createElement('textarea', 'search_value')
            ->setLabel('')
            ->setAttribs(array('class' => 'required', 'rows' => 5))
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 2500));
        $zipFilter->addElement($searchValue);

        $this->addSubForm($zipFilter, 'zip_filter');


        $stateFilter = new Application_Form_CreateFilter();

        $stateFilter->search_data_type->setAttrib('class', 'hide')->removeDecorator('Label')->setValue('require')
            ->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $stateId = $fieldsModel->idByName('state');
        $stateFilter->field_id->addMultiOptions(array($stateId => 'State'))
            ->setAttrib('class', 'hide')->removeDecorator('Label')
            ->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $stateFilter->criteria->setValue('contains')
            ->setAttrib('class', 'hide')->removeDecorator('Label')
            ->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $statesModel = new Application_Model_DbTable_States();
        $searchValue = $this->createElement('Multiselect', 'search_value')
            ->addMultiOptions($statesModel->getPairs())
            ->setAttrib('class', 'chosen-select required')
            ->setAttrib('data-placeholder', 'Select States');
        $stateFilter->addElement($searchValue);

        $this->addSubForm($stateFilter, 'state_filter');


        //Step 3: Caps options / Timed Delivery options
        $enableCap = $this->createElement('checkbox', 'cap_enabled')
            ->setLabel('Enable Caps')
            ->setAttrib('class', 'cap_delivery_options');
        $this->addElement($enableCap);

        $enableTimedDelivery = $this->createElement('checkbox', 'timed_delivery_enabled')
            ->setLabel('Enable Timed Delivery')
            ->setAttrib('class', 'cap_delivery_options');
        $this->addElement($enableTimedDelivery);

        $capOptions = new Twitter_Form_SubForm();

        $cap = $this->createElement('select', 'regularity')
            ->setLabel('Cap leads')
            ->setRequired(true)
            ->addMultiOptions(array('day' => 'Daily', 'week' => 'Weekly', 'month' => 'Monthly'))
            ->setAttrib('class', 'required');
        $capOptions->addElement($cap);

        $capCriteria = $this->createElement('select', 'criteria')
            ->setLabel('Cap by')
            ->setRequired(true)
            ->addMultiOptions(array('all' => 'All leads', 'state' => 'State'))
            ->setAttrib('class', 'required');
        $capOptions->addElement($capCriteria);

        $capAmount = $this->createElement('text', 'all')
            ->setLabel('Amount')
            ->setAttrib('class', 'required');
        $capOptions->addElement($capAmount);

        $states = $this->createElement('MultiCheckbox', 'states')
            ->setRegisterInArrayValidator(false);
        $capOptions->addElement($states);

        $cappedDelivery = $this->createElement('checkbox', 'capped_delivery')
            ->setLabel('If cap is hit, deliver extra leads the next available delivery time using an interval of')
            ->setAttribs(array('style' => 'position: relative'));
        $capOptions->addElement($cappedDelivery);
        //$capOptions->capped_delivery->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $cappedDeliveryInterval = $this->createElement('text', 'capped_delivery_interval')
            ->setAttrib('class', 'input-mini')
            ->setAttrib('readonly', 'true')
            ->setValue(15);
        $capOptions->addElement($cappedDeliveryInterval);
        $capOptions->capped_delivery_interval->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $this->addSubForm($capOptions, 'cap');


        $timedDeliveryOptions = new Twitter_Form_SubForm();

        $weekDays = $this->createElement('multicheckbox', 'weekdays')
            ->addMultiOptions(array('1' => 'Monday', '2' => 'Tuesday', '3' => 'Wednesday', '4' => 'Thursday', '5' => 'Friday', '6' => 'Saturday', '7' => 'Sunday'))
            ->setAttrib('style', 'position:relative');
        $timedDeliveryOptions->addElement($weekDays);

        $beginDeliveryTime = $this->createElement('text', 'begin')
            ->setRequired(true)
            ->setAttrib('class', 'form-control');
        $timedDeliveryOptions->addElement($beginDeliveryTime);
        $timedDeliveryOptions->begin->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $stopDeliveryTime = $this->createElement('text', 'stop')
            ->setRequired(true)
            ->setAttrib('class', 'form-control');
        $timedDeliveryOptions->addElement($stopDeliveryTime);
        $timedDeliveryOptions->stop->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $postponeDelivery = $this->createElement('checkbox', 'postpone_delivery')
            ->setLabel('If leads are captured during an "Off time", deliver them during the next available delivery time')
            ->setAttribs(array('style' => 'position: relative'));
        $timedDeliveryOptions->addElement($postponeDelivery);
        //$timedDeliveryOptions->postpone_delivery->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $postponeDeliveryInterval = $this->createElement('text', 'postpone_delivery_interval')
            ->setAttrib('class', 'input-mini')
            ->setAttrib('readonly', 'true')
            ->setValue(15);
        $timedDeliveryOptions->addElement($postponeDeliveryInterval);
        $timedDeliveryOptions->postpone_delivery_interval->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $this->addSubForm($timedDeliveryOptions, 'timed_delivery');


        //Step4: Delivery Settings
        $delivery = new Twitter_Form_SubForm();
        $type = $this->createElement('select', 'type')
            ->setLabel('Delivery options')
            ->setRequired(true)
            ->addMultiOptions(array('post' => 'HTTP Post', 'email' => 'Email'))
            ->setAttrib('class', 'required');
        $delivery->addElement($type);

        $url = $this->createElement('text', 'url')
            ->setLabel('POST Url')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 500))
            ->setAttrib('class', 'required url');
        $delivery->addElement($url);

        //Response Filter
        $responseFilter = $this->createElement('checkbox', 'response_filter')
            ->setLabel('Enable API Response Filtering')
            ->setAttribs(array('style' => 'position: relative'));
        $delivery->addElement($responseFilter);

        $criteriaOptions = array('begins_with' => 'Text begins With',
            'contains' => 'Text Contains',
            'equal' => 'Equal To',
            'greater' => 'Greater Than',
            'less' => 'Less Than',
            'not_equal' => 'Not Equal');

        $responseCriteria = $this->createElement('select', 'response_criteria')
            ->addMultiOptions($criteriaOptions)
            ->setAttrib('class', 'required');
        $delivery->addElement($responseCriteria);
        $delivery->response_criteria->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');

        $responseValue = $this->createElement('text', 'response_value')
            ->setAttrib('class', 'required');
        $delivery->addElement($responseValue);
        $delivery->response_value->removeDecorator('innerwrapper')->removeDecorator('outerwrapper');


        $deliveryEmail = $this->createElement('Multiselect', 'emails')
            ->setLabel('Email for delivery')
            ->setAttrib('class', 'email')
            ->setRegisterInArrayValidator(false);
        $delivery->addElement($deliveryEmail);

        $subject = $this->createElement('text', 'subject')
            ->setLabel('Email Subject')
            ->addFilter('StringTrim')
            ->addValidator('stringLength', false, array('min' => 1, 'max' => 255))
            ->setAttrib('class', 'required')
            ->setValue(Application_Model_DbTable_ClientLeadDelivery::DEFAULT_EMAIL_SUBJECT);
        $delivery->addElement($subject);

        $senderEmail = $this->createElement('select', 'sender_email')
            ->setLabel('Send email from')
            ->setRequired(true)
            ->addMultiOptions(array('leads@leadssistant.com' => 'leads@leadssistant.com'))
            ->setAttrib('class', 'required');
        $delivery->addElement($senderEmail);

        $deliveryFields = $this->createElement('Multiselect', 'delivery_fields')
            ->setRegisterInArrayValidator(false);
        $delivery->addElement($deliveryFields);

        $staticFields = $this->createElement('Multiselect', 'static_fields')
            ->setRegisterInArrayValidator(false);
        $delivery->addElement($staticFields);

        $linkTrustUri = $this->createElement('text', 'link_trust_uri')
            ->setLabel('Posting URL')
            ->setAttrib('class', 'form-control url');
        $delivery->addElement($linkTrustUri);

        $this->addSubForm($delivery, 'delivery');
    }

    public function populate(array $values) {
        $values['postpone_delivery'] = (bool)$values['postpone_delivery_interval'];
        if(empty($values['postpone_delivery_interval'])) {
            unset($values['postpone_delivery_interval']);
        }

        $values['capped_delivery'] = (bool)$values['capped_delivery_interval'];
        if(empty($values['capped_delivery_interval'])) {
            unset($values['capped_delivery_interval']);
        }

        parent::populate($values);
    }
}