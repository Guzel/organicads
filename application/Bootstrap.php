<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initAuthenticate()
    {
        $this->bootstrap('db');
        Zend_Registry::set('bootstrap', $this);

        Zend_Session::start();
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $model = new Application_Model_DbTable_Users();
            $user = $model->find(Zend_Auth::getInstance()->getStorage()->read()->id)->current();

            if (null !== $user) {
                Zend_Registry::set('user', $user);
                if($user->role == 'admin') {
                    Zend_Registry::set('admin', $user);
                }
            }
        }
    }

    protected function _initLoadAclIni()
    {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/acl.ini');
        Zend_Registry::set('acl', $config);
    }

    protected function _initAclControllerPlugin()
    {
        $aclPlugin = new App_Controller_Plugin_Acl(new App_Acl_Main());

        $front = Zend_Controller_Front::getInstance();
        $front->registerPlugin($aclPlugin);
    }

    protected function _initNavigation()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml', 'nav');

        $navigation = new Zend_Navigation($config);
        $view->navigation($navigation);
    }

    protected function _initComposer()
    {
        require_once APPLICATION_PATH . '/../vendor/autoload.php';
    }

    protected function setConstants($constants)
    {
        foreach($constants as $name => $value) {
            if(!defined($name)) {
                define($name, $value);
            }
        }
    }
}