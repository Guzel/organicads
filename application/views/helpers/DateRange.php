<?php

class View_Helper_DateRange extends Zend_View_Helper_Abstract
{
    public function dateRange()
    {
        return $this;
    }

    public function getName($dateRange, $default = 'today')
    {
        $dateRange = $dateRange?:$default;

        switch($dateRange) {
            case '7d': $name = '7 Day'; break;
            case 'wtd': $name = 'WTD'; break;
            case 'mtd': $name = 'MTD'; break;
            case '30d': $name = '30 Days'; break;
            case 'yesterday': $name = 'Yesterday'; break;
            case 'ytd': $name = 'YTD'; break;
            case '12m': $name = '12 Months'; break;
            default: $name = 'Today'; break;
        }

        return $name;
    }
}