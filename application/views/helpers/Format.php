<?php

class View_Helper_Format extends Zend_View_Helper_Abstract
{
    public function format()
    {
        return $this;
    }

    public function phone($phoneNumber)
    {
        return  preg_replace('~.*(\d{3})[^\d]*(\d{3})[^\d]*(\d{4}).*~', '($1) $2-$3', $phoneNumber);
    }
}