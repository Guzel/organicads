<?php

/**
 * ROI View Helper
 */
class View_Helper_Roi extends Zend_View_Helper_Abstract
{
    public function roi()
    {
        return $this;
    }

    /**
     * Calculate ROI
     * 
     * @param int|float $profit
     * @param int|float $spend
     * @param bool $format
     * @return float|string
     */
    public function get($profit, $spend, $format = false)
    {
        $roi = ($spend == 0) ? 1 : ($profit/$spend);

        if($format) {
            return $this->format($roi);
        }

        return $roi;
    }

    /**
     * Format given ROI
     * 
     * @param float|int $roi
     * @return string
     */
    public function format($roi)
    {
        return number_format(($roi * 100), 2);
    }
}