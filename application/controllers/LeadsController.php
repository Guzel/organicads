<?php

class LeadsController extends Zend_Controller_Action
{
    public function newAction()
    {
        $leadData = $this->getRequest()->getPost();

        $userId = (int)$leadData['acct_id'];
        $formId = (int)$leadData['form_id'];

        $formModel = new Application_Model_DbTable_Forms();
        $form = $formModel->find($formId)->current();
        if(!$formId || $form->user_id != $userId) {
            die();
        }

        $leadsClient = new Application_Model_Leads($leadData);
        if($leadsClient->proceed()) {
            $this->redirect($form->redirect_page); die();
        }
    }

    public function responseAction()
    {
        file_put_contents(PUBLIC_PATH . '/response', serialize($_POST));
        header("Content-type: text/xml");
        echo '<xml version="1.0" encoding="UTF-8"><response><status>success</status></response></xml>'; 
        die();
    }
}