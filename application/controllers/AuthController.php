<?php

/**
 * AuthController
 */
class AuthController extends Zend_Controller_Action
{
    protected $_user;

    public function init()
    {
        if (Zend_Registry::isRegistered('user')) {
            $this->_user = Zend_Registry::get('user');
        }
        $this->_helper->layout->setLayout("auth");
    }

    /**
     * Login Action
     */
    public function loginAction()
    {
        if(!empty($this->_user)) {
            $this->_redirect('/');
        }

        $loginForm = new Application_Form_Login();

        if($this->getRequest()->isPost()) {
            if($loginForm->isValid($this->getRequest()->getPost())) {
                $userModel = new Application_Model_User();
                $currentUser = $userModel->login($loginForm->getValue('email'), 
                    $loginForm->getValue('password'));

                if($currentUser) {
                    //check if user needs to upload new data:

                    $lastUpdate = new DateTime($currentUser->last_update);
                    if((!$currentUser->last_update || ((time() - $lastUpdate->getTimestamp())/60 > 30))
                        && $currentUser->login_stat_update) {
                        $currentUser->need_update = true;
                        $currentUser->save();
                    }

                    $redirectPage = $currentUser->default_page ? $currentUser->default_page : '/accounting/summary';
                    $this->redirect($redirectPage);
                }
                $loginForm->email->addError('Your login or password is incorrect!');
            }
        }

        $this->view->form = $loginForm;
        $this->view->forgotForm = new Application_Form_ForgotPassword();
    }

    /**
     * Logout Action
     */
    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        $this->_redirect('/login');
    }

    public function forgotAction()
    {
        $form = new Application_Form_ForgotPassword();

        if($this->getRequest()->isPost()) {
            if($form->isValid($this->getRequest()->getPost())) {
                $userModel = new Application_Model_User();
                $userModel->startResetPassword($form->email->getValue());

                $response['success'] = true;
            } else {
                $response['error'] = $form->getMessages('email');
            }
        }
        $this->_helper->json($response);
    }

    public function resetPasswordAction()
    {
        if(!$hash = $this->getParam('hash')) {
            $this->_redirect('/');
        }

        $userModel = new Application_Model_DbTable_Users();
        $select = $userModel->select()->where('reset_password_hash = ?', $hash)
            ->where('hash_created >= ?', new Zend_Db_Expr('NOW()-INTERVAL 1 DAY'));

        if(!$user = $userModel->fetchRow($select)) {
            $this->view->error = true;
        }

        $form = new Application_Form_ResetPassword();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                $user->password = Application_Model_User::getPasswordHash($form->password->getValue());
                $user->reset_password_hash = null;
                $user->save();

                $this->view->success = true;
            }
        }

        $this->view->form = $form;
    }
}