<?php

class NotificationsController extends Zend_Controller_Action
{
    protected $_user;
    protected $_campaignsModel;
    protected $_usersContactsModel;

    public function init()
    {
        if (Zend_Registry::isRegistered('user')) {
            $this->_user = Zend_Registry::get('user');
        }

        $this->_helper->ajaxContext
            ->addActionContext('contacts', array('html'))
            ->initContext();

        $this->_helper->contextSwitch
            ->addActionContext('request', 'xml')->initContext('xml');

        $this->_campaignsModel = new Application_Model_DbTable_Campaigns();
        $this->_usersContactsModel = new Application_Model_DbTable_UserContacts();
    }

    public function indexAction()
    {
        
    }

    public function overviewAction()
    {
        $this->view->campaigns = $this->_campaignsModel->getPairs($this->_user->id);
        $notificationContactsModel = new Application_Model_DbTable_NotificationContacts();
        $this->view->contacts = $notificationContactsModel->getContacts($this->_user->id);

        $notificationsModel = new Application_Model_DbTable_Notifications();
        $notifications = $notificationsModel->getNotifications($this->_user->id);

        $paginator = Zend_Paginator::factory($notifications);
        $paginator->setCurrentPageNumber($this->_getParam("page"));
        $paginator->setItemCountPerPage(RECORDS_PER_PAGE);

        $this->view->notifications = $paginator;
        $this->view->page = 'notifications-overview';
    }

    public function createAction()
    {
        if(!$this->_user->twilio_phone){
            $this->_helper->redirector('overview'); die();
        }

        $campaigns = $this->_campaignsModel->getPairs($this->_user->id);
        $contacts = $this->_usersContactsModel->getPairs($this->_user->id);
        $notificationsModel = new Application_Model_DbTable_Notifications();
        $form = new Application_Form_CreateNotification(array('campaigns' => $campaigns, 'contacts' => $contacts));

        if($id = (int)$this->_getParam('id')){
            $notification = $notificationsModel->find($id)->current();
            if(!$notification || $notification->user_id != $this->_user->id) {
                $this->_helper->redirector('overview');
            }
            $form->populate($notification->toArray());
            $this->view->notification = $notification;
        }

        if($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            if($form->getSubForm('general')->isValid($postData)) {
                $notificationType = $form->getSubForm('general')->type->getValue();
                $settingsForm = $form->getSubForm($notificationType);

                if($settingsForm->isValid($postData)) {
                    $values = $form->getValues();
                    $notification = array_merge($values['general'], $values[$notificationType]);
                    $notificationsModel->save($notification, $this->_user->id, $id);

                    $this->view->added = true;
                }
            }
        }
        $this->view->form = $form;
        $this->view->contacts = $contacts;
        $this->view->page = 'notification-create';
    }

    public function editNotificationAction()
    {
        $removeType = $actionType = $this->_getParam('type');
        $notificationId = (int)$this->_getParam('id');

        $notificationsModel = new Application_Model_DbTable_Notifications();
        $notification = $notificationsModel->find($notificationId)->current();

        if($notification && $notification->user_id == $this->_user->id && $removeType) {
            if($removeType == 'delete') {
                $notification->delete();
            } else {
                $notification->disabled = ($removeType == 'disable') ? new Zend_Db_Expr('NOW()') : null;
                $notification->save();
            }
        }

        if($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->json(1);
        }
    }

    public function contactsAction()
    {
        $this->view->contacts = $this->_usersContactsModel->getUserContacts($this->_user->id);
    }

    public function contactAction()
    {
        $operation = $this->_getParam('operation');
        $value = trim($this->_getParam('value'));
        $field = $this->_getParam('field');

        if($field == 'phone') {
            if(!($value = App_Utils::normalizePhone($value))) {
                $this->_helper->json(array('error' => 'Phone Number is invalid'));
            }
        }

        if($id = (int)$this->_getParam('id')) {
            $contact = $this->_usersContactsModel->find($id)->current();

            if($contact->user_id == $this->_user->id) {
                if($operation == 'delete') {
                    $contact->delete();
                } elseif($operation == 'edit'){
                    if($value && in_array($field, array('name', 'phone'))) {
                        $contact->$field = $value;
                        $contact->save();
                    }
                }
                $result['success'] = true;
            }
            $this->_helper->json($result);
        }

        if($operation == 'add') {
            $newContact = $this->_usersContactsModel->createRow();
            $newContact->user_id = $this->_user->id;
            $newContact->name = $this->_getParam('name');
            $newContact->phone = App_Utils::normalizePhone($this->_getParam('phone'));
            $newContact->added = new Zend_Db_Expr('NOW()');
            $newContact->save();

            $result['success'] = true;
            $this->_helper->json($result);
        }
    }

    public function statRequestAction()
    {
        $mail = new App_Mail('new-notification', 'Params');
        $mail->addTo('guze4ka@gmail.com');
        $mail->assign('text', serialize($this->getRequest()->getPost()));
        $mail->send();

        $notifications = new Application_Model_Notifications();
        echo $notifications->processRequest($this->getRequest()->getPost());
        die();
    }
}