<?php

class CrmController extends Zend_Controller_Action
{
    protected $_user;

    public function init()
    {
        if (Zend_Registry::isRegistered('user')) {
            $this->_user = Zend_Registry::get('user');
        }

        $this->_helper->ajaxContext
            ->addActionContext('edit-form', array('html'))
            ->addActionContext('update-widget', array('html'))
            ->addActionContext('leads-operations', array('html'))
            ->initContext();
    }

    public function overviewAction()
    {
        $today = new DateTime();
        $dateFrom = $today->sub(new DateInterval('P30D'))->format('Y-m-d');

        $leadsModel = new Application_Model_DbTable_Leads();
        $this->view->soldLeadsStat = $leadsModel->getStatistics(array('user_id' => $this->_user->id), true, $dateFrom);
        $this->view->receivedLeadsStat = $leadsModel->getStatistics(array('user_id' => $this->_user->id), null, $dateFrom);

        $this->view->mtdSoldStat = $leadsModel->findTotalStatistics($this->_user->id, true, date('Y-m-01'));
        $this->view->mtdReceivedStat = $leadsModel->findTotalStatistics($this->_user->id, null, date('Y-m-01'));
        $this->view->page = 'crm_overview';
    }

    public function contactsAction()
    {
        $leadsModel = new Application_Model_DbTable_Leads();

        if($this->getParam('duplicate-field')) {
            $leadsModel->deleteDuplicates($this->_user->id, $this->getParam('duplicate-field'));
        }

        if($this->getParam('fields-update')) {
            $this->_user->contact_fields = $this->getParam('show-fields') ? $this->getParam('show-fields') : null;
            $this->_user->save(); 

            if($this->_request->isXmlHttpRequest()) { return; }
        }

        $select = $leadsModel->select();
        $this->view->leads = $leadsModel->fetchAll($select);

        $formsModel = new Application_Model_DbTable_Forms();
        $this->view->forms = $formsModel->getPairs($this->_user->id);

        $clientsModel = new Application_Model_DbTable_Clients();
        $this->view->clients = $clientsModel->getPairs($this->_user->id);

        $fieldsModel = new Application_Model_DbTable_Fields();
        $this->view->fields = $fieldsModel->getPairs($this->_user->id);

        $this->view->viewFields = ($fileds = $this->_user->contact_fields) ? explode(',', $fileds) : array();
        $this->view->page = 'crm_contacts';
    }

    public function contactsDataAction()
    {
        $response = array();
        $order = $this->getParam('sidx');
        $orderType = $this->getParam('sord');

        $leadsModel = new Application_Model_DbTable_Leads();
        $select = $leadsModel->select()->where('user_id = ?', $this->_user->id)
            ->order("{$order} {$orderType}");
        if($this->getParam('filter')) {
            $field = $this->getParam('field');
            $value = $this->getParam('search_value');

            if($field == 'date') {
                $select->where('DATE(date) = ?', App_Utils::abbrevToDate($value));
            } else {
                $select->where("`{$field}` = ?", $value);
            }
        }

        $paginator = Zend_Paginator::factory($select);
        $paginator->setCurrentPageNumber($this->_getParam("page"));
        $paginator->setItemCountPerPage($this->getParam('rows'));

        $response['page'] = $paginator->getCurrentPageNumber(); 
        $response['total'] = ceil($paginator->getTotalItemCount()/$paginator->getItemCountPerPage());
        $response['records'] = $paginator->getTotalItemCount();
        $leads = $paginator->getCurrentItems();

        $formsModel = new Application_Model_DbTable_Forms();
        $forms = $formsModel->getPairs($this->_user->id);

        $clientsModel = new Application_Model_DbTable_Clients();
        $clients = $clientsModel->getPairs($this->_user->id);

        $additionalFields = ($val = $this->_user->contact_fields) ? explode(',', $val) : array();

        if($this->getParam('csv')) {
            $filename = "contacts_" . date("m-d-Y_H-i") . ".csv";
            $fieldsModel = new Application_Model_DbTable_Fields();
            $fields = $fieldsModel->getPairs($this->_user->id);

            $titles = array('ID', 'Time Added');
            foreach($additionalFields as $fieldId) {
                if(is_numeric($fieldId)) {
                    $titles[] = $fields[$fieldId];
                } else {
                    $titles[] = $fieldId == 'form_id' ? 'Form' : ($fieldId == 'response' ? 'Response' : 'Sold To');
                }
            }

            $csvDir = "files/{$this->_user->id}";
            if(!is_dir($csvDir)) {
                mkdir($csvDir);
            }
            $fp = fopen("{$csvDir}/{$filename}", 'w');
            fputcsv($fp, $titles, ',');
        }

        foreach($leads as $lead) {
            $leadOptions = array();
            $leadOptions['id'] = $lead->id;
            $leadOptions['date'] = strftime("%B %d, %Y %H:%M", strtotime($lead->date));

            $fields = unserialize($lead->fields);
            foreach($additionalFields as $fieldId) {
                if($fieldId == 'form_id') {
                    $leadOptions[$fieldId] = $forms[$lead->form_id];
                } elseif($fieldId == 'client_id') {
                    $leadOptions[$fieldId] = $lead->sold ? $clients[$lead->client_id] : '';
                } elseif($fieldId == 'response') {
                    $xmlResponse = strpos($lead->response, 'html') !== false ? 'Not valid XML' : $lead->response;
                    $leadOptions[$fieldId] = $xmlResponse;
                } else {
                    $leadOptions["field_{$fieldId}"] = !empty($fields[$fieldId]) ? $fields[$fieldId] : '';
                }
            }
            $response['rows'][] = $leadOptions;

            if($this->getParam('csv')) {
                fputcsv($fp, $leadOptions, ',');
            }
        }

        if($this->getParam('csv')) {
            $this->_helper->viewRenderer->setNoRender(); 
            $this->_helper->layout->disableLayout();

            fclose($fp);

            header('Content-type: application/csv');
            header('Content-Disposition: attachment; filename="'.$filename.'"');
            echo file_get_contents("{$csvDir}/{$filename}"); die();
        }

        echo $this->_helper->json($response);
        die();
    }

    public function leadsOperationsAction()
    {
        $leadsModel = new Application_Model_DbTable_Leads();
        if($this->getParam('oper') == 'del') {
            $ids = explode(',', $this->getParam('id'));
            $leadsModel->deleteLeadsById($ids, $this->_user->id);
            echo $this->_helper->json(true); die();
        } elseif($this->getParam('oper') == 'details') {
            $id = (int)$this->getParam('id');
            $lead = $leadsModel->find($id)->current();
            $this->view->lead = $lead;

            $formModel = new Application_Model_DbTable_Forms();
            $this->view->form = $formModel->find($lead->form_id)->current();

            $fieldsModel = new Application_Model_DbTable_Fields();
            $this->view->fields = $fieldsModel->getPairs($this->_user->id);

            $this->_helper->viewRenderer("widget/lead-details");
        } elseif($this->getParam('oper') == 'deliver') {
            $leadsModel = new Application_Model_Leads();
            $leads = $this->getParam('lead_ids');
            $clientId = (int)$this->getParam('client_id');

            $result = array();
            foreach($leads as $id) {
                $result[$id] = $leadsModel->update($id, $clientId);
            }
            echo $this->_helper->json($result); die();
        }
    }

    public function clientsAction()
    {
        $clientsModel = new Application_Model_DbTable_Clients();
        $formsModel = new Application_Model_DbTable_Forms();

        $select = $clientsModel->select()->where('user_id = ?', $this->_user->id);
        $paginator = Zend_Paginator::factory($select);
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setDefaultItemCountPerPage(RECORDS_PER_PAGE);

        $this->view->clientsList = $paginator;
        $this->view->forms = $formsModel->getPairs($this->_user->id);
        $this->view->page = 'clients';
    }

    public function clientAction()
    {
        $form = new Application_Form_CreateClient();
        $clientsModel = new Application_Model_DbTable_Clients();
        $clientFiltersModel = new Application_Model_DbTable_ClientFilters();
        $changePriority = (bool)$this->getParam('change-priority');
        $formId = (int)$this->getParam('form');

        if($this->_getParam('edit') && ($id = $this->_getParam('id'))) {
            $client = $clientsModel->find($id)->current();
            if(!$client || ($client->user_id !== $this->_user->id)) {
                die();
            }

            if($this->_getParam('status')) {
                if($client->status == Application_Model_DbTable_Clients::TESTING_STATUS) {
                    $leads = new Application_Model_DbTable_Leads();
                    $leads->deleteLeads($client->id, true);
                }
                $client->status = $this->_getParam('status');
            }
            $client->save();

            $this->_helper->json(true); die();
        }

        if($this->_getParam('clients_priority')) {
            $priories = explode(',', $this->_getParam('clients_priority'));
            $clientsModel->changePriority($priories);
            $changePriority = true;
            $this->view->success = true;
        }

        if($this->getRequest()->isPost()) {
            if($form->isValid($this->getRequest()->getPost())) {
                $generalInfo = current($form->getSubForm('client_general')->getValues());
                $generalInfo['user_id'] = $this->_user->id;
                $generalInfo['cap_enabled'] = $form->cap_enabled->getValue();
                $generalInfo['timed_delivery_enabled'] = $form->timed_delivery_enabled->getValue();

                $client = $clientsModel->save($generalInfo);

                $clientFiltersModel->save($this->getRequest()->getPost('filters'), $client->id);

                $clientCap = new Application_Model_DbTable_ClientCap();
                $clientCap->deleteByClientId($client->id);
                if($client->cap_enabled) {
                    $clientCap->save(current($form->getSubForm('cap')->getValues()), $client->id); //Step3: Caps
                    if($form->getSubForm('cap')->capped_delivery->getValue()) {
                        $client->capped_delivery_interval = $form->getSubForm('cap')->capped_delivery_interval->getValue();
                        $client->save();
                    }
                }

                $clientTimedDelivery = new Application_Model_DbTable_ClientTimedDelivery();
                $data = current($form->getSubForm('timed_delivery')->getValues());
                $clientTimedDelivery->save($data, $client->id); //Step3: Timed Delivery
                if($data['postpone_delivery']) {
                    $client->postpone_delivery_interval = $data['postpone_delivery_interval'];
                    $client->save();
                }

                $clientDelivery = new Application_Model_DbTable_ClientLeadDelivery();
                $clientDelivery->save(current($form->getSubForm('delivery')->getValues()), $client->id); //step4: Delivery

                $this->redirect('/crm/client/change-priority/?form=' . $client->form_id); die(); //redirect to clients' priority
            }
        }

        if($changePriority) {
            $formsModel = new Application_Model_DbTable_Forms();
            $fieldsModel = new Application_Model_DbTable_Fields();
            $clientsForm = $formsModel->find($formId)->current();
            if(!$clientsForm || $clientsForm->user_id != $this->_user->id) {
                $this->_helper->redirector('clients'); die();
            }

            $select = $clientsModel->select()->where('form_id = ?', $formId)->order('priority');
            $this->view->clientsList = $clientsModel->fetchAll($select);
            $this->view->clientsFilters = $clientFiltersModel->getFiltersGroupByClient(true);
            $this->view->clientsForm = $clientsForm;
            $this->view->zipId = $fieldsModel->idByName('state');
        }

        $this->view->changePriority = $changePriority;
        $this->view->form = $form;
        $this->view->page = 'clients';
    }

    public function editClientAction()
    {
        $clientsModel = new Application_Model_DbTable_Clients();
        $form = new Application_Form_CreateClient();

        $clientId = $this->_getParam('id');
        $currentClient = $clientsModel->find($clientId)->current();
        if(null == $currentClient) {
            $this->_helper->redirector('clients'); die();
        }

        //general
        $form->populate($currentClient->toArray());

        //filters
        $clientFiltersModel = new Application_Model_DbTable_ClientFilters();
        $this->view->currentFilters = $clientFiltersModel->getClientFilters($currentClient->id);

        //timed Delivery
        if($currentClient->timed_delivery_enabled) {
            $clientTimedDelivery = new Application_Model_DbTable_ClientTimedDelivery();
            $timedOptions = $clientTimedDelivery->getFormPopulateData($currentClient->id);
            $form->getSubForm('timed_delivery')->populate($timedOptions);
        }

        //caps
        if($currentClient->cap_enabled) {
            $clientCap = new Application_Model_DbTable_ClientCap();
            $capsOptions = $clientCap->getFormPopulateData($currentClient->id);
            $form->getSubForm('cap')->populate($capsOptions);
        }

        $clientDelivery = new Application_Model_DbTable_ClientLeadDelivery();
        $deliveryOptions = $clientDelivery->fetchRow($clientDelivery->select()->where('client_id = ?', $clientId))->toArray();
        $deliveryOptions['response_filter'] = (bool)$deliveryOptions['response_value'];
        $this->view->deliveryEmails = unserialize($deliveryOptions['emails']);
        $this->view->deliveryFields = unserialize($deliveryOptions['delivery_fields']);
        $this->view->staticFields = unserialize($deliveryOptions['static_fields']);
        $form->getSubForm('delivery')->populate($deliveryOptions);

        $formsModel = new Application_Model_DbTable_Forms();
        $clientForm = $formsModel->find($currentClient->form_id)->current();

        $fieldsModel = new Application_Model_DbTable_Fields();
        $this->view->fields = $fieldsModel->getPairs($this->_user->id);

        $this->view->client = $currentClient;
        $this->view->formFields = $fieldsModel->getPairs(null, unserialize($clientForm->fields));
        $this->view->filterCriteria = $form->getSubForm('search_data_filter')->criteria->getMultiOptions();
        $this->view->states = $form->getSubForm('state_filter')->search_value->getMultiOptions();
        $this->view->form = $form;
        $this->view->page = 'clients';
        $this->_helper->viewRenderer("client");
    }

    public function fieldsAction()
    {
        $fieldsModel = new Application_Model_DbTable_Fields();
        $fieldsOptionsModel = new Application_Model_DbTable_FieldsOptions();

        if($this->_getParam('save') || $this->_getParam('delete')) {
            $result = array();

            if($id = $this->_getParam('id')) {
                $field = $fieldsModel->find($id)->current();

                //edit Alter Name:
                if($this->_getParam('field') == 'name' && $field && ($field->user_id == $this->_user->id || !$field->user_id)) {
                    $name = trim($this->_getParam('value'));
                    if($fieldsOptionsModel->findByName($name, $this->_user->id)) {
                        $result['error'] = 'Field with name ' . $name . ' already exists';
                    } else {
                        $fieldsOptionsModel->save(array('alter_name' => $name), $id, $this->_user->id);
                        $field->save();
                        $result['success'] = true;
                    }
                } elseif(!$field || ($field->user_id !== $this->_user->id)) {
                    die();
                }

                if($this->_getParam('delete')) {
                    $field->delete();
                    $this->_helper->json(true);
                }
            }

            //add new field or edit title
            $title = $this->_getParam('title') ? trim($this->_getParam('title')) : 
                ($this->_getParam('field') == 'title' ? $this->_getParam('value') : null);
            if($title) {
                if($fieldsModel->findByTitle($title, $this->_user->id)) {
                    $result['error'] = 'Field with title ' . $title . ' already exists';
                } else {
                    if(!$field) {
                        $field = $fieldsModel->createRow();
                    }
                    $field->title = $title;
                    $field->user_id = $this->_user->id;
                    $field->save();
                    $result['success'] = true;
                }
            }

            if($this->_request->isXmlHttpRequest()) {
                $this->_helper->json($result); die();
            }
            $this->view->saved = $result;
        }

        $this->view->fields = $fieldsModel->getFields($this->_user->id);
        $this->view->fieldsOptions = $fieldsOptionsModel->getPairs($this->_user->id);
        $this->view->page = 'fields';
    }

    public function formsAction()
    {
        $formsModel = new Application_Model_DbTable_Forms();
        $filedsModel = new Application_Model_DbTable_Fields();
        $fieldsOptionsModel = new Application_Model_DbTable_FieldsOptions();
        $createForm = new Application_Form_CreateForm();

        if($this->getRequest()->isPost()) {
            if($createForm->isValid($this->getRequest()->getPost())) {
                $formsModel->save($this->_user->id, $createForm->getValues());

                $this->view->success = true;
            }
        }

        $this->view->formsList = $formsModel->getForms($this->_user->id);
        $this->view->fields = $filedsModel->getPairs($this->_user->id);
        $this->view->fieldsOptions = $fieldsOptionsModel->getPairs($this->_user->id);
        $this->view->page = 'forms';
    }

    public function editFormAction()
    {
        $formsModel = new Application_Model_DbTable_Forms();
        $createForm = new Application_Form_CreateForm();

        if($id = $this->getParam('id')) {
            $currentForm = $formsModel->find($id)->current();
            if(!$currentForm || ($currentForm->user_id !== $this->_user->id)) {
                die();
            }

            if($this->_getParam('delete')) {
                $currentForm->delete();
                $this->_helper->json(true);
            }

            if($this->_getParam('get_fields')) {
                $fieldsModel = new Application_Model_DbTable_Fields();
                $fields = $fieldsModel->getPairs(null, unserialize($currentForm->fields));
                $this->_helper->json($fields);
            }

            $createForm->populate($currentForm->toArray());
        }

        $this->view->form = $createForm;
    }

    public function uploadTargetsAction()
    {
        $adsModel = new Application_Model_DbTable_Ads();
        $advertListModel = new Application_Model_DbTable_AdvertsList();

        if($this->getParam('adId')) {
            $adId = $this->getParam('adId');
            $adCampaignsModel = new Application_Model_DbTable_AdCampaigns();
            $adCampaigns = $adCampaignsModel->getPairs($this->_user->id, $adId, 'id', 'name');
            if($this->getParam('getCampaigns')) {
                echo $this->_helper->json($adCampaigns); die();
            }

            if($this->getParam('getAdverts') && $this->getParam('campaignId')) {
                $campaignId = $this->getParam('campaignId');
                if(empty($adCampaigns[$campaignId])) {
                    die();
                }

                $adverts = $advertListModel->getByAdCampaignIds($campaignId);
                echo $this->_helper->json($adverts); die();
            }
        }

        if($this->getRequest()->isPost()) {
            $error = null;
            $advertId = $this->getParam('advert_id');
            $maxBid = $this->getParam('bid');
            $advert = $advertListModel->find($advertId)->current();

            $ad = $adsModel->find($advert->ad_id)->current();
            if(!$ad || $ad->user_id != $this->_user->id) {
                $this->_helper->redirector('upload-targets'); die();
            }
            $upload = new Zend_File_Transfer_Adapter_Http();
            $upload->addValidator('Extension', false, 'csv');

            if (!$upload->isValid()) {
                $error = current($upload->getMessages());
            } else {
                $name = "{$ad->source}_{$advert->id}_" . time();
                $target = PUBLIC_PATH . "/../data/upload_targets/{$name}.csv";
                $upload->addFilter('Rename', array('overwrite'=>true, 'target'=> $target));

                $added = $ignored = array();
                try {
                    $upload->receive();
                    $content = file_get_contents($target);
                    $targets = array_filter(preg_split("/\r\n|\n|\r/", $content));

                    if(!$targets) {
                        throw new Exception('Targets was not found in uploaded file');
                    }

                    if($targets) {
                        $decryptedCredentials = App_Utils::decryptString(base64_decode($ad->credentials));
                        $sourceModel = Application_Model_SourceAdapter::getInstanse($ad->source, unserialize($decryptedCredentials));

                        $targetChunks = array_chunk($targets, 1000);
                        foreach($targetChunks as $targetList) {
                            $res = $sourceModel->addTarget($advert->api_id, $targetList, $maxBid);

                            if(!empty($res->added->item)) {
                                $addedArray = !is_array($res->added->item) ? array($res->added->item) : $res->added->item;
                                $added = array_merge($added, $addedArray);
                            }
                            if(!empty($res->ignored->item)) {
                                $ignoredArray = !is_array($res->ignored->item) ? array($res->ignored->item) : $res->ignored->item;
                                $ignored = array_merge($ignored, $ignoredArray);
                            }
                        }
                    }
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
                $this->view->addedNumber = count($added);
                $this->view->ignored = $ignored;
            }
            $this->view->error = $error;
            $this->view->completed = true;
        }

        $ads = $adsModel->getAdsByUserId($this->_user->id, Application_Model_DbTable_Ads::TRAFFICVANCE);
        $this->view->ads = $ads;
        $this->view->page = 'upload-targets';
    }

    public function updateWidgetAction()
    {
        $widget = $this->_getParam('widget');
        $dateFrom = App_Utils::abbrevToDate($this->_getParam('dateRange'));
        $leadsModel = new Application_Model_DbTable_Leads();

        $this->view->clientsReceivedStat = $leadsModel->findStatisticsByClients($this->_user->id, null, $dateFrom);
        $this->view->clientsSoldStat = $leadsModel->findStatisticsByClients($this->_user->id, true, $dateFrom);

        $clientsModel = new Application_Model_DbTable_Clients();
        $this->view->clients = $clientsModel->getPairs($this->_user->id);
        $this->view->dateRange = $this->_getParam('dateRange');

        $this->_helper->viewRenderer("widget/{$widget}");
    }
}