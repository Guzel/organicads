<?php

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;

class AdminpanelController extends Zend_Controller_Action
{
    protected $_user;

    public function init()
    {
        if (Zend_Registry::isRegistered('user')) {
            $this->_user = Zend_Registry::get('user');
        }
    }

    public function usersAction()
    {
        $usersModel = new Application_Model_DbTable_Users();
        $users = $usersModel->getUsers($this->_user->id);

        $paginator = Zend_Paginator::factory($users);
        $paginator->setCurrentPageNumber($this->_getParam("page"));
        $paginator->setItemCountPerPage(RECORDS_PER_PAGE);

        $aclConfig = Zend_Registry::get('acl');

        $this->view->users = $paginator;
        $this->view->roles = $aclConfig->acl->public_roles->toArray();
        $this->view->page = 'users';
    }

    public function editUserAction()
    {
        $form = new Application_Form_EditUser();
        $usersModel = new Application_Model_DbTable_Users();

        if($userId = (int)$this->_getParam('id')){
            $user = $usersModel->find($userId)->current();
            if(!$user || $user->parent_id != $this->_user->id) {
                $this->_helper->redirector('users');
            }

            $form->populate($user->toArray());
            $this->view->user = $user;
        }

        if($this->getRequest()->isPost()) {
            if($form->isValid($this->getRequest()->getPost())) {
                $data = $form->getValues();
                $data['parent_id'] = $this->_user->id;
                $usersModel->save($data, $userId);

                $this->view->success = true;
            }
        }

        $this->view->form = $form;
        $this->view->page = 'user';
    }

    /**
     * Disable or delete user
     */
    public function removeUserAction()
    {
        $removeType = $actionType = $this->_getParam('type');
        $userId = (int)$this->_getParam('id');

        $usersModel = new Application_Model_DbTable_Users();
        $user = $usersModel->find($userId)->current();

        if($user && $removeType) {
            if($removeType == 'delete') {
                $user->delete();
            } else {
                $user->disabled = ($removeType == 'disable') ? new Zend_Db_Expr('NOW()') : null;
                $user->save();
            }
        }

        if($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->json(1);
        }
    }

    /*
     * Login to the FB App's admin account, get list of connected ad accounds
     * Add log Access Token to the credentials
     */
    public function facebookTokenAction()
    {
        $currentUrl = $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost() . '/adminpanel/facebook-token';
        FacebookSession::setDefaultApplication(FACEBOOK_APP_ID, FACEBOOK_APP_SECRET);
        $helper = new FacebookRedirectLoginHelper($currentUrl);

        if(!empty($_GET['code'])) {
            if (($session = $helper->getSessionFromRedirect())) {
                $accessToken = $session->getAccessToken();
                $longAccessToken = $accessToken->extend();
                $request = new FacebookRequest( $session, 'GET', '/me/adaccounts' );
                $response = $request->execute();
                $adAccounts = $response->getResponse();

                if($longAccessToken && $adAccounts->data) {
                    foreach($adAccounts->data as $adAccount) {
                        $adModel = new Application_Model_DbTable_Ads();
                        $sources = $adModel->fetchAll($adModel->select()->where("account_id = {$adAccount->account_id} AND `source` = 'facebook'"));
                        foreach($sources as $source) {
                            $decryptedCredentials = App_Utils::decryptString(base64_decode($source->credentials));
                            $credentials = unserialize($decryptedCredentials);
                            $credentials['access_token'] = (string)$longAccessToken;
                            $source->credentials = base64_encode(App_Utils::encryptString(serialize($credentials)));
                            $source->save();
                        }
                    }
                }
            } else {
                die('Session was not started!');
            }
        } else {
            echo '<a href="' . $helper->getLoginUrl() . '">Login</a>';
        }
        die();
    }
}