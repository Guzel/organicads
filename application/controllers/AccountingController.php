<?php

class AccountingController extends Zend_Controller_Action
{
    protected $_user;

    public function init()
    {
        if (Zend_Registry::isRegistered('user')) {
            $this->_user = Zend_Registry::get('user');
        }

        $this->_helper->ajaxContext
            ->addActionContext('update-chart', array('html'))
            ->addActionContext('update-table', array('html'))
            ->initContext();
    }

    public function summaryAction()
    {
        $affiliateStat = new Application_Model_DbTable_AffiliateCampaignStatistics();
        $adStat = new Application_Model_DbTable_AdCampaignStatistics();

        $dateTo = null;
        if($from = $this->_getParam('from')) {
            $dateFrom = date('Y-m-d', strtotime($from));
            $dateTo = ($to = $this->_getParam('to')) ? date('Y-m-d', strtotime($to)) : null;
            $this->view->from = $dateFrom;
            $this->view->to = $dateTo;
        } else {
            $dateRange = $this->_getParam('dateRange');
            $dateFrom = App_Utils::abbrevToDate($dateRange, '30d');
            $this->view->dateRange = $dateRange;
        }

        $this->view->affiliateStats = $affiliateStat->findTotalByCampaign(array('user_id' => $this->_user->id), $dateFrom, $dateTo);
        $this->view->mtdAffiliateStat = $affiliateStat->findTotalCommission($this->_user->id, date('Y-m-01'));

        $this->view->adStats = $adStat->findTotalByCampaign(array('user_id' => $this->_user->id), $dateFrom, $dateTo);
        $this->view->mtdSpend = $adStat->findTotalSpend($this->_user->id, date('Y-m-01'));

        $this->view->yesterday = App_Utils::abbrevToDate('yesterday');
        $this->view->page = 'dashboard';
    }

    public function overviewAction()
    {
        $affiliateStat = new Application_Model_DbTable_AffiliateCampaignStatistics();
        $adStat = new Application_Model_DbTable_AdCampaignStatistics();
        $dateTo = null;

        if($from = $this->_getParam('from')) {
            $dateFrom = date('Y-m-d', strtotime($from));
            $dateTo = ($to = $this->_getParam('to')) ? date('Y-m-d', strtotime($to)) : null;
            $this->view->from = $dateFrom;
            $this->view->to = $dateTo;
        } else {
            $dateRange = $this->_getParam('dateRange');
            $dateFrom = App_Utils::abbrevToDate($dateRange, '30d');
            $this->view->dateRange = $dateRange;
        }

        $this->view->affiliateStats = $affiliateStat->findTotalByCampaign(array('user_id' => $this->_user->id), $dateFrom, $dateTo);
        $this->view->affiliateByDate = $affiliateStat->findTotalByCampaign(array('user_id' => $this->_user->id), date('Y-m-01'));
        //Recap Table -> MTD stat
        $this->view->mtdAffiliateStat = $affiliateStat->findTotalCommission($this->_user->id, date('Y-m-01'));

        $this->view->adStats = $adStat->findTotalByCampaign(array('user_id' => $this->_user->id), $dateFrom, $dateTo);
        $this->view->mtdSpend = $adStat->findTotalSpend($this->_user->id, date('Y-m-01'));

        $this->view->yesterday = App_Utils::abbrevToDate('yesterday');
        $this->view->page = 'overview';
    }

    public function recapAction()
    {
        $affiliateStat = new Application_Model_DbTable_AffiliateCampaignStatistics();
        $adStat = new Application_Model_DbTable_AdCampaignStatistics();

        $today = new DateTime();
        $dateFrom = $this->getParam('date_from') ? : $today->format('Y-m-01');
        $dateTo = $this->getParam('date_to') ? : $today->format('Y-m-t');

        $this->view->affiliateStats = $affiliateStat->findTotalByCampaign(array('user_id' => $this->_user->id), $dateFrom, $dateTo);
        $this->view->adStats = $adStat->findTotalByCampaign(array('user_id' => $this->_user->id), $dateFrom, $dateTo);

        $this->view->page = 'recap';
        $this->view->dateFrom = $dateFrom;
        $this->view->dateTo = $dateTo;
    }

    public function campaignsAction()
    {
        $form = new Application_Form_CampaignStatistic();

        $today = new DateTime();
        $form->date_to->setValue($today->format('Y-m-t'));
        $form->date_from->setValue($today->format('Y-m-01'));

        if($this->getParam('campaign')) {
            if($form->isValid($this->getRequest()->getParams())) {
                if(!$form->date_from->getValue()) {
                    $form->date_to->setValue($today->format('Y-m-t'));
                    $form->date_from->setValue($today->format('Y-m-01'));
                }

                $dateFrom = $form->date_from->getValue();
                $dateTo = $form->date_to->getValue();

                $affiliateStat = new Application_Model_DbTable_AffiliateCampaignStatistics();
                $adStat = new Application_Model_DbTable_AdCampaignStatistics();

                $statOptions = array('campaign_id' => $form->campaign->getValue());
                $this->view->adSpendStats = $adStat->findSpendByAccounts($form->campaign->getValue(), $dateFrom, $dateTo);
                $this->view->affiliateStats = $affiliateStat->findTotalByCampaign($statOptions, $dateFrom, $dateTo);
                $this->view->adStats = $adStat->findTotalByCampaign($statOptions, $dateFrom, $dateTo);
                $this->view->exportResources = $this->getInvokeArg('bootstrap')->getOption('exportResources');
            }
        }

        $this->view->form = $form;
        $this->view->page = 'campaigns';
    }

    public function dayPartingAction()
    {
        $form = new Application_Form_CampaignStatistic();

        $today = new DateTime();
        $form->date_to->setValue($today->format('Y-m-d H:i'));
        $form->date_from->setValue($today->format('Y-m-d 00:00'));

        if($this->getParam('campaign')) {
            if($form->isValid($this->getRequest()->getParams())) {
                if(!$form->date_from->getValue()) {
                    $form->date_to->setValue($today->format('Y-m-d H:i'));
                    $form->date_from->setValue($today->format('Y-m-d 00:00'));
                }
                $dateFrom = $form->date_from->getValue();
                $dateTo = $form->date_to->getValue();

                $affiliateStatModel = new Application_Model_DbTable_AffiliateCampaignDetailedStatistics();
                $adStatModel = new Application_Model_DbTable_AdCampaignDetailedStatistics();

                $affiliateStat = $affiliateStatModel->findTotalByCampaign(
                    array('campaign_id'=>$form->campaign->getValue()), $dateFrom, $dateTo); 
                $adStat = $adStatModel->findTotalByCampaign(
                    array('campaign_id'=>$form->campaign->getValue()), $dateFrom, $dateTo);

                $statDates = array_unique(array_merge(array_keys($affiliateStat), array_keys($adStat)));
                asort($statDates);

                $this->view->statDates = $statDates;
                $this->view->affiliateStats = $affiliateStat;
                $this->view->adStats = $adStat;
                $this->view->adSpendStats = $adStatModel->findSpendByAccounts($form->campaign->getValue(), $dateFrom, $dateTo);
                $this->view->exportResources = $this->getInvokeArg('bootstrap')->getOption('exportResources');
            }
        }

        $this->view->form = $form;
        $this->view->page = 'day-parting';
    }

    public function adPerformanceAction()
    {
        $dateFrom = $this->getParam('date_from') ? : App_Utils::abbrevToDate();
        $dateTo = $this->getParam('date_to') ? : App_Utils::abbrevToDate();

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $campaignsList = $campaignsModel->getPairs($this->_user->id);

        $affiliateStat = new Application_Model_DbTable_AffiliateCampaignStatistics();
        $adStat = new Application_Model_DbTable_AdCampaignStatistics();

        $advertStat = $adStat->findTotalAdvertSpend(array('user_id' => $this->_user->id), $dateFrom, $dateTo);
        $this->view->adSpendStats = $advertStat;
        $this->view->affiliateStats = $affiliateStat->findTotalSubIdCommission(array('user_id' => $this->_user->id), $dateFrom, $dateTo);

        $pairsModel = new Application_Model_DbTable_AdvertSubidPairs();
        $subidsModel = new Application_Model_DbTable_AffiliateSubids();
        $advertsListModel = new Application_Model_DbTable_AdvertsList();

        $this->view->pairs = $pairsModel->getByUserId($this->_user->id);
        $this->view->subids = $subidsModel->getPairs(array('user_id' => $this->_user->id));
        $this->view->advertsList = $advertsListModel->getPairs(null, 'id', 'header');
        $this->view->campaignsList = $campaignsList;
        $this->view->date_from = $dateFrom;
        $this->view->date_to = $dateTo;
        $this->view->page = 'ad-performance';
    }

    public function campaignListAction()
    {
        $affiliateCampaignsModel = new Application_Model_DbTable_AffiliateCampaigns();
        $adCampaignsModel = new Application_Model_DbTable_AdCampaigns();
        $campaignsModel = new Application_Model_DbTable_Campaigns();

        $this->view->campaigns = $campaignsModel->findByUser($this->_user->id);
        $this->view->adCampaignsAmt = $adCampaignsModel->assignedAmtByCampaign($this->_user->id);
        $this->view->affiliateCampaignsAmt = $affiliateCampaignsModel->assignedAmtByCampaign($this->_user->id);
        $this->view->page = 'campaign-list';
    }

    public function fbAdPerformanceAction()
    {
        $campaignId = (int)$this->getParam('campaign'); 
        $this->view->dateFrom = $dateFrom = $this->getParam('date_from') ? : App_Utils::abbrevToDate();
        $this->view->dateTo  = $dateTo = $this->getParam('date_to') ? : App_Utils::abbrevToDate();
        $this->view->query = $query = $this->getParam('query');

        $campaigns = new Application_Model_DbTable_Campaigns();
        $this->view->campaign = $campaign = $campaigns->find($campaignId)->current();
        if(!$campaign || $campaign->user_id != $this->_user->id) {
            $this->_helper->redirector('campaign-list');
        }

        $options = array('campaign_id' => $campaignId, 'query' => $query);
        $adStat = new Application_Model_DbTable_AdCampaignStatistics();
        $advertsStat = $adStat->findTotalByAdverName($options, $dateFrom, $dateTo);
        $this->view->advertsStat = $advertsStat;

        $affiliateStatModel = new Application_Model_DbTable_AffiliateCampaignStatistics();
        $subidStat = $affiliateStatModel->findTotalBySubidName($options, $dateFrom, $dateTo);
        $this->view->subidStat = $subidStat;
        $this->view->adverts = array_keys(array_intersect_key($advertsStat, $subidStat));
    }

    public function updateDataAction()
    {
        $this->_user->need_update = null;
        $this->_user->save();

        $statistics = new Application_Model_Statistic();
        $response = array();

        if(!$statistics->update($this->_user->id, MANUAL_UPDATE_LIMIT)) {
            $response['completed'] = true;

            $this->_user->last_update = new Zend_Db_Expr('NOW()');
            $this->_user->save();
        }

        $this->_helper->json($response);
    }

    /**
     * Check if page need to be refreshed and show notification
     * (it could happend if stat was updated by cron while page has been open)
     */
    public function refreshPageAction()
    {
        $prevUpdate = new DateTime($this->getParam('last_update'));
        $lastUpdate = new DateTime($this->_user->last_update);
        $notification = false;
        $notificationText = '';

        if(!$this->_user->last_update || ($lastUpdate > $prevUpdate)) {
            $notification = 'success';
            $notificationText = 'Your stats have now been updated';
        } else {
            $now = new DateTime();
            $planUpdate = $lastUpdate->add(new DateInterval("PT" . ($this->_user->stat_update_interval + 10) . "M"));
            if($planUpdate < $now) {
                $notification = 'error';
                $notificationText = 'Your planned stat sync failed';

                $statistic = new Application_Model_Statistic();
                if(($failedSources = $statistic->getFailedList($this->_user->id))) {
                    foreach($failedSources as $failedSource) {
                        $notificationText .= "<br><strong>{$failedSource->source}: {$failedSource->name}</strong>";
                    }
                }
            }
        }

        if($notification) {
            $session = new Zend_Session_Namespace('refresh_data');
            $session->notification = $notification;
            $session->notificationText = $notificationText;
        }

        $this->_helper->json($notification);
    }

    public function updateChartAction()
    {
        $chartType = $this->_getParam('chartType');

        if(is_array($this->_getParam('dateRange'))) {
            list($dateFrom, $dateTo) = $this->_getParam('dateRange');
            $this->view->from = $dateFrom;
            $this->view->to = $dateTo;
        } else {
            $dateFrom = App_Utils::abbrevToDate($this->_getParam('dateRange'));
            $dateTo = ($this->_getParam('dateRange') == 'yesterday') ? $dateFrom : null;
            $this->view->dateRange = $this->_getParam('dateRange');
        }

        $options = array('user_id'=>$this->_user->id);
        if($chartType == 'campaign-revenue') {
            $affiliateStat = new Application_Model_DbTable_AffiliateCampaignStatistics();
            $this->view->affiliateTotalStatByCampaigns = $affiliateStat->findTotalCampaignCommission($options, true, $dateFrom, $dateTo);
        } else {
            $adStat = new Application_Model_DbTable_AdCampaignStatistics();
            $this->view->totalAdPlatformSpend = $adStat->findTotalPlatformSpend($options, $dateFrom, $dateTo);
        }

        $campaigns = new Application_Model_DbTable_Campaigns();
        $this->view->campaigns = $campaigns->getPairs($this->_user->id);
        $this->view->dateRange = $this->_getParam('dateRange');
        $this->view->exportResources = $this->getInvokeArg('bootstrap')->getOption('exportResources');

        $this->_helper->viewRenderer("widget/{$chartType}");
    }

    public function updateTableAction()
    {
        $tableType = $this->_getParam('table');
        $campaignId = (int)$this->_getParam('campaign');

        if(is_array($this->_getParam('dateRange'))) {
            list($dateFrom, $dateTo) = $this->_getParam('dateRange');
            $this->view->from = $dateFrom;
            $this->view->to = $dateTo;
        } else {
            $dateFrom = App_Utils::abbrevToDate($this->_getParam('dateRange'));
            $dateTo = ($this->_getParam('dateRange') == 'yesterday') ? $dateFrom : null;
            $this->view->dateRange = $this->_getParam('dateRange');
        }

        $userOptions = array('user_id' => $this->_user->id);
        $campaignOptions = array('campaign_id' => $campaignId);

        $adStat = new Application_Model_DbTable_AdCampaignStatistics();
        $affiliateStat = new Application_Model_DbTable_AffiliateCampaignStatistics();

        if($tableType == 'campaigns-stat') {
            $this->view->affiliateStatByCampaigns = $affiliateStat->findTotalCampaignCommission($userOptions, false, $dateFrom, $dateTo); //mtd campaigns total statistics
            $this->view->adStatByCampaigns = $adStat->findTotalCampaignSpend($userOptions, $dateFrom, $dateTo); //mtd campaigns total statistics

            $detailedStat = new Application_Model_DbTable_AffiliateCampaignDetailedStatistics();
            $this->view->trend = $detailedStat->getTrend($this->_user->id);

            $campaigns = new Application_Model_DbTable_Campaigns();
            $this->view->campaigns = $campaigns->getPairs($this->_user->id);
        } elseif($tableType == 'traffic-stat') {
            $this->view->mtdAdPlatformSpend = $adStat->findTotalPlatformSpend($userOptions, $dateFrom, $dateTo);
        } elseif($tableType == 'campaign-performance') {
            $from = new DateTime($dateFrom);

            $this->view->affiliateTotal = $affiliateStat->findTotalCampaignCommission($campaignOptions, false, $dateFrom, $dateTo);
            $this->view->affiliateStatByTraffic = $affiliateStat->findStatByTraffic($campaignOptions, $dateFrom, $dateTo);
            $this->view->adTrafficStats = $adStat->findTotalPlatformSpend($campaignOptions, $dateFrom, $dateTo);
            $this->view->statDays = $from->diff(new DateTime($dateTo))->format('%d') + 1;
        } else {
            $response = array();
            $dateRanges = array('today', 'wtd', '7d', 'mtd', '30d', 'ytd', '12m');

            foreach($dateRanges as $dateRange) {
                $dateFrom = App_Utils::abbrevToDate($dateRange); $dateTo = null;
                $spend = $adStat->findTotalCampaignSpend($campaignOptions, $dateFrom, $dateTo);
                $affiliate = $affiliateStat->findTotalCampaignCommission($campaignOptions, false, $dateFrom, $dateTo);

                $prevDateRange = App_Utils::abbrevToDate($dateRange, null, true);
                $prevSpend = $adStat->findTotalCampaignSpend($campaignOptions, $prevDateRange[0], $prevDateRange[1]);
                $prevAffiliate = $affiliateStat->findTotalCampaignCommission($campaignOptions, false, $prevDateRange[0], $prevDateRange[1]);

                $response[$dateRange] = array('current' => $spend + $affiliate, 'prev' => $prevSpend + $prevAffiliate);
            }
            $this->view->campaignStat = $response;
            $this->view->statDateRanges = $dateRanges;
        }

        $this->view->exportResources = $this->getInvokeArg('bootstrap')->getOption('exportResources');

        $this->_helper->viewRenderer("widget/{$tableType}");
    }
}