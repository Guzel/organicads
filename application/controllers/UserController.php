<?php

class UserController extends Zend_Controller_Action
{
    protected $_user;

    public function init()
    {
        if (Zend_Registry::isRegistered('user')) {
            $this->_user = Zend_Registry::get('user');
        }
    }

    public function profileAction()
    {
        $form = new Application_Form_Profile();
        $form->populate($this->_user->toArray());
        $formId = '';

        if($this->getRequest()->isPost()) {
            $formId = $this->getRequest()->getPost('form_id');
            $currentForm = $form->getSubForm($formId);
            if($currentForm->isValid($this->getRequest()->getPost())) {
                $usersModel = new Application_Model_DbTable_Users();
                $values = $form->getValues();

                $this->view->success = true;
                switch ($formId) {
                    case 'edit-basic':
                    case 'edit-settings': 
                        $this->_user = $usersModel->save($values[$currentForm->getName()], $this->_user->id);
                        break;
                    case 'edit-password':
                        if(!Application_Model_User::isValidPassword($currentForm->current_password->getValue(), $this->_user->password)) {
                            $currentForm->current_password->addError('Password is invalid');
                            $this->view->success = false;
                        } else {
                            $passwordHash = Application_Model_User::getPasswordHash($currentForm->new_password->getValue());
                            $this->_user->password = $passwordHash;
                            $this->_user->save();
                        }
                        break;
                }
            }
        }

        $form->form_id->setValue($formId);
        $this->view->currentUser = $this->_user;
        $this->view->form = $form;
        $this->view->page = 'user-profile';
    }

    public function customImageAction()
    {
        //Remove current logo
        if($this->_getParam('delete')) {
            unlink(PUBLIC_PATH.'/assets/img/logo/'.$this->_user->logo);
            $this->_user->logo = null;
            $this->_user->save();
            $this->_helper->json(1); die();
        }

        $result = array();

        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'png,jpg')
            ->addValidator('Size', false, '2Mb')
            ->addValidator('ImageSize', false, array('maxwidth' => 140, 'maxheight' => 25));

        if (!$upload->isValid()) {
            $error = current($upload->getMessages());
        } else {
            $ext = App_Utils::getFileExtension($upload->getFileName());
            $logoName = $this->_user->id . $ext;

            $upload->addFilter('Rename', array('overwrite'=>true, 
                'target'=> PUBLIC_PATH.'/assets/img/logo/' . $logoName));
            try {
                $upload->receive();
                if($this->_user->logo && $this->_user->logo != $logoName) {
                    unlink(PUBLIC_PATH.'/assets/img/logo/'.$this->_user->logo);
                }

                $this->_user->logo = $logoName;
                $this->_user->save();

                $result = array('status' => 1, 'url' => '/assets/img/logo/'.$this->_user->logo);
            } catch (Zend_File_Transfer_Exception $e) {
                $error = $e->message();
            }
        }

        if(!empty($error)) {
            $result = array('status' => 0, 'message' => $error);
        }

        if($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->json($result);
        } else {
           //for browsers that don't support uploading via ajax,
           //we have used an iframe instead and the response is sent as a script
           echo '<script language="javascript" type="text/javascript">';
           echo 'var iframe = window.top.window.jQuery("#'.$_POST['temporary-iframe-id'].'").data("deferrer").resolve('.json_encode($result).');';
           echo '</script>';
        }
        die();
    }
}