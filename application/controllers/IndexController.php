<?php
/**
 * IndexController
 */
class IndexController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
    }

    public function fbUpdateAction()
    {
        $adId = (int)$this->_getParam('id');
        $credentials = $this->_getParam('credentials');

        $adModel = new Application_Model_DbTable_Ads();
        $source = $adModel->find($adId)->current();
        if(!$adId || !$source || $credentials !== $source->credentials) {
            die();
        }

        $decryptedCredentials = App_Utils::decryptString(base64_decode($source->credentials));
        $fbModel = new Application_Model_Facebook(unserialize($decryptedCredentials));
        $result = $fbModel->getCampaignsStatistics($source);
        echo json_encode($result);
        die();
    }
}