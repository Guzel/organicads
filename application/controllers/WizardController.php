<?php
class WizardController extends Zend_Controller_Action
{   
    protected $_user;

    public function init()
    {
        if (Zend_Registry::isRegistered('user')) {
            $this->_user = Zend_Registry::get('user');
        }

        $this->_helper->ajaxContext
            ->addActionContext('ad-pairing-list', array('html'))
            ->initContext();
    }

    public function accountConnectAction()
    {
        $exportResources = $this->getInvokeArg('bootstrap')->getOption('exportResources');
        $form = new Application_Form_AccountConnect(array('resources' => $exportResources));
        $response = array();

        if($this->getParam('code')) {
            $adWords = new Application_Model_AdWords();
            $response = $adWords->addAccount($this->_user->id, $this->getParam('code'));
        }

        if($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            if($form->getSubForm('selectapi')->isValid($postData)) {
                $resource = $form->getSubForm('selectapi')->getValue('resource');
                $credentialsForm = $form->getSubForm($resource);
                if($credentialsForm->isValid($postData)) {
                    $credentials = current($credentialsForm->getValues());

                    switch($resource) {
                        case 'linktrust':
                            $linkTrust = new Application_Model_LinkTrust($credentials);
                            $response = $linkTrust->getAffiliate($this->_user->id);

                            if(empty($response['errorMessage'])) {
                                $affiliateModel = new Application_Model_DbTable_Affiliates();
                                $affiliateModel->saveCredentials($response->id, $credentials);

                                $response = $linkTrust->getCampaignsStatistics($response);
                            }
                            break;
                        case 'leadnomics':
                            $leadnomics = new Application_Model_Leadnomics($credentials);
                            $affiliate = $response = $leadnomics->addAccount($this->_user->id);

                            if(empty($response['errorMessage'])) {
                                $response = $leadnomics->getCampaignsStatistics($affiliate);
                            }
                            break;
                        case 'cakemarketing':
                            $cakemarketing = new Application_Model_Cakemarketing($credentials);
                            $affiliate = $response = $cakemarketing->addAccount($this->_user->id);

                            if(empty($response['errorMessage'])) {
                                $response = $cakemarketing->getCampaignsStatistics($affiliate);
                            }
                            break;
                         case 'bingoroyalty':
                            $bingoroyalty = new Application_Model_Bingoroyalty($credentials);
                            $affiliate = $response = $bingoroyalty->addAccount($this->_user->id);

                            if(empty($response['errorMessage'])) {
                                $response = $bingoroyalty->getCampaignsStatistics($affiliate);
                            }
                            break;
                        case 'hasoffers':
                            $hasoffers = new Application_Model_Hasoffers($credentials);
                            $affiliate = $response = $hasoffers->addAccount($this->_user->id);

                            if(empty($response['errorMessage'])) {
                                $response = $hasoffers->getCampaignsStatistics($affiliate);
                            }
                            break;
                        case 'facebook':
                            $facebook = new Application_Model_Facebook($credentials);
                            $ads = $response = $facebook->addAccount($this->_user->id);
                            if(empty($response['errorMessage'])) {
                                $response = $facebook->getCampaignsStatistics($ads);
                            }
                            break;
                        case 'google':
                            $adWords = new Application_Model_AdWords();
                            $response = $adWords->getRedirectUrl($this->_user->id, $credentials['email']);
                            if(!empty($response['redirect_url'])) {
                                $this->redirect($response['redirect_url']); die();
                            }
                            break;
                        case 'bing':
                            $bingAds = new Application_Model_BingAds($credentials);
                            $ads = $response = $bingAds->addAccounts($this->_user->id);

                            if(empty($response['errorMessage'])) {
                                $response = $bingAds->getCampaignsStatistics($ads);
                            }
                            break;
                        case 'fiftyonred':
                            $fiftyonred = new Application_Model_FiftyOnRed($credentials);
                            $ads = $response = $fiftyonred->addAccounts($this->_user->id);

                            if(empty($response['errorMessage'])) {
                                $response = $fiftyonred->getCampaignsStatistics($ads);
                            }
                            break;
                        case 'yahoo':
                            $yahooStream = new Application_Model_YahooStreamads($credentials);
                            $ad = $response = $yahooStream->addAccount($this->_user->id);

                            if(empty($response['errorMessage'])) {
                                $response = $yahooStream->getCampaignsStatistics($ad);
                            }
                            break;
                        case 'trafficvance':
                            $trafficvance = new Application_Model_Trafficvance($credentials);
                            $ad = $response = $trafficvance->addAccount($this->_user->id);

                            if(empty($response['errorMessage'])) {
                                $response = $trafficvance->getCampaignsStatistics($ad);
                            }
                            break;
                        case 'leadimpact':
                            $leadimpact = new Application_Model_Leadimpact($credentials);
                            $ad = $response = $leadimpact->addAccount($this->_user->id);

                            if(empty($response['errorMessage'])) {
                                $response = $leadimpact->getCampaignsStatistics($ad);
                            }
                            break;
                        case 'hastraffic':
                            $leadimpact = new Application_Model_Hastraffic($credentials);
                            $ad = $response = $leadimpact->addAccount($this->_user->id);

                            if(empty($response['errorMessage'])) {
                                $response = $leadimpact->getCampaignsStatistics($ad);
                            }
                            break;
                        case 'zeropark':
                            $zeropark = new Application_Model_Zeropark($credentials);
                            $ad = $response = $zeropark->addAccount($this->_user->id);

                            if(empty($response['errorMessage'])) {
                                $response = $zeropark->getCampaignsStatistics($ad);
                            }
                            break;
                        case 'airpush':
                            $airpush = new Application_Model_Airpush($credentials);
                            $ad = $response = $airpush->addAccount($this->_user->id);

                            if(empty($response['errorMessage'])) {
                                $response = $airpush->getCampaignsStatistics($ad);
                            }
                            break;
                    }
                }
            }
        }

        $adsModel = new Application_Model_DbTable_Ads();
        $this->view->fbAds = $adsModel->getAdsByUserId($this->_user->id, Application_Model_DbTable_Ads::FACEBOOK);

        $this->view->response = $response;
        $this->view->form = $form;
        $this->view->resources = json_encode($exportResources);
        $this->view->page = 'account-connect';
    }

    public function pairingAction()
    {
        $form = new Application_Form_CampaignPairing();
        $affiliateCampaignsModel = new Application_Model_DbTable_AffiliateCampaigns();
        $adCampaignsModel = new Application_Model_DbTable_AdCampaigns();
        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $campaignSubids = new Application_Model_DbTable_CampaignSubids();

        if($campaignId = $this->getParam('id')) {
            $campaign = $campaignsModel->find($campaignId)->current();
            $form->populate($campaign->toArray());
            $this->view->campaign = $campaign;
            $this->view->subidSettings = $campaignSubids->findByCampaign($campaignId);
        }

        if($this->getRequest()->isPost()){
            $isValid = $form->isValid($this->getRequest()->getPost());
            if($this->_request->isXmlHttpRequest()) {
                $response['valid'] = $isValid;
                $response['errors'] = $form->getMessages();
                echo $this->_helper->json($response); die();
            }

            if($isValid) {
                $campaign = $campaignsModel->addNew($this->_user->id, $form->getValues());

                $affiliateCampaignsModel->updateCampaignId($campaign->id, $form->affiliate_campaigns->getValue());
                $adCampaignsModel->updateCampaignId($campaign->id, $form->ad_campaigns->getValue());

                $campaignSubids->create($campaign->id, $this->getRequest()->getPost('subid_settings'));

                $this->view->success = true;
            }
        }

        $this->view->form = $form;
        $this->view->page = 'pairing';
    }

    public function adPairingAction()
    {
        $form = new Application_Form_AdPairing();
        $campaignsModel = new Application_Model_DbTable_Campaigns();

        if($this->getRequest()->isPost()) {
            if($form->isValid($this->getRequest()->getPost())) {
                $campaign = $campaignsModel->findByName($form->campaign->getValue(), $this->_user->id);

                $advertSubidModel = new Application_Model_DbTable_AdvertSubidPairs();
                $advertSubidModel->save($this->_user->id, $campaign->id, $form->advertslist->getValue());

                /*$subidsModel = new Application_Model_DbTable_AffiliateSubids();
                $subidsModel->updateCampaignId($campaign->id, $form->advertslist->getValue());

                $advertsListModel = new Application_Model_DbTable_AdvertsList();
                $advertsListModel->updateCampaignId($campaign->id, $form->advertslist->getValue());*/

                $this->view->success = true;
            }
        }

        $this->view->form = $form;
        $this->view->page = 'ad-pairing';
    }

    public function adPairingListAction()
    {
        $selectedAdCampaigns = $this->_getParam('ad_campaigns');
        $selectedAdCampaigns = explode(',', $selectedAdCampaigns);

        //Advert List updating along with ad stat, we don't need to make API calls here
        $advertsListModel = new Application_Model_DbTable_AdvertsList();
        $advertsList = $advertsListModel->getByAdCampaignIds($selectedAdCampaigns);
        $this->view->advertsList = $advertsList;
        $this->view->exportResources = $this->getInvokeArg('bootstrap')->getOption('exportResources');

        $selectedAffiliateCampaigns = $this->_getParam('affiliate_campaigns');
        $selectedAffiliateCampaigns = explode(',', $selectedAffiliateCampaigns);

        //Find all connected Affiliate accounts
        $affiliatesModel = new Application_Model_DbTable_Affiliates();
        $affiliates = $affiliatesModel->getAffiliatesByUserId($this->_user->id, $affiliatesModel->subid_allowed);

        //Find all Affiliate Campaigns
        foreach($affiliates as $affiliate) {
            $affiliateCampaignsModel = new Application_Model_DbTable_AffiliateCampaigns();
            $campaigns = $affiliateCampaignsModel->getPairs($this->_user->id, $affiliate->id);

            //Find Affiliate campaigns API IDs
            $campaignsApiIds = array_keys(array_intersect($campaigns, $selectedAffiliateCampaigns));
            if($campaignsApiIds) {
                $decryptedCredentials = App_Utils::decryptString(base64_decode($affiliate->credentials));
                $sourceModel = Application_Model_SourceAdapter::getInstanse($affiliate->source, unserialize($decryptedCredentials));

                $sourceModel->getSubIds($affiliate, $campaignsApiIds);
            }
        }

        $affiliateSubidsModel = new Application_Model_DbTable_AffiliateSubids();
        $subIds = $affiliateSubidsModel->fetchAll($affiliateSubidsModel->select()->where('affiliate_campaign_id IN (?)', $selectedAffiliateCampaigns));
        $this->view->subIds = $subIds;

        $advertSubidModel = new Application_Model_DbTable_AdvertSubidPairs();
        $this->view->pairs = $advertSubidModel->getAdPairs($this->_user->id);
    }

    public function settingsAction()
    {
        $affiliatesModel = new Application_Model_DbTable_Affiliates();
        $adsModel = new Application_Model_DbTable_Ads();
        $campaigns = new Application_Model_DbTable_Campaigns();

        if(($resource = $this->_getParam('resource')) && ($id = (int)$this->_getParam('id'))) {
            if($resource == 'affiliate') {
                $source = $affiliatesModel->find($id)->current();
            } elseif($resource == 'ad') {
                $source = $adsModel->find($id)->current();
            } elseif($resource == 'campaign') {
                $source = $campaigns->find($id)->current();
            }

            if($source->user_id == $this->_user->id) {
                $operation = $this->_getParam('operation');
                $value = $this->_getParam('value');
                if($operation == 'delete') {
                    $source->delete();
                } elseif($operation == 'edit') {
                    $field = $this->_getParam('field');
                    if($field == 'password') {
                        $currentCredentials = unserialize(App_Utils::decryptString(base64_decode($source->credentials)));
                        $currentCredentials['password'] = $value;
                        $source->credentials = base64_encode(App_Utils::encryptString(serialize($currentCredentials)));
                    } else {
                        $source->{$field} = $value;
                    }
                    $source->save();
                }
                $result['success'] = true;
            }
            $this->_helper->json($result);
        }

        $this->view->affiliates = $affiliatesModel->getAffiliatesByUserId($this->_user->id);
        $this->view->ads = $adsModel->getAdsByUserId($this->_user->id);
        $this->view->campaigns = $campaigns->findByUser($this->_user->id);
        $this->view->exportResources = $this->getInvokeArg('bootstrap')->getOption('exportResources');
        $this->view->page = 'wizard-settings';
    }

    public function getAffiliatesAction()
    {
        $campaignName = $this->_getParam('campaign');
        $adPairing = $this->_getParam('ad_pairing');
        $exportResources = $this->getInvokeArg('bootstrap')->getOption('exportResources');
        $data = array();

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $campaign = $campaignsModel->findByName($campaignName, $this->_user->id);
        $campaignId = $campaign ? $campaign->id : null;

        $affiliateCampaignsModel = new Application_Model_DbTable_AffiliateCampaigns();
        $affiliateCampaigns = $affiliateCampaignsModel->getNotPairedCampaigns($this->_user->id, $campaignId);

        //For "Ad Performance" Wizard
        $subidsModel = new Application_Model_DbTable_AffiliateSubids();
        $pairedAffiliateCampaigns = $subidsModel->getPairedAffiliateCampaigs($campaignId);

        if($affiliateCampaigns) {
            $affiliatesModel = new Application_Model_DbTable_Affiliates();
            $affiliates = $affiliatesModel->getAffiliatesById(array_keys($affiliateCampaigns));

            foreach($affiliates as $affiliate) {
                //include only Affiliate Sources with existing SubId parsing (for Ad Performance Wizard)
                if($adPairing && !in_array($affiliate->source, $affiliatesModel->subid_allowed)) {
                    continue;
                }

                $source = array(
                    'name' => $exportResources['affiliate'][$affiliate->source],
                    'type' => 'folder',
                    'id' => ucfirst($affiliate->source),
                    'source' => 'affiliate'
                );

                $item = array(
                    'name' => $affiliate->name,
                    'type' => 'folder',
                    'id' => $affiliate->id
                );

                if($affiliateCampaigns[$affiliate->id]) {
                    $children = array();
                    foreach($affiliateCampaigns[$affiliate->id] as $campaign){
                        //For "Ad Performance" Wizard
                        if($adPairing && !$campaign->campaign_id) { 
                            continue; 
                        }
                        $hasAdPair = !empty($pairedAffiliateCampaigns[$campaign->id]);

                        $name = $campaign->api_id . ' — ' . $campaign->name . ($campaign->payout ? " — {$campaign->payout}" : '');
                        $spanClass = $campaign->date ? 'red' : ''; //red if affiliate had revenue in last 30 days
                        $children[$campaign->id] = array('name' => "<span class='{$spanClass}'>{$name}</span>", 
                            'type' => 'item', 'id' => $campaign->id, 
                            'campaign_id' => $campaign->campaign_id,
                            'revenue_date' => $campaign->date,
                            'ad_pair' => (int)$hasAdPair);
                    }
                    $item['additionalParameters'] = array('children' => $children);
                }

                if(!$children) {
                    continue;
                }

                $children = array($affiliate->id => $item);
                if(!empty($data[$affiliate->source])) {
                    $children = $data[$affiliate->source]['additionalParameters']['children'] + $children;
                }

                $data[$affiliate->source] = $source;
                $data[$affiliate->source]['additionalParameters']['children'] = $children;
            }
        }

        $result['status'] = 'OK';
        $result['data'] = $data;

        $this->_helper->json($result);
    }

    public function getAdsAction()
    {
        $campaignName = $this->_getParam('campaign');
        $adPairing = $this->_getParam('ad_pairing');
        $exportResources = $this->getInvokeArg('bootstrap')->getOption('exportResources');
        $data = array();

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $campaign = $campaignsModel->findByName($campaignName, $this->_user->id);
        $campaignId = $campaign ? $campaign->id : null;

        $adCampaignsModel = new Application_Model_DbTable_AdCampaigns();
        $adCampaigns = $adCampaignsModel->getNotPairedCampaigns($this->_user->id, $campaignId);

        //For "Ad Performance" Wizard
        $advertListModel = new Application_Model_DbTable_AdvertsList();
        $paredAdCampaigns = $advertListModel->getPairedAdCampaigs($campaignId);

        if($adCampaigns) {
            $adsModel = new Application_Model_DbTable_Ads();
            $ads = $adsModel->getAdsById(array_keys($adCampaigns));

            $groupAds = array();
            foreach($ads as $ad) {
                //include only Ad Sources with existing Adverts parsing (for Ad Performance Wizard)
                if($adPairing && !in_array($ad->source, $adsModel->advert_allowed)) {
                    continue;
                }
                $groupAds[$ad->source][$ad->parent][] = $ad;
            }

            foreach($groupAds as $adSource => $groupAds1) {
                $childrens = array();

                foreach($groupAds1 as $parent => $ads) {
                    $parentChilds = array();

                    foreach($ads as $ad) {
                        $item = array(
                            'name' => $ad['name'],
                            'type' => 'folder',
                            'id' => $ad['id'],
                            'source' => $ad['source']
                        );
                        if($adCampaigns[$ad['id']]) {
                            $children = array();
                            foreach($adCampaigns[$ad['id']] as $campaign){
                                //For "Ad Performance" wizard
                                if($adPairing && !$campaign->campaign_id) {
                                    continue;
                                }
                                $hasAdPair = !empty($paredAdCampaigns[$campaign->id]);

                                $children[$campaign->id] = array('name' => $campaign->name, 
                                    'type' => 'item', 'id' => $campaign->id, 'campaign_id' => $campaign->campaign_id,
                                    'ad_pair' => (int)$hasAdPair, 'ad_id' => $ad['id']);
                            }
                            $item['additionalParameters'] = array('children' => $children);
                        }

                        if(!$children && !$parentChilds) {
                            continue;
                        }

                        if($parent) {
                            $parentChilds = $parentChilds + array($ad['id']=>$item);
                        } else {
                            $childrens = $childrens + array($ad['id']=>$item);
                        }
                    }

                    if($parent && $parentChilds) {
                        $parent = array(
                            'name' => $ad->parent,
                            'type' => 'folder',
                            'id' => $ad->parent,
                            'additionalParameters' => array('children' => $parentChilds)
                        );
                        $childrens = $childrens + array($ad->parent => $parent);
                    }
                }

                if(!empty($data[$adSource])) {
                    $childrens = $data[$adSource]['additionalParameters']['children'] + $childrens;
                }

                if($childrens) {
                    $source = array(
                        'name' => $exportResources['ad'][$adSource],
                        'type' => 'folder',
                        'id' => $adSource,
                        'source' => 'ad'
                    );
                    $data[$adSource] = $source;
                    $data[$adSource]['additionalParameters']['children']=$childrens;
                }
            }
        }

        $result['status'] = 'OK';
        $result['data'] = $data;

        $this->_helper->json($result);
    }
}