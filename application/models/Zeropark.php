<?php

class Application_Model_Zeropark
{
    protected $_mainUrl = 'https://panel.zeropark.com/';
    protected $_loginUri = 'signin';
    protected $_reportUri = 'bidding/getCampaigns';
    protected $_email;
    protected $_password;

    protected $_dateFrom;
    protected $_dateTo;

    protected $_cookieJar;
    protected $_logger;
    
    protected $_errors = array(
        'login_error' => 'Login failed. Please, check your credentials',
        'server_unavailable' => 'Zeropark Server not responding',
        'account_connected' => 'This Ad Account is already connected to your Leadssistant account'
    );

    /**
     * Class constructor
     * 
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->_email = !empty($options['email']) ? $options['email'] : null;
        $this->_password = !empty($options['password']) ? $options['password'] : null;

        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/zeropark');
        $this->_logger = new Zend_Log($writer);
    }

    /**
     * Add new account to DB
     * 
     * @param int $userId
     * @return Zend_Db_Table_Row|array
     */
    public function addAccount($userId)
    {
        $result = $this->login();
        if(!is_array($result)) {
            $result = array();

            $adsModel = new Application_Model_DbTable_Ads();
            if($adsModel->getAdsByName($userId, $this->_email, Application_Model_DbTable_Ads::ZEROPARK)) {
                $result['errorMessage'] = $this->_errors['account_connected'];
            } else {
                $credentials = array('email' => $this->_email, 'password' => $this->_password);
                $options = array('source' => Application_Model_DbTable_Ads::ZEROPARK,
                    'name' => $this->_email, 'credentials' => $credentials);

                $result = $adsModel->create($userId, $options); 
             }
        }

        return $result;
    }

     /**
     * Add Campaign
     * 
     * @param Zend_Db_Table_Row $ad
     * @param string $campaignApiId
     * @param string $campaignName
     * @param int $adCampaignId Advert Campaign Id. Pass to perform update
     * 
     * @return Zend_Db_Table_Row
     */
    public function addCampaign($ad, $campaignApiId, $campaignName, $adCampaignId = null)
    {
        if(!$campaignApiId || !$campaignName) {
            return;
        }

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $keywords = $campaignsModel->getKeywords($ad->user_id);

        $data = array('api_id' => $campaignApiId, 'user_id' => $ad->user_id, 'ad_id' => $ad->id,
            'name' => $campaignName, 'source' => Application_Model_DbTable_Ads::ZEROPARK);

         //automatic Pairing
        if(false !== ($campaignId = App_Utils::findKeyword($campaignName, $keywords))) {
            $data['campaign_id'] = $campaignId;
        }

        $adCampaigns = new Application_Model_DbTable_AdCampaigns();
        return $adCampaigns->create($data, $adCampaignId);
    }

    /**
     * Add/Update campaigns statistics
     * 
     * @param Zend_Db_Table_Row $ad
     * @param bool $updateCampaigns Update the campaigns
     * 
     * @return array
     */
    public function getCampaignsStatistics($ad, $updateCampaigns = true)
    {
        $this->login();

        $this->_dateFrom = $this->getDateFrom();
        $this->_dateTo = $this->getDateTo();

        if($ad->last_statistic_date) {
            $lastUpdate = new DateTime($ad->last_statistic_date);
            $this->_dateFrom = $lastUpdate->sub(new DateInterval('P1D'));
        }

        if($statistics = $this->_getStat()) {
            $adCampaigns = new Application_Model_DbTable_AdCampaigns();
            $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

            $campaignStat = new Application_Model_DbTable_AdCampaignStatistics();
            $campaignStat->getAdapter()->beginTransaction();

            //delete crossing stat:
            $campaignStat->deleteStatByDate($ad->id, $this->_dateFrom->format("Y-m-d"));

            foreach($statistics as $date => $row) {
                foreach($row as $stat) {
                    $campaignApiId = $stat->id;
                    if(empty($campaignIds[$campaignApiId])) {
                        //add new Campaign
                        $newCampaign = $this->addCampaign($ad, $campaignApiId, $stat->name);
                        $campaignIds[$newCampaign->api_id] = $newCampaign->id;
                    }

                    $newStat = $campaignStat->createRow();
                    $newStat->user_id = $ad->user_id;
                    $newStat->ad_id = $ad->id;
                    $newStat->ad_campaign_id = $campaignIds[$campaignApiId];
                    $newStat->date = strftime("%Y-%m-%d", strtotime($date));
                    $newStat->clicks = (int)$stat->clicks;
                    $newStat->spend = (float)$stat->spend;
                    $newStat->status = strtolower($stat->campaignState);
                    $newStat->added = new Zend_Db_Expr('NOW()');
                    $newStat->save();
                }
            }
            $campaignStat->getAdapter()->commit();
        }

        $ad->last_statistic_date = new Zend_Db_Expr('CURDATE()');
        $ad->last_update = new Zend_Db_Expr('NOW()');
        $ad->save();

        $result['success'] = true;
        return $result;
    }

    /**
     * Find statistics
     * 
     * @return array
     */
    protected function _getStat()
    {
        $postData = array(
            'interval' => 'CUSTOM',
            'limit' => 100,
            'page' => 0,
            'showDeleted' => false
        );

        $statistics = array();
        $startDate = clone $this->_dateFrom;
        while($startDate <= $this->_dateTo) {
            $postData['startDate'] = $postData['endDate'] = $startDate->format('d/m/Y');

            $content = $this->_sendRequest($this->_mainUrl . $this->_reportUri,  null, $postData);
            if(!is_array($content)) {
                $content = json_decode($content);
                if(isset($content->response)) {
                    $statDay = $startDate->format('Y-m-d');
                    $statistics = array_merge($statistics, array($statDay => (array)$content->response));
                }
            }

            $startDate->add(new DateInterval('P1D'));
        }

        return $statistics;
    }

    /**
     * Login to https://panel.zeropark.com/signin
     * 
     * @return string|array
     */
    public function login()
    {
        $this->_cookieJar = new Zend_Http_CookieJar();
        $loginCredentials = array('email' => $this->_email, 'password' => $this->_password, 'wp-submit' =>'Log in');

        try {
            $login = $this->_sendRequest($this->_mainUrl . $this->_loginUri, null, $loginCredentials);
            if(!$login || false === strpos($login, 'signout')) {
                throw new Exception($this->_errors['login_error']);
            }
            return true;
        } catch (Application_Model_Exceptions_Curl $e) {
            $result['errorMessage'] = $e->getMessage();
        }

        $this->_logger->err("{$this->_email} {$result['errorMessage']}");
        return $result;
    }

    /**
     * Get DateFrom
     * Set report starting date
     * 
     * @return DateTime
     **/
    public function getDateFrom()
    {
        if(!$this->_dateFrom) {
            $today = new DateTime();
            $this->_dateFrom = $today->sub(new DateInterval('P10D'));
        }
        return $this->_dateFrom;
    }

    /**
     * Get DateTo
     * Set report end date
     * 
     * @return DateTime
     **/
    public function getDateTo()
    {
        if(!$this->_dateTo) {
            $this->_dateTo = new DateTime();
        }
        return $this->_dateTo;
    }

    /**
     * Get XPath
     * 
     * @param string $content
     * @return \DOMXpath
     */
    protected function getXPath($content)
    {
        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($content);
        libxml_use_internal_errors(false);
        return new DOMXpath($dom);
    }

    /**
     * Send CURL request
     * 
     * @param string $url
     * @param array $getParams
     * @param array $postParams
     * 
     * @return array|string
     */
    protected function _sendRequest($url, $getParams = array(), $postParams = array())
    {
        $result = array();

        $config = array(
            'timeout' => 30,
            'useragent' => "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)",
            'curloptions' => array(CURLOPT_RETURNTRANSFER => 1, CURLOPT_SSL_VERIFYPEER => false)
        );

        $adapter = new Zend_Http_Client_Adapter_Curl();
        $client = new Zend_Http_Client($url, $config);

        $client->setAdapter($adapter);
        $client->setCookieJar($this->_cookieJar);

        if($postParams) {
            $client->setParameterPost($postParams);
            $client->setMethod(Zend_Http_Client::POST);
        } else {
            $client->setParameterGet($getParams);
            $client->setMethod(Zend_Http_Client::GET);
        }

        try{
            $response = $client->request();
            if ($response->isSuccessful()){
                return $response->getBody();
            } else {
                $result['errorMessage'] = $this->_errors['server_unavailable'] . " URL: {$url} Message: " . $response->getMessage();
            }
        } catch (Zend_Http_Client_Exception $e) {
            $result['errorMessage'] = $e->getMessage();
        }

        $this->_logger->err("{$this->_email}. URL {$url}: {$result['errorMessage']}");
        return $result;
    }
}