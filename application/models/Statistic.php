<?php

class Application_Model_Statistic
{
    protected $_logger;

    public function __construct()
    {
        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . '/../data/logs/upload-stat');
        $this->_logger = new Zend_Log($writer);
    }

    /**
     * Update Affiliate/Ads statistics throught APIs calls
     * 
     * @param int $userId
     * @param int|null $limit
     * @return int
     */
    public function update($userId, $limit = null)
    {
        $affiliateModel = new Application_Model_DbTable_Affiliates();
        $sources = $affiliateModel->getAffiliateToUpdate($userId);

        $this->_proceed($sources, $limit); //update Affiliates

        if(!$limit || $sources->count() < $limit) {
            $limit = $limit ? $limit - $sources->count() : null;

            $adsModel = new Application_Model_DbTable_Ads();
            $sources = $adsModel->getAdsToUpdate($userId); //update Ads
            $this->_proceed($sources, $limit);
        }

        return $sources->count();
    }

    /**
     * Call APIs, update statistics for given sources
     * 
     * @param Zend_Db_Table_Rowset $sources
     * @param int|null $limit
     */
    protected function _proceed($sources, $limit = null)
    {
        foreach($sources as $key => $source) {
            $decryptedCredentials = App_Utils::decryptString(base64_decode($source->credentials));
            $credentials = unserialize($decryptedCredentials);

            $this->_logger->info("Proceed {$source->source} ID: {$source->id}");

            $model = Application_Model_SourceAdapter::getInstanse($source->source, $credentials);
            if($model) {
                if(!$source->disabled) {
                    if($source->source == 'facebook' && !empty($credentials['access_token'])) {
                        $url = "http://leadssistant.organicads.com/index/fb-update?id={$source->id}&credentials=" . urlencode($source->credentials);
                        $result = json_decode(file_get_contents($url), true);
                        $this->_logger->info("FB API Call: {$url}. Result: " . serialize($result));
                    } else {
                        $result = $model->getCampaignsStatistics($source, true);
                    }
                }
                $source->last_update = new Zend_Db_Expr('NOW()');

                $source->error = null;
                if(!empty($result['errorMessage'])) {
                    $source->error = $result['errorMessage'];
                    $this->_logger->err($result['errorMessage']);
                }
                $source->save();
            }

            $this->_logger->info("Completed {$source->source} ID: {$source->id}");

            if($limit && ($limit == $key+1)) {
                break;
            }
        }
    }

    /**
     * Return list of sources that returned an error in the last attempt of update
     * 
     * @param int $userId
     * @return array
     */
    public function getFailedList($userId)
    {
        $affiliateModel = new Application_Model_DbTable_Affiliates();
        $affiliates = $affiliateModel->getAffiliatesByUserId($userId)->toArray();

        $adsModel = new Application_Model_DbTable_Ads();
        $ads = $adsModel->getAdsByUserId($userId)->toArray();

        $sources = array_merge($affiliates, $ads);

        $failed = array();
        foreach($sources as $source) {
            $source= (object)$source;
            if(!$source->error) {
                continue;
            }
            $failed[] = $source;
        }

        return $failed;
    }
}