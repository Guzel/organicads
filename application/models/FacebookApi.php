<?php
use Facebook\Entities\AccessToken;
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;

class Application_Model_FacebookApi
{
    protected $_accessToken;
    protected $_fbSession;
    protected $_accountId;
    public $redirectUrl = 'http://leadssistant.organicads.com/index/fb-test';

    public function __construct($options = array())
    {
        FacebookSession::setDefaultApplication(FACEBOOK_APP_ID, FACEBOOK_APP_SECRET);
        if(!empty($options['access_token'])) {
            $accessToken = new AccessToken($options['access_token']);
            /*if(!$accessToken->isValid()) {
                $accessToken->extend();
            }*/
            $this->_fbSession = new FacebookSession($accessToken);
            $this->_accessToken = $options['access_token'];
        }
        if(!empty($options['account_id'])) {
            $this->_fbAccountId = $options['account_id'];
        }
    }

    public function getRedirectUrl()
    {
        $fbHelper = new FacebookRedirectLoginHelper($this->redirectUrl);
        return $fbHelper->getLoginUrl();
    }

    public function getStatistics($fields = array())
    {
        $getParams = http_build_query($fields);
        $request = new FacebookRequest($this->_fbSession, 'GET', "/act_{$this->_fbAccountId}/reportstats?{$getParams}" );
        $response = $request->execute();

        return $response->getResponse();
    }
}