<?php

include 'Twilio/Twilio.php';

class Application_Model_Twilio
{
    //Test Data
    //protected $_sid = 'AC3dab14818d6369b242a34988fc5fc17a';
    //protected $_authToken = '1a3d7956d86464d3b57b75fc9f7b66d8';
    protected $_sid = 'ACd2e17de9c27adb402afa9171f4f940d5';
    protected $_authToken = '2028aeee4a0e11896163320703878f6c';
    protected $_from = '+19542464501';
    protected $_client;

    const SMS_LIMIT = 1600;

    public function __construct()
    {
        $this->_client = new Services_Twilio($this->_sid, $this->_authToken);
    }

    public function sendMessage($to, $message, $from = null)
    {
        $message = substr($message, 0, self::SMS_LIMIT);
        $from = $this->addUSCode($from ? $from : $this->_from);
        $to = $this->addUSCode($to);

        $sms = $this->_client->account->messages->sendMessage($from, $to, $message);
        return $sms->sid;
    }

    protected function addUSCode($phone)
    {
        if(0 !== strpos($phone, '+1')) {
            $phone = '+1' . $phone;
        }
        return $phone;
    }
}