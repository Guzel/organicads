<?php

include 'BingAds/CustomerManagementClasses.php';
include 'BingAds/ReportingClasses.php';
include 'BingAds/ClientProxy.php';

use BingAds\Reporting\SubmitGenerateReportRequest;
use BingAds\CustomerManagement\GetCustomersInfoRequest;
use BingAds\CustomerManagement\ApplicationType;
use BingAds\CustomerManagement\GetAccountsInfoRequest;

use BingAds\Reporting\CampaignPerformanceReportRequest;

use BingAds\Reporting\ReportFormat;
use BingAds\Reporting\ReportAggregation;
use BingAds\Reporting\AccountThroughAdGroupReportScope;
use BingAds\Reporting\CampaignReportScope;
use BingAds\Reporting\Date;
use BingAds\Reporting\ReportTime;
use BingAds\Reporting\ReportTimePeriod;
use BingAds\Reporting\KeywordPerformanceReportFilter;
use BingAds\Reporting\DeviceTypeReportFilter;
use BingAds\Reporting\KeywordPerformanceReportColumn;
use BingAds\Reporting\PollGenerateReportRequest;
use BingAds\Reporting\ReportRequestStatusType;

class Application_Model_BingAds
{
    protected $_apiUsername;
    protected $_apiPassword;
    protected $_apiToken;

    protected $_dateFrom;
    protected $_dateTo;

    protected $_apiAccountUrl = 'https://sharedservices.adcenterapi.microsoft.com/Api/';
    protected $_apiAdsUrl = 'https://adcenterapi.microsoft.com/api/advertiser/v8/';

    protected $_reportPath;
    protected $_logger;

    public function __construct($options = array())
    {
        $this->_apiUsername = !empty($options['username']) ? $options['username'] : null;
        $this->_apiPassword = !empty($options['password']) ? $options['password'] : null;
        $this->_apiToken = !empty($options['token']) ? $options['token'] : null;

        $this->_reportPath = PUBLIC_PATH . "/../data/bing_reports/";

        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/bing');
        $this->_logger = new Zend_Log($writer);
    }

    public function addAccounts($userId)
    {
        $wsdl = $this->_apiAccountUrl . 'CustomerManagement/v8/CustomerManagementService.svc?wsdl';
        $proxy = new ClientProxy($this->_apiUsername, $this->_apiPassword, $this->_apiToken, $wsdl);
        $customer = $result = $this->getCustomers('', 100, $proxy);

        if(!is_array($customer)) {
            $result = array();
            $adsModel = new Application_Model_DbTable_Ads();

            $credentials = array('username' => $this->_apiUsername, 'password' => $this->_apiPassword, 'token' => $this->_apiToken);
            $ads = array();
            foreach ($customer->CustomerInfo as $customer) {
                if ($customer->Id) {
                    $account = $this->getAccounts($customer->Id, $proxy);
                    if(is_array($account)) {
                        $result = $account; continue;
                    }

                    foreach ($account->AccountInfo as $account) {
                        if (!$account->Id) {
                            $result['errorMessage'] = 'The customer does not have any accounts';
                            continue;
                        }

                        if($adsModel->getAdsByAccountId($userId, $account->Id, Application_Model_DbTable_Ads::BING)) {
                            $result['errorMessage'] = 'This Account is already connected to your account';
                        } else {
                            $credentials['customer_id'] = $customer->Id;
                            $options = array('account_id' => $account->Id, 'source' => Application_Model_DbTable_Ads::BING,
                                'name' => $customer->Name, 'credentials' => $credentials);
                            $ads[] = $ad = $adsModel->create($userId, $options);
                            $this->addCampaigns($userId, $ad);
                        }
                    }
                }
            }

            if(!empty($ads)) {
                $result = $ads;
            }
        }

        return $result;
    }

    public function addCampaigns($userId, $ad)
    {
        $wsdl = $this->_apiAdsUrl . 'CampaignManagement/CampaignManagementService.svc?wsdl';

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $keywords = $campaignsModel->getKeywords($userId);

        $proxy = new ClientProxy($this->_apiUsername, $this->_apiPassword, $this->_apiToken, $wsdl, $ad->account_id);

        $campaigns = $this->getCampaigns($ad->account_id, $proxy);

        $adCampaigns = new Application_Model_DbTable_AdCampaigns();
        $campaignIds = $adCampaigns->getPairs($userId, $ad->id);

        if(!empty($campaigns->Campaign)) {
            foreach($campaigns->Campaign as $campaign) {
                $campaignApiId = (int)$campaign->Id;
                if(!empty($campaignIds[$campaignApiId])) { continue; }

                $data = array('api_id' => $campaignApiId, 'user_id' => $userId, 'ad_id' => $ad->id,
                    'name' => $campaign->Name, 'monthly_budget' => $campaign->MonthlyBudget,
                    'daily_budget' => $campaign->DailyBudget, 'status' => strtolower($campaign->Status),
                    'source' => Application_Model_DbTable_Ads::BING);

                //automatic Pairing
                if(false !== ($campaignId = App_Utils::findKeyword($campaign->Name, $keywords))) {
                    $data['campaign_id'] = $campaignId;
                }

                $newCampaign = $adCampaigns->create($data);
                $campaignIds[$newCampaign->api_id] = $newCampaign->id;
            }
        }
        return true;
    }

    public function getCampaignsStatistics($ads, $updateCampaigns = false)
    {
        $ads = is_array($ads) ? $ads : array($ads);
        foreach($ads as $ad) {
            if($updateCampaigns) {
                $this->addCampaigns($ad->user_id, $ad);
            }

            if($ad->last_statistic_date) {
                $lastUpdate = new DateTime($ad->last_statistic_date);
                $this->_dateFrom = $lastUpdate->sub(new DateInterval('P1D'));
            }

            $wsdl = $this->_apiAdsUrl . 'Reporting/ReportingService.svc?wsdl';
            $proxy = new ClientProxy($this->_apiUsername, $this->_apiPassword, $this->_apiToken, $wsdl, $ad->account_id);

            $report = $this->getRequestData($ad->account_id, null, $proxy);

            if (null != ($reportId = $this->requestReport($report, $proxy))) {
                $response = $this->getDownloadUrl($reportId, $proxy);

                if (null != $response) {
                    if (ReportRequestStatusType::Success == $response->Status) {
                        $downloadPath = $this->_reportPath . $reportId . '.zip';

                        try {
                            $reportDownloaded = $this->downloadSSLFile($response->ReportDownloadUrl, $downloadPath);
                        } catch (Exception $e) {
                            $this->_logger->err("Account ID {$ad->account_id}: {$e->getMessage()}");
                            $reportDownloaded = false;
                        }


                        if($reportDownloaded) {
                            $statistics = $this->proceedReport($downloadPath);

                            if($statistics && $statistics->Table->Row) {
                                $adCampaigns = new Application_Model_DbTable_AdCampaigns();
                                $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

                                $campaignStat = new Application_Model_DbTable_AdCampaignStatistics();
                                $campaignStat->getAdapter()->beginTransaction();

                                //delete crossing stat:
                                $campaignStat->deleteStatByDate($ad->id, $this->getDateFrom()->format("Y/m/d"));

                                foreach($statistics->Table->Row as $stat) {
                                    $campaignId = (int)$stat->CampaignId['value'];

                                    $newStat = $campaignStat->createRow();
                                    $newStat->user_id = $ad->user_id;
                                    $newStat->ad_id = $ad->id;
                                    $newStat->ad_campaign_id = $campaignIds[$campaignId];
                                    $newStat->date = strftime("%Y-%m-%d", strtotime($stat->GregorianDate['value']));
                                    $newStat->impressions = (int)$stat->Impressions['value'];
                                    $newStat->clicks = (int)$stat->Clicks['value'];
                                    $newStat->ctr = (float)$stat->Ctr['value'];
                                    $newStat->spend = (float)$stat->Spend['value'];
                                    $newStat->average_cpc = (float)$stat->AverageCpc['value'];
                                    $newStat->average_position = (float)$stat->AveragePosition['value'];
                                    $newStat->currency = (string)$stat->CurrencyCode['value'];
                                    $newStat->average_cpm = (float)$stat->AverageCpm['value'];
                                    $newStat->status = (string)strtolower($stat->Status['value']);
                                    $newStat->added = new Zend_Db_Expr('NOW()');
                                    $newStat->save();
                                }

                                $campaignStat->getAdapter()->commit();
                            }
                        }

                        $ad->last_statistic_date = new Zend_Db_Expr('CURDATE()');
                        $ad->last_update = new Zend_Db_Expr('NOW()');
                        $ad->save();

                        $result['success'] = true;

                        $bingReports = new Application_Model_DbTable_BingReports();
                        $bingReports->save(array('user_id' => $ad->user_id, 
                            'report_id' => $reportId, 'ad_id' => $ad->id));
                     } else if (ReportRequestStatusType::Error == $response->Status) {
                         $result['errorMessage'] = "The request failed. Try requesting the report " .
                             "later.\nIf the request continues to fail, contact support.\n";
                     } else { // Pending
                         $result['errorMessage'] = "The request is taking longer than expected.\nPlease try again later";
                     }
                }
            }
        }

        return $result;
    }

    protected function proceedReport($report)
    {
        if ($zip = zip_open($report)) {
            while ($zip_entry = zip_read($zip)) {
                $statistics = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
            }
            zip_close($zip);
        }

        try{
            return new SimpleXMLElement($statistics); 
        } catch (Exception $e) {
            $this->_logger->err("Report '{$report}': {$e->getMessage()}");
            return false;
        }
    }

    protected function getCampaigns($accountId, $proxy)
    {
        try {
            $options = array('AccountId' => $accountId);

            $response = $proxy->GetService()->GetCampaignsByAccountId($options);
            $response = $response->Campaigns;

            if (!$response->Campaign) {
                $response['errorMessage'] = "There are no campaigns for that account";
                $this->_logger->err("{$accountId} {$response['errorMessage']}");
            }
        } catch (SoapFault $e) {
            $response['errorMessage'] = "The operation failed. Check your credentials.";
            $this->_logger->err("{$accountId} {$e->getMessage()}");
        }

        return $response;
    }

    protected function getCustomers($filter, $topN, $proxy)
    {
        $request = new GetCustomersInfoRequest();
        $response = false; // GetCustomersInfoResponse

        try {
            $request->ApplicationScope = ApplicationType::Advertiser;
            $request->CustomerNameFilter = $filter;
            $request->TopN = $topN;

            $response = $proxy->GetService()->GetCustomersInfo($request);
            $response = $response->CustomersInfo;

            if (empty($response->CustomerInfo)) {
                $response['errorMessage'] = "There are no customers that match the specified filter criteria";
            }
        } catch (SoapFault $e) {
            $response['errorMessage'] = "The operation failed. Check your credentials.";
        }

        return $response;
    }

    // Gets the accounts info that the customer owns and manages
    protected function getAccounts($customerId, $proxy)
    {
        $request = new GetAccountsInfoRequest();
        $response = null; // GetAccountsInfoResponse

        try {
            // Request only parent accounts such that the returned accounts
            // are within the current customer identifier, and not managed
            // through an agency or reseller.
            $request->OnlyParentAccounts = "true";
            $request->CustomerId = $customerId;

            $response = $proxy->GetService()->GetAccountsInfo($request);
            $response = $response->AccountsInfo;

            if (empty($response->AccountInfo)) {
                $response['errorMessage'] = 'The customer does not have any accounts';
            }
        } catch (SoapFault $e) {
            $response['errorMessage'] = "The operation failed. Check your credentials.";
        }

        return $response;
    }

    function getRequestData($accountIdScope, $campaignIdScope, $proxy)
    {
        //ReportRequest
        $report = new CampaignPerformanceReportRequest();

        $report->Format = ReportFormat::Xml;
        $report->ReportName = 'My Keyword Performance Report';
        //$report->ReturnOnlyCompleteData = true;
        $report->Aggregation = ReportAggregation::Daily;

        $report->Scope = new AccountThroughAdGroupReportScope();
        $report->Scope->AccountIds = null;
        $report->Scope->AdGroups = null;
        $report->Scope->Campaigns = array ();
        /*$campaignScope = new CampaignReportScope();
        $campaignScope->CampaignId = $campaignIdScope;
        $campaignScope->ParentAccountId = $accountIdScope;
        $report->Scope->Campaigns[] = $campaignScope;

        $campaignScope = new CampaignReportScope();  // Add another campaign scope.
        $campaignScope->CampaignId = <campaignidgoeshere>;
        $campaignScope->ParentAccountId = <accountidgoeshere>;
        $report->Scope->Campaigns[] = $campaignScope;*/

        $report->Time = new ReportTime();
        //$report->Time->PredefinedTime = ReportTimePeriod::LastMonth;
        $report->Time->CustomDateRangeStart = new Date();
        $report->Time->CustomDateRangeStart->Month = $this->getDateFrom()->format('m');
        $report->Time->CustomDateRangeStart->Day = $this->getDateFrom()->format('d');
        $report->Time->CustomDateRangeStart->Year = $this->getDateFrom()->format('Y');
        $report->Time->CustomDateRangeEnd = new Date();
        $report->Time->CustomDateRangeEnd->Month = $this->getDateTo()->format('m');;
        $report->Time->CustomDateRangeEnd->Day = $this->getDateTo()->format('d');;
        $report->Time->CustomDateRangeEnd->Year = $this->getDateTo()->format('Y');;

        $report->Filter = new KeywordPerformanceReportFilter();
        /*$report->Filter->DeviceType = array (
            DeviceTypeReportFilter::Computer,
            DeviceTypeReportFilter::SmartPhone
        );*/

        $report->Filter->Status = null;

        $report->Columns = array (
            KeywordPerformanceReportColumn::TimePeriod,
            KeywordPerformanceReportColumn::AccountName,
            KeywordPerformanceReportColumn::AccountNumber,
            KeywordPerformanceReportColumn::AverageCpc,
            KeywordPerformanceReportColumn::AveragePosition,
            KeywordPerformanceReportColumn::CampaignName,
            KeywordPerformanceReportColumn::CampaignId,
            KeywordPerformanceReportColumn::CurrencyCode,
            KeywordPerformanceReportColumn::Clicks,
            KeywordPerformanceReportColumn::Ctr,
            KeywordPerformanceReportColumn::Impressions,
            KeywordPerformanceReportColumn::Spend,
            KeywordPerformanceReportColumn::AverageCpm,
            'Status'
        );

        return new SoapVar($report, SOAP_ENC_OBJECT, 'CampaignPerformanceReportRequest', $proxy->getNamespace());
    }

    // Request the report. Use the ID that the request returns to
    // check for the completion of the report.
    function requestReport($report, $proxy)
    {
        $request = new SubmitGenerateReportRequest();
        $response = null; // SubmitGenerateReportResponse

        try {
            $request->ReportRequest = $report;
            $response = $proxy->GetService()->SubmitGenerateReport($request);
        } catch (SoapFault $e) {
            return false;
        }

        return (null == $response) ? null : $response->ReportRequestId;
    }

    // Check the status of the report request. The guidance of how often to poll
    // for status is from every five to 15 minutes depending on the amount
    // of data being requested. For smaller reports, you can poll every couple
    // of minutes. You should stop polling and try again later if the request
    // is taking longer than an hour.
    function getDownloadUrl($reportId, $proxy)
    {
        $request = new PollGenerateReportRequest();
        $response = null; // PollGenerateReportResponse
        $waitTime = 40; // Poll at 1 to 2 minute intervals for small reports.

        try {
            $request->ReportRequestId = $reportId;
            // Poll for status up to 12 times (or up to one hour).
            // If the call succeeds or fails, stop polling.
            for ($i = 0; $i < 12; $i++)
            {
                 $response = $proxy->GetService()->PollGenerateReport($request);

                 if (ReportRequestStatusType::Success == $response->ReportRequestStatus->Status ||
                     ReportRequestStatusType::Error == $response->ReportRequestStatus->Status)
                 {
                     break;
                 }
                 sleep($waitTime);
            }
        } catch (SoapFault $e) {
            $this->_logger->err("Report ID '{$reportId}': {$e->getMessage()}");
            return false;
        }

        return (null == $response) ? null : $response->ReportRequestStatus;
    }

    // Using the URL that the PollGenerateReport operation returned,
    // send an HTTP request to get the report and write it to the specified
    // ZIP file.
    function downloadReport($reportDownloadUrl, $downloadPath)
    {
        if (!$reader = fopen($reportDownloadUrl, 'rb')) {
            throw new Exception("Failed to open URL " . $reportDownloadUrl . ".");
        }

        if (!$writer = fopen($downloadPath, 'wb')) {
            fclose($reader);
            throw new Exception("Failed to create ZIP file " . $downloadPath . ".");
        }

        $bufferSize = 100 * 1024;

        while (!feof($reader)) {
            if (false === ($buffer = fread($reader, $bufferSize))) {
                 fclose($reader);
                 fclose($writer);
                 throw new Exception("Read operation from URL failed.");
            }

            if (fwrite($writer, $buffer) === false) {
                 fclose($reader);
                 fclose($writer);
                 throw new Exception("Write operation to ZIP file failed.");
            }
        }

        fclose($reader);
        fflush($writer);
        fclose($writer);

        return true;
    }

    protected function downloadSSLFile($url, $dest) {
        set_time_limit(0); //prevent timeout

        $ch2=curl_init();
        $fileDestination = fopen($dest,'w+');
        curl_setopt($ch2, CURLOPT_URL, $url);

        curl_setopt($ch2, CURLOPT_FILE, $fileDestination); //auto write to file

        curl_setopt($ch2, CURLOPT_TIMEOUT, 5040);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, 1);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, FALSE); 
        curl_setopt($ch2, CURLOPT_SSLVERSION, 3); 

        if(curl_exec($ch2) === false) {
            throw new Exception("Failed to open URL " . $url . ".");
        }
        curl_close($ch2);
        fclose($fileDestination);
        return true;
    }

    public function getDateFrom()
    {
        if(!$this->_dateFrom) {
            $today = new DateTime();
            $this->_dateFrom = new DateTime($today->sub(new DateInterval('P1M'))->format("m/01/Y"));
        }
        return $this->_dateFrom;
    }

    public function getDateTo()
    {
        if(!$this->_dateTo) {
            $today = new DateTime();
            $this->_dateTo = $today;
        }
        return $this->_dateTo;
    }

}