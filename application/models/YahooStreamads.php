<?php
/**
 * Yahoo Stream Ads Scraper
 */
class Application_Model_YahooStreamads
{
    protected $_loginUrl = 'https://login.yahoo.com/config/login';
    protected $_mainUrl = 'http://admanager.yahoo.com/';
    protected $_reportUri = 'report/perf';
    protected $_login;
    protected $_password;
    protected $_rmxAdvertiserId;

    protected $_dateFrom;
    protected $_dateTo;

    protected $_cookieFile;
    protected $_cookieJar;
    protected $_logger;

    protected $_errors = array(
        'login_error' => 'Login failed. Please, check your credentials',
        'account_id_missing' => 'Account ID was not found',
        'server_unavailable' => 'Yahoo Server not responding',
        'account_connected' => 'This Account is already connected to your Leadssistant account',
        'campaigns_missing' => 'Campaigns was not found!'
    );

    /**
     * Class constructor
     * 
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->_login = !empty($options['login']) ? $options['login'] : null;
        $this->_password = !empty($options['password']) ? $options['password'] : null;
        $this->_rmxAdvertiserId = !empty($options['rmxAdvertiserId']) ? $options['rmxAdvertiserId'] : null;
        $this->_cookieFile = APPLICATION_PATH . "/../data/curl_cookie/yahoo_{$options['login']}";

        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/yahoo');
        $this->_logger = new Zend_Log($writer);
    }

    /**
     * Add new Yahoo Stream account
     * 
     * @param int $userId
     * @return array|Zend_Db_Table_Row
     */
    public function addAccount($userId)
    {
        $result = $this->login();
        if(true === $result) {
            $result = array();

            try{
                $mainPage = $this->_sendRawRequest($this->_mainUrl);

                if(preg_match('/advertiser\: ([^\r\n]+)/ium', $mainPage, $matches)) {
                    $advertiser = json_decode(substr($matches[1], 0, -1));
                    $this->_rmxAdvertiserId = $advertiser->rmxAdvertiserId;

                    $adsModel = new Application_Model_DbTable_Ads();
                    if($adsModel->getAdsByAccountId($userId, $advertiser->id, Application_Model_DbTable_Ads::YAHOO)) {
                        $result['errorMessage'] = $this->_errors['account_connected'];
                    } else {
                        preg_match('/campaignCount\: ([0-9]+)/imu', $mainPage, $matches); //campaign count
                        if($matches[1]) {
                            if(preg_match('/advertisers\: ([^\r\n]+)/ium', $mainPage, $matches)) {
                                $ads = array();
                                $accounts = json_decode(substr($matches[1], 0, -1));
                                foreach ($accounts as $account) {
                                    $credentials = array('login' => $this->_login, 'password' => $this->_password, 'rmxAdvertiserId' => $advertiser->rmxAdvertiserId);

                                    $options = array('parent' => $this->_login, 'account_id' => $account->id, 
                                        'source' => Application_Model_DbTable_Ads::YAHOO,
                                        'name' => $account->name, 'credentials' => $credentials);
                                    $ads[] = $adsModel->create($userId, $options);
                                }
                                return $ads;
                            } else {
                                $result['errorMessage'] = $this->_errors['account_id_missing'];
                            }
                        } else {
                            $result['errorMessage'] = $this->_errors['campaigns_missing'];
                        }
                    }
                } else {
                    $result['errorMessage'] = $this->_errors['account_id_missing'];
                }
            } catch (Application_Model_Exceptions_Curl $e) {
                $result['errorMessage'] = $e->getMessage();
            }
        }

        $this->_logger->err("{$this->_login} {$result['errorMessage']}");
        return $result;
    }

    /**
     * Add Yahoo Stream Campaigns from main page
     * 
     * @param Zend_Db_Table_Row $ad
     * @param stdClass $campaigns Campaigns list
     * @return array|boolean
     */
    public function addCampaigns($ad, $campaigns)
    {
        if(!$campaigns) {
            return;
        }

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $keywords = $campaignsModel->getKeywords($ad->user_id);

        $adCampaigns = new Application_Model_DbTable_AdCampaigns();
        $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

        foreach($campaigns as $campaign) {
            $campaignApiId = $campaign->id;
            if(!empty($campaignIds[$campaignApiId])) { continue; }

            $status = strtolower($campaign->static->status) == 'off' ? 
                Application_Model_DbTable_AdCampaigns::STATUS_PAUSED : 
                Application_Model_DbTable_AdCampaigns::STATUS_ACTIVE;

            $data = array('api_id' => $campaignApiId, 'user_id' => $ad->user_id, 'ad_id' => $ad->id,
                'name' => $campaign->static->name, 'status' => $status, 
                'source' => Application_Model_DbTable_Ads::YAHOO);

            //automatic Pairing
            if(false !== ($campaignId = App_Utils::findKeyword($campaign->static->name, $keywords))) {
                $data['campaign_id'] = $campaignId;
            }

            $newCampaign = $adCampaigns->create($data);
            $campaignIds[$newCampaign->api_id] = $newCampaign->id;
        }

        return true;
    }

    /**
     * Get and Save Campaigns and Statistics
     * 
     * @param Zend_Db_Table_Row $ad
     * @param bool $updateCampaigns
     * @return array
     */
    public function getCampaignsStatistics($ads, $updateCampaigns = true)
    {
        $ads = is_array($ads) ? $ads : array($ads);
        foreach($ads as $ad) {
            //Get total stat from "Campaigns" page:
            $statContent = $this->_getStatContent($ad);
            if(!$statContent) {
                return;
            }

            if($content = json_decode($statContent)) {
                if($updateCampaigns) {
                    //Update campaigns list:
                    $this->addCampaigns($ad, $content->report->apiresponse->entity->campaigns);
                }

                //Get Daily Stat for each campaign
                $statForCampaigns = $this->_getDailyCampaignStat($ad);

                $campaignStat = new Application_Model_DbTable_AdCampaignStatistics();
                $campaignStat->getAdapter()->beginTransaction();

                //delete crossing stat:
                $campaignStat->deleteStatByDate($ad->id, $this->_dateFrom->format("Y-m-d"));

                foreach($statForCampaigns as $campaignId => $data) {
                    foreach($data as $row) {
                        $stat = $row->stat;

                        $newStat = $campaignStat->createRow();
                        $newStat->user_id = $ad->user_id;
                        $newStat->ad_id = $ad->id;
                        $newStat->ad_campaign_id = $campaignId;
                        $newStat->date = strftime("%Y-%m-%d", strtotime($row->date));
                        $newStat->impressions = (int)$stat->impr;
                        $newStat->clicks = (int)$stat->clicks;
                        $newStat->ctr = (float)$stat->ctr;
                        $newStat->spend = (float)$stat->spnd;
                        $newStat->average_cpc = (float)$stat->cpc;
                        $newStat->added = new Zend_Db_Expr('NOW()');
                        $newStat->save();
                    }
                }
                $campaignStat->getAdapter()->commit();
            }

            $ad->last_statistic_date = new Zend_Db_Expr('CURDATE()');
            $ad->last_update = new Zend_Db_Expr('NOW()');
            $ad->save();
        }
        $result['success'] = true;

        return $result;
    }

    /**
     * Get Statistic page content
     * 
     * @param type $ad
     * @return boolean|string
     */
    protected function _getStatContent($ad)
    {
        if(true !== ($result = $this->login())) {
            return false;
        }

        $this->_dateFrom = $this->getDateFrom();
        $this->_dateTo = $this->getDateTo();

        if($ad->last_statistic_date) {
            $lastUpdate = new DateTime($ad->last_statistic_date);
            $this->_dateFrom = $lastUpdate->sub(new DateInterval('P1D'));
        }

        $adCampaigns = new Application_Model_DbTable_AdCampaigns();
        $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

        $rc = rawurlencode(implode(',', array_keys($campaignIds)));
        $getParams = array('a' => $ad->account_id, 'l' => 'campaign', 'i'=>'day', 'ivalues' => 'c', 'm_mode' => 'data');
        $getParams['tf'] = $this->_dateFrom->format('Y-m-d');
        $getParams['tt'] = $this->_dateTo->format('Y-m-d');
        $getParams['ra'] = $this->_rmxAdvertiserId;
        $getParams['rc'] = $rc;

        try{
            return $this->_sendRawRequest($this->_mainUrl.$this->_reportUri, $getParams);
        } catch (Application_Model_Exceptions_Curl $e) {
            $this->_logger->err("{$this->_login} {$e->getMessage()}");
            return false;
        }
    }

    /**
     * Get Daily Detailed Statistic for each campaign
     * 
     * @param type $ad
     * @return array
     */
    protected function _getDailyCampaignStat($ad)
    {
        $statistics = array();

        $this->_dateFrom = $this->getDateFrom();
        $this->_dateTo = $this->getDateTo();

        $adCampaigns = new Application_Model_DbTable_AdCampaigns();
        $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

        //open the reporting page first:
        $reportContent = $this->_sendRawRequest($this->_mainUrl . $this->_reportUri, array('a' => $ad->account_id));
        preg_match("/\"u_id\"\:\"([^\"]+)\"/", $reportContent, $matches);
        $instanceId = !empty($matches[1]) ? $matches[1] : null;
        $url = "https://admanager.yahoo.com/report/_remote/?a={$ad->account_id}&m_mode=fragment&m_id=cbdatatable";

        foreach($campaignIds as $apiId => $campaignId) {
            $postData = array(
                "m_id" => "cbdatatable", "m_mode" => "fragment",
                "instance_id" => $instanceId,
                "tf" => $this->_dateFrom->format('Y-m-d'),
                "tt" => $this->_dateTo->format('Y-m-d'),
                "i" => "day", "l" => "campaign",
                "crumbs" => "campaign|{$apiId}|1|,day||%page_number%|",
                "prt" => "cpc", "propertyStatus" => "",
                "channel" => "", "cur" => "USD",
                "loc" => "en-US", "a" => $ad->account_id,
                ".crumb" => "", "c" => implode(',', array_keys($campaignIds)),
                "ivalues" => "c", "from" => "secondary-breakdown-select",
                "origin" => "ads", "mode" => "data",
                "m_container" => false
            );

            //get statistics including pagination:
            $stat = array();
            $hasNext = true;
            $fromRecord = 0;
            while($hasNext) {
                $hasNext = false;
                $params = $postData;
                $params['crumbs'] = str_replace('%page_number%', $fromRecord+1, $postData['crumbs']);

                $response = $this->_sendRawRequest($url, null, json_encode($params));
                if($response && ($response = json_decode($response))) {
                    $hasNext = $response->data->depth1->pagination->hasNext;
                    $fromRecord = $response->data->depth1->pagination->to;
                    $stat = array_merge($stat, (array)$response->data->depth1->data);
                }
            }
            $statistics[$campaignId] = $stat;
        }

        return $statistics;
    }

    /**
     * Get DateFrom
     * Set report starting date
     * 
     * @return DateTime
     **/
    public function getDateFrom()
    {
        if(!$this->_dateFrom) {
            $today = new DateTime();
            $this->_dateFrom = $today->sub(new DateInterval('P30D'));
        }
        return $this->_dateFrom;
    }

    /**
     * Get DateTo
     * Set report end date
     * 
     * @return DateTime
     **/
    public function getDateTo()
    {
        if(!$this->_dateTo) {
            $this->_dateTo = new DateTime();
        }
        return $this->_dateTo;
    }

    /**
     * Yahoo login
     * 
     * @return array|string
     */
    public function login()
    {
        if(file_exists($this->_cookieFile)) {
            unlink($this->_cookieFile);
        }
        $postParams = array('.tries' => 1, '.src'=>'', '.md5'=>'', '.hash'=>'', '.js'=>'','.last'=>'', 'promo'=>'', '.intl'=>'us',
            '.lang'=>'en-US', '.bypass'=>'', '.partner'=>'', '.u'=>'da0con598a3ru', '.v'=>0, '.challenge'=>'uqMeiDTbkwYsiJjQgWOqz0xS6RAj',
            '.yplus'=>'', '.emailCode'=>'', 'pkg'=>'', 'stepid'=>'', '.ev'=>'', 'hasMsgr'=>0, '.chkP'=>'Y', '.done'=>'https://streamads.yahoo.com:443/',
            '.pd'=>'_ver%3D0%26c%3D%26ivt%3D%26sg%3D', '.ws'=>1, '.cp'=>0, 'nr'=>0, 'pad'=>1, 'aad'=>1, 'login'=>$this->_login,
            'passwd'=>$this->_password, '.persistent'=>'y', '.save'=>'', 'passwd_raw'=>'');

        try {
            $response = $this->_sendRawRequest($this->_loginUrl, null, $postParams);
            $content = json_decode($response);
            if(!empty($content->status) && $content->status == 'redirect') {
                return true;
            }
            $result['errorMessage'] = $this->_errors['login_error'];
        } catch (Application_Model_Exceptions_Curl $e) {
            $result['errorMessage'] = $e->getMessage();
        }

        $this->_logger->err("{$this->_login} {$result['errorMessage']}");
        return $result;
    }

    /**
     * Send CURL request
     * 
     * @param string $url
     * @param array $getParams
     * @param array $postParams
     * 
     * @return bool|string|Application_Model_Exceptions_Curl
     */
    protected function _sendRawRequest($url, $getParams = array(), $postParams = array())
    {
        $curl = curl_init ();
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($curl, CURLOPT_ENCODING, "");
        curl_setopt($curl, CURLOPT_COOKIEFILE, $this->_cookieFile);
        curl_setopt($curl, CURLOPT_COOKIEJAR, $this->_cookieFile);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0");

        if($postParams) {
            curl_setopt($curl, CURLOPT_POST, 1);
            $postParams = is_array($postParams) ? http_build_query($postParams) : $postParams;
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postParams);
        } else {
             $url .= '?' . http_build_query($getParams);
        }

        curl_setopt ($curl, CURLOPT_URL, $url);
        $content = curl_exec ($curl); 
        $err = curl_errno($curl);
        $errMsg = curl_error($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        curl_close($curl);

        if ($err == 0) {
            $body = substr($content, $header_size);
            if ($httpCode == "200") {
                return $body;
            } else {
                return false;
            }
        } else {
            throw new Application_Model_Exceptions_Curl($errMsg, $err);
        }
    }
}