<?php

class Application_Model_Cakemarketing
{
    protected $_mainUrl;
    protected $_service;
    protected $_method;
    protected $_apiKey;
    protected $_apiAffiliateId;
    protected $_domain;

    protected $_dateFrom;
    protected $_dateTo;
    protected $_logger;

    const REPORTS_SERVICE = 'reports';
    const ACCOUNT_SERVICE = 'account';
    const OFFERS_SERVICE = 'offers';

    const CAMPAIGNS_SUMMARY = 'CampaignSummary';
    const COUNTRIES_INFO = 'GetCountries';
    const GET_CAMPAIGN = 'GetCampaign';
    const SUB_AFFILIATES = 'GetSubAffiliates';
    const SUB_AFFILIATES_SUMMARY = 'SubAffiliateSummary';

    protected $_errors = array(
        'login_error' => 'Login failed. Please, check your credentials',
        'server_unavailable' => 'Lead Impact Server is not responding',
        'account_connected' => 'This Account is already connected to your Leadssistant account',
        'campaigns_missing' => 'Campaigns was not found!'
    );

    public function __construct($options = array())
    {
        $this->_apiKey = !empty($options['api_key']) ? $options['api_key'] : null;
        $this->_apiAffiliateId = !empty($options['api_affiliate_id']) ? $options['api_affiliate_id'] : null;
        $this->_username = !empty($options['username']) ? $options['username'] : null;
        $this->_domain = !empty($options['domain']) ? $options['domain'] : 'http://network.leadnomics.com';

        $this->_mainUrl = $this->_domain . '/affiliates/api/2/';

        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/cakemarketing');
        $this->_logger = new Zend_Log($writer);
    }

    public function addAccount($userId)
    {
        $this->_service = self::ACCOUNT_SERVICE;
        $this->_method = self::COUNTRIES_INFO;
        $result = array();

        $content = $this->_sendRequest($this->makeUrl());
        if(!empty($content->success) && $content->success) {
            $affiliateModel = new Application_Model_DbTable_Affiliates();
            if($affiliateModel->getAffiliateByApiId($this->_apiAffiliateId, $userId, Application_Model_DbTable_Affiliates::CAKEMARKETING)) {
                $result['errorMessage'] = $this->_errors['account_connected'];
            } else {
                $credentials = array('api_key' => $this->_apiKey, 'api_affiliate_id' => $this->_apiAffiliateId, 'domain'=> $this->_domain);
                $affiliateOptions = array('name' => $this->_username, 'credentials' => $credentials, 
                    'api_id' => $this->_apiAffiliateId, 'source' => Application_Model_DbTable_Affiliates::CAKEMARKETING);
                $result = $affiliateModel->create($affiliateOptions, $userId);
            }
        } else {
            $result = is_array($content) ? $content : $this->_errors['login_error'];
        }
        return $result;
    }
    
    /*
     * GetCampaign method, used to update the missing offer_id
    public function getCampaign($affiliate)
    {
        $this->_service = self::OFFERS_SERVICE;
        $this->_method = self::GET_CAMPAIGN;

        $affiliateCampaigns = new Application_Model_DbTable_AffiliateCampaigns();
        $where = $affiliateCampaigns->select()->where('affiliate_id = ?', $affiliate->id);
        $campaigns = $affiliateCampaigns->fetchAll($where);

        foreach($campaigns as $campaign) {
            $params['campaign_id'] = $campaign->api_id;
            $response = $this->_sendRequest($this->makeUrl(), $params);
            if(!empty($response->campaign)) {
                $campaign->api_offer_id = (string)$response->campaign->offer_id;
                $campaign->save();
            }
        }
    }*/

    /**
     * Add Yahoo Stream Campaigns from main page
     * 
     * @param Zend_Db_Table_Row $affiliate
     * @param int $campaigId Campaigns api id
     * @param string $campaignName Campaign name
     * @return Zend_Db_Table_Row
     */
    public function addCampaign($affiliate, $campaignId, $campaignName, $offerId = null)
    {
        if(!$campaignId || !$campaignName) {
            return;
        }

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $keywords = $campaignsModel->getKeywords($affiliate->user_id);

        $affiliateCampaigns = new Application_Model_DbTable_AffiliateCampaigns();
        $campaignIds = $affiliateCampaigns->getPairs($affiliate->user_id, $affiliate->id);

        $campaignApiId = (int)$campaignId;

        $id = !empty($campaignIds[$campaignApiId]) ? $campaignIds[$campaignApiId] : null;
        //if($id) { continue; } //remove this if need to update existing campaigns

        $data = array('api_id' => $campaignApiId, 'user_id' => $affiliate->user_id, 
            'name' => $campaignName, 'affiliate_id' => $affiliate->id, 'api_offer_id' => $offerId);

        //automatic Pairing
        if(false !== ($campaignId = App_Utils::findKeyword($campaignName, $keywords))) {
            $data['campaign_id'] = $campaignId;
        }

        return $affiliateCampaigns->create($data, $id);
    }

    protected function _addSubId($affiliate, $campaignId, $affiliateName)
    {
        $data = array('user_id' => $affiliate->user_id, 
            'affiliate_id' => $affiliate->id,
            'affiliate_campaign_id' => $campaignId,
            'name' => $affiliateName);

        $affiliateSubids = new Application_Model_DbTable_AffiliateSubids();
        return $affiliateSubids->create($data);
    }

    /**
     * Get the list of Affiliate Sub IDs
     * 
     * @param Zend_DbTable_Row $affiliate
     * @param array $campaignIds
     */
    public function getSubIds($affiliate, $campaignIds = array())
    {
        if(!$campaignIds) {
            return;
        }
        $campaignIds = (array)$campaignIds;

        $affiliateCampaignsModel = new Application_Model_DbTable_AffiliateCampaigns();
        $select = $affiliateCampaignsModel->select()->where('api_id IN (?)', $campaignIds)->where('user_id = ?', $affiliate->user_id);
        $campaigns = $affiliateCampaignsModel->fetchAll($select); //select affiliate campaigns by given api_campaigns_id

        $affiliateSubids = new Application_Model_DbTable_AffiliateSubids();

        $this->_service = self::REPORTS_SERVICE;
        $this->_method = self::SUB_AFFILIATES_SUMMARY;

        $date = new DateTime();
        $params['end_date'] = $date->format('Y-m-d');
        $params['start_date'] = $date->sub(new DateInterval('P10D'))->format('Y-m-d');
        $params['start_at_row'] = 0;
        $params['row_limit'] = 1000;

        foreach($campaigns as $campaign) {
            $existingSubids = $affiliateSubids->getPairs(array('affiliate_campaign_id' => (string)$campaign->id));
            if(!$campaign->api_offer_id) {
                continue;
            }

            $params['offer_id'] = $campaign->api_offer_id;
            $statistics = $this->_sendRequest($this->makeUrl(), $params);
            if(empty($statistics->success)) {
                continue;
            }

            foreach($statistics->sub_affiliates->sub_affiliate as $subId) {
                if(false === array_search($subId->sub_id, $existingSubids)) {
                    $newRecord = $this->_addSubId($affiliate, $campaign->id, $subId->sub_id);
                    $existingSubids[$newRecord->id] = $subId->sub_id;
                }
            }
        }
    }

    public function getCampaignsStatistics($affiliate, $updateCampaigns = true)
    {
        $this->_dateFrom = $this->getDateFrom();
        $this->_dateTo = $this->getDateTo();

        if($affiliate->last_statistic_date) {
            $lastUpdate = new DateTime($affiliate->last_statistic_date);
            $this->_dateFrom = $lastUpdate->sub(new DateInterval('P1D'));
        }

        $statistics = array();
        $startDate = clone $this->_dateFrom;
        while($startDate <= $this->_dateTo) {
            $subIdStatistics = $this->_geSubIdtStat($affiliate, $startDate);
            $currDataStat = array($startDate->format('Y-m-d') => array_merge($this->_getStat($startDate), $subIdStatistics));
            $statistics = array_merge($statistics, $currDataStat);

            $startDate->add(new DateInterval('P1D'));
        }

        if($statistics) {
            //get existing Camaigns:
            $affiliateCampaigns = new Application_Model_DbTable_AffiliateCampaigns();
            $campaignIds = $affiliateCampaigns->getPairs($affiliate->user_id, $affiliate->id);

            $campaignStat = new Application_Model_DbTable_AffiliateCampaignStatistics();
            $campaignStat->getAdapter()->beginTransaction();

            //delete crossing stat:
            $campaignStat->deleteStatByDate($affiliate->id, $this->_dateFrom->format("Y-m-d"));
            foreach($statistics as $date => $row) {
                foreach($row as $stat) {
                    $newStat = $campaignStat->createRow();

                    if(!empty($stat->affiliate_subid)) {
                        $newStat->affiliate_campaign_id = (string)$stat->affiliate_campaign_id;
                        $newStat->affiliate_subid = (string)$stat->affiliate_subid;
                    } else {
                        $campaignApiId = (int)$stat->campaign_id;
                        if(empty($campaignIds[$campaignApiId])) {
                            //add new Campaign
                            $newCampaign = $this->addCampaign($affiliate, $campaignApiId, $stat->offer_name, $stat->offer_id);
                            $campaignIds[$newCampaign->api_id] = $newCampaign->id;
                        }
                        $newStat->affiliate_campaign_id = $campaignIds[$campaignApiId];
                    }

                    $newStat->affiliate_id = $affiliate->id;
                    $newStat->user_id = $affiliate->user_id;
                    $newStat->date = $date;
                    $newStat->clicks = (int)$stat->clicks;
                    $newStat->approved = (int)$stat->conversions;
                    $newStat->impression = (int)$stat->impression;
                    $newStat->approved_percentage = (float)$stat->conversion_rate;
                    $newStat->epc = (float)$stat->epc;
                    $newStat->commission = (float)$stat->revenue;
                    $newStat->added = new Zend_Db_Expr('NOW()');
                    $newStat->save();
                }
            }
            $campaignStat->getAdapter()->commit();
        }

        $affiliate->last_statistic_date = new Zend_Db_Expr('CURDATE()');
        $affiliate->last_update = new Zend_Db_Expr('NOW()');
        $affiliate->save();
        $result['success'] = true;

        return $result;
    }

    /**
     * Get Statistic for given date
     * 
     * @param DateTime $date
     * @return array
     */
    protected function _getStat($date)
    {
        $this->_service = self::REPORTS_SERVICE;
        $this->_method = self::CAMPAIGNS_SUMMARY;
        $result = array();

        $reportDate = clone $date;
        $params['start_date'] = $reportDate->format('Y-m-d');
        $params['end_date'] = $reportDate->add(new DateInterval('P1D'))->format('Y-m-d');
        $params['start_at_row'] = 0;
        $params['row_limit'] = 1000;

        $response = $this->_sendRequest($this->makeUrl(), $params);
        if(!empty($response->campaigns)) {
            $result = current($response->campaigns);
            $result = is_array($result) ? $result : array($result);
        }

        return $result;
    }

    /**
     * Get Sub Affiliates Statistics for given date and campaigns
     * 
     * @param DateTime $date
     * @param array $campaignIds
     * @return array
     */
    protected function _geSubIdtStat($affiliate, $date, $campaignIds = array())
    {
        $this->_service = self::REPORTS_SERVICE;
        $this->_method = self::SUB_AFFILIATES_SUMMARY;
        $result = array();
        $campaignIds = (array)$campaignIds;

        $reportDate = clone $date;
        $params['start_date'] = $reportDate->format('Y-m-d');
        $params['end_date'] = $reportDate->add(new DateInterval('P1D'))->format('Y-m-d');
        $params['start_at_row'] = 0;
        $params['row_limit'] = 1000;

        $affiliateSubids = new Application_Model_DbTable_AffiliateSubids();
        $affiliateCampaignsModel = new Application_Model_DbTable_AffiliateCampaigns();

        if(!$campaignIds) {
            //Find Cakemarketing campaigns API IDs paired for "Ad Performance"
            $campaignIds = $affiliateSubids->getPairedCampaignIds($affiliate->id);
        }

        if(!$campaignIds) {
            return $result;
        }
        $select = $affiliateCampaignsModel->select()->where('api_id IN (?)', $campaignIds);
        $campaigns = $affiliateCampaignsModel->fetchAll($select); //select affiliate campaigns by given api_campaigns_id

        foreach($campaigns as $campaign) {
            $params['offer_id'] = $campaign->api_offer_id;
            $response = $this->_sendRequest($this->makeUrl(), $params);

            if(!empty($response->sub_affiliates)) {
                //get existing SubIds
                $existingSubids = $affiliateSubids->getPairs(array('affiliate_campaign_id' => $campaign->id));

                $subAffiliates = current($response->sub_affiliates);
                $subAffiliates = is_array($subAffiliates) ? $subAffiliates : array($subAffiliates);

                //add affiliate_campaign_id to each stat
                foreach($subAffiliates as $subAffiliate) {
                    $subId = array_search($subAffiliate->sub_id, $existingSubids);
                    if(false === $subId) {
                        $newRecord = $this->_addSubId($affiliate, $campaign->id, $subAffiliate->sub_id);
                        $existingSubids[$newRecord->id] = $subAffiliate->sub_id;
                        $subId = $newRecord->id;
                    }

                    $subAffiliate->affiliate_subid = $subId;
                    $subAffiliate->affiliate_campaign_id = $campaign->id;
                }
                $result = array_merge($result, $subAffiliates);
            }
        }
        return $result;
    }

    protected function makeUrl()
    {
        return $this->_mainUrl . $this->_service . ".asmx/" . $this->_method;
    }

    /**
     * Get DateFrom
     * Set report starting date
     * 
     * @return DateTime
     **/
    public function getDateFrom()
    {
        if(!$this->_dateFrom) {
            $today = new DateTime();
            $this->_dateFrom = $today->sub(new DateInterval('P30D'));
        }
        return $this->_dateFrom;
    }
    

    /**
     * Get DateTo
     * Set report end date
     * 
     * @return DateTime
     **/
    public function getDateTo()
    {
        if(!$this->_dateTo) {
            $this->_dateTo = new DateTime();
        }
        return $this->_dateTo;
    }

    /**
     * Send CURL request
     * 
     * @param string $url
     * @param array $getParams
     * @param array $postParams
     * 
     * @return array|string
     */
    protected function _sendRequest($url, $getParams = array(), $postParams = array())
    {
        $authParams = array('api_key' => $this->_apiKey, 'affiliate_id' => $this->_apiAffiliateId);
        $config = array(
            'timeout' => 30,
            'useragent' => "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)",
            'curloptions' => array(CURLOPT_RETURNTRANSFER => 1, CURLOPT_SSL_VERIFYPEER => false)
        );

        $client = new Zend_Http_Client($url, $config);

        if($postParams) {
            $client->setParameterPost(array_merge($authParams, $postParams));
            $client->setMethod(Zend_Http_Client::POST);
        } else {
            $client->setParameterGet(array_merge($authParams, $getParams));
            $client->setMethod(Zend_Http_Client::GET);
        }

        try{
            $response = $client->request();
            if ($response->isSuccessful()){
                try{
                    $respone = new SimpleXMLElement($response->getBody()); 
                    if('false' === (string)$respone->success) {
                        $result['errorMessage'] = (string)$respone->message;
                    } else {
                        return $respone;
                    }
                } catch (Exception $e) {
                    $result['errorMessage'] = $e->getMessage();
                }
            } else {
                $result['errorMessage'] = $this->_errors['server_unavailable'];
            }
        } catch (Zend_Http_Client_Exception $e) {
            $result['errorMessage'] = $e->getMessage();
        }

        $this->_logger->err("{$this->_apiAffiliateId}. URL {$url}: {$result['errorMessage']}");
        return $result;
    }
}