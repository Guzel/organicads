<?php

class Application_Model_DbTable_Forms extends Zend_Db_Table_Abstract
{
    protected $_name = 'forms';

    public function save($userId, $data)
    {
        if($data['form_id']) {
            $form = $this->find($data['form_id'])->current();
            if($form->user_id !== $userId) {
                return;
            }
        } else {
            $form = $this->createRow();
            $form->added = new Zend_Db_Expr('NOW()');
            $form->user_id = $userId;
        }

        $form->title = $data['form_title'];
        $form->redirect_page = $data['redirect_page'];
        $form->fields = serialize($data['form_fields']);
        $form->save();

        return $form;
    }

    public function getForms($userId = null)
    {
        $select = $this->select()
            ->order('id ASC');
        if($userId) {
            $select->where('user_id = ?', (int)$userId);
        }
        return $this->fetchAll($select);
    }

    public function getPairs($userId = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('id', 'title'))
            ->order('id ASC');
        if($userId) {
            $select->where('user_id = ?', (int)$userId);
        }

        return $this->getAdapter()->fetchPairs($select);
    }
}