<?php

class Application_Model_DbTable_ClientLeadDelivery extends Zend_Db_Table_Abstract
{
    protected $_name = 'client_lead_delivery';

    const DEFAULT_EMAIL_SUBJECT = 'You Have A New Lead!';

    public function save($data, $clientId)
    {
        $this->delete('client_id = ' . (int)$clientId);

        $data['emails'] = array_filter($data['emails']);
        $deliveryFields = null;
        if(!empty($data['delivery_fields'])) {
            $deliveryFields = array_combine($data['delivery_fields']['id'], $data['delivery_fields']['alternative_name']);
            $deliveryFields = serialize($deliveryFields);
        }

        $deliveryOption = $this->createRow();
        $deliveryOption->setFromArray($data);
        $deliveryOption->emails = !empty($data['emails']) ? serialize($data['emails']) : null;
        $deliveryOption->subject = !empty($data['subject']) ? $data['subject'] : null;
        $deliveryOption->delivery_fields = $deliveryFields;
        $deliveryOption->static_fields = !empty($data['static_fields']) ? serialize($data['static_fields']) : null;
        $deliveryOption->response_criteria = $data['response_criteria'];
        $deliveryOption->response_value = $data['response_value'];
        $deliveryOption->client_id = $clientId;
        $deliveryOption->added = new Zend_Db_Expr('NOW()');
        $deliveryOption->save();
    }

    public function getOptions($clientId)
    {
        $select = $this->select()->where('client_id = ?', (int)$clientId);
        return $this->fetchRow($select);
    }
}