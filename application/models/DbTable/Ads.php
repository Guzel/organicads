<?php

class Application_Model_DbTable_Ads extends Zend_Db_Table_Abstract
{
    protected $_name = 'ads';
    const BING = 'bing';
    const FACEBOOK = 'facebook';
    const FIFTYONRED = 'fiftyonred';
    const GOOGLE = 'google';
    const YAHOO = 'yahoo';
    const TRAFFICVANCE = 'trafficvance';
    const LEADIMPACT = 'leadimpact';
    const HASTRAFFIC = 'hastraffic';
    const ZEROPARK = 'zeropark';
    const AIRPUSH = 'airpush';

    public $advert_allowed = array(self::TRAFFICVANCE, self::FACEBOOK);

    /**
     * Create new ad account
     * 
     * @param int $userId
     * @param array $data
     * 
     * @return Zend_Db_Table_Row
     */
    public function create($userId, $data)
    {
        $newAds = $this->createRow();
        $newAds->setFromArray($data);
        $newAds->user_id = $userId;
        $newAds->added = new Zend_Db_Expr('NOW()');
        if(!empty($data['credentials'])) {
            $newAds->credentials = base64_encode(App_Utils::encryptString(serialize($data['credentials'])));
        }
        $newAds->save();

        return $newAds;
    }

    /**
     * Returns ads by given user Id
     * 
     * @param int $userId
     * @param string|null $source
     * 
     * @return Zend_Db_Table_Rowset
     */
    public function getAdsByUserId($userId, $source = null)
    {
        $select = $this->select()->where('user_id = ?', (int)$userId);
        if($source) {
            $select->where('source = ?', $source);
        }
        return $this->fetchAll($select);
    }

    /**
     * Returns ad accounts to update the statistics
     * 
     * @param int $userId
     * @return Zend_Db_Table_Rowset
     */
    public function getAdsToUpdate($userId)
    {
        $select = $this->select()->where('user_id = ?', $userId)
            ->where('last_update IS NULL OR last_update <= ?', new Zend_Db_Expr('NOW() - INTERVAL 20 MINUTE'));
        return $this->fetchAll($select);
    }

    /**
     * Return ad accounts by id
     * 
     * @param int|array $id
     * 
     * @return Zend_Db_Table_Rowset
     */
    public function getAdsById($id)
    {
        $ids = is_array($id) ? $id : array($id);

        $select = $this->select()
            ->where('id IN(?)', $ids);
        return $this->fetchAll($select);
    }

    /**
     * Return ad accounts by name
     * 
     * @param int $userId
     * @param string $name
     * @param string $source
     * 
     * @return Zend_Db_Table_Row
     */
    public function getAdsByName($userId, $name, $source)
    {
        $select = $this->select()->where('name = ?', $name)
            ->where('user_id = ?', (int)$userId)
            ->where('source = ?', $source);

        return $this->fetchRow($select);
    }

    /**
     * Return ad accounts by account_id
     * 
     * @param int $userId
     * @param string $accountId
     * @param string $source
     * 
     * @return Zend_Db_Table_Row
     */
    public function getAdsByAccountId($userId, $accountId, $source)
    {
        $select = $this->select()->where('account_id = ?', $accountId)
            ->where('user_id = ?', (int)$userId)
            ->where('source = ?', $source);

        return $this->fetchRow($select);
    }

    /**
     * Return ad accounts by parent
     * 
     * @param int $userId
     * @param int $parent
     * 
     * @return Zend_Db_Table_Row
     */
    public function getAdsByParent($userId, $parent)
    {
        $select = $this->select()->where('parent = ?', $parent)
            ->where('user_id = ?', (int)$userId);

        return $this->fetchRow($select);
    }
}