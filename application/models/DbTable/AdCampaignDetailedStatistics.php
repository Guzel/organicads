<?php

class Application_Model_DbTable_AdCampaignDetailedStatistics extends Zend_Db_Table_Abstract
{
    protected $_name = 'ad_campaign_detailed_statistics';

    /**
     * Find total clicks, impressions and spends by given campaignId or userId and dates,
     * group results by date
     * 
     * @param array $options
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function findTotalByCampaign($options, $startDate, $endDate = null)
    {
        $startDate = strtotime($startDate);
        $endDate = strtotime($endDate);

        $select = $this->getAdapter()->select()
            ->join('ad_campaigns', 'ad_campaigns.id = ad_campaign_detailed_statistics.ad_campaign_id', array())
            ->from($this->_name, array('date_upload', 'clicks' => new Zend_DB_Expr('SUM(clicks)'), 
                'impressions' => new Zend_DB_Expr('SUM(impressions)'),
                'spend' => new Zend_DB_Expr('SUM(spend)')))
            ->where('ad_campaigns.campaign_id IS NOT NULL')
            ->where('ad_campaign_detailed_statistics.date_upload >= ?', strftime("%Y-%m-%d %H:%M:%S", $startDate))
            ->group('date_upload')
            ->order('ad_campaign_detailed_statistics.date_upload ASC');

        if(!empty($options['campaign_id'])) {
            $select->where('ad_campaigns.campaign_id = ?', (int)$options['campaign_id']);
        }

        if(!empty($options['user_id'])) {
            $select->where('ad_campaign_detailed_statistics.user_id = ?', (int)$options['user_id']);
        }

        if($endDate) {
            $select->where('ad_campaign_detailed_statistics.date_upload <= ?', strftime("%Y-%m-%d %H:%M:%S", $endDate));
        }

        $data = $this->getAdapter()->fetchAssoc($select);

        return $data;
    }

    /**
     * Find total spends by given campaignId and datetimes,
     * group results by date and ad account. Returns statistics and ad accounts
     * 
     * @param array $options
     * @param string $startDate
     * @param string $endDate
     * @return array 
     * 'statistics' => array('datetime' => array('ad_id' => 'spend'))
     * 'ad_accounts' => array('ad_id' => Zend_Db_Table_Row)
     */
    public function findSpendByAccounts($campaignId, $startDate, $endDate = null)
    {
        $startDate = strtotime($startDate);
        $endDate = strtotime($endDate);

        $select = $this->getAdapter()->select()
            ->join('ad_campaigns', 'ad_campaigns.id = ad_campaign_detailed_statistics.ad_campaign_id', array())
            ->from($this->_name, array('ad_campaign_detailed_statistics.*'))
            ->where('ad_campaigns.campaign_id = ?', (int)$campaignId)
            ->where('ad_campaign_detailed_statistics.date_upload >= ?', strftime("%Y-%m-%d %H:%M:%S", $startDate))
            ->order('ad_campaign_detailed_statistics.date_upload ASC');

        if($endDate) {
            $select->where('ad_campaign_detailed_statistics.date_upload <= ?', strftime("%Y-%m-%d %H:%M:%S", $endDate));
        }

        $data = $this->getAdapter()->fetchAll($select);

        $adsModel = new Application_Model_DbTable_Ads();
        $result = $statistics = $adAccounts = array();
        foreach($data as $row) {
            if(!isset($statistics[$row['date_upload']][$row['ad_id']])) {
                $statistics[$row['date_upload']][$row['ad_id']] = 0;
            }
            $statistics[$row['date_upload']][$row['ad_id']] += $row['spend'];
            if(empty($adAccounts[$row['ad_id']])) {
                $adAccounts[$row['ad_id']] = $adsModel->find($row['ad_id'])->current();
            }
        }
        $result['statistics'] = $statistics;
        $result['ad_accounts'] = $adAccounts;

        return $result;
    }

}