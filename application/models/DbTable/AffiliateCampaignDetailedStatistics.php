<?php

class Application_Model_DbTable_AffiliateCampaignDetailedStatistics extends Zend_Db_Table_Abstract
{
    protected $_name = 'affiliate_campaign_detailed_statistics';

    /**
     * Find total leads, impressions and revenue by given campaignId or userId and dates,
     * group results by date
     * 
     * @param arrat $options
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function findTotalByCampaign($options, $startDate, $endDate = null)
    {
        $startDate = strtotime($startDate);
        $endDate = strtotime($endDate);

        $userId = !empty($options['user_id']) ? $options['user_id'] : '';
        $campaignId = !empty($options['campaign_id']) ? $options['campaign_id'] : '';

        $select = $this->getAdapter()->select()
            ->from($this->_name, array('date_upload', 'leads' => new Zend_DB_Expr('SUM(approved)'), 
                'impression' => new Zend_DB_Expr('SUM(impression)'),
                'qualified' => new Zend_DB_Expr('SUM(qualified)'), 'approved' => new Zend_DB_Expr('SUM(approved)'), 
                'commission' => new Zend_DB_Expr('SUM(commission)')))
            ->join('affiliate_campaigns', 'affiliate_campaigns.id = affiliate_campaign_detailed_statistics.affiliate_campaign_id', array())
            ->where('affiliate_campaigns.campaign_id IS NOT NULL')
            ->where('affiliate_campaign_detailed_statistics.date_upload >= ?', strftime("%Y-%m-%d %H:%M:%S", $startDate))
            ->group('date_upload')
            ->order('affiliate_campaign_detailed_statistics.date_upload ASC');

        if($campaignId) {
            $select->where('affiliate_campaigns.campaign_id = ?', (int)$campaignId);
        }

        if($userId) {
            $select->where('affiliate_campaign_detailed_statistics.user_id = ?', (int)$userId);
        }

        if($endDate) {
            $select->where('affiliate_campaign_detailed_statistics.date_upload <= ?', strftime("%Y-%m-%d %H:%M:%S", $endDate));
        }

        $data = $this->getAdapter()->fetchAssoc($select);
        return $data;
    }

    public function getTrend($userId)
    {
        $campaigns = new Application_Model_DbTable_Campaigns();
        $campaigns = $campaigns->getPairs($userId);

        $result = array();
        foreach($campaigns as $campaignId => $campaignName) {
            $select = $this->getAdapter()->select()
                ->from($this->_name, array('date_upload', 'commission' => new Zend_DB_Expr('SUM(commission)')))
                ->join('affiliate_campaigns', 'affiliate_campaigns.id = affiliate_campaign_detailed_statistics.affiliate_campaign_id', array())
                ->where('affiliate_campaigns.campaign_id = ?', (int)$campaignId)
                ->group('date_upload')
                ->order('affiliate_campaign_detailed_statistics.date_upload DESC')
                ->limit(4);
            $affiliateStat = $this->getAdapter()->fetchAssoc($select);

            $adSelect = $this->getAdapter()->select()
                ->join('ad_campaigns', 'ad_campaigns.id = ad_campaign_detailed_statistics.ad_campaign_id', array())
                ->from('ad_campaign_detailed_statistics', array('date_upload', 'spend' => new Zend_DB_Expr('SUM(spend)')))
                ->where('ad_campaigns.campaign_id = ?', (int)$campaignId)
                ->group('date_upload')
                ->order('ad_campaign_detailed_statistics.date_upload DESC')
                ->limit(4);
            $adStat = $this->getAdapter()->fetchAssoc($adSelect);

            $statDates = array_unique(array_merge(array_keys($affiliateStat), array_keys($adStat)));
            arsort($statDates);
            $statDates = array_slice($statDates, 0, 2);
            if(!$statDates) {
                continue;
            }

            $prevCommission = !empty($affiliateStat[$statDates[1]]) ? $affiliateStat[$statDates[1]]['commission'] : 0;
            $newCommission = !empty($affiliateStat[$statDates[0]]) ? $affiliateStat[$statDates[0]]['commission'] : 0;

            $prevSpend = !empty($adStat[$statDates[1]]) ? $adStat[$statDates[1]]['spend'] : 0;
            $newSpend = !empty($adStat[$statDates[0]]) ? $adStat[$statDates[0]]['spend'] : 0;

            $result[$campaignId] = array('prev_profit' => $prevCommission-$prevSpend,
                'new_profit' => $newCommission - $newSpend);
        }

        return $result;
    }
}