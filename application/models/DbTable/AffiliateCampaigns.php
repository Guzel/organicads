<?php

class Application_Model_DbTable_AffiliateCampaigns extends Zend_Db_Table_Abstract
{
    protected $_name = 'affiliate_campaigns';

    public function create($data, $id = null)
    {
        if($id) {
            $newCampaign = $this->find($id)->current();
            if($newCampaign->campaign_id) {
                unset($data['campaign_id']); //do not rewrite campaign_id
            }
        } else {
            $newCampaign = $this->createRow();
            $newCampaign->added = new Zend_Db_Expr('NOW()');
        }
        $newCampaign->setFromArray($data);
        $newCampaign->save();

        return $newCampaign;
    }

    public function getPairs($userId = null, $affiliateId = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('api_id', 'id'));
        if($userId) {
            $select->where('user_id = ?', (int)$userId);
        }
        if($affiliateId) {
            $select->where('affiliate_id = ?', (int)$affiliateId);
        }
        return $this->getAdapter()->fetchPairs($select);
    }

    public function getCampaignsByUser($userId)
    {
        $select = $this->select()->where('user_id = ?', (int)$userId);
        return $this->fetchAll($select);
    }

    /**
     * Set campaign_id for given affiliate campaigns
     * 
     * @param int $campaignId
     * @param array|int $affiliateCampaigns
     * 
     * @return void
     */
    public function updateCampaignId($campaignId, $affiliateCampaigns)
    {
        $affiliateCampaignIds = is_array($affiliateCampaigns) ? $affiliateCampaigns : explode(',', $affiliateCampaigns);

        //set null to current affiliate campaigns by given campaign_id:
        $this->update(array('campaign_id' => null), array('campaign_id = ?' => (int)$campaignId));
        $this->update(array('campaign_id' => (int)$campaignId), array('id IN(?)' => $affiliateCampaignIds));
    }

    /**
     * Automatic Pairing by given Keyword
     * 
     * @param int $campaignId
     * @param string $keyword
     * @param int $userId
     * 
     * @return void
     */
    public function pairingByKeyword($campaignId, $keyword, $userId)
    {
        $where[] = 'campaign_id IS NULL';
        $where[] = $this->getAdapter()->quoteInto('`user_id` = ?', (int)$userId);
        $where[] = $this->getAdapter()->quoteInto("`name` LIKE BINARY ?", "%{$keyword}%");

        $this->update(array('campaign_id' => (int)$campaignId), $where);
    }

    public function getNotPairedCampaigns($userId, $campaignId = null, $withRevenueOnly = false)
    {
        $today = new DateTime();
        $revenueFrom = $today->sub(new DateInterval('P30D'))->format('Y-m-d');

        $select = $this->getAdapter()->select()
            ->from($this->_name)
            ->joinLeft('affiliate_campaign_statistics', 
                "affiliate_campaign_statistics.affiliate_campaign_id = affiliate_campaigns.id AND affiliate_campaign_statistics.date > '" . $revenueFrom . "'", 
                array('date')
            )
            ->where('affiliate_campaigns.user_id = ?', (int)$userId)
            ->where('campaign_id IS NULL' . ($campaignId ? " OR campaign_id = {$campaignId}" : ''))
            ->group('affiliate_campaigns.id');

        if($withRevenueOnly) {
            $select->where('date IS NOT NULL');
            return  $this->getAdapter()->fetchAll($select);
        }

        $campaigns = $this->getAdapter()->fetchAll($select);

        $result = array();
        foreach($campaigns as $campaign) {
            $campaign = (object)$campaign;
            $result[$campaign->affiliate_id][] = $campaign;
        }

        return $result;
    }

    public function assignedAmtByCampaign($userId = null)
    {
        $select = $this->getAdapter()->select()
                ->from($this->_name, array('campaign_id', 'COUNT(id)'))
                ->where('user_id = ?', (int)$userId)
                ->where('campaign_id IS NOT NULL')
                ->group('campaign_id');
        return $this->getAdapter()->fetchPairs($select);
    }
}