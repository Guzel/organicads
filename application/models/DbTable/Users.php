<?php

class Application_Model_DbTable_Users extends Zend_Db_Table_Abstract
{
    protected $_name = 'users';

    const USER = 'user';
    const USER_CRM = 'user_crm';
    const USER_ACC = 'user_acc';
    const ADMIN = 'admin';

    /**
     * Create or update user's data
     * 
     * @param array $data
     * @param int $userId
     * @return Zend_Db_Table_Row
     */
    public function save($data, $userId = null)
    {
        if($userId) {
            $user = $this->find($userId)->current();
        } else {
            $user = $this->createRow();
            $user->added = new Zend_Db_Expr('NOW()');
        }

        $user->setFromArray($data);
        if(!empty($data['user_password'])) {
            $user->password = Application_Model_User::getPasswordHash($data['user_password']);
        }
        $user->save();

        return $user;
    }

    /**
     * Find user by email
     * 
     * @param string $email
     * @return Application_Model_DbTable_User
     */
    public function findUserByEmail($email)
    {
        $select = $this->select()->where('email = ?', $email);
        return $this->fetchRow($select);
    }

    /**
     * Find user by twilio phone
     * 
     * @param string $phone
     * @return Application_Model_DbTable_User
     */
    public function findUserByTwilio($phone)
    {
        $phone = App_Utils::normalizePhone($phone);

        $select = $this->select()->where('twilio_phone = ?', $phone);
        return $this->fetchRow($select);
    }

    /**
     * Returns users
     * 
     * @param int|null $parentId
     * @return Zend_Db_Select
     */
    public function getUsers($parentId = null)
    {
        $select = $this->select()->order('added DESC');
        if($parentId) {
            $select->where('parent_id = ?', (int)$parentId);
        }
        return $select;
    }

    /**
     * Returns users with enabled option "automatic stat update"
     * 
     * @return array
     */
    public function getUsersForStatUpdate()
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name)
            ->joinLeft('affiliates', 'affiliates.user_id = users.id', array('affiliate_id' => 'affiliates.id'))
            ->joinLeft('ads', 'ads.user_id = users.id', array('ad_id' => 'ads.id'))
            ->where('auto_stat_update = 1')
            ->where(new Zend_Db_Expr('(users.last_update + INTERVAL `stat_update_interval` MINUTE <= NOW()) OR (users.last_update IS NULL)'))
            ->having('affiliate_id IS NOT NULL OR ad_id IS NOT NULL')
            ->group('users.id');
        return $this->getAdapter()->fetchAll($select);
    }
}