<?php

class Application_Model_DbTable_Leads extends Zend_Db_Table_Abstract
{
    protected $_name = 'leads';

    public function getStatistics($options, $sold, $startDate, $endDate = null)
    {
        $startDate = strtotime($startDate);
        $endDate = strtotime($endDate);

        $select = $this->getAdapter()->select()
            ->from($this->_name, array('DATE(date)', 'leads' => new Zend_DB_Expr('COUNT(id)')))
            ->where('date >= ?', strftime("%Y-%m-%d", $startDate))
            ->group('DATE(date)')
            ->order('date ASC');

        if(null !== $sold) {
            $select->where('sold = ?', (int)$sold);
        }

        if(!empty($options['client_id'])) {
            $select->where('client_id = ?', (int)$options['client_id']);
        }

        if(!empty($options['user_id'])) {
            $select->where('user_id = ?', (int)$options['user_id']);
        }

        if($endDate) {
            $select->where('date <= ?', strftime("%Y-%m-%d", $endDate));
        }

        $data = $this->getAdapter()->fetchAssoc($select);
        return $data;
    }

    public function findTotalStatistics($userId, $sold, $startDate=null, $endDate = null)
    {
        $startDate = strtotime($startDate);
        $endDate = strtotime($endDate);

        $select = $this->getAdapter()->select()
            ->from($this->_name, array('leads' => new Zend_DB_Expr('COUNT(id)')))
            ->where('user_id = ?', (int)$userId);

        if(null !== $sold) {
            $select->where('sold = ?', (int)$sold);
        }

        if($startDate) {
            $select->where('date >= ?', strftime("%Y-%m-%d", strtotime($startDate)));
        }
        if($endDate) {
            $select->where('date <= ?', strftime("%Y-%m-%d", strtotime($endDate)));
        }

        return $this->getAdapter()->fetchRow($select);
    }

    public function findStatisticsByClients($userId, $sold, $startDate=null, $endDate = null)
    {
        $startDate = strtotime($startDate);
        $endDate = strtotime($endDate);

        $select = $this->getAdapter()->select()
            ->from($this->_name, array('client_id', 'leads' => new Zend_DB_Expr('COUNT(id)')))
            ->where('client_id IS NOT NULL')
            ->where('user_id = ?', $userId)
            ->where('date >= ?', strftime("%Y-%m-%d", $startDate))
            ->group('client_id')
            ->order('COUNT(id) DESC');

        if(null !== $sold) {
            $select->where('sold = ?', (int)$sold);
        }

        if($endDate) {
            $select->where('date <= ?', strftime("%Y-%m-%d", $endDate));
        }

        $data = $this->getAdapter()->fetchAssoc($select);
        return $data;
    }

    public function deleteLeads($clientId, $testing = false)
    {
        $where[] = $this->getAdapter()->quoteInto('client_id = ?', (int)$clientId);
        if($testing) {
            $where[] = 'testing = 1';
        }

        $this->delete($where);
    }

    public function deleteLeadsById($id, $userId = null)
    {
        $ids = is_array($id) ? $id : array($id);
        $where[] = $this->getAdapter()->quoteInto('id IN(?)', $ids);
        if($userId) {
            $where[] = $this->getAdapter()->quoteInto('user_id = ?', $userId);
        }

        $this->delete($where);
    }

    public function exctractFields($lead)
    {
        if(!$lead instanceof Zend_Db_Table_Row) {
            $lead = $this->find((int)$lead)->current();
        }

        $leadFieldsModel = new Application_Model_DbTable_LeadFields();
        $fields = unserialize($lead->fields);
        foreach($fields as $fieldId => $value) {
            $newField = $leadFieldsModel->createRow();
            $newField->lead_id = $lead->id;
            $newField->field_id = $fieldId;
            $newField->value = $value;
            $newField->save();
        }
    }

    /**
     * Delete lead duplicates for given user id by field ids
     * 
     * @param type $userId
     * @param type $fields
     */
    public function deleteDuplicates($userId, $fields = array())
    {
        $fields = array_filter($fields);
        $leadFields = new Application_Model_DbTable_LeadFields();

        $i = 1;
        foreach($fields as $fieldId) {
            $leadIds = $leadFields->getDuplicates($fieldId);
            if($i == 1) {
                $duplicateLeads = $leadIds;
            }
            $duplicateLeads = array_intersect($duplicateLeads, $leadIds);
            $i++;
        }

        if($duplicateLeads) {
            $where[] = $this->getAdapter()->quoteInto('id IN(?)', $duplicateLeads);
            $where[] = $this->getAdapter()->quoteInto('user_id = ?', (int)$userId);
            $this->delete($where);
        }
    }
}