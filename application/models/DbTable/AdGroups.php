<?php

class Application_Model_DbTable_AdGroups extends Zend_Db_Table_Abstract
{
    protected $_name = 'ad_groups';

    /**
     * Create/Update new ad group
     * 
     * @param array $data
     * 
     * @return Zend_Db_Table_Row
     */
    public function create($data)
    {
        $newGroup = $this->createRow();
        $newGroup->added = new Zend_Db_Expr('NOW()');
        $newGroup->setFromArray($data);
        $newGroup->save();

        return $newGroup;
    }

     /**
     * Get ad group pairs
     * 
     * @param int|null $adId
     * 
     * @return array [api_id] => [id]
     */
    public function getPairs($adId = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('api_id', 'id'));
        if($adId) {
            $select->where('ad_id = ?', $adId);
        }
        return $this->getAdapter()->fetchPairs($select);
    }
}