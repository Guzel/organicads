<?php

class Application_Model_DbTable_AdvertSubidPairs extends Zend_Db_Table_Abstract
{
    protected $_name = 'advert_subid_pairs';

    public function save($userId, $campaignId, $data)
    {
        $this->delete('campaign_id = ' . (int)$campaignId);

        $data = (array)$data;
        foreach($data as $row) {
            if(!$row['name'] || !$row['subid'] || !$row['advert_id']) {
                continue;
            }
            $pair = $this->createRow();
            $pair->user_id = (int)$userId;
            $pair->name = $row['name'];
            $pair->campaign_id = (int)$campaignId;
            $pair->advert_id = (int)$row['advert_id'];
            $pair->affiliate_subid_id = (int)$row['subid'];
            $pair->added = new Zend_Db_Expr('NOW()');
            $pair->save();
        }
    }

    public function getByUserId($userId) 
    {
        $select = $this->select()->where('user_id = ?', (int)$userId);
        $pairs = $this->fetchAll($select);

        $result = array();
        foreach($pairs as $pair) {
            $result[$pair->campaign_id][] = $pair;
        }
        return $result;
    }

    public function getPairs($userId, $key='advert_id', $value='affiliate_subid_id')
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array($key, $value))
            ->where('user_id = ?', (int)$userId);
        return $this->getAdapter()->fetchPairs($select);
    }

    public function getAdPairs($userId)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('advert_subid_pairs.advert_id', 'advert_subid_pairs.*'))
            ->where('user_id = ?', (int)$userId)
            ->group('advert_subid_pairs.advert_id');
        return $this->getAdapter()->fetchAssoc($select);
    }
}