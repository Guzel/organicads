<?php

class Application_Model_DbTable_ClientCap extends Zend_Db_Table_Abstract
{
    protected $_name = 'client_cap';

    public function save($data, $clientId)
    {
        $cap = $this->createRow();
        $cap->client_id = $clientId;
        $cap->regularity = $data['regularity'];
        $cap->all_leads = !empty($data['all']) ? $data['all'] : null;
        $cap->states_options = !empty($data['states']) ? serialize($data['states']) : null;
        $cap->save();

        return $cap;
    }

    public function deleteByClientId($clientId)
    {
        $this->delete('client_id = ' . (int)$clientId);
    }

    public function getFormPopulateData($clientId)
    {
        $select = $this->select()->where('client_id = ?', $clientId);
        $capsValues = $this->fetchRow($select)->toArray();

        $capsValues['criteria'] = $capsValues['all_leads'] ? 'all' : 'state';
        $capsValues['states'] = unserialize($capsValues['states_options']);
        $capsValues['all'] = $capsValues['all_leads'];

        return $capsValues;
    }
}