<?php

class Application_Model_DbTable_AdvertsList extends Zend_Db_Table_Abstract
{
    protected $_name = 'adverts_list';

    /**
     * Create/Update new ad
     * 
     * @param array $data
     * 
     * @return Zend_Db_Table_Row
     */
    public function create($data, $id = null)
    {
        if($id) {
            $newAd = $this->find($id)->current();
        } else {
            $newAd = $this->createRow();
            $newAd->added = new Zend_Db_Expr('NOW()');
        }
        $newAd->setFromArray($data);
        $newAd->save();

        return $newAd;
    }

     /**
     * Get ad pairs
     * 
     * @param int|null $adId
     * 
     * @return array [$key] => [$value]
     */
    public function getPairs($adId = null, $key = 'api_id', $value = 'id')
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array($key, $value));
        if($adId) {
            $select->where('ad_id = ?', $adId);
        }
        return $this->getAdapter()->fetchPairs($select);
    }

    public function getByAdCampaignIds($adCampaigns = array())
    {
        $adCampaigns = (array)$adCampaigns;
        $select = $this->getAdapter()->select('adverts_list.*')
            ->from($this->_name)
            ->joinLeft('ad_groups', 'ad_groups.id = adverts_list.ad_group_id', array('ad_group' => 'ad_groups.name'))
            ->join('ads', 'ads.id = adverts_list.ad_id', array('ads.source'))
            ->where('ad_groups.ad_campaign_id IN(?)', $adCampaigns)
            ->orWhere('adverts_list.ad_campaign_id IN(?)', $adCampaigns)
            ->order('ads.source')
            ->order('status');
        return $this->getAdapter()->fetchAll($select);
    }

    public function getByCampaigns($campaigns = array())
    {
        $campaigns = (array)$campaigns;
        $select = $this->select()->where('campaign_id IN(?)', $campaigns);
        return $this->fetchAll($select);
    }

    /**
     * Set campaign_id for given adverts list
     * 
     * @param int $campaignId
     * @param array $advertsList
     * @param int|bool $setNew
     * 
     * @return void
     */
    public function updateCampaignId($campaignId, $advertsList, $setNew = true)
    {
        //set null to current affiliate campaigns by given campaign_id:
        $this->update(array('campaign_id' => null, 'affiliate_subid' => null), array('campaign_id = ?' => (int)$campaignId));
        if($setNew) {
            foreach($advertsList as $advertId => $subId) {
                if(!$subId) {continue;}
                $this->update(array('campaign_id' => (int)$campaignId, 'affiliate_subid' => $subId), array('id = ?' => (int)$advertId));
            }
        }
    }

    public function getPairedAdCampaigs($campaignId)
    {
        /*$select = $this->getAdapter()->select()
            ->from($this->_name, array('ad_groups.ad_campaign_id', 'ad_groups.ad_campaign_id'))
            ->join('advert_subid_pairs', 'advert_subid_pairs.advert_id = adverts_list.id')
            ->join('ad_groups', 'ad_groups.id = adverts_list.ad_group_id')
            ->where('advert_subid_pairs.campaign_id = ?', (int)$campaignId);
        return $this->getAdapter()->fetchPairs($select);*/
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('ad_campaign_id', 'ad_campaign_id'))
            ->join('advert_subid_pairs', 'advert_subid_pairs.advert_id = adverts_list.id')
            ->where('advert_subid_pairs.campaign_id = ?', (int)$campaignId);
        return $this->getAdapter()->fetchPairs($select);
    }
}