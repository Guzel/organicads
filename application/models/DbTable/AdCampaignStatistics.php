<?php

class Application_Model_DbTable_AdCampaignStatistics extends Zend_Db_Table_Abstract
{
    protected $_name = 'ad_campaign_statistics';

    /**
     * Delete stattistics started from given date
     * 
     * @param int $adId
     * @param string $dateFrom
     */
    public function deleteStatByDate($adId, $dateFrom)
    {
        $where[] = $this->getAdapter()->quoteInto('ad_id = ?', (int)$adId);
        $where[] = $this->getAdapter()->quoteInto('date >= ?', 
            new Zend_Db_Expr("DATE('" . strftime("%Y-%m-%d", strtotime($dateFrom)) . "')"));

        $this->delete($where);
    }

    /**
     * Find total clicks, impressions and spends by given campaignId or userId and dates,
     * group results by date
     * 
     * @param array $options
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function findTotalByCampaign($options, $startDate, $endDate = null)
    {
        $startDate = strtotime($startDate);
        $endDate = strtotime($endDate);

        $select = $this->getAdapter()->select()
            ->join('ad_campaigns', 'ad_campaigns.id = ad_campaign_statistics.ad_campaign_id', array())
            ->from($this->_name, array('date', 'clicks' => new Zend_DB_Expr('SUM(clicks)'), 'impressions' => new Zend_DB_Expr('SUM(impressions)'),
                'spend' => new Zend_DB_Expr('SUM(spend)')))
            ->where('ad_campaigns.campaign_id IS NOT NULL')
            ->where('ad_campaign_statistics.advert_id IS NULL')
            ->where('date >= ?', strftime("%Y-%m-%d", $startDate))
            ->group('date')
            ->order('date ASC');

        if(!empty($options['campaign_id'])) {
            $select->where('ad_campaigns.campaign_id = ?', (int)$options['campaign_id']);
        }

        if(!empty($options['user_id'])) {
            $select->where('ad_campaign_statistics.user_id = ?', (int)$options['user_id']);
        }

        if($endDate) {
            $select->where('date <= ?', strftime("%Y-%m-%d", $endDate));
        }

        $data = $this->getAdapter()->fetchAssoc($select);
        return $data;
    }

    /**
     * Find total spends by given campaignId and dates,
     * group results by date and ad account. Returns statistics and ad accounts
     * 
     * @param array $options
     * @param string $startDate
     * @param string $endDate
     * 
     * @return array 
     * 'statistics' => array('date' => array('ad_id' => 'spend'))
     * 'ad_accounts' => array('ad_id' => Zend_Db_Table_Row)
     */
    public function findSpendByAccounts($campaignId, $startDate, $endDate = null)
    {
        $startDate = strtotime($startDate);
        $endDate = strtotime($endDate);

        $select = $this->getAdapter()->select()
            ->join('ad_campaigns', 'ad_campaigns.id = ad_campaign_statistics.ad_campaign_id', array())
            ->from($this->_name, array('ad_campaign_statistics.*'))
            ->where('ad_campaigns.campaign_id = ?', (int)$campaignId)
            ->where('ad_campaign_statistics.advert_id IS NULL')
            ->where('date >= ?', strftime("%Y-%m-%d", $startDate))
            ->order('date ASC');

        if($endDate) {
            $select->where('date <= ?', strftime("%Y-%m-%d", $endDate));
        }

        $data = $this->getAdapter()->fetchAll($select);

        $adsModel = new Application_Model_DbTable_Ads();
        $result = $statistics = $adAccounts = array();
        foreach($data as $row) {
            if(!isset($statistics[$row['date']][$row['ad_id']])) {
                $statistics[$row['date']][$row['ad_id']] = 0;
            }
            $statistics[$row['date']][$row['ad_id']] += $row['spend'];
            if(empty($adAccounts[$row['ad_id']])) {
                $adAccounts[$row['ad_id']] = $adsModel->find($row['ad_id'])->current();
            }
        }
        $result['statistics'] = $statistics;
        $result['ad_accounts'] = $adAccounts;

        return $result;
    }

    /**
     * Get total amount spent by user in given dates
     * 
     * @param int $userId
     * @param string $startDate
     * @param string $endDate
     * @return float
     */
    public function findTotalSpend($userId, $startDate, $endDate = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array(new Zend_DB_Expr('SUM(spend)')))
            ->join('ad_campaigns', 'ad_campaigns.id = ad_campaign_statistics.ad_campaign_id', array())
            ->where('ad_campaigns.campaign_id IS NOT NULL')
            ->where('ad_campaign_statistics.advert_id IS NULL')
            ->where('ad_campaign_statistics.user_id = ?', (int)$userId);
        if($startDate) {
            $select->where('date >= ?', strftime("%Y-%m-%d", strtotime($startDate)));
        }
        if($endDate) {
            $select->where('date <= ?', strftime("%Y-%m-%d", strtotime($endDate)));
        }
        return $this->getAdapter()->fetchOne($select);
    }

    /**
     * Get total amount spent by user in given dates, group by Campaign
     * 
     * @param array $options
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function findTotalCampaignSpend($options, $startDate = null, $endDate = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('ad_campaigns.campaign_id', 'spend' => new Zend_DB_Expr('SUM(spend)')))
            ->join('ad_campaigns', 'ad_campaigns.id = ad_campaign_statistics.ad_campaign_id', array())
            ->where('ad_campaigns.campaign_id IS NOT NULL')
            ->where('ad_campaign_statistics.advert_id IS NULL');

        if(!empty($options['user_id'])) {
            $select->where('ad_campaigns.user_id = ?', (int)$options['user_id']);
        }
        if(!empty($options['campaign_id'])) {
            $select->where('ad_campaigns.campaign_id = ?', (int)$options['campaign_id']);
        }
        if($startDate) {
            $select->where('date >= ?', strftime("%Y-%m-%d", strtotime($startDate)));
        }
        if($endDate) {
            $select->where('date <= ?', strftime("%Y-%m-%d", strtotime($endDate)));
        }
        //$select->having('spend > 0');
        $select->group('campaign_id');

        if(!empty($options['campaign_id'])) {
            $stat = $this->getAdapter()->fetchRow($select);
            return $stat ? $stat : array();
        }
        return $this->getAdapter()->fetchAssoc($select);
    }

    /**
     * Get total amount spent by user in given dates, group by Campaign
     * 
     * @param array $options
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function findTotalPlatformSpend($options, $startDate = null, $endDate = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('ads.source', 'spend' => new Zend_DB_Expr('SUM(spend)')))
            ->join('ads', 'ads.id = ad_campaign_statistics.ad_id', array())
            ->join('ad_campaigns', 'ad_campaigns.id = ad_campaign_statistics.ad_campaign_id', array())
            ->where('ad_campaigns.campaign_id IS NOT NULL')
            ->where('ad_campaign_statistics.advert_id IS NULL')
            ->order('spend DESC');
        if(!empty($options['user_id'])) {
            $select->where('ad_campaign_statistics.user_id = ?', (int)$options['user_id']);
        }
        if(!empty($options['campaign_id'])) {
            $select->where('ad_campaigns.campaign_id = ?', (int)$options['campaign_id']);
        }
        if($startDate) {
            $select->where('date >= ?', strftime("%Y-%m-%d", strtotime($startDate)));
        }
        if($endDate) {
            $select->where('date <= ?', strftime("%Y-%m-%d", strtotime($endDate)));
        }
        $select->having('spend > 0');
        $select->group('ads.source');

        return $this->getAdapter()->fetchAssoc($select);
    }

    /**
     * Get total Advert spend by given campaign id
     * 
     * @param array $options
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function findTotalAdvertSpend($options, $startDate = null, $endDate = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('advert_subid_pairs.advert_id', 'advert_subid_pairs.campaign_id', 'spend' => new Zend_DB_Expr('SUM(spend)')))
            ->join('advert_subid_pairs', 'advert_subid_pairs.advert_id = ad_campaign_statistics.advert_id', array())
            ->where('ad_campaign_statistics.advert_id IS NOT NULL');
        if(!empty($options['campaign_id'])) {
            $select->where('advert_subid_pairs.campaign_id = ?', (int)$options['campaign_id']);
        }
        if(!empty($options['user_id'])) {
            $select->where('advert_subid_pairs.user_id = ?', (int)$options['user_id']);
        }
        if($startDate) {
            $select->where('date >= ?', strftime("%Y-%m-%d", strtotime($startDate)));
        }
        if($endDate) {
            $select->where('date <= ?', strftime("%Y-%m-%d", strtotime($endDate)));
        }
        $select->group('advert_subid_pairs.advert_id');

        return $this->getAdapter()->fetchAssoc($select);
    }

    /**
     * Find Statistics grouped by advert name
     * 
     * @param array $options
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function findTotalByAdverName($options, $startDate = null, $endDate = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('adverts_list.header', 'clicks' => new Zend_DB_Expr('SUM(clicks)'), 'impressions' => new Zend_DB_Expr('SUM(impressions)'),
                'spend' => new Zend_DB_Expr('SUM(spend)'), 'ctr' => new Zend_DB_Expr('AVG(ctr)')))
            ->join('adverts_list', 'adverts_list.id = ad_campaign_statistics.advert_id', array())
            ->where('ad_campaign_statistics.advert_id IS NOT NULL');
        if(!empty($options['campaign_id'])) {
            $select->join('ad_campaigns', 'ad_campaigns.id = adverts_list.ad_campaign_id', array())
                ->where('ad_campaigns.campaign_id = ?', (int)$options['campaign_id']);
        }
        if(!empty($options['user_id'])) {
            $select->where('ad_campaign_statistics.user_id = ?', (int)$options['user_id']);
        }
        if(!empty($options['query'])) {
            $select->where('adverts_list.header LIKE BINARY ?', "%{$options['query']}%");
        }
        if($startDate) {
            $select->where('date >= ?', strftime("%Y-%m-%d", strtotime($startDate)));
        }
        if($endDate) {
            $select->where('date <= ?', strftime("%Y-%m-%d", strtotime($endDate)));
        }

        $select->group('adverts_list.header');

        return $this->getAdapter()->fetchAssoc($select);
    }
}