<?php

class Application_Model_DbTable_AffiliateSubids extends Zend_Db_Table_Abstract
{
    protected $_name = 'affiliate_subids';

    public function create($data, $id = null)
    {
        if($id) {
            $newSubid = $this->find($id)->current();
        } else {
            $newSubid = $this->createRow();
            $newSubid->added = new Zend_Db_Expr('NOW()');
        }
        $newSubid->setFromArray($data);
        $newSubid->save();

        return $newSubid;
    }

     /**
     * Get subId pairs
     * 
     * @param array|null $options
     * 
     * @return array [id] => [name]
     */
    public function getPairs($options = array())
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('id', 'name'));
        if(!empty($options['userId'])) {
            $select->where('user_id = ?', (int)$options['userId']);
        }
        if(!empty($options['affiliateId'])) {
            $select->where('affiliate_id = ?', (int)$options['affiliateId']);
        }
        if(!empty($options['affiliate_campaign_id'])) {
            $select->where('affiliate_campaign_id IN(?)', (array)$options['affiliate_campaign_id']);
        }
        return $this->getAdapter()->fetchPairs($select);
    }

    public function getPairedCampaignIds($affiliateId)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('affiliate_campaigns.id', 'affiliate_campaigns.api_id'))
            ->join('affiliate_campaigns', 'affiliate_campaigns.id = affiliate_subids.affiliate_campaign_id')
            ->join('advert_subid_pairs', 'advert_subid_pairs.affiliate_subid_id = affiliate_subids.id')
            ->where('affiliate_subids.affiliate_id = ?', (int)$affiliateId);
        return $this->getAdapter()->fetchPairs($select);
    }

    /**
     * Set campaign_id for given affiliate subids
     * 
     * @param int $campaignId
     * @param array|string $affiliateSubids
     * @param int|bool $setNew
     * 
     * @return void
     */
    public function updateCampaignId($campaignId, $affiliateSubids, $setNew = true)
    {
        $affiliateSubids = is_array($affiliateSubids) ? $affiliateSubids : explode(',', $affiliateSubids);

        //set null to current affiliate subids by given campaign_id:
        $this->update(array('campaign_id' => null), array('campaign_id = ?' => (int)$campaignId));
        if($setNew) {
            $this->update(array('campaign_id' => (int)$campaignId), array('id IN(?)' => $affiliateSubids));
        }
    }

    public function getPairedAffiliateCampaigs($campaignId)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('affiliate_subids.affiliate_campaign_id', 'affiliate_subids.affiliate_campaign_id'))
            ->join('advert_subid_pairs', 'advert_subid_pairs.affiliate_subid_id = affiliate_subids.id')
            ->where('advert_subid_pairs.campaign_id = ?', (int)$campaignId);
        return $this->getAdapter()->fetchPairs($select);
    }
}