<?php

class Application_Model_DbTable_NotificationContacts extends Zend_Db_Table_Abstract
{
    protected $_name = 'notification_contacts';

    public function add($notificationId, $contact_id)
    {
        $newRecord = $this->createRow();
        $newRecord->notification_id = (int)$notificationId;
        $newRecord->user_contact_id = (int)$contact_id;
        $newRecord->save();
    }

    /**
     * Delete contacts by notification id
     * 
     * @param int $notificationId
     */
    public function removeByNotificationId($notificationId)
    {
        $where[] = $this->getAdapter()->quoteInto('notification_id = ?', (int)$notificationId);

        $this->delete($where);
    }

    public function getContacts($userId, $notificationId = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name)
            ->join('user_contacts', 'user_contacts.id = notification_contacts.user_contact_id')
            ->where('user_contacts.user_id = ?', (int)$userId);
        if($notificationId) {
            $select->where('notification_id = ?', (int)$notificationId);
        }
        $contacts = $this->getAdapter()->fetchAll($select);

        $result = array();
        foreach($contacts as $contact) {
            $result[$contact['notification_id']][$contact['user_contact_id']] = $contact;
        }

        if($notificationId) {
            return !empty($result[$notificationId]) ? $result[$notificationId] : null;
        }
        return $result;
    }
}