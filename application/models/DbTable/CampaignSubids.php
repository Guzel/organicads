<?php
class Application_Model_DbTable_CampaignSubids extends Zend_Db_Table_Abstract
{
    public $_name = 'campaign_subids';

    public function create($campaignId, $data = array())
    {
        $this->delete('campaign_id = ' . (int)$campaignId);
        foreach($data as $affiliate => $adSettings) {
            foreach($adSettings as $ad => $subidKeyword) {
                if(!$subidKeyword) {
                    continue;
                }
                $row = $this->createRow();
                $row->campaign_id = (int)$campaignId;
                $row->affiliate = $affiliate;
                $row->ad = $ad;
                $row->code = $subidKeyword;
                $row->added = new Zend_Db_Expr('NOW()');
                $row->save();
            }
        }
    }

    public function findByCampaign($campaignId)
    {
        $select = $this->select()->where('campaign_id = ?', (int)$campaignId);
        return $this->fetchAll($select);
    }
}