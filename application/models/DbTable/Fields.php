<?php

class Application_Model_DbTable_Fields extends Zend_Db_Table_Abstract
{
    protected $_name = 'fields';

    public function findByTitle($title, $userId = null)
    {
        $select = $this->select()
            ->where('title = ?', $title);
        if($userId) {
            $select->where('user_id = ?', $userId);
        }

        return $this->fetchRow($select);
    }

    public function idByName($name)
    {
        $name = strtolower($name);

        $manager = Application_Model_Cache::getManager();
        $cache = $manager->getCache('database');
        $cacheId = "basic_fields";
        $fields = $cache->load($cacheId);

        if (!$fields) {
            $generalFields = array_map('strtolower', $this->getPairs());
            $fields = array_combine($generalFields, array_keys($generalFields));

            $cache->save($fields);
        }

        return $fields[$name];
    }

    public function getPairs($userId = null, $fieldId = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('id', 'title'))
            ->order('title ASC');
        if($userId) {
            $select->where('user_id IS NULL OR user_id = ?', (int)$userId);
        }
        if($fieldId) {
            $fieldIds = is_array($fieldId) ? $fieldId : array($fieldId);
            $select->where('id IN (?)', $fieldIds);
        }
        return $this->getAdapter()->fetchPairs($select);
    }

    public function getFields($userId = null) {
        $select = $this->select()
            ->where('user_id IS NULL' . ($userId ? ' OR user_id = ' . (int)$userId : ''))
            ->order('id ASC');
        return $this->fetchAll($select);
    }

    public function getFilterDelimiter()
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('id', 'filter_delimiter'))
            ->where('filter_delimiter IS NOT NULL');
        return $this->getAdapter()->fetchPairs($select);
    }
}