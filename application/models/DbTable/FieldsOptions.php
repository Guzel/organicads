<?php

class Application_Model_DbTable_FieldsOptions extends Zend_Db_Table_Abstract
{
    protected $_name = 'fields_options';

    public function save($data, $fieldId, $userId)
    {
        $options = $this->find($fieldId, $userId)->current();
        if(!$options) {
            $options = $this->createRow();
            $options->field_id = (int)$fieldId;
            $options->user_id = (int)$userId;
        }
        $options->setFromArray($data);
        $options->save();
    }

    public function findByName($name, $userId = null)
    {
        $select = $this->select()
            ->where('alter_name = ?', $name);
        if($userId) {
            $select->where('user_id = ?', $userId);
        }

        return $this->fetchRow($select);
    }

    public function getPairs($userId)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('field_id', 'alter_name'));
        if($userId) {
            $select->where('user_id = ?', (int)$userId);
        }
        return $this->getAdapter()->fetchPairs($select);
    }
}