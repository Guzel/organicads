<?php

class Application_Model_DbTable_LeadFields extends Zend_Db_Table_Abstract
{
    protected $_name = 'lead_fields';
    
    /*
     SELECT lead_fields.*
        FROM lead_fields, lead_fields AS vtable
        WHERE vtable.id > lead_fields.id AND vtable.value = lead_fields.value AND lead_fields.field_id = 4
        GROUP BY lead_fields.lead_id
        
        DELETE
        FROM lead_fields
        USING lead_fields, lead_fields AS vtable
        WHERE vtable.id > lead_fields.id AND vtable.value = lead_fields.value AND lead_fields.field_id = 4
     */

    /**
     * Determine the lead ids with the same value of given field id
     * 
     * @param int $field_id
     * @return array
     */
    public function getDuplicates($field_id)
    {
        $sql = "SELECT lead_fields.lead_id
        FROM lead_fields, lead_fields AS vtable
        WHERE vtable.id > lead_fields.id AND vtable.value = lead_fields.value AND lead_fields.field_id = ?
        GROUP BY lead_fields.lead_id";

        $stmt = $this->getAdapter()->query($sql, array($field_id));
        $leads = $stmt->fetchAll();

        $result = array();
        foreach($leads as $lead) {
            $result[] = $lead['lead_id'];
        }
        return $result;
    }
}