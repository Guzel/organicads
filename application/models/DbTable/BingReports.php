<?php

class Application_Model_DbTable_BingReports extends Zend_Db_Table_Abstract
{
    protected $_name = 'bing_reports';

    public function save($data)
    {
        $newLog = $this->createRow();
        $newLog->setFromArray($data);
        $newLog->date = new Zend_Db_Expr('NOW()');
        $newLog->save();
    }
}