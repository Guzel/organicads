<?php

class Application_Model_DbTable_AffiliateCampaignStatistics extends Zend_Db_Table_Abstract
{
    protected $_name = 'affiliate_campaign_statistics';

    /**
     * Delete stattistics started from given date
     * 
     * @param int $affiliateId
     * @param string $dateFrom
     */
    public function deleteStatByDate($affilateId, $dateFrom)
    {
        $where[] = $this->getAdapter()->quoteInto('affiliate_id = ?', (int)$affilateId);
        $where[] = $this->getAdapter()->quoteInto('date >= ?', 
            new Zend_Db_Expr("DATE('" . strftime("%Y-%m-%d", strtotime($dateFrom)) . "')"));

        $this->delete($where);
    }

    /**
     * Find total leads, impressions and revenue by given campaignId or userId and dates,
     * group results by date
     * 
     * @param arrat $options
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function findTotalByCampaign($options, $startDate, $endDate = null)
    {
        $startDate = strtotime($startDate);
        $endDate = strtotime($endDate);

        $userId = !empty($options['user_id']) ? $options['user_id'] : '';
        $campaignId = !empty($options['campaign_id']) ? $options['campaign_id'] : '';

        $select = $this->getAdapter()->select()
            ->from($this->_name, array('date', 'leads' => new Zend_DB_Expr('SUM(approved)'), 'impression' => new Zend_DB_Expr('SUM(impression)'),
                'qualified' => new Zend_DB_Expr('SUM(qualified)'), 'approved' => new Zend_DB_Expr('SUM(approved)'), 
                'commission' => new Zend_DB_Expr('SUM(commission)'), 'epc' => new Zend_DB_Expr('SUM(epc)')))
            ->join('affiliate_campaigns', 'affiliate_campaigns.id = affiliate_campaign_statistics.affiliate_campaign_id', array())
            ->where('affiliate_campaigns.campaign_id IS NOT NULL')
            ->where('affiliate_subid IS NULL')
            ->where('date >= ?', strftime("%Y-%m-%d", $startDate))
            ->group('date')
            ->order('date ASC');

        if($campaignId) {
            $select->where('affiliate_campaigns.campaign_id = ?', (int)$campaignId);
        }

        if($userId) {
            $select->where('affiliate_campaign_statistics.user_id = ?', (int)$userId);
        }

        if($endDate) {
            $select->where('date <= ?', strftime("%Y-%m-%d", $endDate));
        }

        $data = $this->getAdapter()->fetchAssoc($select);
        return $data;
    }

    /**
     * Get total revenue by user in given dates
     * 
     * @param int $userId
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function findTotalCommission($userId, $startDate = null, $endDate = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('commission' => new Zend_DB_Expr('SUM(commission)'), 
                'leads' => new Zend_DB_Expr('SUM(approved)')))
            ->join('affiliate_campaigns', 'affiliate_campaigns.id = affiliate_campaign_statistics.affiliate_campaign_id', array())
            ->where('affiliate_campaigns.campaign_id IS NOT NULL')
            ->where('affiliate_campaign_statistics.user_id = ?', (int)$userId)
            ->where('affiliate_subid IS NULL');
        if($startDate) {
            $select->where('date >= ?', strftime("%Y-%m-%d", strtotime($startDate)));
        }
        if($endDate) {
            $select->where('date <= ?', strftime("%Y-%m-%d", strtotime($endDate)));
        }

        return $this->getAdapter()->fetchRow($select);

    }

    /**
     * Get total revenue for user in given dates, group by Campaign
     * 
     * @param array $options
     * @param boolean $onlyWithRevenue Return records only with commission > 0
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function findTotalCampaignCommission($options, $onlyWithRevenue = false, $startDate = null, $endDate = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('affiliate_campaigns.campaign_id', 'commission' => new Zend_DB_Expr('SUM(commission)'), 
                'leads' => new Zend_DB_Expr('SUM(approved)'), 'epc' => new Zend_DB_Expr('AVG(epc)'), 'clicks' => new Zend_DB_Expr('SUM(clicks)')))
            ->join('affiliate_campaigns', 'affiliate_campaigns.id = affiliate_campaign_statistics.affiliate_campaign_id', array())
            ->where('affiliate_campaigns.campaign_id IS NOT NULL')
            ->where('affiliate_subid IS NULL')
            ->order('commission DESC');

        if(!empty($options['campaign_id'])) {
            $select->where('affiliate_campaigns.campaign_id = ?', (int)$options['campaign_id']);
        }
        if(!empty($options['user_id'])) {
            $select->where('affiliate_campaign_statistics.user_id = ?', (int)$options['user_id']);
        }
        if($startDate) {
            $select->where('date >= ?', strftime("%Y-%m-%d", strtotime($startDate)));
        }
        if($endDate) {
            $select->where('date <= ?', strftime("%Y-%m-%d", strtotime($endDate)));
        }
        if($onlyWithRevenue) {
            $select->having('commission > 0');
        }

        $select->group('campaign_id');

        if(!empty($options['campaign_id'])) {
            $stat = $this->getAdapter()->fetchRow($select);
            return $stat ? $stat : array();
        }
        return $this->getAdapter()->fetchAssoc($select);
    }

    /**
     * Get subId statistic for given campaign
     * 
     * @param array $options
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function findTotalSubIdCommission($options, $startDate = null, $endDate = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('advert_subid_pairs.affiliate_subid_id', 'commission' => new Zend_DB_Expr('SUM(commission)'), 
                'leads' => new Zend_DB_Expr('SUM(approved)'), 'clicks' => new Zend_DB_Expr('SUM(clicks)'), 'epc' => new Zend_DB_Expr('AVG(epc)')))
            ->join('advert_subid_pairs', 'advert_subid_pairs.affiliate_subid_id = affiliate_campaign_statistics.affiliate_subid', array())
            ->where('affiliate_subid IS NOT NULL')
            ->order('commission DESC');

        if(!empty($options['campaign_id'])) {
            $select->where('advert_subid_pairs.campaign_id = ?', (int)$options['campaign_id']);
        }
        if(!empty($options['user_id'])) {
            $select->where('advert_subid_pairs.user_id = ?', (int)$options['user_id']);
        }
        if($startDate) {
            $select->where('date >= ?', strftime("%Y-%m-%d", strtotime($startDate)));
        }
        if($endDate) {
            $select->where('date <= ?', strftime("%Y-%m-%d", strtotime($endDate)));
        }
        $select->group('advert_subid_pairs.affiliate_subid_id');

        return $this->getAdapter()->fetchAssoc($select);
    }

    public function findStatByTraffic($options, $startDate = null, $endDate = null)
    {
        $conditions = array();
        if($options['campaign_id']) {
            $campaignSubidsModel = new Application_Model_DbTable_CampaignSubids();
            $subidSettings = $campaignSubidsModel->findByCampaign($options['campaign_id']);

            foreach($subidSettings as $setting) {
                $conditions[$setting->ad][] = "(affiliates.source = '{$setting->affiliate}' AND affiliate_subids.name LIKE '%{$setting->code}%')";
            }
        }

        $result = array();
        foreach($conditions as $adTraffic => $condition) {
            $select = $this->getAdapter()->select()
                ->from($this->_name, array('commission' => new Zend_DB_Expr('SUM(commission)'), 
                    'leads' => new Zend_DB_Expr('SUM(approved)'), 'clicks' => new Zend_DB_Expr('SUM(clicks)'), 'epc' => new Zend_DB_Expr('AVG(epc)')))
                ->join('affiliate_campaigns', 'affiliate_campaigns.id = affiliate_campaign_statistics.affiliate_campaign_id', array())
                ->join('affiliate_subids', 'affiliate_subids.id = affiliate_campaign_statistics.affiliate_subid', array())
                ->join('affiliates', 'affiliate_subids.affiliate_id = affiliates.id', array())
                ->group('affiliates.source');
            if($condition) {
                $select->where(implode(' OR ', $condition));
            }
            if($options['campaign_id']) {
                $select->where('affiliate_campaigns.campaign_id = ?', (int)$options['campaign_id']);
            }
            if($startDate) {
                $select->where('date >= ?', strftime("%Y-%m-%d", strtotime($startDate)));
            }
            if($endDate) {
                $select->where('date <= ?', strftime("%Y-%m-%d", strtotime($endDate)));
            }
            $result[$adTraffic] = $this->getAdapter()->fetchRow($select);
        }
        return $result;
    }

    public function findTotalBySubidName($options, $startDate = null, $endDate = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('affiliate_subids.name', 'impressions' => new Zend_DB_Expr('SUM(impression)'),
                'commission' => new Zend_DB_Expr('SUM(commission)'), 
                'leads' => new Zend_DB_Expr('SUM(approved)'), 'clicks' => new Zend_DB_Expr('SUM(clicks)'), 'epc' => new Zend_DB_Expr('SUM(epc)')))
            ->join('affiliate_subids', 'affiliate_subids.id = affiliate_campaign_statistics.affiliate_subid', array())
            ->where('affiliate_subid IS NOT NULL')
            ->order('commission DESC');

        if(!empty($options['campaign_id'])) {
            $select->join('affiliate_campaigns', 'affiliate_campaigns.id = affiliate_subids.affiliate_campaign_id', array())
                    ->where('affiliate_campaigns.campaign_id = ?', (int)$options['campaign_id']);
        }
        if(!empty($options['user_id'])) {
            $select->where('affiliate_campaign_statistics.user_id = ?', (int)$options['user_id']);
        }
        if($startDate) {
            $select->where('date >= ?', strftime("%Y-%m-%d", strtotime($startDate)));
        }
        if($endDate) {
            $select->where('date <= ?', strftime("%Y-%m-%d", strtotime($endDate)));
        }
        $select->group('affiliate_subids.name');

        return $this->getAdapter()->fetchAssoc($select);
    }
}