<?php

class Application_Model_DbTable_Affiliates extends Zend_Db_Table_Abstract
{
    protected $_name = 'affiliates';

    const LINKTRUST = 'linktrust';
    const LEADNOMICS = 'leadnomics';
    const CAKEMARKETING = 'cakemarketing';
    const BINGOROYALTY = 'bingoroyalty';
    const HASOFFERS = 'hasoffers';

    public $subid_allowed = array(self::LINKTRUST, self::CAKEMARKETING, self::LEADNOMICS);

    public function create($data, $userId)
    {
        $newAffiliate = $this->createRow();
        $newAffiliate->setFromArray($data);
        $newAffiliate->user_id = $userId;
        $newAffiliate->added = new Zend_Db_Expr('NOW()');
        if(!empty($data['credentials'])) {
            $newAffiliate->credentials = base64_encode(App_Utils::encryptString(serialize($data['credentials'])));
        }
        $newAffiliate->save();

        return $newAffiliate;
    }

    public function saveCredentials($affiliateId, $credentials)
    {
        $encodedCredentials = base64_encode(App_Utils::encryptString(serialize($credentials)));

        $this->update(array('credentials' => $encodedCredentials), array('id = ?' => (int)$affiliateId));
    }

    public function getAffiliateByApiId($apiId, $userId, $source)
    {
        $select = $this->select()->where('api_id = ?', $apiId)
            ->where('user_id = ?', $userId)
            ->where('source = ?', $source);

        return $this->fetchRow($select);
    }

    public function getAffiliateToUpdate($userId)
    {
        $select = $this->select()->where('user_id = ?', $userId)
            ->where('last_update <= ?', new Zend_Db_Expr('NOW() - INTERVAL 20 MINUTE'));
        return $this->fetchAll($select);
    }

    public function getAffiliatesById($id)
    {
        $ids = is_array($id) ? $id : array($id);

        $select = $this->select()->where('id IN(?)', $ids);
        return $this->fetchAll($select);
    }

    public function getAffiliatesByUserId($userId, $source = null)
    {
        $select = $this->select()->where('user_id = ?', (int)$userId);
        if($source) {
            $source = (array)$source;
            $select->where('source IN (?)', $source);
        }
        return $this->fetchAll($select);
    }

    public function getAffiliatesByName($name, $userId, $source)
    {
        $select = $this->select()->where('name = ?', $name)
            ->where('user_id = ?', $userId)
            ->where('source = ?', $source);
        return $this->fetchRow($select);
    }
}