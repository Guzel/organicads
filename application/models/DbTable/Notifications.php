<?php

class Application_Model_DbTable_Notifications extends Zend_Db_Table_Abstract
{
    protected $_name = 'notifications';

    /**
     * Create or update notification
     * 
     * @param array $data
     * @param int $userId
     * @param int $id Notification id
     * 
     * @return Zend_Db_Table_Row
     */
    public function save($data, $userId, $id = null)
    {
        $contacts = new Application_Model_DbTable_NotificationContacts();

        if($data['type'] == 'stat') {
            $data['campaign_id'] = $data['threshold'] = null;
        } else {
            $data['frequency'] = null;
        }
        if($id) {
            $notification = $this->find($id)->current();

            $contacts->removeByNotificationId($id);
        } else {
            $notification = $this->createRow();
            $notification->user_id = $userId;
            $notification->added = new Zend_Db_Expr('NOW()');
        }

        $notification->setFromArray($data);
        $notification->save();

        foreach($data['contact_id'] as $contactId) {
            $contacts->add($notification->id, $contactId);
        }

        return $notification;
    }

    public function getNotifications($userId)
    {
        $select = $this->select()
            ->where('user_id = ?', (int)$userId)
            ->where('hidden IS NULL');
        return $select;

    }
}