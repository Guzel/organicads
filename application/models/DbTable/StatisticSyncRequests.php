<?php

class Application_Model_DbTable_StatisticSyncRequests extends Zend_Db_Table_Abstract
{
    protected $_name = 'statistic_sync_requests';

    public function save($userId, $from, $to)
    {
        $newRequest = $this->createRow();
        $newRequest->user_id = (int)$userId;
        $newRequest->from = $from;
        $newRequest->to = $to;
        $newRequest->added = new Zend_Db_Expr('NOW()');
        $newRequest->save();
    }

    public function getActiveRequests()
    {
        $select = $this->select()
            ->where('processed IS NULL')
            ->group('user_id');

        return $this->fetchAll($select);
    }
}