<?php

class Application_Model_DbTable_ClientFilters extends Zend_Db_Table_Abstract
{
    protected $_name = 'client_filters';

    public function save($filters, $clientId)
    {
        $this->delete('client_id = ' . (int)$clientId);

        $fieldsModel = new Application_Model_DbTable_Fields();
        $delimiters = $fieldsModel->getFilterDelimiter();

        if($filters) {
            foreach($filters as $filter) {
                $fieldId = $filter['field_id'];

                if($filter['criteria'] == 'between') {
                    $searchValue = array_map('intval', explode(',', $filter['search_value']));
                    sort($searchValue, SORT_NUMERIC);
                    $filter['search_value'] = json_encode($searchValue);
                    $filter['multi'] = 1;
                } elseif(!empty($delimiters[$fieldId])) {
                    $searchValue = str_replace("\n", ' ', $filter['search_value']);
                    $searchValue = explode($delimiters[$fieldId], $searchValue);
                    $filter['search_value'] = json_encode($searchValue);
                    $filter['multi'] = 1;
                }

                $newFilter = $this->createRow();
                $newFilter->client_id = $clientId;
                $newFilter->setFromArray($filter);
                $newFilter->save();
            }
        }
    }

    public function getClientFilters($clientId)
    {
        return $this->fetchAll($this->select()->where('client_id = ?', $clientId));
    }

    public function getFiltersGroupByClient($onlyMulti = false)
    {
        $select = $this->select();
        if($onlyMulti) {
            $select->where('multi IS NOT NULL');
        }

        $filters = $this->fetchAll($select);
        $result = array();
        foreach($filters as $filter) {
            $result[$filter->client_id][$filter->field_id] = $filter->toArray();
        }

        return $result;
    }
}