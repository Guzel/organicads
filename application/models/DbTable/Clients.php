<?php

class Application_Model_DbTable_Clients extends Zend_Db_Table_Abstract
{
    protected $_name = 'clients';

    const TESTING_STATUS = 'testing';

    public function save($data)
    {
        if($data['id']) {
            $client = $this->find($data['id'])->current();
        } else {
            $maxPriorityClient = $this->fetchRow($this->select()->where('form_id = ?', $data['form_id'])->order('priority DESC'));

            $client = $this->createRow();
            $client->added = new Zend_Db_Expr('NOW()');
            $client->priority = ($maxPriorityClient->priority ?:0)+1;
        }

        $client->setFromArray($data);
        $client->save();

        return $client;
    }

    public function changePriority($data)
    {
        foreach($data as $key => $clientId) {
            $this->update(array('priority' => $key+1), array('id = ?' => (int)$clientId));
        }
    }

    public function getPairs($userId = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('id', 'display_name'));
        if($userId) {
            $select->where('user_id = ?', (int)$userId);
        }
        return $this->getAdapter()->fetchPairs($select);
    }

    public function getClients($formId = null, $clientId = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('clients.*'))
            ->joinLeft('client_cap', 'client_cap.client_id = clients.id', array('regularity', 'all_leads', 'states_options'))
            ->order('priority')
            ->group('clients.id');
        if($formId) {
            $select->where('clients.form_id = ?', $formId);
        }
        if($clientId) {
            $select->where('clients.id = ?', $clientId);
        }

        if($clientId) {
            $client = $this->getAdapter()->fetchRow($select);
            return $client ? (object)$client : false;
        }
        return $this->getAdapter()->fetchAll($select);
    }
}