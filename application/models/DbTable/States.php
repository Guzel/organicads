<?php

class Application_Model_DbTable_States extends Zend_Db_Table_Abstract
{
    protected $_name = 'states';

    /**
     * Get array of states list
     * 
     * @return array
     */
    public function getPairs()
    {
        $select = $this->select()->from($this->_name, array('abbrev', 'name'));

        return $this->getAdapter()->fetchPairs($select);
    }
}