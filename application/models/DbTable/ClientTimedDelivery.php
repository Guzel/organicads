<?php

class Application_Model_DbTable_ClientTimedDelivery extends Zend_Db_Table_Abstract
{
    protected $_name = 'client_timed_delivery';

    public function save($data, $clientId)
    {
        $this->deleteByClientId($clientId);

        if(!empty($data['weekdays'])) {
            foreach($data['weekdays'] as $weekday) {
                $deliveryOptions = $this->createRow();
                $deliveryOptions->setFromArray($data);
                $deliveryOptions->week_day = $weekday;
                $deliveryOptions->client_id = $clientId;
                $deliveryOptions->save();
            }
        }
    }

    public function deleteByClientId($clientId)
    {
        $this->delete('client_id = ' . (int)$clientId);
    }

    public function getByClientId($clientId = null)
    {
        $clientIds = is_array($clientId) ? $clientId : array($clientId);

        $select = $this->select();
        if($clientIds) {
            $select->where('client_id IN(?)', $clientIds);
        }

        return $this->fetchAll($select);
    }

    public function getFormPopulateData($clientId)
    {
        $select = $this->select()->where('client_id = ?', $clientId);
        $timedDelivery = $this->fetchAll($select);
        $weekDays = array();
        foreach($timedDelivery as $deliveryOption) {
            $weekDays[] = $deliveryOption->week_day;
            $begin = $deliveryOption->begin;
            $stop = $deliveryOption->stop;
        }
        $values = array();
        $values['weekdays'] = $weekDays;
        $values['begin'] = $begin;
        $values['stop'] = $stop;

        return $values;
    }
}