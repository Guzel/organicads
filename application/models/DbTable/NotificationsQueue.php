<?php

class Application_Model_DbTable_NotificationsQueue extends Zend_Db_Table_Abstract
{
    protected $_name = 'notifications_queue';

    public function save($data)
    {
        $newQueueRow = $this->createRow();
        $newQueueRow->setFromArray($data);
        $newQueueRow->added = new Zend_Db_Expr('NOW()');
        $newQueueRow->save();

        return $newQueueRow;
    }

    public function notificationsToSend()
    {
        $select = $this->select()->where('sent IS NULL')
            ->order('added ASC');
        return $this->fetchAll($select);
    }
}