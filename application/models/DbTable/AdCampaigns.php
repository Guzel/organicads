<?php

class Application_Model_DbTable_AdCampaigns extends Zend_Db_Table_Abstract
{
    protected $_name = 'ad_campaigns';

    const STATUS_PAUSED = 'paused';
    const STATUS_ACTIVE = 'active';

    /**
     * Create/Update new ad campaign
     * 
     * @param array $data
     * @param int|null $id
     * 
     * @return Zend_Db_Table_Row
     */
    public function create($data, $id = null)
    {
        if($id) {
            $newCampaign = $this->find($id)->current();
            if($newCampaign->campaign_id) {
                unset($data['campaign_id']); //do not rewrite campaign_id
            }
        } else {
            $newCampaign = $this->createRow();
            $newCampaign->added = new Zend_Db_Expr('NOW()');
        }
        $newCampaign->setFromArray($data);
        $newCampaign->save();

        return $newCampaign;
    }

    /**
     * Get ad campaign pairs
     * 
     * @param int|null $userId
     * @param int|null $adId
     * @param sting $key Field name to use as array key
     * 
     * @return array [api_id] => [id]
     */
    public function getPairs($userId = null, $adId = null, $key = 'api_id', $value = 'id')
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array($key, $value));
        if($userId) {
            $select->where('user_id = ?', (int)$userId);
        }
        if($adId) {
            $select->where('ad_id = ?', $adId);
        }
        return $this->getAdapter()->fetchPairs($select);
    }

    /**
     * Get not paired campaigns
     * 
     * @param int $userId
     * @param int|null $campaignId
     * 
     * @return array
     */
    public function getNotPairedCampaigns($userId, $campaignId = null)
    {
        $select = $this->select()->where('user_id = ?', (int)$userId)
            ->where('campaign_id IS NULL');
        if($campaignId) {
            $select->orWhere('campaign_id = ?', $campaignId);
        }
        $campaigns = $this->fetchAll($select);

        $result = array();
        foreach($campaigns as $campaign) {
            $result[$campaign->ad_id][] = $campaign;
        }

        return $result;
    }

    /**
     * Set campaign_id for given ad campaigns
     * 
     * @param int $campaignId
     * @param array|int $adCampaigns
     * 
     * @return void
     */
    public function updateCampaignId($campaignId, $adCampaigns)
    {
        $adCampaignIds = is_array($adCampaigns) ? $adCampaigns : explode(',', $adCampaigns);

        //set null to current affiliate campaigns by given campaign_id:
        $this->update(array('campaign_id' => null), array('campaign_id = ?' => (int)$campaignId));
        $this->update(array('campaign_id' => (int)$campaignId), array('id IN(?)' => $adCampaignIds));
    }

    /**
     * Automatic Pairing by given Keyword
     * 
     * @param int $campaignId
     * @param string $keyword
     * @param int $userId
     * 
     * @return void
     */
    public function pairingByKeyword($campaignId, $keyword, $userId)
    {
        $where[] = 'campaign_id IS NULL';
        $where[] = $this->getAdapter()->quoteInto('`user_id` = ?', (int)$userId);
        $where[] = $this->getAdapter()->quoteInto("`name` LIKE BINARY ?", "%{$keyword}%");

        $this->update(array('campaign_id' => (int)$campaignId), $where);
    }

    public function groupApiIdBySource($campaignIds)
    {
        $select = $this->select()->where('`id` IN(?)', (array)$campaignIds);
        $campaigns = $this->fetchAll($select);

        $result = array();
        foreach($campaigns as $campaign) {
            $result[$campaign->ad_id][] = $campaign->api_id;
        }
        return $result;
    }

    public function assignedAmtByCampaign($userId = null)
    {
        $select = $this->getAdapter()->select()
                ->from($this->_name, array('campaign_id', 'COUNT(id)'))
                ->where('user_id = ?', (int)$userId)
                ->where('campaign_id IS NOT NULL')
                ->group('campaign_id');
        return $this->getAdapter()->fetchPairs($select);
    }
}