<?php

class Application_Model_DbTable_UserContacts extends Zend_Db_Table_Abstract
{
    protected $_name = 'user_contacts';

    public function getUserContacts($userId)
    {
        $select = $this->select()->where('user_id = ?', (int)$userId);

        return $this->fetchAll($select);
    }

    public function getPairs($userId)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('id', 'name'))
            ->order('name ASC')
            ->where('user_id = ?', (int)$userId);
        return $this->getAdapter()->fetchPairs($select);
    }

    public function findByPhone($phone, $userId = null)
    {
        $phone = App_Utils::normalizePhone($phone);

        $select = $this->select()
            ->where('phone = ?', $phone);
        if($userId) {
            $select->where('user_id = ?', (int)$userId);
        }

        return $this->fetchAll($select);
    }
}