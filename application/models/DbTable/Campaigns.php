<?php

class Application_Model_DbTable_Campaigns extends Zend_Db_Table_Abstract
{
    protected $_name = 'campaigns';

    public function getPairs($userId = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('id', 'name'))
            ->order('name ASC');
        if($userId) {
            $select->where('user_id = ?', (int)$userId);
        }
        return $this->getAdapter()->fetchPairs($select);
    }

    public function findByUser($userId)
    {
        $select = $this->select()->where('user_id = ?', (int)$userId)
                ->order('name ASC');
        return $this->fetchAll($select);
    }

    public function findByName($name, $userId)
    {
        $select = $this->select()->where('user_id = ?', (int)$userId)
            ->where('name = ?', $name);
        return $this->fetchRow($select);
    }

    public function findByKeyword($keyword, $userId)
    {
        $select = $this->select()->where('keyword = BINARY ?', $keyword)
            ->where('user_id = ?', $userId);
        return $this->fetchRow($select);
    }

    public function getKeywords($userId)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array('id', 'keyword'))
            ->where('keyword IS NOT NULL')
            ->where('user_id = ?', (int)$userId);
        return $this->getAdapter()->fetchPairs($select);
    }

    public function addNew($userId, $data)
    {
        if(!empty($data['id'])) {
            $campaign = $this->find($data['id'])->current();
        }

        if(!$campaign) {
            $campaign = $this->createRow();
            $campaign->user_id = $userId;
            $campaign->added = new Zend_Db_Expr('NOW()');
        }

        $campaign->setFromArray($data);
        $campaign->save();

        return $campaign;
    }
}