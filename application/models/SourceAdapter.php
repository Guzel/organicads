<?php

class Application_Model_SourceAdapter
{
    public static function getInstanse($sourceType, $credentials)
    {
        $model = null;
        switch($sourceType) {
            case Application_Model_DbTable_Affiliates::LINKTRUST:
                $model = new Application_Model_LinkTrust($credentials);
                break;
            case Application_Model_DbTable_Affiliates::LEADNOMICS:
                $model = new Application_Model_Leadnomics($credentials);
                break;
            case Application_Model_DbTable_Affiliates::CAKEMARKETING:
                $model = new Application_Model_Cakemarketing($credentials);
                break;
            case Application_Model_DbTable_Affiliates::BINGOROYALTY:
                $model = new Application_Model_Bingoroyalty($credentials);
                break;
            case Application_Model_DbTable_Affiliates::HASOFFERS:
                $model = new Application_Model_Hasoffers($credentials);
                break;
            case Application_Model_DbTable_Ads::BING:
                $model = new Application_Model_BingAds($credentials);
                break;
            case Application_Model_DbTable_Ads::FIFTYONRED:
                $model = new Application_Model_FiftyOnRed($credentials);
                break;
            case Application_Model_DbTable_Ads::FACEBOOK:
                $model = new Application_Model_Facebook($credentials);
                break;
            case Application_Model_DbTable_Ads::GOOGLE:
                $model = new Application_Model_AdWords($credentials);
                break;
            case Application_Model_DbTable_Ads::YAHOO:
                $model = new Application_Model_YahooStreamads($credentials);
                break;
            case Application_Model_DbTable_Ads::TRAFFICVANCE:
                $model = new Application_Model_Trafficvance($credentials);
                break;
            case Application_Model_DbTable_Ads::LEADIMPACT:
                $model = new Application_Model_Leadimpact($credentials);
                break;
            case Application_Model_DbTable_Ads::HASTRAFFIC:
                $model = new Application_Model_Hastraffic($credentials);
                break;
            case Application_Model_DbTable_Ads::ZEROPARK:
                $model = new Application_Model_Zeropark($credentials);
                break;
            case Application_Model_DbTable_Ads::AIRPUSH:
                $model = new Application_Model_Airpush($credentials);
                break;
            default:
                break;
        }
        return $model;
    }
}