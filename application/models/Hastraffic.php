<?php
/**
 * https://beta.hastraffic.com scraper
 * 
 * @author Guzel Mitroshina <guze4ka@gmail.com>
 */
class Application_Model_Hastraffic
{
    protected $_mainUrl = 'https://platform.hastraffic.com/';
    protected $_loginUri = 'auth/?do=form-form-submit';
    protected $_campaignsUri = 'advertiser/campaigns/';
    protected $_filterUri = '?do=dateFilterForm-form-submit';

    protected $_email;
    protected $_password;
    protected $_token;

    protected $_dateFrom;
    protected $_dateTo;
    protected $_postData;

    protected $_cookieJar;
    protected $_logger;

    protected $_errors = array(
        'login_error' => 'Login failed. Please, check your credentials',
        'server_unavailable' => 'Hastraffic Server is not responding',
        'account_connected' => 'This Account is already connected to your Leadssistant account',
        'campaigns_missing' => 'Campaigns was not found!'
    );

    public function __construct($options = array())
    {
        $this->_email = !empty($options['email']) ? $options['email'] : null;
        $this->_password = !empty($options['password']) ? $options['password'] : null;

        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/hastraffic');
        $this->_logger = new Zend_Log($writer);
    }

    /**
     * Add new account to DB
     * 
     * @param int $userId
     * @return Zend_Db_Table_Row|array
     */
    public function addAccount($userId)
    {
        $result = $content = $this->login();
        if(!is_array($result)) {
            $result = array();

            $adsModel = new Application_Model_DbTable_Ads();
            if($adsModel->getAdsByName($userId, $this->_email, Application_Model_DbTable_Ads::HASTRAFFIC)) {
                $result['errorMessage'] = $this->_errors['account_connected'];
            } else {
                $credentials = array('email' => $this->_email, 'password' => $this->_password);
                $options = array('source' => Application_Model_DbTable_Ads::HASTRAFFIC,
                    'name' => $this->_email, 'credentials' => $credentials);

                $result = $adsModel->create($userId, $options); 
             }
        }

        return $result;
    }

    /**
     * Add Campaign
     * 
     * @param Zend_Db_Table_Row $ad
     * @param string $campaignApiId
     * @param string $campaignName
     * 
     * @return Zend_Db_Table_Row
     */
    public function addCampaign($ad, $campaignApiId, $campaignName)
    {
        if(!$campaignApiId || !$campaignName) {
            return;
        }

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $keywords = $campaignsModel->getKeywords($ad->user_id);

        $data = array('api_id' => $campaignApiId, 'user_id' => $ad->user_id, 'ad_id' => $ad->id,
            'name' => $campaignName, 'source' => Application_Model_DbTable_Ads::HASTRAFFIC);

         //automatic Pairing
        if(false !== ($campaignId = App_Utils::findKeyword($campaignName, $keywords))) {
            $data['campaign_id'] = $campaignId;
        }

        $adCampaigns = new Application_Model_DbTable_AdCampaigns();
        return $adCampaigns->create($data);
    }

    /**
     * Add/Update campaigns statistics
     * 
     * @param Zend_Db_Table_Row $ad
     * @param bool $updateCampaigns Update the campaigns
     * 
     * @return array
     */
    public function getCampaignsStatistics($ad, $updateCampaigns = true)
    {
        $result = $this->login();
        if(!is_array($result)) {
            $this->_dateFrom = $this->getDateFrom();
            $this->_dateTo = $this->getDateTo();

            if($ad->last_statistic_date) {
                $lastUpdate = new DateTime($ad->last_statistic_date);
                $this->_dateFrom = $lastUpdate->sub(new DateInterval('P1D'));
            }

            if($statistics = $this->_getStat()) {
                $adCampaigns = new Application_Model_DbTable_AdCampaigns();
                $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

                $campaignStat = new Application_Model_DbTable_AdCampaignStatistics();
                $campaignStat->getAdapter()->beginTransaction();

                //delete crossing stat:
                $campaignStat->deleteStatByDate($ad->id, $this->_dateFrom->format("Y-m-d"));

                foreach($statistics as $date => $row) {
                    foreach($row as $stat) {
                        $campaignApiId = $stat['campaign_id'];
                        if(empty($campaignIds[$campaignApiId])) {
                            //add new Campaign
                            $newCampaign = $this->addCampaign($ad, $campaignApiId, $stat['campaign']);
                            $campaignIds[$newCampaign->api_id] = $newCampaign->id;
                        }

                        $newStat = $campaignStat->createRow();
                        $newStat->user_id = $ad->user_id;
                        $newStat->ad_id = $ad->id;
                        $newStat->ad_campaign_id = $campaignIds[$campaignApiId];
                        $newStat->date = strftime("%Y-%m-%d", strtotime($date));
                        $newStat->impressions = (int)$stat['impressions'];
                        $newStat->spend = (float)str_replace('$', '', $stat['spend']);
                        $newStat->status = strtolower($stat['status']);
                        $newStat->added = new Zend_Db_Expr('NOW()');
                        $newStat->save();
                    }
                }
                $campaignStat->getAdapter()->commit();
            }
            $result = array('success' => true);
        }

        $ad->last_statistic_date = new Zend_Db_Expr('CURDATE()');
        $ad->last_update = new Zend_Db_Expr('NOW()');
        $ad->save();

        return $result;
    }

    /**
     * Find statistics by parsing Campaigns view page
     * 
     * @return array
     */
    protected function _getStat()
    {
        $this->_postData = array(
            '_token_' => $this->_token,
            'date_period' => 'daterange',
            'daterange' => 1,
            'send' => 'Set'
        );

        $statistics = array();
        $startDate = clone $this->_dateFrom;
        while($startDate <= $this->_dateTo) {
            $statDay = $startDate->format('Y-m-d');
            $this->_postData['date_from'] = $this->_postData['date_to'] = $statDay;

            $this->_sendRequest($this->_mainUrl . $this->_campaignsUri . $this->_filterUri, null, $this->_postData);
            $content = $this->_sendRequest($this->_mainUrl . $this->_campaignsUri);
            if(!$content || is_array($content)) {
                continue;
            }
            $xpath = $this->getXPath($content);

            $stat = array();
            $cols = array(1=>'campaign', 4=> 'status', 7=>'impressions',8=>'spend');
            foreach($xpath->query('//table[@id="toplevelcampaigns"]/tbody/tr') as $tr) {
                if(!$id = $tr->getAttributeNode('id')->nodeValue) {
                    continue;
                }

                $campaignId = str_replace('row_', '', $id);
                $values = array('campaign_id' => $campaignId);
                foreach($xpath->query('td', $tr) as $key => $td) {
                    if(empty($cols[$key])) {
                        continue;
                    }
                    $values[$cols[$key]] = trim($td->nodeValue);
                }
                $stat[$statDay][] = $values;
            }
            $statistics = array_merge($statistics, $stat);

            $startDate->add(new DateInterval('P1D'));
        }

        return $statistics;
    }

    /**
     * Login to https://beta.wantstraffic.com
     * 
     * @return string|array
     */
    public function login()
    {
        $this->_cookieJar = new Zend_Http_CookieJar();
        $loginCredentials = array('username' => $this->_email, 'password' => $this->_password, 'submit' =>'Login');

        try {
            //login
            $login = $this->_sendRequest($this->_mainUrl . $this->_loginUri, null, $loginCredentials);
            //open campaigns page
            $content = $this->_sendRequest($this->_mainUrl . $this->_campaignsUri);

            if(!$content || is_array($content)) {
                $result['errorMessage'] = $this->_errors['campaigns_missing'];
            } else {
                $xpath = $this->getXPath($content);
                if($tokenElement = $xpath->query("//input[@name='_token_']")->item(0)) {
                    $this->_token = $tokenElement->getAttributeNode('value')->nodeValue;
                }
                return $content;
            }
        } catch (Application_Model_Exceptions_Curl $e) {
            $result['errorMessage'] = $e->getMessage();
        }

        $this->_logger->err("{$this->_email} {$result['errorMessage']}");
        return $result;
    }

    /**
     * Get DateFrom
     * Set report starting date
     * 
     * @return DateTime
     **/
    public function getDateFrom()
    {
        if(!$this->_dateFrom) {
            $today = new DateTime();
            $this->_dateFrom = $today->sub(new DateInterval('P10D'));
        }
        return $this->_dateFrom;
    }

    /**
     * Get DateTo
     * Set report end date
     * 
     * @return DateTime
     **/
    public function getDateTo()
    {
        if(!$this->_dateTo) {
            $this->_dateTo = new DateTime();
        }
        return $this->_dateTo;
    }

    /**
     * Get XPath
     * 
     * @param string $content
     * @return \DOMXpath
     */
    protected function getXPath($content)
    {
        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($content);
        libxml_use_internal_errors(false);
        return new DOMXpath($dom);
    }

    /**
     * Send CURL request
     * 
     * @param string $url
     * @param array $getParams
     * @param array $postParams
     * 
     * @return array|string
     */
    protected function _sendRequest($url, $getParams = array(), $postParams = array())
    {
        $result = array();

        $config = array(
            'timeout' => 30,
            'useragent' => "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)",
            'curloptions' => array(CURLOPT_RETURNTRANSFER => 1, CURLOPT_SSL_VERIFYPEER => false)
        );

        $adapter = new Zend_Http_Client_Adapter_Curl();
        $client = new Zend_Http_Client($url, $config);

        $client->setAdapter($adapter);
        $client->setCookieJar($this->_cookieJar);

        if($postParams) {
            $client->setParameterPost($postParams);
            $client->setMethod(Zend_Http_Client::POST);
        } else {
            $client->setParameterGet($getParams);
            $client->setMethod(Zend_Http_Client::GET);
        }

        try{
            $response = $client->request();
            if ($response->isSuccessful()){
                return $response->getBody();
            } else {
                $result['errorMessage'] = $this->_errors['server_unavailable'];
            }
        } catch (Zend_Http_Client_Exception $e) {
            $result['errorMessage'] = $e->getMessage();
        }

        $this->_logger->err("{$this->_email}. URL {$url}: {$result['errorMessage']}");
        return $result;
    }
}