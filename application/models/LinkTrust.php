<?php
class Application_Model_LinkTrust
{
    protected $_apiUrl = 'http://integration.beta.linktrust.com';
    protected $_apiUserId;
    protected $_apiKey;
    protected $_apiMethod;

    protected $_dateFrom;
    protected $_dateTo;
    protected $_cid;

    protected $_logger;

    protected $_errors = array(
        '500' => 'An unexpected error has occurred. Please try again later.',
        '400' => 'Server has returned error. Please try again later.',
        '401' => 'API Credentials are invalid.'
    );

    const AFFILIATE_SUBID_BREAKOUT = 'AffiliateCenterSubIdBreakout';
    const AFFILIATE_CAMPAIGN_PERFORMANCE = 'AffiliateCenterCampaignPerformance';
    const AFFILIATE_CREATIVE_BREAKOUT = 'AffiliateCenterCreativeBreakout';
    const AFFILIATE_DAILY_BREAKOUT = 'AffiliateCenterDailyBreakout';
    const CREATIVES = 'Creatives';
    const AFFILIATE_MY_OFFERS = 'myoffers';

    public function __construct($options = array())
    {
        $this->_apiUserId = !empty($options['api_user_id']) ? $options['api_user_id'] : null;
        $this->_apiKey = !empty($options['api_key']) ? $options['api_key'] : null;

        $this->_apiMethod = self::AFFILIATE_CAMPAIGN_PERFORMANCE;

        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/linktrust');
        $this->_logger = new Zend_Log($writer);
    }

    /**
     * Get Affiliate's general information. Save in DB new affiliate.
     * 
     * @param type $userId
     * @return type
     */
    public function getAffiliate($userId)
    {
        $content = $result = $this->sendRequest();

        if(!is_array($content)) {
            $result = array();
            $affiliateModel = new Application_Model_DbTable_Affiliates();

            if($affiliateModel->getAffiliateByApiId($this->_apiUserId, $userId, Application_Model_DbTable_Affiliates::LINKTRUST)) {
                $result['errorMessage'] = 'Affiliate is already connected to your account';
            } else {
                $name = !empty($content->Affiliate['Name']) ? $content->Affiliate['Name'] : $this->_apiUserId;
                $affiliateOptions = array('api_id' => $this->_apiUserId, 
                    'name' => $name, 'source' => Application_Model_DbTable_Affiliates::LINKTRUST);
                $result = $affiliate = $affiliateModel->create($affiliateOptions, $userId);

                if(!empty($content->Affiliate->Campaigns)) {
                    $result = $this->saveCampaigns($affiliate->user_id, $affiliate->id, $content->Affiliate->Campaigns->Campaign);
                    $result = (true === $result) ? $affiliate : $result;
                }
            }
        }

        return $result;
    }

    /**
     * Get and Save list of SubIds by given LinkTrust campaign Ids
     * 
     * @param Zend_DbTable_Row $affiliate
     * @param array $campaignIds
     * @return array
     */
    public function getSubIds($affiliate, $campaignIds = array())
    {
        $this->_apiMethod = self::AFFILIATE_SUBID_BREAKOUT;
        $today = new DateTime();
        $this->_dateFrom = $today->sub(new DateInterval('P1D'))->format("m/d/Y");
        $subIds = array();

        if($campaignIds) {
            $campaignIds = (array)$campaignIds;
            $affiliateSubids = new Application_Model_DbTable_AffiliateSubids();
            $affiliateCampaigns = new Application_Model_DbTable_AffiliateCampaigns();
            $affiliateCampaignIds = $affiliateCampaigns->getPairs($affiliate->user_id);

            foreach($campaignIds as $cid) {
                usleep(2000000);
                $this->_cid = $cid;
                $stat = $this->sendRequest();
                if(empty($stat->Campaign)) {
                    continue;
                }

                $affiliateCampaignId  = (string)$affiliateCampaignIds[$cid];
                $existingSubids = $affiliateSubids->getPairs(array('affiliate_campaign_id' => (string)$affiliateCampaignId));

                foreach($stat->Campaign->SubIds->SubId as $subId) {
                    if(!trim($subId['Id'])) {
                        continue;
                    }

                    $affiliateSubid = array_search($subId['Id'], $existingSubids);
                    if(false === $affiliateSubid) {
                        $subIdName = (string)$subId['Id'];
                        $payout = str_replace('$', '', (float)$subId['Payout']);
                        $payout = is_numeric($payout) ? $payout : null;

                        $data = array('user_id' => $affiliate->user_id, 
                            'affiliate_id' => $affiliate->id,
                            'affiliate_campaign_id' => $affiliateCampaignId,
                            'name' => $subIdName, 'payout' => $payout);
                        $newRecord = $affiliateSubids->create($data, $affiliateSubid);
                        $affiliateSubid = $newRecord->id;
                        $existingSubids[$affiliateSubid] = $subIdName;
                    }
                }
            }
        }
        return $subIds;
    }

    public function saveOffers()
    {
        $this->_apiMethod = self::AFFILIATE_MY_OFFERS;
        var_dump($this->sendRequest());
        die();
    }

    public function saveCampaigns($userId, $affiliateId, $campaigns = null)
    {
        if(!$campaigns) {
            sleep(2);
            if(is_array($content = $this->sendRequest())) {
                return $content;
            }
            $campaigns = $content->Affiliate->Campaigns->Campaign;
        }

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $keywords = $campaignsModel->getKeywords($userId);

        $affiliateCampaigns = new Application_Model_DbTable_AffiliateCampaigns();
        $campaignIds = $affiliateCampaigns->getPairs($userId, $affiliateId);

        foreach($campaigns as $campaign) {
            $campaignApiId = (int)$campaign['Id'];

            $id = !empty($campaignIds[$campaignApiId]) ? $campaignIds[$campaignApiId] : null;
            //if($id) { continue; } //remove this if need to update existing campaigns

            $data = array('api_id' => $campaignApiId, 'user_id' => $userId, 
                'name' => $campaign['Name'], 'affiliate_id' => $affiliateId);

            //automatic Pairing
            if(false !== ($campaignId = App_Utils::findKeyword($campaign['Name'], $keywords))) {
                $data['campaign_id'] = $campaignId;
            }

            $payout = str_replace('$', '', $campaign['Payout']);
            $data['payout'] = is_numeric($payout) ? $payout : null;

            $newCampaign = $affiliateCampaigns->create($data, $id);

            $campaignIds[$newCampaign->api_id] = $newCampaign->id;
        }

        return true;
    }

    public function getCampaignsStatistics($affiliate, $updateCampaigns = true)
    {
        $this->_dateFrom = $this->getDateFrom();

        if($affiliate->last_statistic_date) {
            $lastUpdate = new DateTime($affiliate->last_statistic_date);
            $this->_dateFrom = $lastUpdate->sub(new DateInterval('P2D'))->format("m/d/Y");
        }
 
        //update Campaigns:
        if($updateCampaigns) {
            $result = $this->saveCampaigns($affiliate->user_id, $affiliate->id);
            if(true !== $result) {
                return $result;
            }
        }

        //find Statistics:
        $this->_apiMethod = self::AFFILIATE_DAILY_BREAKOUT;
        $content = $result = $this->sendRequest();

        if(!is_array($content)) {
            $result = array();
            //get existing Camaigns:
            $affiliateCampaigns = new Application_Model_DbTable_AffiliateCampaigns();
            $campaignIds = $affiliateCampaigns->getPairs($affiliate->user_id, $affiliate->id);

            $campaignStat = new Application_Model_DbTable_AffiliateCampaignStatistics();

            //delete crossing stat:
            $campaignStat->deleteStatByDate($affiliate->id, $this->getDateFrom());

            $campaignStat->getAdapter()->beginTransaction();
            foreach($content->Date as $statDate) {
                $date = (string)$statDate['value'];

                //bulk insert:
                foreach($statDate->Campaigns->Campaign as $campaign) {
                    $campaignId = (int)$campaign['Id'];
                    $statistics = $campaign->StatRow->Statistics;

                    $newStat = $campaignStat->createRow();
                    $newStat->affiliate_campaign_id = $campaignIds[$campaignId];
                    $newStat->affiliate_id = $affiliate->id;
                    $newStat->user_id = $affiliate->user_id;
                    $newStat->date = $date;
                    $newStat->impression = (int)$statistics->Impression;
                    $newStat->clicks = (int)$statistics->Clicks;
                    $newStat->qualified = (int)$statistics->Qualified;
                    $newStat->approved = (int)$statistics->Approved;
                    $newStat->ctr = (float)$statistics->Ctr;
                    $newStat->approved_percentage = (float)$statistics->ApprovedPercentage;
                    $newStat->e_cpm = (float)str_replace('$', '', $statistics->eCPM);
                    $newStat->epc = (float)str_replace('$', '', $statistics->EPC);
                    $newStat->commission = (float)str_replace('$', '', $statistics->Commission);
                    $newStat->added = new Zend_Db_Expr('NOW()');
                    $newStat->save();
                }
            }
            $campaignStat->getAdapter()->commit();

            $affiliate->last_statistic_date =  new Zend_Db_Expr('CURDATE()');
            $affiliate->last_update = new Zend_Db_Expr('NOW()');
            $affiliate->save();

            $result['success'] = true;
        }
        return $result;
    }

    /**
     * Import Sub ID report
     * 
     * @param Zend_Db_Table_Row $affiliate
     */
    public function importSubIdStatistics($affiliate)
    {
        $this->_dateFrom = $this->getDateFrom();

        if($affiliate->last_subid_statistic_date) {
            $lastUpdate = new DateTime($affiliate->last_statistic_date);
            $this->_dateFrom = $lastUpdate->sub(new DateInterval('P1D'))->format("m/d/Y");
        }

        $this->_apiMethod = self::AFFILIATE_SUBID_BREAKOUT;
        $startDate = new DateTime($this->_dateFrom);
        $endDate = new DateTime($this->getDateTo());

        $affiliateCampaigns = new Application_Model_DbTable_AffiliateCampaigns();
        $campaignIds = $affiliateCampaigns->getPairs($affiliate->user_id, $affiliate->id);

        //Find LinkTrust campaigns API IDs
        $affiliateSubidsModel = new Application_Model_DbTable_AffiliateSubids();
        $campaignsApiIds = $affiliateSubidsModel->getPairedCampaignIds($affiliate->id);

        if(!$campaignsApiIds) {
            return;
        }

        $content = array();
        while($startDate <= $endDate) {
            foreach($campaignsApiIds as $cid) {
                usleep(2000000);
                $this->_dateFrom = $this->_dateTo = $startDate->format("m/d/Y");
                $this->_cid = $cid;

                $content[$startDate->format("m/d/Y")] = $this->sendRequest();
                $startDate->add(new DateInterval('P1D'));
            }
        }

        if($content) {
            $affiliateSubids = new Application_Model_DbTable_AffiliateSubids();
            $campaignStat = new Application_Model_DbTable_AffiliateCampaignStatistics();

            foreach($content as $date => $stat) {
                if(empty($stat->Campaign)) {
                    continue;
                }

                $campaign = $stat->Campaign;
                $subIdCount = count($campaign->SubIds->SubId);
                if($subIdCount > 1000) {
                    $this->_logger->err("Campaign {$campaign['Name']} has too many SubIds ({$subIdCount})");
                    continue;
                }

                $apiCampaignId = (string)$campaign['Id'];
                $affiliateCampaignId  = (string)$campaignIds[$apiCampaignId];
                $existingSubids = $affiliateSubids->getPairs(array('affiliate_campaign_id' => (string)$affiliateCampaignId));

                foreach($campaign->SubIds->SubId as $subId) {
                    if(!trim($subId['Id'])) {
                        continue;
                    }

                    $affiliateSubid = array_search($subId['Id'], $existingSubids);
                    if(false === $affiliateSubid) {
                        $subIdName = (string)$subId['Id'];
                        $payout = str_replace('$', '', (float)$subId['Payout']);
                        $payout = is_numeric($payout) ? $payout : null;

                        $data = array('user_id' => $affiliate->user_id, 
                            'affiliate_id' => $affiliate->id,
                            'affiliate_campaign_id' => $affiliateCampaignId,
                            'name' => $subIdName, 'payout' => $payout);
                        $newRecord = $affiliateSubids->create($data, $affiliateSubid);
                        $affiliateSubid = $newRecord->id;
                        $existingSubids[$affiliateSubid] = $subIdName;
                    }

                    $statistics = $subId->StatRow->Statistics;

                    $newStat = $campaignStat->createRow();
                    $newStat->affiliate_campaign_id = $campaignIds[$apiCampaignId];
                    $newStat->affiliate_id = $affiliate->id;
                    $newStat->affiliate_subid = $affiliateSubid;
                    $newStat->user_id = $affiliate->user_id;
                    $newStat->date = date("Y-m-d", strtotime($date));
                    $newStat->impression = (int)$statistics->Impression;
                    $newStat->clicks = (int)$statistics->Clicks;
                    $newStat->qualified = (int)$statistics->Qualified;
                    $newStat->approved = (int)$statistics->Approved;
                    $newStat->ctr = (float)$statistics->Ctr;
                    $newStat->approved_percentage = (float)$statistics->ApprovedPercentage;
                    $newStat->e_cpm = (float)str_replace('$', '', $statistics->eCPM);
                    $newStat->epc = (float)str_replace('$', '', $statistics->EPC);
                    $newStat->commission = (float)str_replace('$', '', $statistics->Commission);
                    $newStat->added = new Zend_Db_Expr('NOW()');
                    $newStat->save();
                }
            }
        }
        $affiliate->last_subid_statistic_date =  new Zend_Db_Expr('CURDATE()');
        $affiliate->save();
    }

    public function getDateFrom()
    {
        if(!$this->_dateFrom) {
            $today = new DateTime();
            //$this->_dateFrom = $today->sub(new DateInterval('P1M'))->format("m/01/Y");
            $this->_dateFrom = $today->sub(new DateInterval('P10D'))->format("m/d/Y");
        }
        return $this->_dateFrom;
    }

    public function getDateTo()
    {
        if(!$this->_dateTo) {
            $today = new DateTime();
            $this->_dateTo = $today->format("m/d/Y");
        }
        return $this->_dateTo;
    }

    protected function getUri()
    {
        return strtolower("/rest/{$this->_apiUserId}/reports/{$this->_apiMethod}.xml");
        //return strtolower("/rest/{$this->_apiUserId}/campaigns/{$this->_apiMethod}.xml");
    }

    protected function getToken()
    {
        $utcTime = $this->getUtcTime();
        $uri = $this->getUri();

        $hash = strtoupper(md5("{$uri}{$utcTime}{$this->_apiKey}"));
        return "{$utcTime}_{$hash}";
    }

    protected function getUtcTime()
    {
        return gmdate("YmdHi");
    }

    protected function sendRequest()
    {
        $requestUrl = $this->_apiUrl . $this->getUri();
        $criteria = array('token' => $this->getToken());
        $criteria['datefrom'] = $this->getDateFrom();
        $criteria['dateto'] = $this->getDateTo();

        if($this->_cid) {
            $criteria['cid'] = $this->_cid;
        }

        $client = new Zend_Http_Client($requestUrl, array('maxredirects' => 0, 'timeout' => 30));
        $client->setParameterGet($criteria);
        $response = $client->request(Zend_Http_Client::GET);

        if ($response->isSuccessful()){
            try{
                $result = new SimpleXMLElement($response->getBody()); 
            } catch (Exception $e) {
                $result['errorMessage'] = $this->_errors[500];
                $this->_logger->err("{$this->_apiUserId} {$e->getMessage()}");
            }
        } else {
            $errorCode = Zend_Http_Response::extractCode($response);
            $result['errorMessage'] = !empty($this->_errors[$errorCode]) ? 
                $this->_errors[$errorCode] : $response->getBody();
            $this->_logger->err("{$this->_apiUserId} {$result['errorMessage']}. Response: {$response->getBody()}");
        }

        return $result;
    }
}