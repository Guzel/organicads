<?php

class Application_Model_Leads
{
    protected $_formData;
    protected $_form;
    protected $_clients = array();
    protected $_timedDelivery;
    protected $_fields;
    protected $_capped;
    protected $_logger;
    public $state;

    /**
     * Class constructor
     * 
     * @param array $formData
     */
    public function __construct($formData = null)
    {
        if($formData) {
            $this->_formData = $formData;
            $this->getFormFields();
        }

        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/leads');
        $this->_logger = new Zend_Log($writer);
    }

    /**
     * Find proper client for the lead, send lead to the client
     */
    public function proceed()
    {
        $this->findClients();
        $client = $this->filterClients();

        $leadsModel = new Application_Model_DbTable_Leads();

        $newLead = $leadsModel->createRow();
        $newLead->user_id = $this->_form->user_id;
        $newLead->form_id = $this->_form->id;
        $newLead->client_id = $client ? $client->id : null;
        $newLead->raw_data = serialize($this->_formData);
        $newLead->fields = serialize($this->_fields);
        $newLead->state = $this->state;
        $newLead->postpone_delivery = $client->postpone_delivery ? new Zend_Db_Expr('NOW()') : null;
        $newLead->postpone_reason = !empty($client->postpone_reason) ? $client->postpone_reason : null;
        $newLead->testing = (int)($client->status == Application_Model_DbTable_Clients::TESTING_STATUS);
        $newLead->date = new Zend_Db_Expr('NOW()');
        $newLead->save();

        $leadsModel->exctractFields($newLead);

        if($client && $client->delivery) {
            $this->deliverLead($newLead);
        }

        return true;
    }

    public function update($lead, $clients = array())
    {
        if(!$lead instanceof Zend_Db_Table_Row) {
            $leadsModel = new Application_Model_DbTable_Leads();
            $lead = $leadsModel->find((int)$lead)->current();
        }

        if(!$lead || $lead->sold) {
            return false;
        }

        $this->_formData = unserialize($lead->raw_data);
        $this->getFormFields();

        if($clients) {
            $this->setClients($clients);
        } else {
            $this->findClients();
        }

        if(!($client = $this->filterClients())) {
            return false;
        }

        $lead->client_id = $client->id;
        $lead ->save();

        if($client->delivery) {
            $this->deliverLead($lead);
        }
        return true;
    }

    /**
     * Get Fields for current Form
     * assosiate lead's value and form fields
     */
    protected function getFormFields()
    {
        $formModel = new Application_Model_DbTable_Forms();
        $this->_form = $formModel->find($this->_formData['form_id'])->current();

        $fieldsOptionsModel = new Application_Model_DbTable_FieldsOptions();
        $fieldsOptions = $fieldsOptionsModel->getPairs($this->_form->user_id);

        $formFields = unserialize($this->_form->fields);

        foreach($formFields as $fieldId) {
            $fieldName = !empty($fieldsOptions[$fieldId]) ? $fieldsOptions[$fieldId] : "field_{$fieldId}";
            $this->_fields[$fieldId] = $this->_formData[$fieldName];
        }
        $this->getLocation();
    }

    /**
     * Set clients
     * 
     * @param array $clients array of clients of client_ids
     */
    public function setClients($clients)
    {
        $clients = is_array($clients) ? $clients : array($clients);
        $clientsModel = new Application_Model_DbTable_Clients();

        foreach($clients as $row) {
            $client = is_int($row) ? $clientsModel->getClients($this->_formData['form_id'], $row) : $row;
            if(!$client) {
                continue;
            }
            $this->_clients[] = (array)$client;
        }
        $this->setDeliveryOptions();
    }

    /**
     * Find all possible clients for current form;
     * find clients' timed delivery options
     */
    protected function findClients()
    {
        $clientsModel = new Application_Model_DbTable_Clients();
        $this->_clients = $clientsModel->getClients($this->_formData['form_id']);
        $this->setDeliveryOptions();
    }

    /**
     * Find delivery options for all current clients
     */
    protected function setDeliveryOptions()
    {
        $clientIds = array();
        foreach($this->_clients as $client) {
            $clientIds[] = $client['id'];
        }

        $clientTimedDelivery = new Application_Model_DbTable_ClientTimedDelivery();
        $timedDelivery = $clientTimedDelivery->getByClientId($clientIds);
        foreach($timedDelivery as $options) {
            $this->_timedDelivery[$options->client_id][$options->week_day] = $options;
        }
    }

    /**
     * Find proper Client by checking delivery time, caps and fields' filters
     * 
     * @return Zend_Db_Table_Row
     */
    protected function filterClients()
    {
        $fieldsFilter = new Application_Model_LeadFieldsFilter($this->_fields);
        $filtersModel = new Application_Model_DbTable_ClientFilters();

        $properClients = array();
        foreach($this->_clients as $client) {
            $client = (object)$client;
            $client->delivery = true;
            $client->postpone_delivery = false;

            $filters = $filtersModel->fetchAll($filtersModel->select()->where('client_id = ?', $client->id));
            if(!$fieldsFilter->isValid($filters)) {
                continue;
            }

            $client->delivery = $this->checkDeliveryTime($client);

            if($client->delivery) {
                $cappedLeads = $this->getCappedLeads($client);
                if($cappedLeads && $cappedLeads->capped >= $cappedLeads->limit) {
                    $client->delivery = false;
                    $client->cappedLimit = true;
                }
            }

            if($client->delivery) {
                return $client;
            }

            if($client->capped_delivery_interval || $client->postpone_delivery_interval) {
                $client->postpone_delivery = true;
                $client->postpone_reason = !empty($client->cappedLimit) ? 'limit' : 'time';
            }
            $properClients[$client->id] = $client;
        }
        return current($properClients);
    }

    /**
     * Check delivery time for given client
     * 
     * @param object $client
     * @return boolean
     */
    public function checkDeliveryTime($client)
    {
        if($client->timed_delivery_enabled) { //timed delivery settings
            $clientTime = new DateTime(null, new DateTimeZone($client->timezone));
            $weekDay = $clientTime->format('w');

            if(empty($this->_timedDelivery[$client->id][$weekDay])) {
                //var_dump('OFF DELIVERY DAY!');
                return false;
            } else {
                $todaySettings = $this->_timedDelivery[$client->id][$weekDay];
                $beginDelivery = new DateTime($todaySettings->begin, new DateTimeZone($client->timezone));
                $stopDelivery = new DateTime($todaySettings->stop, new DateTimeZone($client->timezone));

                if($clientTime < $beginDelivery || $clientTime > $stopDelivery) {
                    //var_dump('OFF TIME!');
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Get caps limits for user
     * 
     * @param object $client
     * @return Zend_Db_Table_Row
     */
    public function getCappedLeads($client)
    {
        $cappedLeadsModel = new Application_Model_DbTable_ClientCappedLeads();
        $checkState = 0;
        $limit = $client->all_leads;

        if($client->states_options) {
            $checkState = $this->state;
            $stateCaps = unserialize($client->states_options);
            $limit = !empty($stateCaps[$this->state]) ? $stateCaps[$this->state] : null;
        }

        if($limit) {
            $period = ($client->regularity == 'day') ? 'z' : ($client->regularity == 'week' ? 'W' : 'm');
            $capPeriod = date("{$period}");
            $currentYear = date("Y");

            $select = $cappedLeadsModel->select()->where('client_id = ?', $client->id)->where('state = ?', $checkState)
                ->where('time_period = ?', $capPeriod)->where('year = ?', $currentYear);

            if(!$cappedLeads = $cappedLeadsModel->fetchRow($select)) {
                $cappedLeads = $cappedLeadsModel->createRow();
                $cappedLeads->client_id = $client->id;
                $cappedLeads->time_period = $capPeriod;
                $cappedLeads->year = $currentYear;
                $cappedLeads->state = $checkState;
            }

            $cappedLeads->limit = $limit;
            $cappedLeads->save();

            //save capped model for future use:
            $this->_capped[$client->id][$checkState] = $cappedLeads;
            return $cappedLeads;
        }
    }

    /**
     * Determine state by state field or zip
     */
    protected function getLocation()
    {
        $fieldsModel = new Application_Model_DbTable_Fields();
        $zipId = $fieldsModel->idByName('zip');
        $stateId = $fieldsModel->idByName('state');
        $cityId = $fieldsModel->idByName('city');

        if(!empty($this->_fields[$stateId])) {
            $this->state = $this->_fields[$stateId];
        }

        if(!empty($this->_fields[$zipId])) {
            $citiesModel = new Application_Model_DbTable_Cities();
            if($city = $citiesModel->find($this->_fields[$zipId])->current()) {
                if(empty($this->_fields[$stateId])) {
                    $this->_fields[$stateId] = $this->state = $city->state;
                }
                if(empty($this->_fields[$cityId])) {
                    $this->_fields[$cityId] = $city->name;
                }
            }
        }
    }

    /**
     * Deliver sold leads to client
     * 
     * @param object $client
     */
    public function deliverLead($lead)
    {
        $clientsModel = new Application_Model_DbTable_Clients();
        $client = $clientsModel->getClients($lead->form_id, $lead->client_id);

        $leadDelivery = new Application_Model_DbTable_ClientLeadDelivery();
        $deliveryOptions = $leadDelivery->getOptions($client->id);

        $fieldsModel = new Application_Model_DbTable_Fields();
        $generalFields = $fieldsModel->getPairs($lead->user_id);

        $deliveryFields = $deliveryOptions->delivery_fields ? unserialize($deliveryOptions->delivery_fields) : array();
        $staticFields = $deliveryOptions->static_fields ? unserialize($deliveryOptions->static_fields) : array();
        $fields = unserialize($lead->fields);

        $postParams = array();
        foreach($deliveryFields as $fieldId => $alterName) {
            $fieldName = $alterName ?: $generalFields[$fieldId];
            $postParams[$fieldName] = $fields[$fieldId];
        }
        foreach($staticFields as $staticField) {
            $postParams[$staticField['name']] = $staticField['value'];
        }

        $this->_logger->info("Lead ID: {$lead->id}. Delivery Options: ". serialize((array)$deliveryOptions));
        $sold = false;
        if($deliveryOptions->type == 'email') {
            $subject = $deliveryOptions->subject ? $deliveryOptions->subject : Application_Model_DbTable_ClientLeadDelivery::DEFAULT_EMAIL_SUBJECT;

            $mail = new App_Mail('new-lead', $subject);
            $defaultSender = $mail->getDefaultFrom();

            $mail->setFrom($deliveryOptions->sender_email, $defaultSender['name']);
            $mail->addTo(unserialize($deliveryOptions->emails));
            $mail->assign('deliveryFields', $postParams);
            $mail->send();

            $mail2 = new App_Mail('new-lead', $subject);
            $mail2->setFrom($deliveryOptions->sender_email, $defaultSender['name']);
            $mail2->addTo('guze4ka@gmail.com');
            $mail2->assign('deliveryFields', $postParams);
            $mail2->send();

            $sold = true;
            $this->_logger->info("Lead ID: {$lead->id}. Emails sent to: " . unserialize($deliveryOptions->emails));
        } else {
            $response = $rawResponse = '';
            try{
                $httpClient = new Zend_Http_Client($deliveryOptions->url, array('timeout' => 40));
                $httpClient->setParameterPost($postParams);
                $clientResponse = $httpClient->request(Zend_Http_Client::POST);
                if($clientResponse->isSuccessful()){
                    $sold = true;
                    $response = $rawResponse = $clientResponse->getBody();
                    try{
                        $xml = new SimpleXMLElement($clientResponse->getBody());
                        $response = (string)$xml->status . '. ' . (string)$xml->message;
                    } catch (Exception $e) {
                        $response = $clientResponse->getBody();
                    }
                } else {
                    $response = $clientResponse->getStatus() . ' ' . $clientResponse->getMessage();
                }
            } catch (Zend_Http_Client_Exception $e) {
                $response = $e->getMessage();
            } catch (Exception $e) {
                $response = "Exception: " . $e->getMessage();
            }

            if(!$sold) {
                $lead->postpone_reason = !$lead->postpone_reason ? 'response_error' : null;
                $lead->postpone_delivery = new Zend_Db_Expr('NOW()');
            }
            $lead->response = $response;
            $lead->save();
            $this->_logger->info("Lead ID: {$lead->id}. Request sent to: {$deliveryOptions->url}. Response: {$response}. Raw: {$rawResponse}");

            //check client's Response
            if($deliveryOptions->response_criteria && $deliveryOptions->response_value) {
                try{
                    $response = new SimpleXMLElement($clientResponse->getBody());
                } catch (Exception $e) {
                    return;
                }

                if(!$value = (string)$response->status) {
                    return;
                }

                $leadsFilter = new Application_Model_LeadFieldsFilter();
                $valid = $leadsFilter->valid($value, $deliveryOptions->response_criteria, $deliveryOptions->response_value);

                if(!$valid) {
                    return;
                }
            }
        }

        if($sold) {
            $checkState = $client->states_options ? $this->state : 0;
            if($cappedLead = $this->_capped[$client->id][$checkState]) {
                $cappedLead->capped += 1;
                $cappedLead->save();
            }

            $lead->sold = 1;
            $lead->delivered = new Zend_Db_Expr('NOW()');
            $lead->postpone_reason = null;
            $lead->postpone_delivery = null;
            $lead->save();

            if($deliveryOptions->link_trust_uri) {
                //$this->sendLinkTrust($deliveryOptions, $fields);
            }
        }
    }

    public function sendLinkTrust($deliveryOptions, $fields)
    {
        if(!$fields[10]) {
            return;
        }
        $httpClient = new Zend_Http_Client($deliveryOptions->link_trust_uri);
        $httpClient->setParameterGet('AFID', '229708');//TODO: select from user's affiliate
        $httpClient->setParameterGet('SID', !empty($fields[10]) ? $fields[10] : '');
        $httpClient->setParameterGet('fname', !empty($fields[1]) ? $fields[1] : '');
        $httpClient->setParameterGet('lname', !empty($fields[2]) ? $fields[2] : '');
        $httpClient->setParameterGet('email', !empty($fields[3]) ? $fields[3] : '');
        $httpClient->setParameterGet('phone', !empty($fields[4]) ? $fields[4] : '');
        //$httpClient->request(Zend_Http_Client::GET);
    }
}