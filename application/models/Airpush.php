<?php

class Application_Model_Airpush
{
    protected $_apiUrl = 'http://openapi.airpush.com/';

    protected $_apiKey;
    protected $_email;
    protected $_method;

    protected $_dateFrom;
    protected $_dateTo;
    protected $_logger;

    const CAMPAIGN_REPORT = 'getCampaignReports';
    const CREATIVES_REPORT = 'getCreativeReports';
    const ADVERTISER_REPORT = 'getAdvertiserReports';

    protected $_errors = array(
        'login_error' => 'Login failed. Please, check your credentials',
        'server_unavailable' => 'Server is not responding',
        'account_connected' => 'This Account is already connected to your Leadssistant account',
        'campaigns_missing' => 'Campaigns was not found!'
    );

    public function __construct($options = array())
    {
        $this->_email = !empty($options['email']) ? $options['email'] : null;
        $this->_apiKey = !empty($options['api_key']) ? $options['api_key'] : null;

        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/airpush');
        $this->_logger = new Zend_Log($writer);
    }

    public function addAccount($userId)
    {
        $this->_method = self::ADVERTISER_REPORT;
        $content = $this->_sendRequest($this->makeUrl());

        if(!empty($content->advertiser_data)) {
            $result = array();
            $adsModel = new Application_Model_DbTable_Ads();
            if($adsModel->getAdsByName($userId, $this->_email, Application_Model_DbTable_Ads::AIRPUSH)) {
                $result['errorMessage'] = $this->_errors['account_connected'];
            } else {
                $credentials = array('api_key' => $this->_apiKey);

                $options = array('source' => Application_Model_DbTable_Ads::AIRPUSH,
                    'name' => $this->_email, 'credentials' => $credentials);
                return $adsModel->create($userId, $options);
            }
        }
        return $result;
    }

    /**
     * Add Campaign
     * 
     * @param Zend_Db_Table_Row $ad
     * @param string $campaignApiId
     * @param string $campaignName
     * @param int $adCampaignId Advert Campaign Id. Pass to perform update
     * 
     * @return Zend_Db_Table_Row
     */
    public function addCampaign($ad, $campaignApiId, $campaignName, $adCampaignId = null)
    {
        if(!$campaignApiId || !$campaignName) {
            return;
        }

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $keywords = $campaignsModel->getKeywords($ad->user_id);

        $data = array('api_id' => $campaignApiId, 'user_id' => $ad->user_id, 'ad_id' => $ad->id,
            'name' => $campaignName, 'source' => Application_Model_DbTable_Ads::AIRPUSH);

         //automatic Pairing
        if(false !== ($campaignId = App_Utils::findKeyword($campaignName, $keywords))) {
            $data['campaign_id'] = $campaignId;
        }

        $adCampaigns = new Application_Model_DbTable_AdCampaigns();
        return $adCampaigns->create($data, $adCampaignId);
    }

    /**
     * Get and Save Campaigns and Statistics
     * 
     * @param Zend_Db_Table_Row $ad
     * @param bool $updateCampaigns
     * @return array
     */
    public function getCampaignsStatistics($ad, $updateCampaigns = true)
    {
        $this->_dateFrom = $this->getDateFrom();
        $this->_dateTo = $this->getDateTo();

        if($ad->last_statistic_date) {
            $lastUpdate = new DateTime($ad->last_statistic_date);
            $this->_dateFrom = $lastUpdate->sub(new DateInterval('P1D'));
        }

        if($statistics = $this->_getStat()) {
            $adCampaigns = new Application_Model_DbTable_AdCampaigns();
            $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

            $campaignStat = new Application_Model_DbTable_AdCampaignStatistics();
            $campaignStat->getAdapter()->beginTransaction();

            //delete crossing stat:
            $campaignStat->deleteStatByDate($ad->id, $this->_dateFrom->format("Y-m-d"));

            foreach($statistics as $date => $row) {
                foreach($row as $stat) {
                    $campaignApiId = $stat->campaignid;
                    if(empty($campaignIds[$campaignApiId])) {
                        //add new Campaign
                        $newCampaign = $this->addCampaign($ad, $campaignApiId, $stat->campaignname);
                        $campaignIds[$newCampaign->api_id] = $newCampaign->id;
                    }

                    $newStat = $campaignStat->createRow();
                    $newStat->user_id = $ad->user_id;
                    $newStat->ad_id = $ad->id;
                    $newStat->ad_campaign_id = $campaignIds[$campaignApiId];
                    $newStat->date = strftime("%Y-%m-%d", strtotime($date));
                    $newStat->impressions = (int)$stat->impression;
                    $newStat->clicks = (int)$stat->clicks;
                    $newStat->spend = (float)$stat->Spent;
                    $newStat->ctr = $stat->ctr;
                    $newStat->status = strtolower($stat->campaignstatus);
                    $newStat->added = new Zend_Db_Expr('NOW()');
                    $newStat->save();
                }
            }
            $campaignStat->getAdapter()->commit();
        }

        $ad->last_statistic_date = new Zend_Db_Expr('CURDATE()');
        $ad->last_update = new Zend_Db_Expr('NOW()');
        $ad->save();
        $result['success'] = true;
        return $result;
    }

    /**
     * Find statistics
     * 
     * @return array
     */
    protected function _getStat()
    {
        $this->_method = self::ADVERTISER_REPORT;
        $statistics = array();
        $startDate = clone $this->_dateFrom;

        while($startDate <= $this->_dateTo) {
            $param['startDate'] = $param['endDate'] = $startDate->format('Y-m-d');

            $content = $this->_sendRequest($this->makeUrl(),  $param);
            if(!is_array($content)) {
                if(isset($content->advertiser_data)) {
                    $statistics = array_merge($statistics, array($param['startDate'] => (array)$content->advertiser_data));
                }
            }

            $startDate->add(new DateInterval('P1D'));
        }

        return $statistics;
    }

    protected function makeUrl()
    {
        return $this->_apiUrl . $this->_method;
    }

    /**
     * Get DateFrom
     * Set report starting date
     * 
     * @return DateTime
     **/
    public function getDateFrom()
    {
        if(!$this->_dateFrom) {
            $today = new DateTime();
            $this->_dateFrom = $today->sub(new DateInterval('P30D'));
        }
        return $this->_dateFrom;
    }

    /**
     * Get DateTo
     * Set report end date
     * 
     * @return DateTime
     **/
    public function getDateTo()
    {
        if(!$this->_dateTo) {
            $this->_dateTo = new DateTime();
        }
        return $this->_dateTo;
    }

    /**
     * Send CURL request
     * 
     * @param string $url
     * @param array $getParams
     * @param array $postParams
     * 
     * @return array|string
     */
    protected function _sendRequest($url, $getParams = array(), $postParams = array())
    {
        $authParams = array('apikey' => $this->_apiKey);
        $dateRange = array('startDate' => $this->getDateFrom()->format('Y-m-d'),
            'endDate'=> $this->getDateTo()->format('Y-m-d'));

        $adapter = new Zend_Http_Client_Adapter_Curl();
        $client = new Zend_Http_Client($url);

        $client->setAdapter($adapter);


        if($postParams) {
            $client->setParameterPost(array_merge($authParams, $dateRange, $postParams));
            $client->setMethod(Zend_Http_Client::POST);
        } else {
            $client->setParameterGet(array_merge($authParams, $dateRange, $getParams));
            $client->setMethod(Zend_Http_Client::GET);
        }

        try{
            $response = $client->request();
            if ($response->isSuccessful()){
                return json_decode($response->getBody());
            } else {
                $result['errorMessage'] = $this->_errors['server_unavailable'];
            }
        } catch (Zend_Http_Client_Exception $e) {
            $result['errorMessage'] = $e->getMessage();
        }

        $this->_logger->err("{$this->_apiKey}. URL {$url}: {$result['errorMessage']}");
        return $result;
    }
}