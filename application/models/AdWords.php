<?php
define('ADWORDS_VERSION', 'v201309');
define('LIB_PATH', 'Google/Api/Ads/AdWords/Lib');
require_once LIB_PATH . '/AdWordsUser.php';

class Application_Model_AdWords
{
    protected $_user;
    protected $_email;
    protected $_redirectUri;
    protected $_offline = true;

    protected $_dateFrom;
    protected $_dateTo;

    protected $_logger;

    /**
     * Class constructor
     * 
     * @param array $credentials
     */
    public function __construct($credentials = array())
    {
        $this->_redirectUri = SERVER_URL . 'adwords_oauth';
        $this->_reportPath = PUBLIC_PATH . "/../data/adwords_reports/";

        $oauth2Info = !empty($credentials) ? $credentials : null;
        $this->_user = new AdWordsUser(null, null, null, ADWORDS_DEVELOPER_TOKEN,
            null, null, null, null, null, $oauth2Info);

        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/adwords');
        $this->_logger = new Zend_Log($writer);
    }

    /**
     * Get Redirect URL. Check is account with given email already exists
     * 
     * @param int $userId
     * @param string $email
     * @return array
     */
    public function getRedirectUrl($userId, $email)
    {
        $result = array();
        $OAuth2Handler = $this->_user->GetOAuth2Handler();

        $adsModel = new Application_Model_DbTable_Ads();

        $ad = $adsModel->getAdsByName($userId, $email, Application_Model_DbTable_Ads::GOOGLE);
        if($ad) {
            $result['errorMessage'] = "Google Adsense Account with email {$email} is already exists";
        } else {
            //Get OAuth Url:
            //Added "approval_prompt=force" => returns refresh_token all the time
            $session = new Zend_Session_Namespace('adwords');
            $session->email = $email;

            $result['redirect_url'] = $OAuth2Handler->GetAuthorizationUrl(
                $this->_user->GetOAuth2Info(), $this->_redirectUri, $this->_offline) . '&approval_prompt=force';
        }

        return $result;
    }

    /**
     * Add new account
     * 
     * @param int $userId
     * @param string $code
     * @return array
     */
    public function addAccount($userId, $code)
    {
        $result = array();
        $accountId = null;

        $OAuth2Handler = $this->_user->GetOAuth2Handler();
        $session = new Zend_Session_Namespace('adwords');

        try {
            $this->_user->SetOAuth2Info(
                $OAuth2Handler->GetAccessToken($this->_user->GetOAuth2Info(), $code, $this->_redirectUri)
            );
            // The access token expires but the refresh token obtained for offline use
            // doesn't, and should be stored for later use.
            //Refresh Token not empty only when user gives access to the APP 
            //for the FIRST time and OFFLINE is TRUEs
            $credentials = $this->_user->GetOAuth2Info();

            $this->_user->ValidateUser();
            $customerInfo = $this->getCustomer();
            $accountId = $customerInfo->customerId;

            $adsModel = new Application_Model_DbTable_Ads();
            $ad = $adsModel->getAdsByAccountId($userId, $accountId, Application_Model_DbTable_Ads::GOOGLE);
            if(null == $ad) {
                $options = array('source' => Application_Model_DbTable_Ads::GOOGLE, 'account_id' => $accountId,
                  'name' => $session->email, 'credentials' => $credentials);

                $ad = $adsModel->create($userId, $options);

                $this->_user = new AdWordsUser(null, null, null, ADWORDS_DEVELOPER_TOKEN,
                    null, null, null, null, null, $credentials);

                //Add Campaigns
                if(true === ($result = $this->addCampaigns($ad))) {
                    $this->getCampaignsStatistics($ad);

                    $result = array();
                    $result['success'] = true;
                } else {
                    $ad->delete();
                }
            } else {
                throw new ValidationException('Account', $accountId,
                    "This Account is already connected");
            }
        } catch(ValidationException $e) {
            $result['errorMessage'] = $e->getMessage();
        } catch(OAuth2Exception $e) {
            $result['errorMessage'] = 'oAuth code invalid. Please, try again';
        }

        if(!empty($result['errorMessage'])) {
            $this->_logger->err("User: {$userId} {$result['errorMessage']}");
        }
        return $result;
    }

    /**
     * Add Campaigns
     * 
     * @param Zend_Db_Table_Row $ad
     * @return array|boolean
     */
    public function addCampaigns($ad)
    {
        $result = array();
        $campaignService = $this->_user->GetService('CampaignService', ADWORDS_VERSION);

        // Create selector.
        $selector = new Selector();
        $selector->fields = array('Id', 'Name', 'Amount', 'Status');
        $selector->ordering[] = new OrderBy('Name', 'ASCENDING');

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $keywords = $campaignsModel->getKeywords($ad->user_id);

        $adCampaigns = new Application_Model_DbTable_AdCampaigns();
        $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

        // Create paging controls.
        $selector->paging = new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE);

        do {
          $page = $campaignService->get($selector);
            if (isset($page->entries)) {
                foreach ($page->entries as $campaign) {
                    $campaignApiId = $campaign->id;
                    if(!empty($campaignIds[$campaignApiId])) { continue; }

                    $dailyBudget = null;
                    if(!empty($campaign->budget) && $campaign->budget->period == 'DAILY') {
                        $dailyBudget = $campaign->budget->amount->microAmount/1000000;
                    }

                    $data = array('api_id' => $campaignApiId, 'user_id' => $ad->user_id, 'ad_id' => $ad->id,
                        'name' => $campaign->name, 'daily_budget' => $dailyBudget,
                        'status' => strtolower($campaign->status), 'source' => Application_Model_DbTable_Ads::GOOGLE);

                    //automatic Pairing
                    if(false !== ($campaignId = App_Utils::findKeyword($campaign->name, $keywords))) {
                        $data['campaign_id'] = $campaignId;
                    }

                    $newCampaign = $adCampaigns->create($data);
                    $campaignIds[$newCampaign->api_id] = $newCampaign->id;
                }
            } else {
                $result['errorMessage'] = 'No campaigns were found';
                return $result;
            }

          // Advance the paging index.
          $selector->paging->startIndex += AdWordsConstants::RECOMMENDED_PAGE_SIZE;
        } while ($page->totalNumEntries > $selector->paging->startIndex);

        return true;
    }

    /**
     * Get and Save Campaign Statistics
     * 
     * @param Zend_Db_Table_Row $ad
     * @param bool $updateCampaigns
     * @return array
     */
    public function getCampaignsStatistics($ad, $updateCampaigns = false)
    {
        $this->_user->SetClientCustomerId($ad->account_id);
        if($updateCampaigns) {
            $this->addCampaigns($ad);
        }

        if($ad->last_statistic_date) {
            $lastUpdate = new DateTime($ad->last_statistic_date);
            $this->_dateFrom = $lastUpdate->sub(new DateInterval('P1D'));
        }
        $report = $this->downloadReport("{$this->_reportPath}{$ad->id}_" . time() . ".xml");

        if($report) {
            $adCampaigns = new Application_Model_DbTable_AdCampaigns();
            $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

            $campaignStat = new Application_Model_DbTable_AdCampaignStatistics();
            $campaignStat->getAdapter()->beginTransaction();

            //delete crossing stat:
            $campaignStat->deleteStatByDate($ad->id, $this->getDateFrom()->format("Y/m/d"));

            foreach($report->table->row as $stat) {
                $campaignId = (string)$stat['campaignID'];

                $newStat = $campaignStat->createRow();
                $newStat->user_id = $ad->user_id;
                $newStat->ad_id = $ad->id;
                $newStat->ad_campaign_id = $campaignIds[$campaignId];
                $newStat->date = strftime("%Y-%m-%d", strtotime($stat['day']));
                $newStat->impressions = (int)$stat['Impressions'];
                $newStat->clicks = (int)$stat['clicks'];
                $newStat->ctr = (float)str_replace('%', '', $stat['ctr']);
                $newStat->spend = (float)round($stat['cost']/1000000, 2);
                $newStat->average_cpc = (float)round($stat['avgCPC']/1000000, 2);
                $newStat->average_position = (float)$stat['avgPosition'];
                $newStat->average_cpm = (float)round($stat['avgCPM']/1000000, 2);
                $newStat->status = (string)strtolower($stat['campaignState']);
                $newStat->added = new Zend_Db_Expr('NOW()');
                $newStat->save();
            }
            $campaignStat->getAdapter()->commit();
        }

        $ad->last_statistic_date = new Zend_Db_Expr('CURDATE()');
        $ad->last_update = new Zend_Db_Expr('NOW()');
        $ad->save();
        $result['success'] = true;

        return $result;
    }

    /**
     * Get Customer info
     * 
     * @return type
     * @throws ValidationException
     */
    public function getCustomer() {
        $customerService = $this->_user->GetService('CustomerService', ADWORDS_VERSION);
        $selector = new Selector();
        $selector->fields = array('Login', 'CustomerId', 'Name');
        $customer = $customerService->get($selector);

        if(!$customer || empty($customer->customerId)) {
            throw new ValidationException('Customer', NULL,
                "Customer ID was not found");
        }
        return $customer;
    }

    /**
     * Downloads report to given file
     * 
     * @param string $filePath
     * @return SimpleXMLElement|boolean
     */
    protected function downloadReport($filePath) {
        // Load the service, so that the required classes are available.
        $this->_user->LoadService('ReportDefinitionService', ADWORDS_VERSION);

        // Create selector.
        $selector = new Selector();
        $selector->fields = array('CampaignId', 'Id', 'Impressions', 'Clicks', 
            'Ctr', 'Cost', 'AverageCpc', 'AveragePosition', 'AverageCpm', 'Status', 'Date');

        //Filter out deleted criteria.
        //$selector->predicates[] = new Predicate('Status', 'NOT_IN', array('DELETED'));

        //Date Range criteria
        $selector->dateRange = new DateRange($this->getDateFrom()->format('Ymd'), 
            $this->getDateTo()->format('Ymd'));

        // Create report definition.
        $reportDefinition = new ReportDefinition();
        $reportDefinition->selector = $selector;
        $reportDefinition->reportName = 'Criteria performance report #' . uniqid();
        $reportDefinition->dateRangeType = 'CUSTOM_DATE';
        $reportDefinition->reportType = 'CAMPAIGN_PERFORMANCE_REPORT';
        $reportDefinition->downloadFormat = 'XML';

        // Exclude criteria that haven't recieved any impressions over the date range.
        $reportDefinition->includeZeroImpressions = FALSE;

        // Set additional options.
        $options = array('version' => ADWORDS_VERSION, 'returnMoneyInMicros' => TRUE);

        // Download report.
        ReportUtils::DownloadReport($reportDefinition, $filePath, $this->_user, $options);

        return $this->proceedReport($filePath);
    }

    /**
     * Open report file and return the report
     * 
     * @param string $report
     * @return boolean|SimpleXMLElement
     */
    protected function proceedReport($report)
    {
        $statistics = file_get_contents($report);
        try{
            return new SimpleXMLElement($statistics); 
        } catch (Exception $e) {
            $this->_logger->err("Report '{$report}': {$e->getMessage()}");
            return false;
        }
    }

    /**
     * Get DateFrom
     * Set report starting date
     * 
     * @return DateTime
     **/
    public function getDateFrom()
    {
        if(!$this->_dateFrom) {
            $today = new DateTime();
            $this->_dateFrom = new DateTime($today->sub(new DateInterval('P1M'))->format("m/01/Y"));
        }
        return $this->_dateFrom;
    }

     /**
     * Get DateTo
     * Set report end date
     * 
     * @return DateTime
     **/
    public function getDateTo()
    {
        if(!$this->_dateTo) {
            $today = new DateTime();
            $this->_dateTo = $today;
        }
        return $this->_dateTo;
    }
}