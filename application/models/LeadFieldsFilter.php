<?php

class Application_Model_LeadFieldsFilter
{
    protected $_fields;

    public function __construct($fields = array())
    {
        $this->_fields = $fields;
    }

    public function isValid($filters)
    {
        $isValid = true;
        foreach($filters as $filter) {
            $result = false;

            if(!empty($this->_fields[$filter->field_id])) {
                $fieldValue = $this->_fields[$filter->field_id];
                $searchValue = $filter->multi ? json_decode($filter->search_value) : $filter->search_value;

                $result = $this->valid($fieldValue, $filter->criteria, $searchValue, $filter->multi);
            }

            if((!$result && $filter->type == 'require') || ($result && $filter->type == 'exclude')) {
                $isValid = false; 
                break;
            }
        }

        return $isValid;
    }

    public function valid($fieldValue, $criteria, $searchValue, $multi = false)
    {
        switch($criteria) {
            case 'begins_with':
                $result = (0 === strpos($fieldValue, $searchValue)) ? true : false;
                break;
            case 'contains':
                if($multi) {
                    $result = in_array($fieldValue, $searchValue);
                } else {
                    $result = (false === strpos($fieldValue, $searchValue)) ? false : true;
                }
                break;
            case 'equal':
                $result = ($fieldValue == $searchValue);
                break;
            case 'greater':
                $result = ($fieldValue > $searchValue);
                break;
            case 'less':
                $result = ($fieldValue < $searchValue);
                break;
            case 'not_equal':
                $result = ($fieldValue != $searchValue);
                break;
            case 'between':
                $result = ($fieldValue > $searchValue[0] && $fieldValue < $searchValue[1]);
                break;
        }

        return $result;
    }
}