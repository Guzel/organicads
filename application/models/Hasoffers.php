<?php
/**
 * Hasoffer API Model
 */
class Application_Model_Hasoffers
{
    protected $_apiUrl = 'http://api.hasoffers.com/v3/';

    protected $_apiKey;
    protected $_networkId;

    protected $_dateFrom;
    protected $_dateTo;

    protected $_logger;

    const AFFILIATE_USER = 'Affiliate_AffiliateUser';
    const AFFILIATE_OFFER = 'Affiliate_Offer';
    const AFFILIATE_REPORT = 'Affiliate_Report';
    const FIND_ALL_METHOD = 'findAll';
    const GET_STATS_METHOD = 'getStats';

    protected $_errors = array(
        'login_error' => 'Login failed. Please, check your credentials',
        'server_unavailable' => 'Hasoffers Server is not responding',
        'account_connected' => 'This Account is already connected to your Leadssistant account',
        'campaigns_missing' => 'Campaigns was not found!',
        'statistic_missing' => 'Statistic for given params was not found'
    );

    public function __construct($options = array())
    {
        $this->_apiKey = !empty($options['api_key']) ? $options['api_key'] : null;
        $this->_networkId = !empty($options['network_id']) ? $options['network_id'] : null;

        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/hasoffers');
        $this->_logger = new Zend_Log($writer);
    }

    /**
     * Add new account
     * 
     * @param int $userId
     * @return array|Zend_Db_Table_Row
     */
    public function addAccount($userId)
    {
        $this->_service = self::AFFILIATE_USER;
        $this->_method = self::FIND_ALL_METHOD;
        $result = array();

        $content = $this->_sendRequest($this->makeUrl());
        if(!is_array($content)) {
            $affiliateUser = current($content->response->data);
            $affiliateUser = $affiliateUser->AffiliateUser;

            $affiliateModel = new Application_Model_DbTable_Affiliates();
            if($affiliateModel->getAffiliateByApiId($affiliateUser->affiliate_id, $userId, Application_Model_DbTable_Affiliates::HASOFFERS)) {
                $result['errorMessage'] = $this->_errors['account_connected'];
            } else {
                $credentials = array('api_key' => $this->_apiKey, 'network_id' => $this->_networkId);
                $affiliateOptions = array('name' => $affiliateUser->email, 'credentials' => $credentials, 
                    'api_id' => $affiliateUser->affiliate_id, 'source' => Application_Model_DbTable_Affiliates::HASOFFERS);
                $result = $affiliateModel->create($affiliateOptions, $userId);
            }
        } else {
            $result = is_array($content) ? $content : $this->_errors['login_error'];
        }
        return $result;
    }

    /**
     * Add Offers
     * 
     * @param Zend_Db_Table_Row $affiliate
     * @return array|boolean
     */
    public function addCampaigns($affiliate)
    {
        $this->_service = self::AFFILIATE_OFFER;
        $this->_method = self::FIND_ALL_METHOD;
        $content = $this->_sendRequest($this->makeUrl());

        if(empty($content->response->data)) {
            return $this->_errors['campaigns_missing'];
        }

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $keywords = $campaignsModel->getKeywords($affiliate->user_id);

        $affiliateCampaigns = new Application_Model_DbTable_AffiliateCampaigns();
        $campaignIds = $affiliateCampaigns->getPairs($affiliate->user_id, $affiliate->id);

        foreach($content->response->data as $campaignApiId => $row) {
            $offer = $row->Offer;
            //update existing Campaigns every time
            $id = !empty($campaignIds[$campaignApiId]) ? $campaignIds[$campaignApiId] : null;
            $campaignName = (string)$offer->name;

            $data = array('api_id' => $campaignApiId, 'user_id' => $affiliate->user_id, 'affiliate_id' => $affiliate->id,
                'name' => $campaignName, 'payout' => (float)$offer->default_payout);

            //automatic Pairing
            if(false !== ($campaignId = App_Utils::findKeyword($campaignName, $keywords))) {
                $data['campaign_id'] = $campaignId;
            }

            $newCampaign = $affiliateCampaigns->create($data, $id);
            $campaignIds[$newCampaign->api_id] = $newCampaign->id;
        }
        return true;
    }

    /**
     * Get and Save Campaigns and Statistics
     * 
     * @param Zend_Db_Table_Row $affiliate
     * @param bool $updateCampaigns
     * @return array
     */
    public function getCampaignsStatistics($affiliate, $updateCampaigns = true)
    {
        $error = $addedStats = null;
        if(true !== ($res = $this->addCampaigns($affiliate))) {
            $error = $res;
        }

        $this->_dateFrom = $this->getDateFrom();
        $this->_dateTo = $this->getDateTo();
        if($affiliate->last_statistic_date) {
            $lastUpdate = new DateTime($affiliate->last_statistic_date);
            $this->_dateFrom = $lastUpdate->sub(new DateInterval('P1D'));
        }

        if(($statistics = $this->_getStat())) {
            //get existing Camaigns:
            $affiliateCampaigns = new Application_Model_DbTable_AffiliateCampaigns();
            $campaignIds = $affiliateCampaigns->getPairs($affiliate->user_id, $affiliate->id);

            $campaignStat = new Application_Model_DbTable_AffiliateCampaignStatistics();
            $campaignStat->getAdapter()->beginTransaction();

            //delete crossing stat:
            $campaignStat->deleteStatByDate($affiliate->id, $this->_dateFrom->format("Y-m-d"));
            foreach($statistics as $row) {
                $stat = $row->Stat;
                $campaignApiId = (int)$stat->offer_id;

                if(empty($campaignIds[$campaignApiId]) || strtotime($stat->date) < $this->_dateFrom->getTimestamp()) {
                    continue;
                }

                $newStat = $campaignStat->createRow();
                $newStat->affiliate_campaign_id = $campaignIds[$campaignApiId];
                $newStat->affiliate_id = $affiliate->id;
                $newStat->user_id = $affiliate->user_id;
                $newStat->date = date('Y-m-d', strtotime($stat->date));
                $newStat->clicks = (int)$stat->clicks;
                $newStat->approved = (int)$stat->conversions;
                $newStat->impression = (int)$stat->impressions;
                $newStat->epc = (float)$stat->epc;
                $newStat->commission = (float)$stat->payout;
                $newStat->added = new Zend_Db_Expr('NOW()');
                $newStat->save();
                $addedStats += 1;
            }
            $campaignStat->getAdapter()->commit();
        }

        if(!$statistics || !$addedStats) {
            $error = $this->_errors['statistic_missing'];
        }

        $affiliate->last_statistic_date = new Zend_Db_Expr('CURDATE()');
        $affiliate->last_update = new Zend_Db_Expr('NOW()');
        $affiliate->save();

        if(!$error) {
            $result['success'] = true;
        }
        $result['errorMessage'] = $error;

        return $result;
    }

     /**
     * Get Statistic using pagination
     * 
     * @param int $page
     * @return array
     */
    protected function _getStat($page = 1)
    {
        $statistics = array();
        $this->_service = self::AFFILIATE_REPORT;
        $this->_method = self::GET_STATS_METHOD;

        $fields = array('Stat.offer_id', 'Stat.conversions', 'Stat.payout', 'Stat.impressions', 'Stat.epc', 'Stat.clicks', 'Stat.ctr');
        $groups = array('Stat.offer_id', 'Stat.date');
        $getParams = array('data_start' => $this->_dateFrom->format("Y-m-d"), 'data_end' => $this->_dateTo->format("Y-m-d"), 
            'field' => array('Stat.conversions', 'Stat.payout'),
            'fields' => $fields,
            'groups' => $groups,
            'hour_offset' => '-3',
            'page' => (int)$page
        );
        $stat = $this->_sendRequest($this->makeUrl(), $getParams);

        if(empty($stat->response->data)) {
            return $statistics;
        }

        $data = $stat->response->data;
        if($data->pageCount > $page) {
            $page++;
            return array_merge($data->data, $this->_getStat($page));
        } else {
            return (array)$data->data;
        }
    }

    /**
     * Return Full API Url using current Method and Service values
     * @return string
     */
    protected function makeUrl()
    {
        return "{$this->_apiUrl}{$this->_service}.json?Method={$this->_method}&api_key={$this->_apiKey}&NetworkId={$this->_networkId}";
    }

    /**
     * Get DateFrom
     * Set report starting date
     * 
     * @return DateTime
     **/
    public function getDateFrom()
    {
        if(!$this->_dateFrom) {
            $today = new DateTime();
            $this->_dateFrom = $today->sub(new DateInterval('P30D'));
        }
        return $this->_dateFrom;
    }
    

    /**
     * Get DateTo
     * Set report end date
     * 
     * @return DateTime
     **/
    public function getDateTo()
    {
        if(!$this->_dateTo) {
            $this->_dateTo = new DateTime();
        }
        return $this->_dateTo;
    }

    /**
     * Send CURL request
     * 
     * @param string $url
     * @param array $getParams
     * @param array $postParams
     * 
     * @return bool|string|Application_Model_Exceptions_Curl
     */
    protected function _sendRequest($url, $getParams = array(), $postParams = array())
    {
        $config = array(
            'timeout' => 30,
            'useragent' => "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)",
            'curloptions' => array(CURLOPT_RETURNTRANSFER => 1, CURLOPT_SSL_VERIFYPEER => false)
        );

        $client = new Zend_Http_Client($url, $config);

        if($postParams) {
            $client->setParameterPost($postParams);
            $client->setMethod(Zend_Http_Client::POST);
        } else {
            $client->setParameterGet($getParams);
            $client->setMethod(Zend_Http_Client::GET);
        }

        try{
            $response = $client->request();
            if ($response->isSuccessful()){
                try{
                    $respone = json_decode($response->getBody());
                    if(!empty($respone->response->errors) || !empty($respone->response->errorMessage)) {
                        $errorMessage = !empty($respone->response->errors->publicMessage) ? 
                            $respone->response->errors->publicMessage : $respone->response->errorMessage;
                        $result['errorMessage'] = (string)$errorMessage;
                    } else {
                        return $respone;
                    }
                } catch (Exception $e) {
                    $result['errorMessage'] = $e->getMessage();
                }
            } else {
                $result['errorMessage'] = $this->_errors['server_unavailable'];
            }
        } catch (Zend_Http_Client_Exception $e) {
            $result['errorMessage'] = $e->getMessage();
        }

        $this->_logger->err("URL {$url}: {$result['errorMessage']}");
        return $result;
    }
}