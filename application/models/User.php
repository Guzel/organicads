<?php

require_once 'App/PasswordHash.php';

class Application_Model_User
{

    protected $_usersModel;

    /**
     * Construct
     * 
     * @return void
     */
    public function __construct()
    {
        $this->_usersModel = new Application_Model_DbTable_Users();
    }

    /**
     * Login user
     * 
     * @param string $login
     * @param string $password
     * @return boolean|Application_Model_DbTable_User
     */
    public function login($login, $password)
    {
        $user = $this->_usersModel->findUserByEmail($login);

        if(!$user || !self::isValidPassword($password, $user->password)) {
            return false;
        }

        $authAdapter = new Zend_Auth_Adapter_DbTable(
                Zend_Db_Table::getDefaultAdapter(), 'users', 'email', 'id');
        $authAdapter->setIdentity($login)->setCredential($user->id);

        $authResult = $authAdapter->authenticate();
        if($authResult->isValid()) {
            $user->last_login = new Zend_Db_Expr('NOW()');
            $user->save();

            $storage = new Zend_Auth_Storage_Session();
            $storage->write($authAdapter->getResultRowObject('id'));

            return $user;
        }

        return false;
    }

     /**
     * Create hash for password reset
     * 
     * @param type $email
     * @return void
     */
    public function startResetPassword($email)
    {
        if(!$user = $this->_usersModel->findUserByEmail($email)) {
            return false;
        }

        $hash = md5($user->email . '%' . $user->username . '%' . RESET_PASSWORD_SALT . '%' . $user->id . time());
        $user->reset_password_hash = $hash;
        $user->hash_created = new Zend_Db_Expr('NOW()');
        $user->save();

        $mail = new App_Mail('reset-password', 'Password Recovery');
        $mail->addTo($user->email);
        $mail->assign('user', $user);
        $mail->send();
    }

    /**
     * Get password hash
     * 
     * @param string $password
     * @return string
     */
    public static function getPasswordHash($password)
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    /**
     * Check if password is correct
     * 
     * @param string $password
     * @param string $passwordHash
     * @return bool
     */
    public static function isValidPassword($password, $passwordHash)
    {
        return password_verify($password, $passwordHash);
    }

    /**
     * Method that sets up the object
     *
     * @return void
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}