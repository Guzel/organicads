<?php

class Application_Model_Cache
{
    public static function getManager()
    {
        //return Zend_Controller_Front::getInstance()->getParam('bootstrap')->getPluginResource('cachemanager')->getCacheManager();
        return Zend_Registry::get('bootstrap')->getResource('cachemanager');
    }

    public static function clear($cacheId)
    {
        $cache = self::getManager()->getCache('database');
        $cache->remove($cacheId);
    }
}