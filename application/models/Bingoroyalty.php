<?php

/**
 * Bingoroyalty Scraper
 */
class Application_Model_Bingoroyalty
{
    protected $_mainUrl = 'http://www.bingoroyalty.com/partners/';
    protected $_csvReportUri = 'app_resources/script/export.aspx';
    protected $_email;
    protected $_password;
    protected $_ui;

    protected $_dateFrom;
    protected $_dateTo;

    protected $_cookieJar;
    protected $_logger;

    const COMPANY_ID = 3891;
    const SIGNUP_REVENUE = 28.80;
    const DEPOSIT_REVENUE = 270;

    protected $_errors = array(
        'login_error' => 'Login failed. Please, check your credentials',
        'account_id_missing' => 'Account ID was not found',
        'server_unavailable' => 'Yahoo Server not responding',
        'account_connected' => 'This Account is already connected to your Leadssistant account',
    );

    /**
     * Class constructor
     * 
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->_email = !empty($options['email']) ? $options['email'] : null;
        $this->_password = !empty($options['password']) ? $options['password'] : null;

        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/bingroyalty');
        $this->_logger = new Zend_Log($writer);
    }

    /**
     * Add new BingRoyalty account
     * 
     * @param int $userId
     * @return array|Zend_Db_Table_Row
     */
    public function addAccount($userId)
    {
        try {
            $xpath = $this->login();
            $affiliateModel = new Application_Model_DbTable_Affiliates();
            $accountId = $result = null;

            if($ui = $xpath->query("//form[@id='frmDefault']")->item(0)) {
                if($action = $ui->getAttributeNode('action')->nodeValue) {
                    preg_match("/t\=([0-9]+)/", $action, $matches);
                    $accountId = $matches[1];

                    if($affiliateModel->getAffiliateByApiId($accountId, $userId, Application_Model_DbTable_Affiliates::LEADNOMICS)) {
                        $result['errorMessage'] = $this->_errors['account_connected'];
                    } else {
                        $credentials = array('email' => $this->_email, 'password' => $this->_password, 'ui' => $this->_ui);

                        $options = array('account_id' => $accountId, 'api_id' => $accountId, 
                            'source' => Application_Model_DbTable_Affiliates::BINGOROYALTY,
                            'name' => $this->_email, 'credentials' => $credentials);
                        return $affiliateModel->create($options, $userId);
                    }
                }
            }
        } catch(Exception $e) {
            $result['errorMessage'] = $e->getMessage();
        }

        if(!$result) {
            $result['errorMessage'] = $this->_errors['account_id_missing'];
        }
        $this->_logger->err("{$this->_email} {$result['errorMessage']}");
        return $result;
    }

    /**
     * Add Campaigns
     * 
     * @param Zend_Db_Table_Row $affiliate
     * @param int $campaigId Campaigns api id
     * @param string $campaignName Campaign name
     * @return Zend_Db_Table_Row
     */
    public function addCampaign($affiliate, $campaignId, $campaignName)
    {
        if(!$campaignId || !$campaignName) {
            return;
        }

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $keywords = $campaignsModel->getKeywords($affiliate->user_id);

        $affiliateCampaigns = new Application_Model_DbTable_AffiliateCampaigns();
        $campaignIds = $affiliateCampaigns->getPairs($affiliate->user_id, $affiliate->id);

        $campaignApiId = (int)$campaignId;

        $id = !empty($campaignIds[$campaignApiId]) ? $campaignIds[$campaignApiId] : null;
        //if($id) { continue; } //remove this if need to update existing campaigns

        $data = array('api_id' => $campaignApiId, 'user_id' => $affiliate->user_id, 
            'name' => $campaignName, 'affiliate_id' => $affiliate->id);

        //automatic Pairing
        if(false !== ($campaignId = App_Utils::findKeyword($campaignName, $keywords))) {
            $data['campaign_id'] = $campaignId;
        }

        return $affiliateCampaigns->create($data, $id);
    }

    /**
     * Get and Save Campaigns and Statistics
     * 
     * @param Zend_Db_Table_Row $affiliate
     * @param bool $updateCampaigns
     * @return array
     */
    public function getCampaignsStatistics($affiliate, $updateCampaigns = true)
    {
        try {
            $this->login();
        } catch(Exception $e) {
            $this->_logger->err("{$this->_email} {$e->getMessage()}");
            return;
        }

        $this->_dateFrom = $this->getDateFrom();
        $this->_dateTo = $this->getDateTo();

        if($affiliate->last_statistic_date) {
            $lastUpdate = new DateTime($affiliate->last_statistic_date);
            $this->_dateFrom = $lastUpdate->sub(new DateInterval('P1D'));
        }

        $statistics = array();
        $startDate = clone $this->_dateFrom;
        while($startDate <= $this->_dateTo) {
            $statistics = array_merge($statistics, $this->_getStat($startDate));
            $startDate->add(new DateInterval('P1M'));
        }

        if($statistics) {
            //get existing Camaigns:
            $affiliateCampaigns = new Application_Model_DbTable_AffiliateCampaigns();
            $campaignIds = $affiliateCampaigns->getPairs($affiliate->user_id, $affiliate->id);

            $campaignStat = new Application_Model_DbTable_AffiliateCampaignStatistics();
            $campaignStat->getAdapter()->beginTransaction();

            //delete crossing stat:
            $campaignStat->deleteStatByDate($affiliate->id, $this->_dateFrom->format("Y-m-d"));
            foreach($statistics as $date => $stat) {
                $campaignApiId = (int)$stat['campaign_api_id'];
                if(empty($campaignIds[$campaignApiId])) {
                    //add new Campaign
                    $newCampaign = $this->addCampaign($affiliate, $campaignApiId, $stat['campaign']);
                    $campaignIds[$newCampaign->api_id] = $newCampaign->id;
                }

                $newStat = $campaignStat->createRow();
                $newStat->affiliate_campaign_id = $campaignIds[$campaignApiId];
                $newStat->affiliate_id = $affiliate->id;
                $newStat->user_id = $affiliate->user_id;
                $newStat->date = $date;
                $newStat->clicks = (int)$stat['signups'];
                $newStat->commission = (float)$stat['revenue'];
                $newStat->added = new Zend_Db_Expr('NOW()');
                $newStat->save();
            }
            $campaignStat->getAdapter()->commit();
        }

        $affiliate->last_statistic_date = $date;
        $affiliate->last_update = new Zend_Db_Expr('NOW()');
        $affiliate->save();
        $result['success'] = true;

        return $result;
    }

    /**
     * CSV file scraping
     * 
     * @param DateTime $date
     * @return array
     */
    protected function _getStat($date)
    {
        $url = $this->_mainUrl . $this->_csvReportUri;
        $get = array(
            'r' => 'specialtagcpa', 
            'c' => "{$this->_ui}|0|" . $date->format('1 F Y') . "|" . $date->format('t F Y') . "|xx3x62x95x65x91"
        );

        $content = $this->_sendRequest($url, $get);
        $content = explode("\r\n", $content);

        $keys = array(0 => 'tag', 'campaign_api_id', 'campaign', 'downloads', 'signups', 'new_deposits', 'deposits');
        foreach($content as $row) {
            $rowStatistic = str_getcsv($row);
            if($rowStatistic[1] == self::COMPANY_ID) {
                $statistic = array_combine($keys, array_filter($rowStatistic, 'strlen'));
                $signupRevenue = $statistic['signups'] * self::SIGNUP_REVENUE;
                $depositsRevenue = $statistic['deposits'] * self::DEPOSIT_REVENUE;
                $statistic['revenue'] = max($signupRevenue, $depositsRevenue);
            }
        }

        $statDate = $date->format('m') == date('m') ? $date->format('Y-m-d') : $date->format('Y-m-t');
        $result[$statDate] = $statistic;
        return $result;
    }

    /**
     * Bingroyalty login
     * 
     * @return array|string
     */
    public function login()
    {
        $this->_cookieJar = new Zend_Http_CookieJar();
        $loginPage = $this->_sendRequest($this->_mainUrl);

        $xpath = $this->getXPath($loginPage);

        $viewstate = $eventvalidation = null;
        if($viewstateElement = $xpath->query("//input[@name='__VIEWSTATE']")->item(0)) {
            $viewstate = $viewstateElement->getAttributeNode('value')->nodeValue;
        }
        if($validationElement = $xpath->query("//input[@name='__EVENTVALIDATION']")->item(0)) {
            $eventvalidation = $validationElement->getAttributeNode('value')->nodeValue;
        }
        if($scrollPosX = $xpath->query("//input[@name='__SCROLLPOSITIONX']")->item(0)) {
            $scrollPosX = $scrollPosX->getAttributeNode('value')->nodeValue;
        }
        if($scrollPosY = $xpath->query("//input[@name='__SCROLLPOSITIONY']")->item(0)) {
            $scrollPosY = $scrollPosY->getAttributeNode('value')->nodeValue;
        }

        $post = array(
            '__EVENTTARGET' => 'loginSUBMIT',
            '__EVENTARGUMENT' => '',
            '__VIEWSTATE' => $viewstate,
            '__SCROLLPOSITIONX' => $scrollPosX,
            '__SCROLLPOSITIONY' => $scrollPosY,
            '__EVENTVALIDATION' => $eventvalidation,
            'loginEMAIL' => $this->_email, 
            'loginPASSWORD' => $this->_password, 
            'loginREMEMBERME' => 'on'
        );

        $login = $this->_sendRequest($this->_mainUrl, null, $post);
        $xpath = $this->getXPath($login);
        if($ui = $xpath->query("//input[@id='ui']")->item(0)) {
            $this->_ui = $ui->getAttributeNode('value')->nodeValue;
            if(!empty($this->_ui)) {
                return $xpath;
            }
        }

        throw new Exception($this->_errors['login_error']);
    }

    /**
     * Get DateFrom
     * Set report starting date
     * 
     * @return DateTime
     **/
    public function getDateFrom()
    {
        if(!$this->_dateFrom) {
            $today = new DateTime();
            $this->_dateFrom = $today->sub(new DateInterval('P1M'));
        }
        return $this->_dateFrom;
    }

    /**
     * Get DateTo
     * Set report end date
     * 
     * @return DateTime
     **/
    public function getDateTo()
    {
        if(!$this->_dateTo) {
            $this->_dateTo = new DateTime();
        }
        return $this->_dateTo;
    }

    /**
     * Get XPath
     * 
     * @param string $content
     * @return \DOMXpath
     */
    protected function getXPath($content)
    {
        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($content);
        libxml_use_internal_errors(false);
        return new DOMXpath($dom);
    }

    /**
     * Send CURL request
     * 
     * @param string $url
     * @param array $getParams
     * @param array $postParams
     * 
     * @return array|string
     */
    protected function _sendRequest($url, $getParams = array(), $postParams = array())
    {
        $result = array();

        $config = array(
            'timeout' => 30,
            'useragent' => "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)",
            'curloptions' => array(CURLOPT_RETURNTRANSFER => 1, CURLOPT_SSL_VERIFYPEER => false)
        );

        $adapter = new Zend_Http_Client_Adapter_Curl();
        $client = new Zend_Http_Client($url, $config);

        $client->setAdapter($adapter);
        $client->setCookieJar($this->_cookieJar);

        if($postParams) {
            $client->setParameterPost($postParams);
            $client->setMethod(Zend_Http_Client::POST);
        } else {
            $client->setParameterGet($getParams);
            $client->setMethod(Zend_Http_Client::GET);
        }

        try{
            $response = $client->request();
            if ($response->isSuccessful()){
                return $response->getBody();
            } else {
                $result['errorMessage'] = $this->_errors['server_unavailable'];
            }
        } catch (Zend_Http_Client_Exception $e) {
            $result['errorMessage'] = $e->getMessage();
        }

        $this->_logger->err("{$this->_email}. URL {$url}: {$result['errorMessage']}");
        return $result;
    }
}