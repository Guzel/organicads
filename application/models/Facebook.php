<?php

class Application_Model_Facebook
{
    protected $_fbUrl = 'https://www.facebook.com/';
    protected $_locale = 'en_US';
    protected $_email;
    protected $_password;
    protected $_ip;
    protected $_userId;
    protected $_fb_dtsg;
    protected $_accessToken;
    protected $_accessTokenLong;

    protected $_dateFrom;
    protected $_dateTo;

    protected $_cookieJar;
    protected $_logger;

    const RECORDS_ON_PAGE = 50;
    protected $_errors = array(
        'login_error' => 'Login failed. Please, check your credentials',
        'account_id_missing' => 'Account ID was not found',
        'server_unavailable' => 'Facebook Server not responding',
        'account_connected' => 'This Account is already connected to your Leadssistant account',
        'campaigns_missing' => 'Campaigns was not found!',
        'account_locked' => 'Your account is temporarily locked. Please, go to your account and answer a few security questions to unlock it. Then try to connect it again'
    );

    protected $_campaigns_report = array(
        'data_columns' => array("campaign_group_name","campaign_group_id", "reach","frequency","impressions","clicks","unique_clicks","ctr","unique_ctr","spend","cpm","cpp","cpc","total_actions","total_unique_actions","actions"),
        'export_columns' => array("date_start","date_stop","campaign_group_name","campaign_group_id","reach","frequency","impressions","clicks","unique_clicks","ctr","unique_ctr","spend","cpm","cpp","cpc","total_actions","total_unique_actions","actions:like")
    );

    protected $_adgroups_report = array(
        'data_columns' => array("campaign_group_name","campaign_group_id", "campaign_id", "campaign_name","adgroup_id", "adgroup_name", "reach","frequency","impressions","clicks","unique_clicks","ctr","unique_ctr","spend","cpm","cpp","cpc","total_actions","total_unique_actions","actions"),
        'export_columns' => array("date_start","date_stop","campaign_group_name","campaign_group_id","adgroup_name","adgroup_id","campaign_name","reach","frequency","impressions","clicks","unique_clicks","ctr","unique_ctr","spend","cpm","cpp","cpc","total_actions","total_unique_actions","actions:like")
    );

    /**
     * Class constructor
     * 
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->_email = !empty($options['email']) ? $options['email'] : null;
        $this->_password = !empty($options['password']) ? $options['password'] : null;
        $this->_ip = !empty($options['ip']) ? $options['ip'] : null;
        $this->_accessTokenLong = !empty($options['access_token']) ? $options['access_token'] : null;

        $this->_dateFrom = $this->getDateFrom();
        $this->_dateTo = $this->getDateTo();

        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/facebook');
        $this->_logger = new Zend_Log($writer);
    }

    /**
     * Add new Facebook account
     * 
     * @param int $userId
     * @return array|Zend_Db_Table_Row
     */
    public function addAccount($userId)
    {
        $result = $content = $this->login();
        if(is_array($result)) {
            return $result;
        }

        $fbAccounts = array();
        if(preg_match('|"all_accounts":(.*),"n_accounts"|msu', $content, $matches)) {
            $accounts = json_decode($matches[1]);
            foreach($accounts as $account) {
                $fbAccounts[] = array('name' => $account->name, 'id' => (int)$account->id);
            }
        } elseif(preg_match("/campaigns\/\?act\=([0-9]+)/msu", $content, $matches)) {
            $fbAccounts[] = array('name' => $this->_email, 'id' => (int)$matches[1]);
        }

        if(!empty($fbAccounts)) {
            $result = array();
            $adsModel = new Application_Model_DbTable_Ads();

            foreach($fbAccounts as $fbAccount) {
                if($adsModel->getAdsByAccountId($userId, $fbAccount['id'], Application_Model_DbTable_Ads::FACEBOOK)) {
                    $result['errorMessage'] = $this->_errors['account_connected'];
                } else {
                    $credentials = array('email' => $this->_email, 'password' => $this->_password, 'ip' => $this->_ip);

                    $options = array('account_id' => $fbAccount['id'], 'source' => Application_Model_DbTable_Ads::FACEBOOK,
                        'name' => $fbAccount['name'], 'credentials' => $credentials);
                    if(count($fbAccounts) > 1) {
                        $options['parent'] = $this->_email;
                    }

                    $ad = $adsModel->create($userId, $options);
                    $this->addCampaigns($adsModel->find($ad->id));
                    $result[] = $ad;
                }
            }
        } else {
            $result = array();
            $result['errorMessage'] = $this->_errors['account_id_missing'];
        }
        return $result;
    }

    /**
     * Add Facebook Campaigns from main page
     * 
     * @param array $ads
     * @return array|boolean
     */
    public function addCampaigns($adList)
    {
        $ads = clone $adList;
        $ad = $ads->current();

        if(empty($this->_accessTokenLong)) { //scraper
            //$campaigns = $this->_getList($ad->account_id);
            $campaigns = $this->_getListHtml($ad->account_id);
        } else {
            $options = array('access_token' => $this->_accessTokenLong, 'account_id' => $ad->account_id);
            $fbApi = new Application_Model_FacebookApi($options);
            $campaigns = $fbApi->getCampaigns('adcampaign_groups', array('fields' =>'name,daily_budget,,campaign_group_status'));
        }

        if(!$campaigns) {
            $result = array();
            $result['errorMessage'] = $this->_errors['campaigns_missing'];
            $this->_logger->err("{$this->_email} {$result['errorMessage']}");
            return $result;
        }

        foreach($ads as $ad) {
            $campaignsModel = new Application_Model_DbTable_Campaigns();
            $keywords = $campaignsModel->getKeywords($ad->user_id);

            $adCampaigns = new Application_Model_DbTable_AdCampaigns();
            $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

            foreach($campaigns as $campaign) {
                $campaign = (object)$campaign;
                $campaignApiId = $campaign->id;
                $status = !empty($campaign->campaign_group_status) ? $campaign->campaign_group_status : $campaign->campaign_ui_status_name;
                if(!empty($campaignIds[$campaignApiId])) { continue; }

                //TODO: update Existing Campaigns
                $data = array('api_id' => $campaignApiId, 'user_id' => $ad->user_id, 'ad_id' => $ad->id,
                    'name' => $campaign->name, 'daily_budget' => $campaign->daily_budget/100,
                    'status' => strtolower($status), 'source' => Application_Model_DbTable_Ads::FACEBOOK);

                //automatic Pairing
                if(false !== ($campaignId = App_Utils::findKeyword($campaign->name, $keywords))) {
                    $data['campaign_id'] = $campaignId;
                }

                $newCampaign = $adCampaigns->create($data);
                $campaignIds[$newCampaign->api_id] = $newCampaign->id;
            }
        }

        return true;
    }

    /**
     * Add Facebook Adverts
     * 
     * @param array $ads
     * @return array|boolean
     */
    public function addAdverts($adList)
    {
        $ads = clone $adList;
        $ad = $ads->current();

        //$adgroups = $this->_getList($ad->account_id, 'adgroups');
        $adgroups = $this->_getListHtml($ad->account_id, 'adgroups');
        if(!$adgroups) {
            $result = array();
            $result['errorMessage'] = $this->_errors['adgroups_missing'];
            $this->_logger->err("{$this->_email} {$result['errorMessage']}");
            return $result;
        }

        foreach($ads as $ad) {
            $adListModel = new Application_Model_DbTable_AdvertsList();
            $adListIds = $adListModel->getPairs($ad->id);

            $adCampaigns = new Application_Model_DbTable_AdCampaigns();
            $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

            foreach($adgroups as $adgroup) {
                $adgroup = (object)$adgroup;
                $adgroupApiId = $adgroup->id;
                if(!empty($adListIds[$adgroupApiId])) { continue; }

                $status = trim(str_replace('Campaign', '', $adgroup->adgroup_ui_status_name));
                $data = array('api_id' => $adgroupApiId, 'ad_id' => $ad->id, 
                    'ad_campaign_id' => $campaignIds[$adgroup->campaign_id], 
                    'header' => $adgroup->name, 'status' => strtolower($status),
                    'options' => serialize((array)$adgroup));

                $adListModel->create($data);
            }
        }
        return true;
    }

    /**
     * Find Campaign using pagination
     * 
     * @param int $accountId
     * @param int $page Fb page count starts from 0
     */
    protected function _getList($accountId, $type = 'campaigns', $page = 0)
    {
        $campaigns = array();

        $url = $this->_fbUrl . 'ads/ajax/campaigns_get_data.php';
        $sortKey = substr($type, 0, -1);
        $postData = array('account_id' => $accountId, 'act' => $accountId, 
            'page_index' => $page, 'fb_dtsg' => $this->_fb_dtsg, 
            'page_size' => self::RECORDS_ON_PAGE, 
            'start_date' => 0, 'end_date' => 0, 'show_status' => 201, 
            'sort_key' => "{$sortKey}_ui_status", '__user' => $this->_userId, 
            '__a' => 1, '__dyn'=>'7Awv98mBwwy8', '_req'=>1, 'ttstamp'=>time()
        );

        if($content = $this->_sendRequest($url, null, $postData)) {
            if($type == 'campaigns') {
                preg_match('/\"data\"\:(.*)\,\"n_rows"\:([0-9]+)/', $content, $matches);
                $totalCampaignsNumber = (int)$matches[2];
            } else {
                preg_match('/\"data\"\:(.*)\,\"totals\"(.*)\"n_rows"\:([0-9]+)/', $content, $matches);
                $totalCampaignsNumber = (int)$matches[3];
            }

            $campaigns = json_decode($matches[1]);
            if($totalCampaignsNumber > (($page+1) * self::RECORDS_ON_PAGE)) {
                $campaigns = array_merge($campaigns, $this->_getList($accountId, $type, $page+1));
            }
        }

        return is_array($campaigns) ? $campaigns : array();
    }

    /**
     * Find Campaigns by parsing HTML response
     * 
     * @param int $accountId
     * @param int $page Fb page count starts from 0
     */
    protected function _getListHtml($accountId, $type = 'campaigns', $page = 0)
    {
        $campaigns = array();

        $url = $this->_fbUrl . "ads/ajax/{$type}";
        $getParams = array('act' => $accountId, 'time_start' => $this->_dateFrom->format('Y-m-d'), 
            'time_end' => $this->_dateTo->format('Y-m-d'), 'is_custom' => true, 
            'status' => 201, 'sort_key' => '', 'sort_dir' => '', 'offset' => self::RECORDS_ON_PAGE * $page,
            '__user' => $this->_userId, '__a' => 1, '__dyn'=>'7Awv98mBwwy8', '_req'=>1, '__rev'=>1114696
        );

        if($content = $this->_sendRequest($url, $getParams)) {
            preg_match("/\"__html\"\:\"(.*)\"\}\]/msuUi", $content, $matches);
            $data = current(json_decode('[{'.$matches[0]));
            $content = $data->__html;

            $dom = new DOMDocument();
            libxml_use_internal_errors(true);
            $dom->loadHTML($content);
            libxml_use_internal_errors(false);
            $xpath = new DOMXpath($dom); 

            if(!$campaignsTable = $xpath->query("//table")->item(0)) {
                return;
            }

            $columnNames = array('name', 'status', 'clicks', 'cost', 'impressions', 'start_date', 'end_date', 'daily_budget', 'remaining', 'spend');
            $campaigns = array();
            foreach($xpath->query("//tbody/tr", $campaignsTable) as $tr) {
                if(!@$tr->getAttributeNode('data-objectid')->nodeValue) {
                    continue;
                }
                $campaign = array();
                foreach($xpath->query("td", $tr) as $key => $td) {
                    $value = null;
                    $tdClass = @$td->getAttributeNode('class')->nodeValue;
                    if(0 === $key && $tdClass != 'firstRow') {
                        break;
                    }

                    if(0 === $key) {
                        foreach($xpath->query("div/div/a", $td) as $a) {
                            if(!$name = $a->nodeValue) {
                                continue;
                            }
                            $href = $a->getAttributeNode('href')->nodeValue;
                            if(preg_match('/campaign_id\=([0-9]+)/msui', $href, $matches)) {
                                $campaign_id = $matches[1];
                                $campaign['id'] = $campaign_id;
                                $campaign[$columnNames[$key]] = $name;
                            } else {
                                break;
                            }
                        }
                    }
                    if(1 === $key) {
                        $value = $td->nodeValue;
                    } elseif(7 === $key) {
                        $value = $td->getAttributeNode('data-sort')->nodeValue;
                    }

                    if(!empty($value) && key_exists($key, $columnNames)) {
                        $campaign[$columnNames[$key]] = $value;
                    }
                }
                $campaigns[$campaign_id] = $campaign;
            }

            if(!empty($campaigns)) {
                $campaigns = array_merge($campaigns, $this->_getListHtml($accountId, $type, $page+1));
            }
        }

        return is_array($campaigns) ? $campaigns : array();
    }

    /**
     * Add Campaign
     * 
     * @param Zend_Db_Table_Row $ad
     * @param string $campaignApiId
     * @param string $campaignName
     * @param int $adCampaignId Advert Campaign Id. Pass to perform update
     * 
     * @return Zend_Db_Table_Row
     */
    public function addCampaign($ad, $campaignApiId, $campaignName, $adCampaignId = null)
    {
        if(!$campaignApiId || !$campaignName) {
            return;
        }

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $keywords = $campaignsModel->getKeywords($ad->user_id);

        $data = array('api_id' => $campaignApiId, 'user_id' => $ad->user_id, 'ad_id' => $ad->id,
            'name' => $campaignName, 'source' => Application_Model_DbTable_Ads::FACEBOOK);

         //automatic Pairing
        if(false !== ($campaignId = App_Utils::findKeyword($campaignName, $keywords))) {
            $data['campaign_id'] = $campaignId;
        }

        $adCampaigns = new Application_Model_DbTable_AdCampaigns();
        return $adCampaigns->create($data, $adCampaignId);
    }

    /**
     * Add Ad Set
     * 
     * @param Zend_Db_Table_Row $ad
     * @param string $adGroupApiId
     * @param string $adGroupName
     * @param int $adGroupId Advert Group Id. Pass to perform update
     * 
     * @return Zend_Db_Table_Row
     */
    public function addAdGroup($ad, $options = array())
    {
        $options['ad_id'] = $ad->id;
        $options['user_id'] = $ad->user_id;

        $adGroupModel = new Application_Model_DbTable_AdGroups();
        return $adGroupModel->create($options);
    }

    /**
     * Get and Save Campaign Statistics
     * 
     * @param Zend_Db_Table_Row|array $ads
     * @param bool $updateCampaigns
     * @return array
     */
    public function getCampaignsStatistics($adsList, $updateCampaigns = false)
    {
        $adsModel = new Application_Model_DbTable_Ads();

        $adsList = !is_array($adsList) ? array($adsList) : $adsList;
        foreach($adsList as $row) {
            //find all ads with the same account_id
            $select = $adsModel->select()->where('account_id = ?', $row->account_id)
                ->where('source = ?', Application_Model_DbTable_Ads::FACEBOOK)->order('last_update');
            $ads = $adsModel->fetchAll($select);
            $ad = $adsModel->fetchRow($select); //The Ad with least last_updated value

            if($ad->last_statistic_date) {
                $lastUpdate = new DateTime($ad->last_statistic_date);
                $this->_dateFrom = $lastUpdate->sub(new DateInterval('P1D'));
            }

            if(empty($this->_accessTokenLong)) { //scraper
                $this->login();
                $this->_accessToken = $this->_getAccessToken($ad->account_id);
            }
            /*if($updateCampaigns) {
                $this->login();
                //$this->addCampaigns($ads);
                //$this->addAdverts($ads);
            }*/

            $statistics = $this->_getStatistics($ad->account_id);
            $statistics = array_merge($statistics, $this->_getStatistics($ad->account_id, 'adgroups'));

            $adCampaigns = new Application_Model_DbTable_AdCampaigns();
            $campaignStat = new Application_Model_DbTable_AdCampaignStatistics();

            //update statistics for all FB accounts with the same account_id
            foreach ($ads as $ad) {
                if($statistics) {
                    $adListModel = new Application_Model_DbTable_AdvertsList();
                    $adListIds = $adListModel->getPairs($ad->id);

                    $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);
                    $campaignStat->getAdapter()->beginTransaction();

                    $adGroupsModel = new Application_Model_DbTable_AdGroups();
                    $adGroupsList = $adGroupsModel->getPairs($ad->id);

                    //delete crossing stat:
                    $campaignStat->deleteStatByDate($ad->id, $this->_dateFrom->format("Y-m-d"));

                    foreach($statistics as $stat) {
                        $campaignApiId = $stat->campaign_group_id;
                        if(empty($campaignIds[$campaignApiId])) {
                            //add new Campaign
                            $newCampaign = $this->addCampaign($ad, $campaignApiId, $stat->campaign_group_name);
                            $campaignIds[$newCampaign->api_id] = $newCampaign->id;
                        }

                        $newStat = $campaignStat->createRow();
                        $newStat->ad_campaign_id = $campaignIds[$campaignApiId];

                        if(!empty($stat->adgroup_id)) {
                            if(empty($adGroupsList[$stat->campaign_id])) {
                                //add New Ad Group:
                                $groupOptions = array('ad_campaign_id' => $campaignIds[$campaignApiId],
                                    'api_id' => $stat->campaign_id, 'name' => $stat->campaign_name);
                                $newAdGroup = $this->addAdGroup($ad, $groupOptions);
                                $adGroupsList[$newAdGroup->api_id] = $newAdGroup->id;
                            }

                            //@TODO: add condition to add Adverts only
                            $advertId = $adListIds[$stat->adgroup_id];
                            //add/update Advert
                            $advertOptions = array('ad_id' => $ad->id, 'ad_campaign_id' => $campaignIds[$campaignApiId],
                                'ad_group_id' => $adGroupsList[$stat->campaign_id],
                                'api_id' => $stat->adgroup_id, 'header' => $stat->adgroup_name);
                            $newAdvert = $adListModel->create($advertOptions, $advertId);
                            $adListIds[$newAdvert->api_id] = $newAdvert->id;

                            $newStat->advert_id = $adListIds[$stat->adgroup_id];
                        }

                        $newStat->user_id = $ad->user_id;
                        $newStat->ad_id = $ad->id;
                        $newStat->date = $lastDate = strftime("%Y-%m-%d", strtotime($stat->date_start));
                        $newStat->impressions = (int)$stat->impressions;
                        $newStat->clicks = (int)$stat->clicks;
                        $newStat->ctr = (float)$stat->ctr;
                        $newStat->spend = (float)$stat->spend;
                        $newStat->average_cpc = (float)$stat->cpc;
                        $newStat->average_cpm = (float)$stat->cpm;
                        $newStat->added = new Zend_Db_Expr('NOW()');
                        $newStat->save();
                    }
                    $campaignStat->getAdapter()->commit();
                }

                $ad->last_statistic_date = new Zend_Db_Expr('CURDATE()');
                $ad->last_update = new Zend_Db_Expr('NOW()');
                $ad->save();
            }
            $result['success'] = true;
        }

        return $result;
    }

    /**
     * Find All Statistics
     * 
     * @param int $accountId
     * @param int $offset Pagination param
     */
    protected function _getStatistics($accountId, $type = 'campaigns', $offset = 0)
    {
        $statistics = array();
        $reportName = "_{$type}_report";
        $dataColumns = $this->{$reportName}['data_columns'];
        $exportColumns = $this->{$reportName}['export_columns'];

        $reportUrl = "https://graph.facebook.com/act_{$accountId}/reportstats?access_token={$this->_accessToken}";
        $getParams = array('accountId' => $accountId, 'actions_group_by' => json_encode(array("action_type")), 'callback' => '__globalCallbacks.f239a7035c',
            'data_columns' => json_encode($dataColumns),
            'endpoint' => "/act_{$accountId}/reportstats",
            'export_columns' => json_encode($exportColumns),
            'filters' => json_encode(array(array('field' => "action_type", "type" => "in", "value" => array("like")))), 
            'method' => 'get', 'name' => 'General Metrics over last 7 days', 'pretty' => 0, 'sort_by'=>'date_start','sort_dir'=>'desc', 'time_increment'=>1,
            'time_interval' => json_encode(array(
                'day_start' => array("day" => $this->_dateFrom->format('d'),"month" => $this->_dateFrom->format('m'),"year" => $this->_dateFrom->format('Y')), 
                'day_stop' => array("day" => $this->_dateTo->format('d'), "month"=> $this->_dateTo->format('m'),"year" => $this->_dateTo->format('Y'))
            )),
            'offset' => $offset
        );

        if(!empty($this->_accessTokenLong)) { //FB API
            unset($getParams['callback']);
            $options = array('access_token' => $this->_accessTokenLong, 'account_id' => $accountId);
            $fbApi = new Application_Model_FacebookApi($options);
            $response = $fbApi->getStatistics($getParams);
            $statistics = $response->data;
            $paging = $response->paging;
        } else {
            $content = $this->_sendRequest($reportUrl, $getParams);
            preg_match('/\"data\"\:(.*)\,\"limit\"(.*)\"paging\"\:(\{[^\}]+\})/msu', $content, $matches);
            $statistics = json_decode($matches[1]);
            $paging = json_decode($matches[3]);
        }

        if($paging && !empty($paging->next)) {
            $statistics = array_merge($statistics, 
                $this->_getStatistics($accountId, $type, $offset+self::RECORDS_ON_PAGE)
            );
        }

        $statistics = !$statistics ? array() : $statistics;
        return $statistics;
    }

    protected function getReport($accountId, $type = 'campaigns')
    {
        $statistics = array();
        $reportName = "_{$type}_report";
        $dataColumns = $this->{$reportName}['data_columns'];
        $exportColumns = $this->{$reportName}['export_columns'];

        $reportUrl = "https://graph.facebook.com/act_{$accountId}/reportstats?access_token={$this->_accessToken}";
        $getParams = array('accountId' => $accountId, 'actions_group_by' => json_encode(array("action_type")),
            'data_columns' => json_encode($dataColumns),
            'export_columns' => json_encode($exportColumns),
            'filters' => json_encode(array(array('field' => "action_type", "type" => "in", "value" => array("like")))), 
            'sort_by'=>'date_start', 'sort_dir'=>'desc', 'time_increment'=>1,
            'time_interval' => json_encode(array(
                'day_start' => array("day" => $this->_dateFrom->format('d'),"month" => $this->_dateFrom->format('m'),"year" => $this->_dateFrom->format('Y')), 
                'day_stop' => array("day" => $this->_dateTo->format('d'), "month"=> $this->_dateTo->format('m'),"year" => $this->_dateTo->format('Y'))
            )),
            'format' => 'csv'
        );
        $content = $this->_sendRequest($reportUrl, $getParams);
    }

    /**
     * Find access token
     * 
     * @param int $accountId Facebook Account Id
     **/
    protected function _getAccessToken($accountId)
    {
        $content = $this->_sendRequest("{$this->_fbUrl}ads/manage/reporting.php?act={$accountId}");
        if(preg_match('/access_token\"\:\"([^\"]+)\"\}/msu', $content, $matches)) {
            return $matches[1];
        }
    }

    /**
     * Get DateFrom
     * Set report starting date
     * 
     * @return DateTime
     **/
    public function getDateFrom()
    {
        if(!$this->_dateFrom) {
            $today = new DateTime();
            $this->_dateFrom = $today->sub(new DateInterval('P25D'));
        }
        return $this->_dateFrom;
    }

    /**
     * Get DateTo
     * Set report end date
     * 
     * @return DateTime
     **/
    public function getDateTo()
    {
        if(!$this->_dateTo) {
            $today = new DateTime();
            $this->_dateTo = $today->add(new DateInterval('P1D'));
        }
        return $this->_dateTo;
    }

    /**
     * Facebook login
     * 
     * @return array|string
     */
    public function login()
    {
        $this->_cookieJar = new Zend_Http_CookieJar();
        $response = $result = $this->_sendRequest($this->_fbUrl);

        $postParams = array();
        if (!is_array($response)){
            $result = array();

            $dom = new DOMDocument();
            libxml_use_internal_errors(true);
            $dom->loadHTML($response);
            libxml_use_internal_errors(false);
            $xpath = new DOMXpath($dom); 

            if($lgnrndElement = $xpath->query("//input[@name='lgnrnd']")->item(0)) {
                $postParams['lgnrnd'] = $lgnrndElement->getAttributeNode('value')->nodeValue;
            }

            if($lgnjsElement = $xpath->query("//input[@name='lgnjs']")->item(0)) {
                $postParams['lgnjs'] = $lgnjsElement->getAttributeNode('value')->nodeValue;
            }

            if($lsdElement = $xpath->query("//input[@name='lsd']")->item(0)) {
                $postParams['lsd'] = $lsdElement->getAttributeNode('value')->nodeValue;
            }
            $postParams['locale'] = $this->_locale;
            $postParams['email'] = $this->_email;
            $postParams['pass'] = $this->_password;
            $postParams['persistent'] = 1;
            $postParams['default_persistent'] = 1;

            if($postParams) {
                //Send Login Form:
                $url = 'https://login.facebook.com/login.php?login_attempt=1';

                $content = $this->_sendRequest($url, null, $postParams);
                if(is_array($content)) {
                    return $content;
                } elseif(false !== stripos($content, 'temporarily locked')) {
                    $content = array();
                    $content['errorMessage'] = $this->_errors['account_locked'];
                    return $content;
                }

                //for Personal Accounts we need to open campaigns page:
                if(preg_match("/sk\=welcome/", $content, $matches)) {
                    if(preg_match("/campaign_id\=([0-9]+)/msu", $content, $matches)) {
                        $campaignId = (int)$matches[1];
                        $campaignsPage = $this->_fbUrl . 'ads/manage/campaigns/?campaign_id=' . $campaignId;
                        $content = $this->_sendRequest($campaignsPage);
                        if(is_array($content)) {
                            return $content;
                        }
                    }
                }

                if(false !== strpos($content, 'campaigns/?act=')) {
                    if(preg_match('/\"id\"\:\"([0-9]+)\"(.*)\"token\"\:\"([^\"]+)\"/msuU', $content, $matches)) {
                        $this->_userId = $matches[1];
                        $this->_fb_dtsg = $matches[3];
                    }
                    return $content;
                }
            }
            $result['errorMessage'] = $this->_errors['login_error'];
            $this->_logger->err("{$this->_email} {$result['errorMessage']}");
        }

        return $result;
    }

    /**
     * Send CURL request
     * 
     * @param string $url
     * @param array $getParams
     * @param array $postParams
     * 
     * @return array|string
     */
    protected function _sendRequest($url, $getParams = array(), $postParams = array())
    {
        $result = array();

        $config = array(
            'timeout' => 30,
            'useragent' => "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)",
            'curloptions' => array(CURLOPT_RETURNTRANSFER => 1, CURLOPT_SSL_VERIFYPEER => false)
        );

        if($this->_ip) {
            //set Request IP
            $config['curloptions'][CURLOPT_INTERFACE] = $this->_ip;
        }

        $adapter = new Zend_Http_Client_Adapter_Curl();
        $client = new Zend_Http_Client($url, $config);

        $client->setAdapter($adapter);
        $client->setCookieJar($this->_cookieJar);

        if($postParams) {
            $client->setParameterPost($postParams);
            $client->setMethod(Zend_Http_Client::POST);
        } else {
            $client->setParameterGet($getParams);
            $client->setMethod(Zend_Http_Client::GET);
        }

        try{
            $response = $client->request();
            $headers = $response->getHeaders();

            $this->_logger->info("{$this->_email}. URL {$url}:" . $response->getStatus() . ' ' . $response->getMessage() .  ' ' . serialize($postParams) . serialize($getParams));

            if ($response->isSuccessful()){
                return $response->getBody();
            } elseif($response->isRedirect() && $headers['Location']) {
                $this->_sendRequest($headers['Location']);
            } else {
                $result['errorMessage'] = $response->getStatus() . ' ' . $response->getMessage();
            }
        } catch (Zend_Http_Client_Exception $e) {
            $result['errorMessage'] = $e->getMessage();
        } catch (Exception $e) {
            $result['errorMessage'] = "Exception: " . $e->getMessage();
        }

        $this->_logger->err("{$this->_email}. URL {$url}: {$result['errorMessage']}" . serialize($result));
        return $result;
    }
}