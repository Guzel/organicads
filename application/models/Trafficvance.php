<?php

class Application_Model_Trafficvance
{
    protected $_apiUrl = 'https://api.trafficvance.com/';
    protected $_apiSandbox = 'https://apitest.trafficvance.com/';
    protected $_version = 'v3';

    protected $_apiKey;
    protected $_apiKeySandbox = '8e002cadaf9d7225a19f0f710650757d35756080';
    protected $_service;

    protected $_sandbox = false;
    protected $_logger;

    const CAMPAIGNS_SERVICE = 'campaigns';
    const CONVERSIONS_SERVICE = 'conversions';
    const CREATIVES_SERVICE = 'creatives';
    const REPORTS_SERVICE = 'reports';
    const SYSTEM_SERVICE = 'system';
    const TARGETS_SERVICE = 'targets';

    const CREATIVE_DISPLAY = 'DISPLAY';
    const CREATIVE_TEXTLINK = 'TEXTLINK';

    protected $_errors = array(
        'login_error' => 'Login failed. Please, check your credentials',
        'account_id_missing' => 'Account ID was not found',
        'server_unavailable' => 'Yahoo Server not responding',
        'account_connected' => 'This Account is already connected to your Leadssistant account',
        'campaigns_missing' => 'Campaigns was not found!'
    );

    public function __construct($options = array())
    {
        $this->_apiKey = !empty($options['api_key']) ? $options['api_key'] : null;

        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/trafficvance');
        $this->_logger = new Zend_Log($writer);
    }

    public function addAccount($userId)
    {
        $this->_service = self::SYSTEM_SERVICE;
        $response = $result = $this->_soapRequest('getUsername');

        if(!empty($response->username)) {
            $result = array();
            $adsModel = new Application_Model_DbTable_Ads();
            if($adsModel->getAdsByName($userId, $response->username, Application_Model_DbTable_Ads::TRAFFICVANCE)) {
                $result['errorMessage'] = $this->_errors['account_connected'];
            } else {
                if(!($campaignIds = $this->getCampaignIds())) {
                    $result['errorMessage'] = $this->_errors['campaigns_missing'];
                } else {
                    $credentials = array('api_key' => $this->_apiKey);

                    $options = array('source' => Application_Model_DbTable_Ads::TRAFFICVANCE,
                        'name' => $response->username, 'credentials' => $credentials);
                    return $adsModel->create($userId, $options);
                }
            }
        }
        return $result;
    }

    public function getCampaignIds()
    {
        $this->_service = self::CAMPAIGNS_SERVICE; 
        $response = $this->_soapRequest('getCampaignIds', array('includeDeleted' => false));
        if(empty($response->ids->item)) {
            return false;
        }
        return $response->ids->item;
    }

    /**
     * Add Campaigns
     * 
     * @param array $ad
     * @return array|boolean
     */
    public function saveCampaigns($ad)
    {
        if(!($campaignIds = $this->getCampaignIds())) {
            return false;
        }

        $this->_service = self::CAMPAIGNS_SERVICE;
        $response = $this->_soapRequest('getCampaigns', array('campaignIds' => $campaignIds));
        if(empty($response->campaigns->item)) {
            return false;
        }

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $keywords = $campaignsModel->getKeywords($ad->user_id);

        $adCampaigns = new Application_Model_DbTable_AdCampaigns();
        $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

        $campaigns = $response->campaigns->item;
        foreach($campaigns as $campaign) {
            $campaignApiId = $campaign->id;
            $id = !empty($campaignIds[$campaignApiId]) ? $campaignIds[$campaignApiId] : null;
            //Skip campaign update:
            //if($id) { continue; }

            $data = array('api_id' => $campaignApiId, 'user_id' => $ad->user_id, 'ad_id' => $ad->id,
                'name' => $campaign->name, 'status' => strtolower($campaign->status), 
                'source' => Application_Model_DbTable_Ads::TRAFFICVANCE);

            //automatic Pairing
            if(false !== ($campaignId = App_Utils::findKeyword($campaign->name, $keywords))) {
                $data['campaign_id'] = $campaignId;
            }

            $newCampaign = $adCampaigns->create($data, $id);
            $campaignIds[$newCampaign->api_id] = $newCampaign->id;
        }
        return true;
    }

    /**
     * Get and Save Campaigns and Statistics
     * 
     * @param Zend_Db_Table_Row $ad
     * @param bool $updateCampaigns
     * @return array
     */
    public function getCampaignsStatistics($ad, $updateCampaigns = true)
    {
        //add/update campaigns
        $this->saveCampaigns($ad);
        $this->addAdverts($ad);

        $this->_dateFrom = $this->getDateFrom();
        $this->_dateTo = $this->getDateTo();

        if($ad->last_statistic_date) {
            $lastUpdate = new DateTime($ad->last_statistic_date);
            $this->_dateFrom = $lastUpdate->sub(new DateInterval('P1D'));
        }

        $statistics = array();
        $startDate = clone $this->_dateFrom;
        while($startDate <= $this->_dateTo) {
            $statistics = array_merge($statistics, $this->_getStat($startDate));
            $startDate->add(new DateInterval('P1D'));
        }

        if($statistics) {
            $adCampaigns = new Application_Model_DbTable_AdCampaigns();
            $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

            $adListModel = new Application_Model_DbTable_AdvertsList();
            $adListIds = $adListModel->getPairs($ad->id);

            $campaignStat = new Application_Model_DbTable_AdCampaignStatistics();
            $campaignStat->getAdapter()->beginTransaction();

            //delete crossing stat:
            $campaignStat->deleteStatByDate($ad->id, $this->_dateFrom->format("Y-m-d"));

            foreach($statistics as $date => $row) {
                foreach($row as $stat) {
                    $statRows = $stat['rows'] ? : 1;
                    $newStat = $campaignStat->createRow();

                    if(!empty($stat['creativeId'])){
                        if(empty($adListIds[$stat['creativeId']])) {
                            continue;
                        }
                        $newStat->advert_id = $adListIds[$stat['creativeId']];
                    } else {
                        $newStat->ad_campaign_id = $campaignIds[$stat['campaignId']];
                    }

                    $newStat->user_id = $ad->user_id;
                    $newStat->ad_id = $ad->id;
                    $newStat->date = strftime("%Y-%m-%d", strtotime($date));
                    $newStat->impressions = (int)$stat['impressions'];
                    $newStat->clicks = (int)$stat['clicks'];
                    $newStat->ctr = round($stat['ctr'] / $statRows, 2);
                    $newStat->spend = (float)$stat['cost'];
                    $newStat->average_cpm = (float)round($stat['cpm'] / $statRows, 2);
                    $newStat->added = new Zend_Db_Expr('NOW()');
                    $newStat->save();
                }
            }
            $campaignStat->getAdapter()->commit();
        }

        $ad->last_statistic_date = new Zend_Db_Expr('CURDATE()');
        $ad->last_update = new Zend_Db_Expr('NOW()');
        $ad->save();
        $result['success'] = true;
        return $result;
    }

    protected function _getStat($date)
    {
        usleep(3000000);
        $this->_service = self::REPORTS_SERVICE;
        $response = $this->_soapRequest('getDailyTargetsStats', array('date' => $date->format('Y-m-d')));

        $statistics = $result = array();
        if(!empty($response->stats->item)) {
            foreach($response->stats->item as $stat) {
                if(empty($statistics[$stat->campaignId])) {
                    $statistics[$stat->campaignId] = (array)$stat;
                    unset($statistics[$stat->campaignId]['creativeId']);
                    $statistics[$stat->campaignId]['rows'] = 0;
                } else {
                    $statistics[$stat->campaignId]['clicks'] += $stat->clicks;
                    $statistics[$stat->campaignId]['impressions'] += $stat->impressions;
                    $statistics[$stat->campaignId]['ctr'] += $stat->ctr;
                    $statistics[$stat->campaignId]['cost'] += $stat->cost;
                    $statistics[$stat->campaignId]['cpm'] += $stat->cpm;
                }
                $statistics[$stat->campaignId]['rows'] += 1;

                //Group Stat by CreativeId
                if(empty($statistics[$stat->creativeId])) {
                    $statistics[$stat->creativeId] = (array)$stat;
                    unset($statistics[$stat->creativeId]['campaignId']);
                    $statistics[$stat->creativeId]['rows'] = 0;
                } else {
                    $statistics[$stat->creativeId]['clicks'] += $stat->clicks;
                    $statistics[$stat->creativeId]['impressions'] += $stat->impressions;
                    $statistics[$stat->creativeId]['ctr'] += $stat->ctr;
                    $statistics[$stat->creativeId]['cost'] += $stat->cost;
                    $statistics[$stat->creativeId]['cpm'] += $stat->cpm;
                }
            }
            $result[$date->format('Y-m-d')] = $statistics;
        }
        return $result;
    }

    public function addAdverts($ad, $campaignIds = array())
    {
        if(!$campaignIds) {
            $adCampaigns = new Application_Model_DbTable_AdCampaigns();
            $campaigns = $adCampaigns->getPairs($ad->user_id, $ad->id);
            $campaignIds = array_keys($campaigns);
        }
        $creatives = $this->getCreativesByCampaign($campaignIds);

        if(empty($creatives->creatives)) {
            return;
        }

        $adListModel = new Application_Model_DbTable_AdvertsList();
        $adListIds = $adListModel->getPairs($ad->id);

        $adCampaigns = new Application_Model_DbTable_AdCampaigns();
        $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

        foreach($creatives->creatives->item as $creative) {
            $apiCreativeId = (int)$creative->id;
            if(!empty($adListIds[$apiCreativeId])) { continue; }

            $data = array('api_id' => $apiCreativeId, 'ad_id' => $ad->id,
                'ad_campaign_id' => $campaignIds[$creative->campaignId],
                'header' => $creative->name, 'url' => $creative->url,
                'status' => strtolower($creative->status),
                'options' => serialize((array)$creative));

            $adListModel->create($data);
        }
    }

    public function getCreativesByCampaign($campaignIds)
    {
        $creativeIds = $this->getCreativeIds($campaignIds);

        if(empty($creativeIds->ids)) {
            return;
        }

        $ids = array();
        foreach($creativeIds->ids->item as $id) {
            $ids[] = (int)$id;
        }
        return $this->getCreative($ids);
    }

    public function getCreativeIds($campaignIds, $includeDeleted = false)
    {
        $campaignIds = (array)$campaignIds;

        $this->_service = self::CREATIVES_SERVICE;
        $response = $this->_soapRequest('getCreativeIds', array('campaignIds'=> $campaignIds,
            'filterNames' => Null, 'includeDeleted' => (bool)$includeDeleted));

        return $response;
    }

    public function getCreative($creativeId)
    {
        $creativeIds = (array)$creativeId;

        $this->_service = self::CREATIVES_SERVICE;
        $response = $this->_soapRequest('getCreatives', array('creativeIds'=> $creativeIds, 
            'fileData' => Null, 'fetchStats' => Null));

        return $response;
    }

//226292 -> campaign ID
    public function createCampaign($data)
    {
        $budgetFields = array('dailyBudgetSunday', 
            'dailyBudgetMonday', 'dailyBudgetTuesday', 'dailyBudgetWednesday', 
            'dailyBudgetThursday', 'dailyBudgetFriday', 'dailyBudgetSaturday');

        $newCampaign = array();
        foreach($budgetFields as $budget) {
            $newCampaign[$budget] = !empty($data[$budget]) ? $data[$budget] : 0;
        }
        $newCampaign['name'] = mb_substr($data['name'], 0, 250);
        $newCampaign['description'] = !empty($data['description']) ? mb_substr($data['description'], 0, 250) : '';

        $this->_service = self::CAMPAIGNS_SERVICE;
        $response = $this->_soapRequest('addCampaign', array('campaign' => $newCampaign));
        var_dump($response); die();
    }

    //4212963
    public function addCreative($campaignId, $data, $paused = false)
    {
        $newCreative = array();
        $newCreative['name'] = $data['name'];
        //@TODO
        $newCreative['type'] = self::CREATIVE_DISPLAY;
        $newCreative['categoryId'] = !empty($data['categoryId']) ? (int)$data['categoryId'] : 0;
        $newCreative['regionCode'] = !empty($data['country']) ? $data['country'] : '';
        $newCreative['url'] = $data['url'];
        $newCreative['matching'] = !empty($newCreative['matching']) ? (int)$newCreative['matching'] : 0;
        $newCreative['totalBudget'] = !empty($data['totalBudget']) ? $data['totalBudget'] : 0;
        $newCreative['adUnit'] = Null;
        //@TODO:
        $newCreative['frequencyCap'] = 24;
        $newCreative['dailyVisitors'] = 1000;

        $newScheduling = array('startDate' => date('Y-m-d'), 'endDate' => null, 'endDateUnlimited' => true,
            'sunday' => null, 'monday' => null, 'tuesday' => null, 'wednesday' => null, 
            'thursday' => array('dailyBudget'=> 10, 'workHours' => array(10, 11,12,13,14)), 
            'friday' => null, 'saturday' => null);
        $newCreative['scheduling'] = $newScheduling;

        $this->_service = self::CREATIVES_SERVICE;
        $response = $this->_soapRequest('addCreative', array('campaignId'=> (int)$campaignId, 
            'creative' => $newCreative, 'addAsPaused' => (bool)$paused));

        var_dump($response);
    }

    /**
     * Add new Target
     * @param int $creativeId
     * @param array $targetList
     * @param float $maxBid
     */
    //'yahoo.com': 'targetId' => 41444937; 'google.com': 'targetId => 41444936
    public function addTarget($creativeId, $targetList, $maxBid)
    {
        $targets = array();
        foreach($targetList as $target) {
            $targets[] = array('target' => $target, 'maxBid' => (float)$maxBid);
        }

        $this->_service = self::TARGETS_SERVICE;
        return $response = $this->_soapRequest('addTargets', array('creativeId' => (int)$creativeId, 'targets' => $targets));
    }

    /**
     * Get TargetIds by given options: campaignIds or creativeIds
     * 
     * @param type $options
     * @return type
     */
    public function getTargetIds($options = array())
    {
        $params = array();
        $params['campaignIds'] = !empty($options['campaignId']) ? (array)$options['campaignId'] : null;
        $params['creativeIds'] = !empty($options['creativeId']) ? (array)$options['creativeId'] : null;

        $this->_service = self::TARGETS_SERVICE;
        $response = $this->_soapRequest('getTargetIds', $params);
        return $response;
    }

    /**
     * Get Target information
     * 
     * @param array $targetList array('creativeId', 'targetId')
     */
    public function getTargets($targetList)
    {
        $this->_service = self::TARGETS_SERVICE;
        $response = $this->_soapRequest('getTargets', array('targetIds' => $targetList));
        return $response;
    }

    /**
     * Get DateFrom
     * Set report starting date
     * 
     * @return DateTime
     **/
    public function getDateFrom()
    {
        if(!$this->_dateFrom) {
            $today = new DateTime();
            $this->_dateFrom = $today->sub(new DateInterval('P10D'));
        }
        return $this->_dateFrom;
    }

    /**
     * Get DateTo
     * Set report end date
     * 
     * @return DateTime
     **/
    public function getDateTo()
    {
        if(!$this->_dateTo) {
            $this->_dateTo = new DateTime();
        }
        return $this->_dateTo;
    }

    protected function _soapRequest($method, $params = array())
    {
        $apiKey = $this->_sandbox ? $this->_apiKeySandbox : $this->_apiKey;
        $url = $this->_sandbox ? $this->_apiSandbox : $this->_apiUrl;
        $url .= "?{$this->_version}={$this->_service}.wsdl";

        $client = new SoapClient(
                  $url,
                  array('soap_version'    => '1.2',
                        'connect_timeout' => 10,
                        'compression'     => (SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP),
                        'cache_wdsl'      => WSDL_CACHE_NONE));
 
        $header = new SoapHeader(
                          $url,
                          'AuthenticateRequest',
                          new SoapVar(array('apiKey' => $apiKey), SOAP_ENC_OBJECT),
                          true);

        $client->__setSoapHeaders($header);
        $parameters = array('parameters' => $params);

        try {
            return $client->__soapCall($method, $parameters);
        } catch (Exception $e) {
            $result['errorMessage'] = $e->getMessage();
        }
        $this->_logger->err("{$this->_apiKey}. Method {$method}: {$result['errorMessage']}");
        return $result;
    }
}