<?php

class Application_Model_FiftyOnRed
{
    protected $_apiUrl = 'http://platform-api.50onred.com';
    protected $_apiKey;
    protected $_secretKey;
    protected $_apiMethod;
    protected $_apiType = 'GET';
    protected $_timestamp;
    protected $_typeSearch = 'search';
    protected $_typeIndext = 'intext';
    protected $_typePops = 'pops';

    protected $_dateFrom;
    protected $_dateTo;

    protected $_accountName;
    protected $_logger;

    protected $_errors = array(
        '500' => 'An unexpected error has occurred. Please try again later.',
        '400' => 'Server has returned error. Please try again later.',
        '401' => 'API Credentials are invalid.'
    );
    
    protected $_apiSettings = array(
        'search' => array('campaigns' => '/search-api/v1/campaigns', 'stat' => '/search-api/v1/stats/campaigns'),
        'pops' => array('campaigns' => '/pops-api/v1/campaigns', 'stat' => '/pops-api/v1/stats/campaigns'),
        'intext' => array('campaigns' => '/intext-api/v2/campaigns', 'stat' => '/intext-api/v2/campaign/stats/summary/'),
    );

    /**
     * Class constructor
     * 
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->_apiKey = !empty($options['api_key']) ? $options['api_key'] : null;
        $this->_secretKey = !empty($options['secret_key']) ? $options['secret_key'] : null;
        $this->_accountName = !empty($options['account_name']) ? $options['account_name'] : null;

        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/50onred');
        $this->_logger = new Zend_Log($writer);
    }

    /**
     * Add new 50onRed accounts
     * 
     * @param int $userId
     * @return array
     */
    public function addAccounts($userId)
    {
        $result = array();

        $adsModel = new Application_Model_DbTable_Ads();
        if($adsModel->getAdsByParent($userId, $this->_accountName)) {
            $result['errorMessage'] = "Account with nickname '{$this->_accountName}' already exists";
        } else {
            foreach($this->_apiSettings as $apiType => $settings) {
                $this->_apiMethod = $settings['campaigns'];
                $result[] = $this->addAccount($userId, $apiType);
            }
        }

        return $result;
    }

    /**
     * Add new 50onRed account
     * 
     * @param int $userId
     * @param string $type
     * @return Zend_Db_Table_Row
     */
    public function addAccount($userId, $type)
    {
        $campaigns = $this->sendRequest();

        if(isset($campaigns['errorMessage'])) {
            return $campaigns;
        }

        $adsModel = new Application_Model_DbTable_Ads();
        $credentials = array('api_key' => $this->_apiKey, 'secret_key' => $this->_secretKey, 'api_type' => $type);

        $options = array('source' => Application_Model_DbTable_Ads::FIFTYONRED,
            'name' => $type, 'parent'=> $this->_accountName, 'credentials' => $credentials);
        $ad = $adsModel->create($userId, $options);

        $this->addCampaigns($ad, $campaigns);
        return $ad;
    }

    /**
     * Add Campaigns for given ad account
     * 
     * @param Zend_Db_table_Row $ad
     * @param string $campaigns
     * @return boolean
     */
    public function addCampaigns($ad, $campaigns = null)
    {
        if(!$campaigns) {
            $this->_apiMethod = $this->_apiSettings[$ad->name]['campaigns'];
            $campaigns = $this->sendRequest();
        }

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $keywords = $campaignsModel->getKeywords($ad->user_id);

        $adCampaigns = new Application_Model_DbTable_AdCampaigns();
        $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

        foreach($campaigns as $campaign) {
            $campaignApiId = (int)$campaign->id;
            if(!empty($campaignIds[$campaignApiId])) { continue; }

            $data = array('api_id' => $campaignApiId, 'user_id' => $ad->user_id, 'ad_id' => $ad->id,
                'name' => $campaign->name, 'daily_budget' => $campaign->daily_budget,
                'status' => strtolower($campaign->status), 'source' => Application_Model_DbTable_Ads::FIFTYONRED);

             //automatic Pairing
            if(false !== ($campaignId = App_Utils::findKeyword($campaign->name, $keywords))) {
                $data['campaign_id'] = $campaignId;
            }

            $newCampaign = $adCampaigns->create($data);
            $campaignIds[$newCampaign->api_id] = $newCampaign->id;
        }

        return true;
    }

    /**
     * Get and Save Campaign Statistics
     * 
     * @param array|int $ads
     * @param bool $updateCampaigns
     * @return array
     */
    public function getCampaignsStatistics($ads, $updateCampaigns = false)
    {
        $ads = is_array($ads) ? $ads : array($ads);

        foreach($ads as $ad) {
            if($updateCampaigns) {
                $this->addCampaigns($ad);
            }

            $this->_dateFrom = $this->getDateFrom();
            $this->_dateTo = $this->getDateTo();

            if($ad->last_statistic_date) {
                $lastUpdate = new DateTime($ad->last_statistic_date);
                $this->_dateFrom = $lastUpdate->sub(new DateInterval('P1D'));
            }

            $statistics = array();
            $this->_apiMethod = $this->_apiSettings[$ad->name]['stat'];
            if($ad->name == $this->_typeIndext) {
                $startDate = clone $this->_dateFrom;
                while($startDate <= $this->_dateTo) {
                    $this->_apiMethod = $this->_apiSettings[$ad->name]['stat'] . $startDate->format('Y-m-d').'/UTC';
                    $statistics = array_merge($statistics, $this->sendRequest());
                    $startDate->add(new DateInterval('P1D'));
                }
            } else {
                 $statistics = $this->sendRequest();
            }

            if($statistics) {
                /* Get Adverts List along with AdGroups. 
                if($ad->name == $this->_typeSearch) {
                    //Get Ad List
                    $adLevelStat = $this->addAdList($ad);
                    $statistics = array_merge($statistics, $adLevelStat);
                }*/

                $adCampaigns = new Application_Model_DbTable_AdCampaigns();
                $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

                $adListModel = new Application_Model_DbTable_AdvertsList();
                $adListIds = $adListModel->getPairs($ad->id);

                $campaignStat = new Application_Model_DbTable_AdCampaignStatistics();
                $campaignStat->getAdapter()->beginTransaction();

                //delete crossing stat:
                $campaignStat->deleteStatByDate($ad->id, $this->getDateFrom()->format("Y-m-d"));

                foreach($statistics as $stat) {
                    $id = (int)$stat->id;
                    $date = !empty($stat->date) ? $stat->date : $stat->date_label;
                    $spend = abs(!empty($stat->spend) ? $stat->spend : $stat->profit);

                    $newStat = $campaignStat->createRow();

                    if(!empty($stat->adgroup_id)) {
                        //Statistic by Ad
                        $newStat->advert_id = $adListIds[$id];
                    } else {
                        //var_dump($id, $campaignIds[$id]);
                        $newStat->ad_campaign_id = $campaignIds[$id];
                    }

                    $newStat->user_id = $ad->user_id;
                    $newStat->ad_id = $ad->id;
                    $newStat->date = $lastDate = strftime("%Y-%m-%d", strtotime($date));
                    $newStat->impressions = (int)$stat->impressions;
                    $newStat->clicks = !empty($stat->clicks) ? (int)$stat->clicks : 0;
                    $newStat->ctr = !empty($stat->ctr) ? (float)$stat->ctr : 0;
                    $newStat->spend = (float)$spend;
                    $newStat->average_cpc = !empty($stat->cpc) ? (float)$stat->cpc : 0;
                    $newStat->added = new Zend_Db_Expr('NOW()');
                    $newStat->save();
                }
                $campaignStat->getAdapter()->commit();
            }

            $ad->last_statistic_date = $this->_dateTo->format('Y-m-d');
            $ad->last_update = new Zend_Db_Expr('NOW()');
            $ad->save();
            $result['success'] = true;
        }

        return $result;
    }

    public function addAdList($ad)
    {
        //Import Ad Groups:
        $this->_apiMethod = '/search-api/v1/adgroups';
        if(!$adGroupList = $this->sendRequest()) {
            return;
        }

        $adCampaigns = new Application_Model_DbTable_AdCampaigns();
        $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

        $adGroups = new Application_Model_DbTable_AdGroups();
        $adGroupIds = $adGroups->getPairs($ad->id);

        foreach($adGroupList as $adGroup) {
            $groupApiId = (int)$adGroup->id;
            if(!empty($adGroupIds[$groupApiId])) { continue; }

            $data = array('api_id' => $groupApiId, 'ad_id' => $ad->id, 'user_id' => $ad->user_id,
                'ad_campaign_id' => $campaignIds[$adGroup->campaign_id],
                'name' => $adGroup->name, 'status' => strtolower($adGroup->status));

            $newAdGroup = $adGroups->create($data);
            $adGroupIds[$newAdGroup->api_id] = $newAdGroup->id;
        }

        //Import Ads:
        $this->_apiMethod = '/search-api/v1/ads';
        $apiAdsList = $this->sendRequest();

        $adListModel = new Application_Model_DbTable_AdvertsList();
        $adListIds = $adListModel->getPairs($ad->id);

        //!!!ad_campaign_id must be added to adverts_list table!!!
        foreach($apiAdsList as $adInfo) {
            $apiAdId = (int)$adInfo->id;
            if(!empty($adListIds[$apiAdId])) { continue; }

            $data = array('api_id' => $apiAdId, 'ad_id' => $ad->id, 
                'ad_group_id' => $adGroupIds[$adInfo->adgroup_id],
                'header' => $adInfo->headline,
                'url' => $adInfo->target_url, 'status' => strtolower($adInfo->status),
                'options' => serialize((array)$adInfo));

            $adListModel->create($data);
        }

        $this->_apiMethod = '/search-api/v1/stats/ads';
        return $this->sendRequest();
    }

    /**
     * Make signature for CURL request
     * 
     * @return string
     */
    protected function getSignature()
    {
        $plaintext = "{$this->_apiType} {$this->_apiMethod}?api_key={$this->_apiKey}&timestamp={$this->_timestamp}";
        return hash_hmac('sha256', $plaintext, $this->_secretKey);
    }

    /**
     * Get DateFrom
     * Set report starting date
     * 
     * @return DateTime
     **/
    public function getDateFrom()
    {
        if(!$this->_dateFrom) {
            $today = new DateTime();
            $this->_dateFrom = new DateTime($today->sub(new DateInterval('P1M'))->format("m/01/Y"));
        }
        return $this->_dateFrom;
    }

    /**
     * Get DateTo
     * Set report end date
     * 
     * @return DateTime
     **/
    public function getDateTo()
    {
        if(!$this->_dateTo) {
            $today = new DateTime();
            $this->_dateTo = $today;
        }
        return $this->_dateTo;
    }

    /**
     * Send CURL request
     * 
     * @return array|string
     */
    protected function sendRequest()
    {
        $this->_timestamp = time();
        $requestUrl = $this->_apiUrl . $this->_apiMethod;

        $criteria = array('api_key' => $this->_apiKey, 'timestamp' => $this->_timestamp, 'signature' => $this->getSignature());
        $criteria['start_date'] = $this->getDateFrom()->format("Y-m-d");
        $criteria['end_date'] = $this->getDateTo()->format("Y-m-d");

        $config = array(
            'maxredirects' => 1,
            'timeout' => 30,
            'useragent' => 'phpdriver',
            'curloptions' => array(CURLOPT_FOLLOWLOCATION => 1, CURLOPT_RETURNTRANSFER => 1),
        );

        $client = new Zend_Http_Client($requestUrl, $config);
        $client->setParameterGet($criteria);
        $response = $client->request(Zend_Http_Client::GET);

        if ($response->isSuccessful()){
            try{
                $result = json_decode($response->getBody()); 
            } catch (Exception $e) {
                $result['errorMessage'] = $this->_errors[500];
                $this->_logger->err("Account {$this->_accountName}: {$e->getMessage()}");
            }
        } else {
            $errorCode = Zend_Http_Response::extractCode($response);
            $result['errorMessage'] = !empty($this->_errors[$errorCode]) ? 
                $this->_errors[$errorCode] : $response->getBody();

            $this->_logger->err("Account {$this->_accountName}: {$result['errorMessage']}");
        }
        return $result;
    }
}