<?php
class Application_Model_Leadnomics
{
    protected $_loginUrl = 'http://platform.leadnomics.com/login';
    protected $_mainUrl = 'http://platform.leadnomics.com/';
    protected $_email;
    protected $_password;

    protected $_dateFrom;
    protected $_dateTo;

    protected $_cookieJar;
    protected $_logger;
    
    protected $_errors = array(
        'login_error' => 'Login failed. Please, check your credentials',
        'server_unavailable' => 'Leadnomics Server not responding',
        'account_connected' => 'This Affiliate Account is already connected to your Leadssistant account'
    );

    /**
     * Class constructor
     * 
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->_email = !empty($options['email']) ? $options['email'] : null;
        $this->_password = !empty($options['password']) ? $options['password'] : null;

        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/leadnomics');
        $this->_logger = new Zend_Log($writer);
    }

    /**
     * Add new Leadnomics account
     * 
     * @param int $userId
     * @return array|Zend_Db_Table_Row
     */
    public function addAccount($userId)
    {
        $content = $result = $this->login();

        if(true === $content) {
            $result = array();

            $affiliateModel = new Application_Model_DbTable_Affiliates();
            if($affiliateModel->getAffiliatesByName($this->_email, $userId, Application_Model_DbTable_Affiliates::LEADNOMICS)) {
                $result['errorMessage'] = $this->_errors['account_connected'];
            } else {
                $credentials = array('email' => $this->_email, 'password' => $this->_password);
                $affiliateOptions = array('name' => $this->_email, 'credentials' => $credentials, 
                    'source' => Application_Model_DbTable_Affiliates::LEADNOMICS);
                $result = $affiliateModel->create($affiliateOptions, $userId);
            }
        }

        return $result;
    }

    /**
     * Add/Update Campaign
     * 
     * @param Zend_Db_Table_Row $affiliate
     * @param int $campaigId Campaigns api id
     * @param string $campaignName Campaign name
     * @return Zend_Db_Table_Row
     */
    public function addCampaign($affiliate, $campaignId, $campaignName)
    {
        if(!$campaignId || !$campaignName) {
            return;
        }

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $keywords = $campaignsModel->getKeywords($affiliate->user_id);

        $affiliateCampaigns = new Application_Model_DbTable_AffiliateCampaigns();
        $campaignIds = $affiliateCampaigns->getPairs($affiliate->user_id, $affiliate->id);

        $campaignApiId = (int)$campaignId;

        $id = !empty($campaignIds[$campaignApiId]) ? $campaignIds[$campaignApiId] : null;
        //if($id) { continue; } //remove this if no need to update existing campaigns

        $data = array('api_id' => $campaignApiId, 'user_id' => $affiliate->user_id, 
            'name' => $campaignName, 'affiliate_id' => $affiliate->id);

        //automatic Pairing
        if(false !== ($campaignId = App_Utils::findKeyword($campaignName, $keywords))) {
            $data['campaign_id'] = $campaignId;
        }

        return $affiliateCampaigns->create($data, $id);
    }

    /**
     * Add new SubId
     * 
     * @param Zend_Db_Table_Row $affiliate
     * @param int $campaignId
     * @param string $subidName
     * 
     * @return Zend_Db_Table_Row
     */
    protected function _addSubId($affiliate, $campaignId, $subidName)
    {
        $data = array('user_id' => $affiliate->user_id, 
            'affiliate_id' => $affiliate->id,
            'affiliate_campaign_id' => $campaignId,
            'name' => $subidName);

        $affiliateSubids = new Application_Model_DbTable_AffiliateSubids();
        return $affiliateSubids->create($data);
    }

    /**
     * Get SubIds for specified campaigns
     * 
     * @param Zend_Db_Table_Row $affiliate
     * @param array $campaignIds Array of campaign API ids
     */
    public function getSubIds($affiliate, $campaignIds = array())
    {
        $this->login();

        $startDate = new DateTime();
        $startDate = $startDate->sub(new DateInterval('P1D'));

        $this->_geSubIdtStat($affiliate, $startDate, $campaignIds, new DateTime());
    }

    /**
     * Get and Save Campaigns and Statistics
     * 
     * @param Zend_Db_Table_Row $affiliate
     * @param bool $updateCampaigns
     * @return array
     */
    public function getCampaignsStatistics($affiliate, $updateCampaigns = true)
    {
        $this->login();

        $this->_dateFrom = $this->getDateFrom();
        $this->_dateTo = $this->getDateTo();

        if($affiliate->last_statistic_date) {
            $lastUpdate = new DateTime($affiliate->last_statistic_date);
            $this->_dateFrom = $lastUpdate->sub(new DateInterval('P1D'));
        }

        $statistics = array();
        $startDate = clone $this->_dateFrom;
        while($startDate <= $this->_dateTo) {
            //get subIds statistic
            $subIdStatistics = $this->_geSubIdtStat($affiliate, $startDate);
            $currDataStat = array($startDate->format('Y-m-d') => array_merge($this->_getStat($startDate), $subIdStatistics));
            $statistics = array_merge($statistics, $currDataStat);
            $startDate->add(new DateInterval('P1D'));
        }

        if($statistics) {
            //get existing Camaigns:
            $affiliateCampaigns = new Application_Model_DbTable_AffiliateCampaigns();
            $campaignIds = $affiliateCampaigns->getPairs($affiliate->user_id, $affiliate->id);

            $campaignStat = new Application_Model_DbTable_AffiliateCampaignStatistics();
            $campaignStat->getAdapter()->beginTransaction();

            //delete crossing stat:
            $campaignStat->deleteStatByDate($affiliate->id, $this->_dateFrom->format("Y-m-d"));

            foreach($statistics as $date => $row) {
                foreach($row as $stat) {
                    $newStat = $campaignStat->createRow();

                    if(!empty($stat['affiliate_subid'])) {
                        $newStat->affiliate_campaign_id = (string)$stat['affiliate_campaign_id'];
                        $newStat->affiliate_subid = (string)$stat['affiliate_subid'];
                    } else {
                        $campaignApiId = (int)$stat['campaign_api_id'];
                        if(empty($campaignIds[$campaignApiId])) {
                            //add new Campaign
                            $newCampaign = $this->addCampaign($affiliate, $campaignApiId, $stat['campaign']);
                            $campaignIds[$newCampaign->api_id] = $newCampaign->id;
                        }
                        $newStat->affiliate_campaign_id = $campaignIds[$campaignApiId];
                    }

                    $newStat->affiliate_id = $affiliate->id;
                    $newStat->user_id = $affiliate->user_id;
                    $newStat->date = $date;
                    $newStat->clicks = (int)$stat['hits'];
                    $newStat->approved = (int)$stat['conversions'];
                    $newStat->approved_percentage = (float)$stat['conversion_rate'];
                    $newStat->epc = (float)$stat['epc'];
                    $newStat->commission = (float)$stat['revenue'];
                    $newStat->added = new Zend_Db_Expr('NOW()');
                    $newStat->save();
                }
            }
            $campaignStat->getAdapter()->commit();
        }

        $affiliate->last_statistic_date = new Zend_Db_Expr('CURDATE()');
        $affiliate->last_update = new Zend_Db_Expr('NOW()');
        $affiliate->save();
        $result['success'] = true;

        return $result;
    }

    /**
     * Report page scraping
     * 
     * @param DateTime $date
     * @return array
     */
    protected function _getStat($date)
    {
        $reportDate = $date->format('F d Y');
        $reportUrl = "{$this->_mainUrl}reporting/index/range";
        $response = $this->_sendRequest($reportUrl, array('fromDate' => $reportDate, 'toDate' => $reportDate));

        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($response);
        libxml_use_internal_errors(false);
        $xpath = new DOMXpath($dom); 

        $statistics = array();
        $keys = array(0 => 'campaign', 'hits', 'conversions', 'conversion_rate', 'revenue', 'epc', 'avg_value');

        $statRow = $xpath->query('//table[@id="offerStats"]/tbody/tr');
        foreach ($statRow as $row) {
            $stat = array();

            $cells = $xpath->query('td', $row);
            if ($cells->length) {
                $campaign = $xpath->query('a', $cells->item(0))->item(0);
                $campaignUri = $campaign->getAttributeNode('href')->nodeValue;
                if(preg_match('/([0-9]+)/', $campaignUri, $matches)) {
                    $stat['campaign_api_id'] = $matches[1];
                }

                foreach($cells as $i => $cell) {
                    $stat[$keys[$i]] = $cell->nodeValue;
                }
                $statistics[] = $stat;
            }
        }
        return $statistics;
    }

    /**
     * Get statistic for campaigns' subIds
     * scraping Reporting page for proper campaigns
     * 
     * @param Zend_Db_Table_Row $affiliate
     * @param DateTime $date
     * @param array $campaignIds
     * @return array
     */
    public function _geSubIdtStat($affiliate, $date, $campaignIds = array(), $dateTo = null)
    {
        $reportDateFrom = $reportDateTo = $date->format('F d Y');
        $reportDateTo = $dateTo ? $dateTo->format('F d Y') : $reportDateTo;
        $statistics = array();

        $affiliateSubids = new Application_Model_DbTable_AffiliateSubids();
        $affiliateCampaigns = new Application_Model_DbTable_AffiliateCampaigns();
        if(!$campaignIds) {
            //Find Leadnomics campaigns API IDs paired for "Ad Performance"
            $campaignIds = $affiliateSubids->getPairedCampaignIds($affiliate->id);
        }
        if(!$campaignIds) {
            return $statistics;
        }

        //select campaigns by given list of API Ids
        $select = $affiliateCampaigns->select()->where('api_id IN (?)', $campaignIds);
        $campaigns = $affiliateCampaigns->fetchAll($select);

        foreach($campaigns as $campaign) {
            $reportUrl = "{$this->_mainUrl}reporting/offer/{$campaign->api_id}/range";
            $response = $this->_sendRequest($reportUrl, array('fromDate' => $reportDateFrom, 'toDate' => $reportDateTo));

            $dom = new DOMDocument();
            libxml_use_internal_errors(true);
            $dom->loadHTML($response);
            libxml_use_internal_errors(false);
            $xpath = new DOMXpath($dom);

            $existingSubids = $affiliateSubids->getPairs(array('affiliate_campaign_id' => $campaign->id));

            $keys = array(0 => 'sub_id', 'hits', 'conversions', 'conversion_rate', 'revenue', 'epc', 'avg_value');
            foreach($xpath->query('//table[contains(@class, "offer")]') as $offerTable) {
                foreach($xpath->query('tbody/tr', $offerTable) as $offerRow) {
                    $stat = array();
                    $cells = $xpath->query('td', $offerRow);
                    if ($cells->length && $cells->length == count($keys)) {
                        foreach($cells as $i => $cell) {
                            $stat[$keys[$i]] = $cell->nodeValue;
                        }

                        //find/create SubId
                        $subId = array_search($stat['sub_id'], $existingSubids);
                        if(false === $subId) {
                            $newRecord = $this->_addSubId($affiliate, $campaign->id, $stat['sub_id']);
                            $existingSubids[$newRecord->id] = $stat['sub_id'];
                            $subId = $newRecord->id;
                        }

                        $stat['affiliate_subid'] = $subId;
                        $stat['affiliate_campaign_id'] = $campaign->id;
                        $statistics[] = $stat;
                    }
                }
            }
        }
        return $statistics;
    }

     /**
     * Get DateFrom
     * Set report starting date
     * 
     * @return DateTime
     **/
    public function getDateFrom()
    {
        if(!$this->_dateFrom) {
            $today = new DateTime();
            $this->_dateFrom = $today->sub(new DateInterval('P10D'));
        }
        return $this->_dateFrom;
    }
    

    /**
     * Get DateTo
     * Set report end date
     * 
     * @return DateTime
     **/
    public function getDateTo()
    {
        if(!$this->_dateTo) {
            $this->_dateTo = new DateTime();
        }
        return $this->_dateTo;
    }

    /**
     * Leadnomics login
     * 
     * @return array|string
     */
    public function login()
    {
        $this->_cookieJar = new Zend_Http_CookieJar();

        $postParams = array('email' => $this->_email, 'password' => $this->_password);
        $result = $response = $this->_sendRequest($this->_loginUrl, null, $postParams);

        if(!is_array($result)) {
            if(false === stripos($response, 'Offer Conversion Breakdown')) {
                $result = array();
                $result['errorMessage'] = $this->_errors['login_error'];
            } else {
                return true;
            }
        }
        $this->_logger->err("{$this->_email} {$result['errorMessage']}");

        return $result;
    }

    /**
     * Send CURL request
     * 
     * @param string $url
     * @param array $getParams
     * @param array $postParams
     * 
     * @return array|string
     */
    protected function _sendRequest($url, $getParams = array(), $postParams = array())
    {
        $result = array();

        $config = array(
            'timeout' => 30,
            'useragent' => "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)",
            'curloptions' => array(CURLOPT_RETURNTRANSFER => 1, CURLOPT_SSL_VERIFYPEER => false)
        );

        $adapter = new Zend_Http_Client_Adapter_Curl();
        $client = new Zend_Http_Client($url, $config);

        $client->setAdapter($adapter);
        $client->setCookieJar($this->_cookieJar);

        if($postParams) {
            $client->setParameterPost($postParams);
            $client->setMethod(Zend_Http_Client::POST);
        } else {
            $client->setParameterGet($getParams);
            $client->setMethod(Zend_Http_Client::GET);
        }

        try{
            $response = $client->request();
            if ($response->isSuccessful()){
                return $response->getBody();
            } else {
                $result['errorMessage'] = $this->_errors['server_unavailable'];
            }
        } catch (Zend_Http_Client_Exception $e) {
            $result['errorMessage'] = $e->getMessage();
        }

        $this->_logger->err("{$this->_email}. URL {$url}: {$result['errorMessage']}");
        return $result;
    }
}