<?php
/**
 * Leadimpact.com scraper
 * 
 * @author Guzel Mitroshina <guze4ka@gmail.com>
 */
class Application_Model_Leadimpact
{
    protected $_mainUrl = 'https://secure.leadimpact.com/';
    protected $_reportsPage = 'reports/report.aspx';
    protected $_reportViewPage = 'Reports/ReportOut.aspx';
    protected $_email;
    protected $_password;

    protected $_dateFrom;
    protected $_dateTo;

    protected $_cookieJar;
    protected $_logger;

    protected $_errors = array(
        'login_error' => 'Login failed. Please, check your credentials',
        'server_unavailable' => 'Lead Impact Server is not responding',
        'account_connected' => 'This Account is already connected to your Leadssistant account',
        'campaigns_missing' => 'Campaigns was not found!'
    );

    public function __construct($options = array())
    {
        $this->_email = !empty($options['email']) ? $options['email'] : null;
        $this->_password = !empty($options['password']) ? $options['password'] : null;

        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/leadimpact');
        $this->_logger = new Zend_Log($writer);
    }

    /**
     * Add new account to DB
     * 
     * @param int $userId
     * @return Zend_Db_Table_Row|array
     */
    public function addAccount($userId)
    {
        $result = $content = $this->login();
        if(!is_array($result)) {
            $result = array();

            $adsModel = new Application_Model_DbTable_Ads();
            if($adsModel->getAdsByName($userId, $this->_email, Application_Model_DbTable_Ads::LEADIMPACT)) {
                $result['errorMessage'] = $this->_errors['account_connected'];
            } else {
                $accountInfo = $this->_getAccountInfo($content);
                $credentials = array('email' => $this->_email, 'password' => $this->_password);
                $options = array('source' => Application_Model_DbTable_Ads::LEADIMPACT, 'account_id' => $accountInfo['id'],
                    'name' => $this->_email, 'credentials' => $credentials);

                $result = $adsModel->create($userId, $options); 
             }
        }

        return $result;
    }

    /**
     * Get Account Info parsing main page
     * 
     * @param string $content
     * @return array
     */
    protected function _getAccountInfo($content)
    {
        $xpath = $this->getXPath($content);

        $info = array();
        if($infoDiv = $xpath->query("//div[@id='ctl00_Tabs_pnlInfo']")->item(0)) {
            preg_match('/\(ID ([0-9]+)\)/', $infoDiv->nodeValue, $matches);
            $info['id'] = (int)$matches[1];
        }
        return $info;
    }

    /**
     * Add Campaigns from reports page
     * 
     * @param array $ads
     * @return boolean
     */
    public function addCampaigns($ad)
    {
        $content = $this->_sendRequest($this->_mainUrl . $this->_reportsPage);
        $xpath = $this->getXPath($content);

        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $keywords = $campaignsModel->getKeywords($ad->user_id);

        $adCampaigns = new Application_Model_DbTable_AdCampaigns();
        $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id);

        $isCampaigns = false;
        foreach($xpath->query("//select[@id='ctl00_phMainContent_lbCampaignsIDs']/option") as $element) {
            $campaignName = trim($element->nodeValue);
            $campaignApiId = $element->getAttribute('value');

            //update existing Campaigns every time
            $id = !empty($campaignIds[$campaignApiId]) ? $campaignIds[$campaignApiId] : null;

            $data = array('api_id' => $campaignApiId, 'user_id' => $ad->user_id, 'ad_id' => $ad->id,
                'name' => $campaignName, 'source' => Application_Model_DbTable_Ads::LEADIMPACT);

            //automatic Pairing
            if(false !== ($campaignId = App_Utils::findKeyword($campaignName, $keywords))) {
                $data['campaign_id'] = $campaignId;
            }

            $newCampaign = $adCampaigns->create($data, $id);
            $campaignIds[$newCampaign->api_id] = $newCampaign->id;
            $isCampaigns = true;
        }
        if(!$isCampaigns) {
            $this->_logger->err("{$this->_email}. " .  $this->_errors['campaigns_missing']);
        }

        return $isCampaigns;
    }

    /**
     * Add/Update campaigns statistics
     * 
     * @param Zend_Db_Table_Row $ad
     * @param bool $updateCampaigns Update the campaigns
     * @return array
     */
    public function getCampaignsStatistics($ad, $updateCampaigns = true)
    {
        $this->login();
        if($updateCampaigns) {
            $this->addCampaigns($ad);
        }

        $this->_dateFrom = $this->getDateFrom();
        $this->_dateTo = $this->getDateTo();

        if($ad->last_statistic_date) {
            $lastUpdate = new DateTime($ad->last_statistic_date);
            $this->_dateFrom = $lastUpdate->sub(new DateInterval('P1D'));
        }

        if($statistics = $this->_getStat()) {
            $adCampaigns = new Application_Model_DbTable_AdCampaigns();
            $campaignIds = $adCampaigns->getPairs($ad->user_id, $ad->id, 'name');

            $campaignStat = new Application_Model_DbTable_AdCampaignStatistics();
            $campaignStat->getAdapter()->beginTransaction();

            //delete crossing stat:
            $campaignStat->deleteStatByDate($ad->id, $this->_dateFrom->format("Y-m-d"));

            foreach($statistics as $stat) {
                $campaignName = $stat['campaign_name'];
                if(!$campaignIds[$campaignName]) {
                    continue;
                }

                $newStat = $campaignStat->createRow();
                $newStat->user_id = $ad->user_id;
                $newStat->ad_id = $ad->id;
                $newStat->ad_campaign_id = $campaignIds[$campaignName];
                $newStat->date = strftime("%Y-%m-%d", strtotime($stat['date']));
                $newStat->impressions = (int)$stat['impressions'];
                $newStat->spend = (float)str_replace('$', '', $stat['spend']);
                $newStat->added = new Zend_Db_Expr('NOW()');
                $newStat->save();
            }
            $campaignStat->getAdapter()->commit();
        }

        $ad->last_statistic_date = new Zend_Db_Expr('CURDATE()');
        $ad->last_update = new Zend_Db_Expr('NOW()');
        $ad->save();
        $result['success'] = true;
        return $result;

    }

    /**
     * Find statistics by parsing Reports view page
     * 
     * @return array
     */
    protected function _getStat()
    {
        $url = $this->_mainUrl . $this->_reportViewPage;
        $params = array('level' => 'Campaign', 'type' => 'ByDate', 
            'begin_date' => $this->_dateFrom->format('m/d/Y'), 'end_date' => $this->_dateTo->format('m/d/Y'));

        $content = $this->_sendRequest($url, $params);
        $xpath = $this->getXPath($content);

        $stat = array();
        $keys = array('date', 'campaign_name', 'impressions', 'spend', 'conversions');
        foreach($xpath->query("//table[@id='ctl00_phMainContent_CampaignGrid']/tr") as $row) {
            if($row->getAttribute('class') == 'GridHeader') {
                continue;
            }
            $values = array();
            foreach($xpath->query('td', $row) as $field) {
                $values[] = trim($field->nodeValue);
            }
            $stat[] = array_combine($keys, $values);
        }
        return $stat;
    }

    /**
     * Login to leadimpact.com
     * 
     * @return string|array
     */
    public function login()
    {
        $this->_cookieJar = new Zend_Http_CookieJar();
        $loginPage = $this->_sendRequest($this->_mainUrl);

        $xpath = $this->getXPath($loginPage);

        $viewstate = $eventvalidation = null;
        if($viewstateElement = $xpath->query("//input[@name='__VIEWSTATE']")->item(0)) {
            $viewstate = $viewstateElement->getAttributeNode('value')->nodeValue;
        }
        if($validationElement = $xpath->query("//input[@name='__EVENTVALIDATION']")->item(0)) {
            $eventvalidation = $validationElement->getAttributeNode('value')->nodeValue;
        }

        $post = array(
            '__LASTFOCUS' => '',
            '__EVENTTARGET' => '',
            '__EVENTARGUMENT' => '',
            '__VIEWSTATE' => $viewstate,
            '__EVENTVALIDATION' => $eventvalidation,
            'ctl00$phMainContent$txtUserID' => $this->_email,
            'ctl00$phMainContent$txtPassword' => $this->_password,
            'ctl00$phMainContent$btnLogin' => 'Login'
        );

        try {
            $login = $this->_sendRequest($this->_mainUrl, null, $post);
            if(false !== strpos($login, 'Current Balance')) {
                return $login;
            }
            $result['errorMessage'] = $this->_errors['login_error'];
        } catch (Application_Model_Exceptions_Curl $e) {
            $result['errorMessage'] = $e->getMessage();
        }

        $this->_logger->err("{$this->_email} {$result['errorMessage']}");
        return $result;
    }

    /**
     * Get DateFrom
     * Set report starting date
     * 
     * @return DateTime
     **/
    public function getDateFrom()
    {
        if(!$this->_dateFrom) {
            $today = new DateTime();
            $this->_dateFrom = $today->sub(new DateInterval('P30D'));
        }
        return $this->_dateFrom;
    }

    /**
     * Get DateTo
     * Set report end date
     * 
     * @return DateTime
     **/
    public function getDateTo()
    {
        if(!$this->_dateTo) {
            $this->_dateTo = new DateTime();
        }
        return $this->_dateTo;
    }

    /**
     * Get XPath
     * 
     * @param string $content
     * @return \DOMXpath
     */
    protected function getXPath($content)
    {
        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($content);
        libxml_use_internal_errors(false);
        return new DOMXpath($dom);
    }

    /**
     * Send CURL request
     * 
     * @param string $url
     * @param array $getParams
     * @param array $postParams
     * 
     * @return array|string
     */
    protected function _sendRequest($url, $getParams = array(), $postParams = array())
    {
        $result = array();

        $config = array(
            'timeout' => 30,
            'useragent' => "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)",
            'curloptions' => array(CURLOPT_RETURNTRANSFER => 1, CURLOPT_SSL_VERIFYPEER => false)
        );

        $adapter = new Zend_Http_Client_Adapter_Curl();
        $client = new Zend_Http_Client($url, $config);

        $client->setAdapter($adapter);
        $client->setCookieJar($this->_cookieJar);

        if($postParams) {
            $client->setParameterPost($postParams);
            $client->setMethod(Zend_Http_Client::POST);
        } else {
            $client->setParameterGet($getParams);
            $client->setMethod(Zend_Http_Client::GET);
        }

        try{
            $response = $client->request();
            if ($response->isSuccessful()){
                return $response->getBody();
            } else {
                $result['errorMessage'] = $this->_errors['server_unavailable'];
            }
        } catch (Zend_Http_Client_Exception $e) {
            $result['errorMessage'] = $e->getMessage();
        }

        $this->_logger->err("{$this->_email}. URL {$url}: {$result['errorMessage']}");
        return $result;
    }
}