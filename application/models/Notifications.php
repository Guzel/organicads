<?php

class Application_Model_Notifications
{
    const CURRENT_REQUEST = 'current';
    const SYNC_REQUEST = 'sync';
    const OVERALL_REQUEST = 'overall';
    const AUTO_ON_REQUEST = 'auto on';
    const AUTO_OFF_REQUEST = 'auto off';

    protected $_allowedRequests;
    protected $_method; //One of the allowed current, sync or overall
    protected $_user;
    protected $_fromPhone;
    protected $_toPhone;

    public function __construct()
    {
        $this->_allowedRequests = array(self::CURRENT_REQUEST, self::SYNC_REQUEST, self::OVERALL_REQUEST,
                self::AUTO_ON_REQUEST, self::AUTO_OFF_REQUEST);
    }

    public function processStatNotifications($userId, $frequency = 'auto')
    {
        $statDay = new DateTime();
        if($frequency != 'auto') {
            $frequency = array('day');

            $statDay->sub(new DateInterval('P1D')); //using day before because we run the script at 00:20
            if($statDay->format('w') == '6') {
                $frequency[] = 'week';
            }
            if($statDay->format('t') == $statDay->format('d')) {
                $frequency[] = 'month';
            }
        }
        $frequency = array($frequency);

        $notificationsModel = new Application_Model_DbTable_Notifications();
        $select = $notificationsModel->select()->where('frequency IN (?)', $frequency)
                ->where("type = 'stat'")
                ->where('disabled IS NULL')
                ->where("user_id = ?", (int)$userId);
        $notifications = $notificationsModel->fetchAll($select);

        if($notifications) {
            $notificationsQueueModel = new Application_Model_DbTable_NotificationsQueue();
            $notificationContactsModel = new Application_Model_DbTable_NotificationContacts();
            $usersModel = new Application_Model_DbTable_Users();

            foreach($notifications as $notification) {
                $contacts = $notificationContactsModel->getContacts($notification->user_id, $notification->id);
                if(!$contacts) {
                    continue;
                }

                $date = clone $statDay;
                $startDate = $date->format('Y-m-d');
                if($notification->frequency == 'week') {
                    $startDate = $date->sub(new DateInterval('P7D'))->format('Y-m-d');
                } elseif($notification->frequency == 'month') {
                    $startDate = $date->format('Y-m-01');
                }

                $this->_user = $usersModel->find($notification->user_id)->current();
                $stat = $this->generateTotalStats($startDate);

                //Add to Notification Queue:
                foreach($contacts as $contact) {
                    $greeting = "Hi {$contact['name']}, \r\n";
                    $greeting .= ($notification->frequency == 'auto') ? '' : "Here is how we ended today: \r\n";
                    $message = $greeting . "------------------------------\r\n" . $stat;

                    $data = array('notification_id' => $notification->id, 
                        'phone' => $contact['phone'], 'text' => $message,
                        'from' => ($this->_user->twilio_phone ? : null));
                    $notificationsQueueModel->save($data);
                }

                $notification->last_processed = new Zend_Db_Expr('NOW()');
                $notification->save();
            }
        }
    }

    public function processNotifications($type = array(), $userId = null)
    {
        if($type == 'stat' || $type == 'auto') {
            $frequency = ($type == 'auto') ? $type : '';
            $this->processStatNotifications($userId, $frequency);
            return;
        }

        $type = is_array($type) ? $type : array($type);

        $notificationsModel = new Application_Model_DbTable_Notifications();
        $select = $notificationsModel->select()->where("type IN(?)", $type)
                ->where('disabled IS NULL')
                ->where(new Zend_Db_Expr('DATE(last_processed) != CURDATE() OR last_processed IS NULL'));
        if($userId) {
            $select->where('user_id = ?', $userId);
        }
        $notifications = $notificationsModel->fetchAll($select);

        $notificationsQueueModel = new Application_Model_DbTable_NotificationsQueue();
        $affiliateStat = new Application_Model_DbTable_AffiliateCampaignStatistics();
        $adStat = new Application_Model_DbTable_AdCampaignStatistics();
        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $notificationContactsModel = new Application_Model_DbTable_NotificationContacts();

        $usersModel = new Application_Model_DbTable_Users();

        foreach($notifications as $notification) {
            $text = '';
            $contacts = $notificationContactsModel->getContacts($notification->user_id, $notification->id);
            if(!$contacts) {
                continue;
            }
            $today = new DateTime();
            $startDate = $today->format('Y-m-d');

            $options = array('user_id' => $notification->user_id, 'campaign_id' => $notification->campaign_id);
            $campaignRevenue = current($affiliateStat->findTotalByCampaign($options, $startDate));
            $campaignSpend = current($adStat->findTotalByCampaign($options, $startDate));

            $campaignName = 'Total';
            if($notification->campaign_id) {
                $campaign = $campaignsModel->find($notification->campaign_id)->current();
                $campaignName = $campaign->nickname ? : $campaign->name;
            }

            $spend = (float)(!empty($campaignSpend) ? $campaignSpend['spend'] : 0);
            $revenue = (float)(!empty($campaignRevenue) ? $campaignRevenue['commission'] : 0);
            $profit = $revenue - abs($spend);
            $roi = ($spend == 0) ? 1 : $profit/$spend;

            if($notification->type == 'profit' && $profit < $notification->threshold) {
                $text = "{$campaignName} profit has fallen to {profit} | {roi}%";
            } elseif($notification->type == 'roi' && ($roi * 100) < $notification->threshold) {
                $text = "{$campaignName} ROI has fallen to {roi}% with a P/L of {profit}";
            } else {
                continue;
            }

            $text = str_replace('{profit}', number_format($profit, 2), $text);
            $text = str_replace('{roi}', number_format($roi * 100, 2), $text);

            $user = $usersModel->find($notification->user_id)->current();
            //Add to Notification Queue:
            foreach($contacts as $contact) {
                $message = "Hi {$contact['name']}, \r\n" . $text;
                $data = array('notification_id' => $notification->id, 
                    'phone' => $contact['phone'], 'text' => $message, 
                    'from' => ($user->twilio_phone ? : null));
                $notificationsQueueModel->save($data);
            }
            $notification->last_processed = new Zend_Db_Expr('NOW()');
            $notification->save();
        }
    }

    public function addSyncStatToQueue($userId, $phone, $fromPhone)
    {
        $notificationsQueueModel = new Application_Model_DbTable_NotificationsQueue();
        $usersModel = new Application_Model_DbTable_Users();
        $this->_user = $usersModel->find($userId)->current();

        $today = new DateTime();
        $text = $this->generateTotalStats($today->format('Y-m-d'));
        return $notificationsQueueModel->save(array('phone' => $phone, 'text' => $text, 'from' => $fromPhone));
    }

    public function processRequest($request)
    {
        $this->_fromPhone = App_Utils::normalizePhone($request['From']);
        $this->_toPhone = App_Utils::normalizePhone($request['To']);
        $this->_method = strtolower(trim($request['Body']));

        $userContacts = new Application_Model_DbTable_UserContacts();
        $contacts = $userContacts->findByPhone($this->_fromPhone);

        $usersModel = new Application_Model_DbTable_Users();
        $this->_user = $usersModel->findUserByTwilio($this->_toPhone);

        if(!in_array($this->_method, $this->_allowedRequests) || !$contacts || !$this->_user) {
            return $this->formatResponse('Not allowed Request or From/To Phone Number');
        }

        return $this->response();
    }

    protected function response()
    {
        $today = new DateTime();
        switch($this->_method) {
            case self::CURRENT_REQUEST:
                $text = $this->generateTotalStats($today->format('Y-m-d'));
                break;
            case self::SYNC_REQUEST:
                $syncRequest = new Application_Model_DbTable_StatisticSyncRequests();
                $syncRequest->save($this->_user->id, $this->_fromPhone, $this->_toPhone);
                $text = 'Please wait…';
                break;
            case self::OVERALL_REQUEST:
                $text = $this->generateOverallStats();
                break;
            case self::AUTO_ON_REQUEST:
                $this->autoNotification();
                $text = "Auto Updates On";
                break;
            case self::AUTO_OFF_REQUEST:
                $this->autoNotification(false);
                $text = "Auto Updates Off";
                break;
            default:
                $text = 'Command name was not recognize';
                break;
        }

        return $this->formatResponse($text);
    }

    protected function formatResponse($response)
    {
        $xml = new SimpleXMLElement('<Response/>');
        $xml->addChild('Message', $response);

        return $xml->asXML();
    }

    protected function autoNotification($on = true)
    {
        $notificationsModel = new Application_Model_DbTable_Notifications();
        $notification = $notificationsModel->fetchRow($notificationsModel->select()
                ->where('user_id = ?', $this->_user->id)
                ->where('type = ?', 'stat')->where('frequency = ?', 'auto'));

        if($on && !$notification) {
            $data = array('type' => 'stat', 'frequency' => 'auto', 'hidden' => 1);
            $notification = $notificationsModel->save($data, $this->_user->id);
        }

        //add contact
        $notificationContacts = new Application_Model_DbTable_NotificationContacts();
        $userContacts = new Application_Model_DbTable_UserContacts();
        $addedContacts = $notificationContacts->getContacts($this->_user->id, $notification->id);
        $contacts = $userContacts->findByPhone($this->_fromPhone, $this->_user->id);
        foreach($contacts as $contact) {
            if(!$addedContacts[$contact->id]) {
                $notificationContacts->add($notification->id, $contact->id);
            }
        }

        $notification->disabled = $on ? null : new Zend_Db_Expr('NOW()');
        $notification->save();
    }

    protected function generateTotalStats($startDate)
    {
        $affiliateStat = new Application_Model_DbTable_AffiliateCampaignStatistics();
        $adStat = new Application_Model_DbTable_AdCampaignStatistics();
        $campaignsModel = new Application_Model_DbTable_Campaigns();
        $userOptions = array('user_id' => $this->_user->id);

        $campaignRevenue = $affiliateStat->findTotalCampaignCommission($userOptions, false, $startDate);
        $campaignSpend = $adStat->findTotalCampaignSpend($userOptions, $startDate);
        $campaigns = $campaignsModel->findByUser($this->_user->id);

        $stat = '';
        $totalProfit = $totalSpend = 0;
        foreach($campaigns as $campaign) {
            //Show only campaigns with Revenue or Spend
            if((empty($campaignRevenue[$campaign->id]) || $campaignRevenue[$campaign->id]['commission'] == 0) && 
                (empty($campaignSpend[$campaign->id]) || $campaignSpend[$campaign->id]['spend'] == 0)) { 
                continue; 
            }

            $spend = (float)(!empty($campaignSpend[$campaign->id]) ? $campaignSpend[$campaign->id]['spend'] : 0);
            $revenue = (float)(!empty($campaignRevenue[$campaign->id]) ? $campaignRevenue[$campaign->id]['commission'] : 0);
            $profit = $revenue - abs($spend);
            $roi = ($spend == 0) ? 1 : $profit/$spend;
            $totalProfit += $profit;
            $totalSpend += $spend;

            $campaignName = $campaign->nickname ? : $campaign->name;
            $stat .= "{$campaignName}: " . ($profit < 0 ? "-" : "") . "$" . 
                number_format(abs($profit), 2) . " | " . number_format(($roi * 100), 2) . "% \n";
        }

        $totalRoi = ($totalSpend == 0) ? 1 : $totalProfit/$totalSpend;
        $overall = 'Overall: ' . ($totalProfit < 0 ? "-" : "") . "$" . 
            number_format(abs($totalProfit), 2) . " | " . number_format(($totalRoi * 100), 2) . "% \n";
        return $overall . "------------------------------\n" . $stat;
    }

    protected function generateOverallStats()
    {
        $affiliateStat = new Application_Model_DbTable_AffiliateCampaignStatistics();
        $adStat = new Application_Model_DbTable_AdCampaignStatistics();

        $dates = array('Today' => App_Utils::abbrevToDate('today'), 
            'Yesterday' => App_Utils::abbrevToDate('yesterday'), 
            'MTD' => App_Utils::abbrevToDate('mtd'));

        $stat = '';
        foreach ($dates as $key => $date) {
            $campaignRevenue = $affiliateStat->findTotalCommission($this->_user->id, $date);
            $campaignSpend = $adStat->findTotalSpend($this->_user->id, $date);

            $spend = (float)$campaignSpend;
            $revenue = (float)(!empty($campaignRevenue['commission']) ? $campaignRevenue['commission'] : 0);
            $profit = $revenue - abs($spend);
            $roi = ($spend == 0) ? 1 : $profit/$spend;

            $stat .= "{$key}: " . ($profit < 0 ? "-" : "") . "$" . 
                number_format(abs($profit), 2) . " | " . number_format(($roi * 100), 2) . "% \n";
        }
        return $stat;
    }
}